// vite.config.js
import {resolve} from 'path'
import {defineConfig} from 'vite'

export default defineConfig({
    build: {
        lib: {
            // Could also be a dictionary or array of multiple entry points
            entry: resolve(__dirname, 'src/index.js'),
            name: 'VueReactWrapper',
            // the proper extensions will be added
            fileName: 'vue-react-wrapper',
            formats: ['es'],
        },
        rollupOptions: {
            input: 'src/index.js',
            external: [
                'vue',
                'livekit-client'
            ],
            output: {
                dir: 'dist',
                format: 'es'
            }
        }
    }
})
