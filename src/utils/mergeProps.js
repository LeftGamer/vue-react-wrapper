import { mergeProps as mergePropsReactAria } from './mergePropsUtil';

/** @internal */
export function isProp(prop){
  return prop !== undefined;
}

/** @internal */
export default function mergeProps(...props) {
  return mergePropsReactAria(...props.filter(isProp));
}
