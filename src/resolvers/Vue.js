import {ReactWrapper} from '../'
import {h} from 'vue';

export default function VueResolver(component) {
    return {
        components: {ReactWrapper},
        props: ['passedProps'],
        inheritAttrs: false,
        render() {
            return h(
                'react-wrapper',
                {
                    component,
                    passedProps: this.$props.passedProps,
                    ...this.$attrs,
                }, () => this.$slots.default()
            )
        },
    }
}
