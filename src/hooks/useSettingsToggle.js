import { useLayoutContext } from '@livekit/components-react';
import mergeProps from '../utils/mergeProps';
import * as React from 'react';

/**
 * The `useSettingsToggle` hook provides state and functions for toggling the settings menu.
 * @remarks
 * Depends on the `LayoutContext` to work properly.
 * @alpha
 */
export default function useSettingsToggle({ props }) {
  const { dispatch, state } = useLayoutContext().widget;
  const className = 'lk-button lk-settings-toggle';

  const mergedProps = React.useMemo(() => {
    return mergeProps(props, {
      className,
      onClick: () => {
        if (dispatch) dispatch({ msg: 'toggle_settings' });
      },
      'aria-pressed': (state && state.showSettings) ? 'true' : 'false',
    });
  }, [props, className, dispatch, state]);

  return { mergedProps };
}
