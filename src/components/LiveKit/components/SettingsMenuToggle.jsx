import * as React from 'react'
import useSettingsToggle from '../../../hooks/useSettingsToggle'

/**
 * The `SettingsMenuToggle` component is a button that toggles the visibility of the `SettingsMenu` component.
 * @remarks
 * For the component to have any effect it has to live inside a `LayoutContext` context.
 *
 * @alpha
 */

const SettingsMenuToggle = React.forwardRef(function SettingsMenuToggle(props, ref) {
  const { mergedProps } = useSettingsToggle({ props })

  return (
    <button ref={ref} {...mergedProps}>
      {props.children}
    </button>
  )
})

export default SettingsMenuToggle
