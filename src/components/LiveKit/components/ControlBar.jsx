import * as React from 'react'
import { Track } from 'livekit-client'
import { supportsScreenSharing } from '@livekit/components-core'
import useMediaQuery from '../../../hooks/useMediaQuery'
import mergeProps from '../../../utils/mergeProps'
import StartMediaButton from './StartMediaButton'
import SettingsMenuToggle from './SettingsMenuToggle'

import {
  MediaDeviceMenu,
  DisconnectButton,
  TrackToggle,
  GearIcon,
  LeaveIcon,
  useLocalParticipantPermissions,
  usePersistentUserChoices,
} from '@livekit/components-react'

/**
 * The `ControlBar` prefab gives the user the basic user interface to control their
 * media devices (camera, microphone and screen share), open the `Chat` and leave the room.
 *
 * @remarks
 * This component is build with other LiveKit components like `TrackToggle`,
 * `DeviceSelectorButton`, `DisconnectButton` and `StartAudio`.
 *
 * @example
 * ```tsx
 * <LiveKitRoom>
 *   <ControlBar />
 * </LiveKitRoom>
 * ```
 * @public
 */
export default function ControlBar({
                                     variation,
                                     controls,
                                     saveUserChoices = true,
                                     trans = (key) => key,
                                     ...props
                                   }) {
  const isTooLittleSpace = useMediaQuery(`(max-width: ${760}px)`)

  const defaultVariation = isTooLittleSpace ? 'minimal' : 'verbose'
  const localVariation = variation ? variation : defaultVariation

  const visibleControls = { leave: true, ...controls }

  const localPermissions = useLocalParticipantPermissions()

  if (!localPermissions) {
    visibleControls.camera = false
    visibleControls.microphone = false
    visibleControls.screenShare = false
  } else {
    visibleControls.camera = visibleControls.camera ? visibleControls.camera : localPermissions.canPublish
    visibleControls.microphone = visibleControls.microphone ? visibleControls.microphone : localPermissions.canPublish
    visibleControls.screenShare = visibleControls.screenShare ? visibleControls.screenShare : localPermissions.canPublish
  }

  const showIcon = React.useMemo(
    () => localVariation === 'minimal' || localVariation === 'verbose',
    [localVariation]
  )
  const showText = React.useMemo(
    () => localVariation === 'textOnly' || localVariation === 'verbose',
    [localVariation]
  )

  const browserSupportsScreenSharing = supportsScreenSharing()

  const [isScreenShareEnabled, setIsScreenShareEnabled] = React.useState(false)

  const onScreenShareChange = React.useCallback(
    (enabled) => {
      setIsScreenShareEnabled(enabled)
    },
    [setIsScreenShareEnabled]
  )

  const htmlProps = mergeProps({ className: 'lk-control-bar' }, props)

  const {
    saveAudioInputEnabled,
    saveVideoInputEnabled,
    saveAudioInputDeviceId,
    saveVideoInputDeviceId
  } = usePersistentUserChoices({ preventSave: !saveUserChoices })

  const microphoneOnChange = React.useCallback(
    (enabled, isUserInitiated) =>
      isUserInitiated ? saveAudioInputEnabled(enabled) : null,
    [saveAudioInputEnabled]
  )

  const cameraOnChange = React.useCallback(
    (enabled, isUserInitiated) =>
      isUserInitiated ? saveVideoInputEnabled(enabled) : null,
    [saveVideoInputEnabled]
  )

  return (
    <div {...htmlProps}>
      {visibleControls.microphone && (
        <div className="lk-button-group">
          <TrackToggle
            source={Track.Source.Microphone}
            showIcon={showIcon}
            onChange={microphoneOnChange}
          >
            {showText && trans('livekit.microphone')}
          </TrackToggle>
          <div className="lk-button-group-menu">
            <MediaDeviceMenu
              kind="audioinput"
              onActiveDeviceChange={(_kind, deviceId) => saveAudioInputDeviceId(deviceId ? deviceId : '')}
            />
          </div>
        </div>
      )}
      {visibleControls.camera && (
        <div className="lk-button-group">
          <TrackToggle source={Track.Source.Camera} showIcon={showIcon} onChange={cameraOnChange}>
            {showText && trans('livekit.camera')}
          </TrackToggle>
          <div className="lk-button-group-menu">
            <MediaDeviceMenu
              kind="videoinput"
              onActiveDeviceChange={(_kind, deviceId) => saveVideoInputDeviceId(deviceId ? deviceId : '')}
            />
          </div>
        </div>
      )}
      {visibleControls.screenShare && browserSupportsScreenSharing && (
        <TrackToggle
          source={Track.Source.ScreenShare}
          captureOptions={{ audio: true, selfBrowserSurface: 'include' }}
          showIcon={showIcon}
          onChange={onScreenShareChange}
        >
          {showText && (isScreenShareEnabled ? trans('livekit.share_stop') : trans('livekit.share_start'))}
        </TrackToggle>
      )}
      {visibleControls.settings && (
        <SettingsMenuToggle>
          {showIcon && <GearIcon />}
          {showText && 'Settings'}
        </SettingsMenuToggle>
      )}
      {visibleControls.leave && (
        <DisconnectButton>
          {showIcon && <LeaveIcon />}
          {showText && trans('livekit.leave')}
        </DisconnectButton>
      )}
      <StartMediaButton label={trans('livekit.start_media')} />
    </div>
  )
}
