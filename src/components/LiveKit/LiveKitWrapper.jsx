import React from 'react';
import {createRoot} from 'react-dom/client'
import {LiveKitRoom} from '@livekit/components-react'
import LiveKitLayout from './components/LiveKitLayout'
import {h, markRaw} from 'vue';

const makeReactContainer = () => {
  return class ReactInVue extends React.Component {
    static displayName = `ReactInVue${LiveKitRoom.displayName || LiveKitRoom.name || 'Component'}`

    constructor(props) {
      super(props)

      /**
       * We create a stateful component in order to attach a ref on it. We will use that ref to
       * update component's state, which seems better than re-rendering the whole thing with
       * ReactDOM.
       */
      this.state = { ...props }
    }

    render() {
      const {
        // Vue attaches an event handler, but it is missing an event name, so
        // it ends up using an empty string. Prevent passing an empty string
        // named prop to React.
        '': _invoker,
        ...rest
      } = this.state

      return (
        <LiveKitRoom
          data-lk-theme="default"
          {...rest}
          trans={undefined}
        >
          <LiveKitLayout trans={rest.trans}></LiveKitLayout>
        </LiveKitRoom>
      )
    }
  }
}

export default {
  render() {
    return h('div', { ref: 'react' })
  },
  data() {
    return {
      root: null,
      reactRef: null
    }
  },
  methods: {
    mountReactComponent() {
      if(this.$refs.react) {
        const Component = makeReactContainer();
        const ref = markRaw(this.$refs.react);
        this.root = markRaw(createRoot(ref));
        this.root.render(
            <Component
              {...markRaw(this.$attrs)}
              ref={ref => { this.reactRef = markRaw(ref) }}
            />
        )
      }
    }
  },
  mounted() {
    this.mountReactComponent()
  },
  // beforeUnmount() {
    // this.root.unmount()
  // },
  inheritAttrs: false,
  watch:
    {
      $attrs: {
        handler(val) {
          if (this.reactRef && this.reactRef.setState) {
            const state = val ? {...val} : {}
            this.reactRef.setState(markRaw(state))
          }
        },
        deep: true
      },
    }
}
