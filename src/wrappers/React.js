import React from 'react'
import { createRoot } from 'react-dom/client'
import VueWrapper from './Vue'
import { h } from 'vue';

const makeReactContainer = Component => {
  return class ReactInVue extends React.Component {
    static displayName = `ReactInVue${Component.displayName || Component.name || 'Component'}`

    constructor(props) {
      super(props)

      /**
       * We create a stateful component in order to attach a ref on it. We will use that ref to
       * update component's state, which seems better than re-rendering the whole thing with
       * ReactDOM.
       */
      this.state = { ...props }
    }

    wrapVueChildren(children) {
      return {
        render: createElement => createElement('div', children)
      }
    }

    render() {
      const {
        children,
        // Vue attaches an event handler, but it is missing an event name, so
        // it ends up using an empty string. Prevent passing an empty string
        // named prop to React.
        '': _invoker,
        ...rest
      } = this.state
      const wrappedChildren = this.wrapVueChildren(children)

      return (
        <Component {...rest}>
          {children && <VueWrapper component={wrappedChildren} />}
        </Component>
      )
    }
  }
}

export default {
  props: ['component', 'passedProps'],
  render() {
    return h('div', { ref: 'react' })
  },
  data() {
    return {
      root: null,
      reactRef: null
    }
  },
  methods: {
    mountReactComponent() {
      const Component = makeReactContainer()
      const children = this.$slots.default !== undefined ? { children: this.$slots.default } : {}
      this.root = createRoot(this.$refs.react)
      this.root.render(
        <Component
          {...this.$props.passedProps}
          {...this.$attrs}
          {...children}
          ref={ref => {this.reactRef = ref}}
        />
      )
    }
  },
  mounted() {
    this.mountReactComponent(this.$props.component)
  },
  beforeUnmount() {
    this.root.unmount()
  },
  updated() {
    /**
     * AFAIK, this is the only way to update children. It doesn't seem to be possible to watch
     * `$slots` or `$children`.
     */
    if (this.reactRef && this.reactRef.setState) {
      if (this.$slots.default !== undefined) {
        this.reactRef.setState({ children: this.$slots.default })
      } else {
        this.reactRef.setState({ children: null })
      }
    }
  },
  inheritAttrs: false,
  watch:
    {
      '$props.component': {
        handler(newValue) {
          this.mountReactComponent(newValue)
        }
      },
      '$props.passedProps': {
        handler() {
          if (this.reactRef && this.reactRef.setState) {
            this.reactRef.setState({ ...this.$props.passedProps })
          }
        },
        deep: true
      },
      $attrs: {
        handler(val) {
          if (this.reactRef && this.reactRef.setState) {
            this.reactRef.setState({ ...val })
          }
        },
        deep: true
      },
    }
}
