var O2 = Object.defineProperty;
var M2 = (l, s, c) => s in l ? O2(l, s, { enumerable: !0, configurable: !0, writable: !0, value: c }) : l[s] = c;
var Kw = (l, s, c) => M2(l, typeof s != "symbol" ? s + "" : s, c);
import { RoomEvent as Pe, ParticipantEvent as De, LocalParticipant as Z1, Track as ht, Room as M1, MediaDeviceFailure as L2, ConnectionState as L1, ConnectionQuality as Ly, LocalTrackPublication as N2, facingModeFromLocalTrack as A2, RemoteTrackPublication as N1, RemoteAudioTrack as z2 } from "livekit-client";
import { h as U2, markRaw as Dv } from "vue";
var P2 = typeof globalThis < "u" ? globalThis : typeof window < "u" ? window : typeof global < "u" ? global : typeof self < "u" ? self : {};
function LT(l) {
  return l && l.__esModule && Object.prototype.hasOwnProperty.call(l, "default") ? l.default : l;
}
var A1 = { exports: {} }, Tt = {};
/**
 * @license React
 * react.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var Zw;
function F2() {
  if (Zw) return Tt;
  Zw = 1;
  var l = Symbol.for("react.element"), s = Symbol.for("react.portal"), c = Symbol.for("react.fragment"), p = Symbol.for("react.strict_mode"), y = Symbol.for("react.profiler"), E = Symbol.for("react.provider"), m = Symbol.for("react.context"), x = Symbol.for("react.forward_ref"), T = Symbol.for("react.suspense"), D = Symbol.for("react.memo"), A = Symbol.for("react.lazy"), L = Symbol.iterator;
  function j(V) {
    return V === null || typeof V != "object" ? null : (V = L && V[L] || V["@@iterator"], typeof V == "function" ? V : null);
  }
  var F = { isMounted: function() {
    return !1;
  }, enqueueForceUpdate: function() {
  }, enqueueReplaceState: function() {
  }, enqueueSetState: function() {
  } }, $ = Object.assign, H = {};
  function G(V, ae, Je) {
    this.props = V, this.context = ae, this.refs = H, this.updater = Je || F;
  }
  G.prototype.isReactComponent = {}, G.prototype.setState = function(V, ae) {
    if (typeof V != "object" && typeof V != "function" && V != null) throw Error("setState(...): takes an object of state variables to update or a function which returns an object of state variables.");
    this.updater.enqueueSetState(this, V, ae, "setState");
  }, G.prototype.forceUpdate = function(V) {
    this.updater.enqueueForceUpdate(this, V, "forceUpdate");
  };
  function W() {
  }
  W.prototype = G.prototype;
  function ve(V, ae, Je) {
    this.props = V, this.context = ae, this.refs = H, this.updater = Je || F;
  }
  var fe = ve.prototype = new W();
  fe.constructor = ve, $(fe, G.prototype), fe.isPureReactComponent = !0;
  var oe = Array.isArray, ne = Object.prototype.hasOwnProperty, Ce = { current: null }, me = { key: !0, ref: !0, __self: !0, __source: !0 };
  function We(V, ae, Je) {
    var at, ct = {}, yt = null, Lt = null;
    if (ae != null) for (at in ae.ref !== void 0 && (Lt = ae.ref), ae.key !== void 0 && (yt = "" + ae.key), ae) ne.call(ae, at) && !me.hasOwnProperty(at) && (ct[at] = ae[at]);
    var xt = arguments.length - 2;
    if (xt === 1) ct.children = Je;
    else if (1 < xt) {
      for (var bt = Array(xt), En = 0; En < xt; En++) bt[En] = arguments[En + 2];
      ct.children = bt;
    }
    if (V && V.defaultProps) for (at in xt = V.defaultProps, xt) ct[at] === void 0 && (ct[at] = xt[at]);
    return { $$typeof: l, type: V, key: yt, ref: Lt, props: ct, _owner: Ce.current };
  }
  function Et(V, ae) {
    return { $$typeof: l, type: V.type, key: ae, ref: V.ref, props: V.props, _owner: V._owner };
  }
  function Rt(V) {
    return typeof V == "object" && V !== null && V.$$typeof === l;
  }
  function ye(V) {
    var ae = { "=": "=0", ":": "=2" };
    return "$" + V.replace(/[=:]/g, function(Je) {
      return ae[Je];
    });
  }
  var Ue = /\/+/g;
  function qe(V, ae) {
    return typeof V == "object" && V !== null && V.key != null ? ye("" + V.key) : ae.toString(36);
  }
  function rt(V, ae, Je, at, ct) {
    var yt = typeof V;
    (yt === "undefined" || yt === "boolean") && (V = null);
    var Lt = !1;
    if (V === null) Lt = !0;
    else switch (yt) {
      case "string":
      case "number":
        Lt = !0;
        break;
      case "object":
        switch (V.$$typeof) {
          case l:
          case s:
            Lt = !0;
        }
    }
    if (Lt) return Lt = V, ct = ct(Lt), V = at === "" ? "." + qe(Lt, 0) : at, oe(ct) ? (Je = "", V != null && (Je = V.replace(Ue, "$&/") + "/"), rt(ct, ae, Je, "", function(En) {
      return En;
    })) : ct != null && (Rt(ct) && (ct = Et(ct, Je + (!ct.key || Lt && Lt.key === ct.key ? "" : ("" + ct.key).replace(Ue, "$&/") + "/") + V)), ae.push(ct)), 1;
    if (Lt = 0, at = at === "" ? "." : at + ":", oe(V)) for (var xt = 0; xt < V.length; xt++) {
      yt = V[xt];
      var bt = at + qe(yt, xt);
      Lt += rt(yt, ae, Je, bt, ct);
    }
    else if (bt = j(V), typeof bt == "function") for (V = bt.call(V), xt = 0; !(yt = V.next()).done; ) yt = yt.value, bt = at + qe(yt, xt++), Lt += rt(yt, ae, Je, bt, ct);
    else if (yt === "object") throw ae = String(V), Error("Objects are not valid as a React child (found: " + (ae === "[object Object]" ? "object with keys {" + Object.keys(V).join(", ") + "}" : ae) + "). If you meant to render a collection of children, use an array instead.");
    return Lt;
  }
  function Ot(V, ae, Je) {
    if (V == null) return V;
    var at = [], ct = 0;
    return rt(V, at, "", "", function(yt) {
      return ae.call(Je, yt, ct++);
    }), at;
  }
  function mt(V) {
    if (V._status === -1) {
      var ae = V._result;
      ae = ae(), ae.then(function(Je) {
        (V._status === 0 || V._status === -1) && (V._status = 1, V._result = Je);
      }, function(Je) {
        (V._status === 0 || V._status === -1) && (V._status = 2, V._result = Je);
      }), V._status === -1 && (V._status = 0, V._result = ae);
    }
    if (V._status === 1) return V._result.default;
    throw V._result;
  }
  var Oe = { current: null }, he = { transition: null }, Be = { ReactCurrentDispatcher: Oe, ReactCurrentBatchConfig: he, ReactCurrentOwner: Ce };
  function Te() {
    throw Error("act(...) is not supported in production builds of React.");
  }
  return Tt.Children = { map: Ot, forEach: function(V, ae, Je) {
    Ot(V, function() {
      ae.apply(this, arguments);
    }, Je);
  }, count: function(V) {
    var ae = 0;
    return Ot(V, function() {
      ae++;
    }), ae;
  }, toArray: function(V) {
    return Ot(V, function(ae) {
      return ae;
    }) || [];
  }, only: function(V) {
    if (!Rt(V)) throw Error("React.Children.only expected to receive a single React element child.");
    return V;
  } }, Tt.Component = G, Tt.Fragment = c, Tt.Profiler = y, Tt.PureComponent = ve, Tt.StrictMode = p, Tt.Suspense = T, Tt.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED = Be, Tt.act = Te, Tt.cloneElement = function(V, ae, Je) {
    if (V == null) throw Error("React.cloneElement(...): The argument must be a React element, but you passed " + V + ".");
    var at = $({}, V.props), ct = V.key, yt = V.ref, Lt = V._owner;
    if (ae != null) {
      if (ae.ref !== void 0 && (yt = ae.ref, Lt = Ce.current), ae.key !== void 0 && (ct = "" + ae.key), V.type && V.type.defaultProps) var xt = V.type.defaultProps;
      for (bt in ae) ne.call(ae, bt) && !me.hasOwnProperty(bt) && (at[bt] = ae[bt] === void 0 && xt !== void 0 ? xt[bt] : ae[bt]);
    }
    var bt = arguments.length - 2;
    if (bt === 1) at.children = Je;
    else if (1 < bt) {
      xt = Array(bt);
      for (var En = 0; En < bt; En++) xt[En] = arguments[En + 2];
      at.children = xt;
    }
    return { $$typeof: l, type: V.type, key: ct, ref: yt, props: at, _owner: Lt };
  }, Tt.createContext = function(V) {
    return V = { $$typeof: m, _currentValue: V, _currentValue2: V, _threadCount: 0, Provider: null, Consumer: null, _defaultValue: null, _globalName: null }, V.Provider = { $$typeof: E, _context: V }, V.Consumer = V;
  }, Tt.createElement = We, Tt.createFactory = function(V) {
    var ae = We.bind(null, V);
    return ae.type = V, ae;
  }, Tt.createRef = function() {
    return { current: null };
  }, Tt.forwardRef = function(V) {
    return { $$typeof: x, render: V };
  }, Tt.isValidElement = Rt, Tt.lazy = function(V) {
    return { $$typeof: A, _payload: { _status: -1, _result: V }, _init: mt };
  }, Tt.memo = function(V, ae) {
    return { $$typeof: D, type: V, compare: ae === void 0 ? null : ae };
  }, Tt.startTransition = function(V) {
    var ae = he.transition;
    he.transition = {};
    try {
      V();
    } finally {
      he.transition = ae;
    }
  }, Tt.unstable_act = Te, Tt.useCallback = function(V, ae) {
    return Oe.current.useCallback(V, ae);
  }, Tt.useContext = function(V) {
    return Oe.current.useContext(V);
  }, Tt.useDebugValue = function() {
  }, Tt.useDeferredValue = function(V) {
    return Oe.current.useDeferredValue(V);
  }, Tt.useEffect = function(V, ae) {
    return Oe.current.useEffect(V, ae);
  }, Tt.useId = function() {
    return Oe.current.useId();
  }, Tt.useImperativeHandle = function(V, ae, Je) {
    return Oe.current.useImperativeHandle(V, ae, Je);
  }, Tt.useInsertionEffect = function(V, ae) {
    return Oe.current.useInsertionEffect(V, ae);
  }, Tt.useLayoutEffect = function(V, ae) {
    return Oe.current.useLayoutEffect(V, ae);
  }, Tt.useMemo = function(V, ae) {
    return Oe.current.useMemo(V, ae);
  }, Tt.useReducer = function(V, ae, Je) {
    return Oe.current.useReducer(V, ae, Je);
  }, Tt.useRef = function(V) {
    return Oe.current.useRef(V);
  }, Tt.useState = function(V) {
    return Oe.current.useState(V);
  }, Tt.useSyncExternalStore = function(V, ae, Je) {
    return Oe.current.useSyncExternalStore(V, ae, Je);
  }, Tt.useTransition = function() {
    return Oe.current.useTransition();
  }, Tt.version = "18.3.1", Tt;
}
var Mv = { exports: {} };
/**
 * @license React
 * react.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
Mv.exports;
var Jw;
function H2() {
  return Jw || (Jw = 1, function(l, s) {
    process.env.NODE_ENV !== "production" && function() {
      typeof __REACT_DEVTOOLS_GLOBAL_HOOK__ < "u" && typeof __REACT_DEVTOOLS_GLOBAL_HOOK__.registerInternalModuleStart == "function" && __REACT_DEVTOOLS_GLOBAL_HOOK__.registerInternalModuleStart(new Error());
      var c = "18.3.1", p = Symbol.for("react.element"), y = Symbol.for("react.portal"), E = Symbol.for("react.fragment"), m = Symbol.for("react.strict_mode"), x = Symbol.for("react.profiler"), T = Symbol.for("react.provider"), D = Symbol.for("react.context"), A = Symbol.for("react.forward_ref"), L = Symbol.for("react.suspense"), j = Symbol.for("react.suspense_list"), F = Symbol.for("react.memo"), $ = Symbol.for("react.lazy"), H = Symbol.for("react.offscreen"), G = Symbol.iterator, W = "@@iterator";
      function ve(C) {
        if (C === null || typeof C != "object")
          return null;
        var M = G && C[G] || C[W];
        return typeof M == "function" ? M : null;
      }
      var fe = {
        /**
         * @internal
         * @type {ReactComponent}
         */
        current: null
      }, oe = {
        transition: null
      }, ne = {
        current: null,
        // Used to reproduce behavior of `batchedUpdates` in legacy mode.
        isBatchingLegacy: !1,
        didScheduleLegacyUpdate: !1
      }, Ce = {
        /**
         * @internal
         * @type {ReactComponent}
         */
        current: null
      }, me = {}, We = null;
      function Et(C) {
        We = C;
      }
      me.setExtraStackFrame = function(C) {
        We = C;
      }, me.getCurrentStack = null, me.getStackAddendum = function() {
        var C = "";
        We && (C += We);
        var M = me.getCurrentStack;
        return M && (C += M() || ""), C;
      };
      var Rt = !1, ye = !1, Ue = !1, qe = !1, rt = !1, Ot = {
        ReactCurrentDispatcher: fe,
        ReactCurrentBatchConfig: oe,
        ReactCurrentOwner: Ce
      };
      Ot.ReactDebugCurrentFrame = me, Ot.ReactCurrentActQueue = ne;
      function mt(C) {
        {
          for (var M = arguments.length, X = new Array(M > 1 ? M - 1 : 0), J = 1; J < M; J++)
            X[J - 1] = arguments[J];
          he("warn", C, X);
        }
      }
      function Oe(C) {
        {
          for (var M = arguments.length, X = new Array(M > 1 ? M - 1 : 0), J = 1; J < M; J++)
            X[J - 1] = arguments[J];
          he("error", C, X);
        }
      }
      function he(C, M, X) {
        {
          var J = Ot.ReactDebugCurrentFrame, de = J.getStackAddendum();
          de !== "" && (M += "%s", X = X.concat([de]));
          var Xe = X.map(function(be) {
            return String(be);
          });
          Xe.unshift("Warning: " + M), Function.prototype.apply.call(console[C], console, Xe);
        }
      }
      var Be = {};
      function Te(C, M) {
        {
          var X = C.constructor, J = X && (X.displayName || X.name) || "ReactClass", de = J + "." + M;
          if (Be[de])
            return;
          Oe("Can't call %s on a component that is not yet mounted. This is a no-op, but it might indicate a bug in your application. Instead, assign to `this.state` directly or define a `state = {};` class property with the desired state in the %s component.", M, J), Be[de] = !0;
        }
      }
      var V = {
        /**
         * Checks whether or not this composite component is mounted.
         * @param {ReactClass} publicInstance The instance we want to test.
         * @return {boolean} True if mounted, false otherwise.
         * @protected
         * @final
         */
        isMounted: function(C) {
          return !1;
        },
        /**
         * Forces an update. This should only be invoked when it is known with
         * certainty that we are **not** in a DOM transaction.
         *
         * You may want to call this when you know that some deeper aspect of the
         * component's state has changed but `setState` was not called.
         *
         * This will not invoke `shouldComponentUpdate`, but it will invoke
         * `componentWillUpdate` and `componentDidUpdate`.
         *
         * @param {ReactClass} publicInstance The instance that should rerender.
         * @param {?function} callback Called after component is updated.
         * @param {?string} callerName name of the calling function in the public API.
         * @internal
         */
        enqueueForceUpdate: function(C, M, X) {
          Te(C, "forceUpdate");
        },
        /**
         * Replaces all of the state. Always use this or `setState` to mutate state.
         * You should treat `this.state` as immutable.
         *
         * There is no guarantee that `this.state` will be immediately updated, so
         * accessing `this.state` after calling this method may return the old value.
         *
         * @param {ReactClass} publicInstance The instance that should rerender.
         * @param {object} completeState Next state.
         * @param {?function} callback Called after component is updated.
         * @param {?string} callerName name of the calling function in the public API.
         * @internal
         */
        enqueueReplaceState: function(C, M, X, J) {
          Te(C, "replaceState");
        },
        /**
         * Sets a subset of the state. This only exists because _pendingState is
         * internal. This provides a merging strategy that is not available to deep
         * properties which is confusing. TODO: Expose pendingState or don't use it
         * during the merge.
         *
         * @param {ReactClass} publicInstance The instance that should rerender.
         * @param {object} partialState Next partial state to be merged with state.
         * @param {?function} callback Called after component is updated.
         * @param {?string} Name of the calling function in the public API.
         * @internal
         */
        enqueueSetState: function(C, M, X, J) {
          Te(C, "setState");
        }
      }, ae = Object.assign, Je = {};
      Object.freeze(Je);
      function at(C, M, X) {
        this.props = C, this.context = M, this.refs = Je, this.updater = X || V;
      }
      at.prototype.isReactComponent = {}, at.prototype.setState = function(C, M) {
        if (typeof C != "object" && typeof C != "function" && C != null)
          throw new Error("setState(...): takes an object of state variables to update or a function which returns an object of state variables.");
        this.updater.enqueueSetState(this, C, M, "setState");
      }, at.prototype.forceUpdate = function(C) {
        this.updater.enqueueForceUpdate(this, C, "forceUpdate");
      };
      {
        var ct = {
          isMounted: ["isMounted", "Instead, make sure to clean up subscriptions and pending requests in componentWillUnmount to prevent memory leaks."],
          replaceState: ["replaceState", "Refactor your code to use setState instead (see https://github.com/facebook/react/issues/3236)."]
        }, yt = function(C, M) {
          Object.defineProperty(at.prototype, C, {
            get: function() {
              mt("%s(...) is deprecated in plain JavaScript React classes. %s", M[0], M[1]);
            }
          });
        };
        for (var Lt in ct)
          ct.hasOwnProperty(Lt) && yt(Lt, ct[Lt]);
      }
      function xt() {
      }
      xt.prototype = at.prototype;
      function bt(C, M, X) {
        this.props = C, this.context = M, this.refs = Je, this.updater = X || V;
      }
      var En = bt.prototype = new xt();
      En.constructor = bt, ae(En, at.prototype), En.isPureReactComponent = !0;
      function xr() {
        var C = {
          current: null
        };
        return Object.seal(C), C;
      }
      var Fa = Array.isArray;
      function bn(C) {
        return Fa(C);
      }
      function ir(C) {
        {
          var M = typeof Symbol == "function" && Symbol.toStringTag, X = M && C[Symbol.toStringTag] || C.constructor.name || "Object";
          return X;
        }
      }
      function kr(C) {
        try {
          return _r(C), !1;
        } catch {
          return !0;
        }
      }
      function _r(C) {
        return "" + C;
      }
      function Fn(C) {
        if (kr(C))
          return Oe("The provided key is an unsupported type %s. This value must be coerced to a string before before using it here.", ir(C)), _r(C);
      }
      function Ha(C, M, X) {
        var J = C.displayName;
        if (J)
          return J;
        var de = M.displayName || M.name || "";
        return de !== "" ? X + "(" + de + ")" : X;
      }
      function xi(C) {
        return C.displayName || "Context";
      }
      function or(C) {
        if (C == null)
          return null;
        if (typeof C.tag == "number" && Oe("Received an unexpected object in getComponentNameFromType(). This is likely a bug in React. Please file an issue."), typeof C == "function")
          return C.displayName || C.name || null;
        if (typeof C == "string")
          return C;
        switch (C) {
          case E:
            return "Fragment";
          case y:
            return "Portal";
          case x:
            return "Profiler";
          case m:
            return "StrictMode";
          case L:
            return "Suspense";
          case j:
            return "SuspenseList";
        }
        if (typeof C == "object")
          switch (C.$$typeof) {
            case D:
              var M = C;
              return xi(M) + ".Consumer";
            case T:
              var X = C;
              return xi(X._context) + ".Provider";
            case A:
              return Ha(C, C.render, "ForwardRef");
            case F:
              var J = C.displayName || null;
              return J !== null ? J : or(C.type) || "Memo";
            case $: {
              var de = C, Xe = de._payload, be = de._init;
              try {
                return or(be(Xe));
              } catch {
                return null;
              }
            }
          }
        return null;
      }
      var Dr = Object.prototype.hasOwnProperty, ni = {
        key: !0,
        ref: !0,
        __self: !0,
        __source: !0
      }, Or, ja, Mr;
      Mr = {};
      function Va(C) {
        if (Dr.call(C, "ref")) {
          var M = Object.getOwnPropertyDescriptor(C, "ref").get;
          if (M && M.isReactWarning)
            return !1;
        }
        return C.ref !== void 0;
      }
      function Wn(C) {
        if (Dr.call(C, "key")) {
          var M = Object.getOwnPropertyDescriptor(C, "key").get;
          if (M && M.isReactWarning)
            return !1;
        }
        return C.key !== void 0;
      }
      function ea(C, M) {
        var X = function() {
          Or || (Or = !0, Oe("%s: `key` is not a prop. Trying to access it will result in `undefined` being returned. If you need to access the same value within the child component, you should pass it as a different prop. (https://reactjs.org/link/special-props)", M));
        };
        X.isReactWarning = !0, Object.defineProperty(C, "key", {
          get: X,
          configurable: !0
        });
      }
      function Xo(C, M) {
        var X = function() {
          ja || (ja = !0, Oe("%s: `ref` is not a prop. Trying to access it will result in `undefined` being returned. If you need to access the same value within the child component, you should pass it as a different prop. (https://reactjs.org/link/special-props)", M));
        };
        X.isReactWarning = !0, Object.defineProperty(C, "ref", {
          get: X,
          configurable: !0
        });
      }
      function ri(C) {
        if (typeof C.ref == "string" && Ce.current && C.__self && Ce.current.stateNode !== C.__self) {
          var M = or(Ce.current.type);
          Mr[M] || (Oe('Component "%s" contains the string ref "%s". Support for string refs will be removed in a future major release. This case cannot be automatically converted to an arrow function. We ask you to manually fix this case by using useRef() or createRef() instead. Learn more about using refs safely here: https://reactjs.org/link/strict-mode-string-ref', M, C.ref), Mr[M] = !0);
        }
      }
      var Ee = function(C, M, X, J, de, Xe, be) {
        var Ge = {
          // This tag allows us to uniquely identify this as a React Element
          $$typeof: p,
          // Built-in properties that belong on the element
          type: C,
          key: M,
          ref: X,
          props: be,
          // Record the component responsible for creating this element.
          _owner: Xe
        };
        return Ge._store = {}, Object.defineProperty(Ge._store, "validated", {
          configurable: !1,
          enumerable: !1,
          writable: !0,
          value: !1
        }), Object.defineProperty(Ge, "_self", {
          configurable: !1,
          enumerable: !1,
          writable: !1,
          value: J
        }), Object.defineProperty(Ge, "_source", {
          configurable: !1,
          enumerable: !1,
          writable: !1,
          value: de
        }), Object.freeze && (Object.freeze(Ge.props), Object.freeze(Ge)), Ge;
      };
      function Ie(C, M, X) {
        var J, de = {}, Xe = null, be = null, Ge = null, pt = null;
        if (M != null) {
          Va(M) && (be = M.ref, ri(M)), Wn(M) && (Fn(M.key), Xe = "" + M.key), Ge = M.__self === void 0 ? null : M.__self, pt = M.__source === void 0 ? null : M.__source;
          for (J in M)
            Dr.call(M, J) && !ni.hasOwnProperty(J) && (de[J] = M[J]);
        }
        var Dt = arguments.length - 2;
        if (Dt === 1)
          de.children = X;
        else if (Dt > 1) {
          for (var Wt = Array(Dt), $t = 0; $t < Dt; $t++)
            Wt[$t] = arguments[$t + 2];
          Object.freeze && Object.freeze(Wt), de.children = Wt;
        }
        if (C && C.defaultProps) {
          var Qt = C.defaultProps;
          for (J in Qt)
            de[J] === void 0 && (de[J] = Qt[J]);
        }
        if (Xe || be) {
          var Xt = typeof C == "function" ? C.displayName || C.name || "Unknown" : C;
          Xe && ea(de, Xt), be && Xo(de, Xt);
        }
        return Ee(C, Xe, be, Ge, pt, Ce.current, de);
      }
      function ft(C, M) {
        var X = Ee(C.type, M, C.ref, C._self, C._source, C._owner, C.props);
        return X;
      }
      function Vt(C, M, X) {
        if (C == null)
          throw new Error("React.cloneElement(...): The argument must be a React element, but you passed " + C + ".");
        var J, de = ae({}, C.props), Xe = C.key, be = C.ref, Ge = C._self, pt = C._source, Dt = C._owner;
        if (M != null) {
          Va(M) && (be = M.ref, Dt = Ce.current), Wn(M) && (Fn(M.key), Xe = "" + M.key);
          var Wt;
          C.type && C.type.defaultProps && (Wt = C.type.defaultProps);
          for (J in M)
            Dr.call(M, J) && !ni.hasOwnProperty(J) && (M[J] === void 0 && Wt !== void 0 ? de[J] = Wt[J] : de[J] = M[J]);
        }
        var $t = arguments.length - 2;
        if ($t === 1)
          de.children = X;
        else if ($t > 1) {
          for (var Qt = Array($t), Xt = 0; Xt < $t; Xt++)
            Qt[Xt] = arguments[Xt + 2];
          de.children = Qt;
        }
        return Ee(C.type, Xe, be, Ge, pt, Dt, de);
      }
      function Bt(C) {
        return typeof C == "object" && C !== null && C.$$typeof === p;
      }
      var Mn = ".", Cn = ":";
      function Lr(C) {
        var M = /[=:]/g, X = {
          "=": "=0",
          ":": "=2"
        }, J = C.replace(M, function(de) {
          return X[de];
        });
        return "$" + J;
      }
      var Zt = !1, pa = /\/+/g;
      function sn(C) {
        return C.replace(pa, "$&/");
      }
      function qt(C, M) {
        return typeof C == "object" && C !== null && C.key != null ? (Fn(C.key), Lr("" + C.key)) : M.toString(36);
      }
      function Ko(C, M, X, J, de) {
        var Xe = typeof C;
        (Xe === "undefined" || Xe === "boolean") && (C = null);
        var be = !1;
        if (C === null)
          be = !0;
        else
          switch (Xe) {
            case "string":
            case "number":
              be = !0;
              break;
            case "object":
              switch (C.$$typeof) {
                case p:
                case y:
                  be = !0;
              }
          }
        if (be) {
          var Ge = C, pt = de(Ge), Dt = J === "" ? Mn + qt(Ge, 0) : J;
          if (bn(pt)) {
            var Wt = "";
            Dt != null && (Wt = sn(Dt) + "/"), Ko(pt, M, Wt, "", function(_d) {
              return _d;
            });
          } else pt != null && (Bt(pt) && (pt.key && (!Ge || Ge.key !== pt.key) && Fn(pt.key), pt = ft(
            pt,
            // Keep both the (mapped) and old keys if they differ, just as
            // traverseAllChildren used to do for objects as children
            X + // $FlowFixMe Flow incorrectly thinks React.Portal doesn't have a key
            (pt.key && (!Ge || Ge.key !== pt.key) ? (
              // $FlowFixMe Flow incorrectly thinks existing element's key can be a number
              // eslint-disable-next-line react-internal/safe-string-coercion
              sn("" + pt.key) + "/"
            ) : "") + Dt
          )), M.push(pt));
          return 1;
        }
        var $t, Qt, Xt = 0, wt = J === "" ? Mn : J + Cn;
        if (bn(C))
          for (var Mi = 0; Mi < C.length; Mi++)
            $t = C[Mi], Qt = wt + qt($t, Mi), Xt += Ko($t, M, X, Qt, de);
        else {
          var eu = ve(C);
          if (typeof eu == "function") {
            var ps = C;
            eu === ps.entries && (Zt || mt("Using Maps as children is not supported. Use an array of keyed ReactElements instead."), Zt = !0);
            for (var kd = eu.call(ps), Qa, vs = 0; !(Qa = kd.next()).done; )
              $t = Qa.value, Qt = wt + qt($t, vs++), Xt += Ko($t, M, X, Qt, de);
          } else if (Xe === "object") {
            var hs = String(C);
            throw new Error("Objects are not valid as a React child (found: " + (hs === "[object Object]" ? "object with keys {" + Object.keys(C).join(", ") + "}" : hs) + "). If you meant to render a collection of children, use an array instead.");
          }
        }
        return Xt;
      }
      function ki(C, M, X) {
        if (C == null)
          return C;
        var J = [], de = 0;
        return Ko(C, J, "", "", function(Xe) {
          return M.call(X, Xe, de++);
        }), J;
      }
      function Vl(C) {
        var M = 0;
        return ki(C, function() {
          M++;
        }), M;
      }
      function gc(C, M, X) {
        ki(C, function() {
          M.apply(this, arguments);
        }, X);
      }
      function Sc(C) {
        return ki(C, function(M) {
          return M;
        }) || [];
      }
      function $l(C) {
        if (!Bt(C))
          throw new Error("React.Children.only expected to receive a single React element child.");
        return C;
      }
      function os(C) {
        var M = {
          $$typeof: D,
          // As a workaround to support multiple concurrent renderers, we categorize
          // some renderers as primary and others as secondary. We only expect
          // there to be two concurrent renderers at most: React Native (primary) and
          // Fabric (secondary); React DOM (primary) and React ART (secondary).
          // Secondary renderers store their context values on separate fields.
          _currentValue: C,
          _currentValue2: C,
          // Used to track how many concurrent renderers this context currently
          // supports within in a single renderer. Such as parallel server rendering.
          _threadCount: 0,
          // These are circular
          Provider: null,
          Consumer: null,
          // Add these to use same hidden class in VM as ServerContext
          _defaultValue: null,
          _globalName: null
        };
        M.Provider = {
          $$typeof: T,
          _context: M
        };
        var X = !1, J = !1, de = !1;
        {
          var Xe = {
            $$typeof: D,
            _context: M
          };
          Object.defineProperties(Xe, {
            Provider: {
              get: function() {
                return J || (J = !0, Oe("Rendering <Context.Consumer.Provider> is not supported and will be removed in a future major release. Did you mean to render <Context.Provider> instead?")), M.Provider;
              },
              set: function(be) {
                M.Provider = be;
              }
            },
            _currentValue: {
              get: function() {
                return M._currentValue;
              },
              set: function(be) {
                M._currentValue = be;
              }
            },
            _currentValue2: {
              get: function() {
                return M._currentValue2;
              },
              set: function(be) {
                M._currentValue2 = be;
              }
            },
            _threadCount: {
              get: function() {
                return M._threadCount;
              },
              set: function(be) {
                M._threadCount = be;
              }
            },
            Consumer: {
              get: function() {
                return X || (X = !0, Oe("Rendering <Context.Consumer.Consumer> is not supported and will be removed in a future major release. Did you mean to render <Context.Consumer> instead?")), M.Consumer;
              }
            },
            displayName: {
              get: function() {
                return M.displayName;
              },
              set: function(be) {
                de || (mt("Setting `displayName` on Context.Consumer has no effect. You should set it directly on the context with Context.displayName = '%s'.", be), de = !0);
              }
            }
          }), M.Consumer = Xe;
        }
        return M._currentRenderer = null, M._currentRenderer2 = null, M;
      }
      var $a = -1, io = 0, ai = 1, oo = 2;
      function ta(C) {
        if (C._status === $a) {
          var M = C._result, X = M();
          if (X.then(function(Xe) {
            if (C._status === io || C._status === $a) {
              var be = C;
              be._status = ai, be._result = Xe;
            }
          }, function(Xe) {
            if (C._status === io || C._status === $a) {
              var be = C;
              be._status = oo, be._result = Xe;
            }
          }), C._status === $a) {
            var J = C;
            J._status = io, J._result = X;
          }
        }
        if (C._status === ai) {
          var de = C._result;
          return de === void 0 && Oe(`lazy: Expected the result of a dynamic import() call. Instead received: %s

Your code should look like: 
  const MyComponent = lazy(() => import('./MyComponent'))

Did you accidentally put curly braces around the import?`, de), "default" in de || Oe(`lazy: Expected the result of a dynamic import() call. Instead received: %s

Your code should look like: 
  const MyComponent = lazy(() => import('./MyComponent'))`, de), de.default;
        } else
          throw C._result;
      }
      function Ba(C) {
        var M = {
          // We use these fields to store the result.
          _status: $a,
          _result: C
        }, X = {
          $$typeof: $,
          _payload: M,
          _init: ta
        };
        {
          var J, de;
          Object.defineProperties(X, {
            defaultProps: {
              configurable: !0,
              get: function() {
                return J;
              },
              set: function(Xe) {
                Oe("React.lazy(...): It is not supported to assign `defaultProps` to a lazy component import. Either specify them where the component is defined, or create a wrapping component around it."), J = Xe, Object.defineProperty(X, "defaultProps", {
                  enumerable: !0
                });
              }
            },
            propTypes: {
              configurable: !0,
              get: function() {
                return de;
              },
              set: function(Xe) {
                Oe("React.lazy(...): It is not supported to assign `propTypes` to a lazy component import. Either specify them where the component is defined, or create a wrapping component around it."), de = Xe, Object.defineProperty(X, "propTypes", {
                  enumerable: !0
                });
              }
            }
          });
        }
        return X;
      }
      function lo(C) {
        C != null && C.$$typeof === F ? Oe("forwardRef requires a render function but received a `memo` component. Instead of forwardRef(memo(...)), use memo(forwardRef(...)).") : typeof C != "function" ? Oe("forwardRef requires a render function but was given %s.", C === null ? "null" : typeof C) : C.length !== 0 && C.length !== 2 && Oe("forwardRef render functions accept exactly two parameters: props and ref. %s", C.length === 1 ? "Did you forget to use the ref parameter?" : "Any additional parameter will be undefined."), C != null && (C.defaultProps != null || C.propTypes != null) && Oe("forwardRef render functions do not support propTypes or defaultProps. Did you accidentally pass a React component?");
        var M = {
          $$typeof: A,
          render: C
        };
        {
          var X;
          Object.defineProperty(M, "displayName", {
            enumerable: !1,
            configurable: !0,
            get: function() {
              return X;
            },
            set: function(J) {
              X = J, !C.name && !C.displayName && (C.displayName = J);
            }
          });
        }
        return M;
      }
      var uo;
      uo = Symbol.for("react.module.reference");
      function Bl(C) {
        return !!(typeof C == "string" || typeof C == "function" || C === E || C === x || rt || C === m || C === L || C === j || qe || C === H || Rt || ye || Ue || typeof C == "object" && C !== null && (C.$$typeof === $ || C.$$typeof === F || C.$$typeof === T || C.$$typeof === D || C.$$typeof === A || // This needs to include all possible module reference object
        // types supported by any Flight configuration anywhere since
        // we don't know which Flight build this will end up being used
        // with.
        C.$$typeof === uo || C.getModuleId !== void 0));
      }
      function ls(C, M) {
        Bl(C) || Oe("memo: The first argument must be a component. Instead received: %s", C === null ? "null" : typeof C);
        var X = {
          $$typeof: F,
          type: C,
          compare: M === void 0 ? null : M
        };
        {
          var J;
          Object.defineProperty(X, "displayName", {
            enumerable: !1,
            configurable: !0,
            get: function() {
              return J;
            },
            set: function(de) {
              J = de, !C.name && !C.displayName && (C.displayName = de);
            }
          });
        }
        return X;
      }
      function vn() {
        var C = fe.current;
        return C === null && Oe(`Invalid hook call. Hooks can only be called inside of the body of a function component. This could happen for one of the following reasons:
1. You might have mismatching versions of React and the renderer (such as React DOM)
2. You might be breaking the Rules of Hooks
3. You might have more than one copy of React in the same app
See https://reactjs.org/link/invalid-hook-call for tips about how to debug and fix this problem.`), C;
      }
      function va(C) {
        var M = vn();
        if (C._context !== void 0) {
          var X = C._context;
          X.Consumer === C ? Oe("Calling useContext(Context.Consumer) is not supported, may cause bugs, and will be removed in a future major release. Did you mean to call useContext(Context) instead?") : X.Provider === C && Oe("Calling useContext(Context.Provider) is not supported. Did you mean to call useContext(Context) instead?");
        }
        return M.useContext(C);
      }
      function Zo(C) {
        var M = vn();
        return M.useState(C);
      }
      function so(C, M, X) {
        var J = vn();
        return J.useReducer(C, M, X);
      }
      function Qn(C) {
        var M = vn();
        return M.useRef(C);
      }
      function Ec(C, M) {
        var X = vn();
        return X.useEffect(C, M);
      }
      function bc(C, M) {
        var X = vn();
        return X.useInsertionEffect(C, M);
      }
      function us(C, M) {
        var X = vn();
        return X.useLayoutEffect(C, M);
      }
      function Cc(C, M) {
        var X = vn();
        return X.useCallback(C, M);
      }
      function wd(C, M) {
        var X = vn();
        return X.useMemo(C, M);
      }
      function Td(C, M, X) {
        var J = vn();
        return J.useImperativeHandle(C, M, X);
      }
      function rn(C, M) {
        {
          var X = vn();
          return X.useDebugValue(C, M);
        }
      }
      function Rd() {
        var C = vn();
        return C.useTransition();
      }
      function Ia(C) {
        var M = vn();
        return M.useDeferredValue(C);
      }
      function dt() {
        var C = vn();
        return C.useId();
      }
      function ii(C, M, X) {
        var J = vn();
        return J.useSyncExternalStore(C, M, X);
      }
      var co = 0, Il, fo, Nr, ss, dr, cs, fs;
      function wc() {
      }
      wc.__reactDisabledLog = !0;
      function Yl() {
        {
          if (co === 0) {
            Il = console.log, fo = console.info, Nr = console.warn, ss = console.error, dr = console.group, cs = console.groupCollapsed, fs = console.groupEnd;
            var C = {
              configurable: !0,
              enumerable: !0,
              value: wc,
              writable: !0
            };
            Object.defineProperties(console, {
              info: C,
              log: C,
              warn: C,
              error: C,
              group: C,
              groupCollapsed: C,
              groupEnd: C
            });
          }
          co++;
        }
      }
      function po() {
        {
          if (co--, co === 0) {
            var C = {
              configurable: !0,
              enumerable: !0,
              writable: !0
            };
            Object.defineProperties(console, {
              log: ae({}, C, {
                value: Il
              }),
              info: ae({}, C, {
                value: fo
              }),
              warn: ae({}, C, {
                value: Nr
              }),
              error: ae({}, C, {
                value: ss
              }),
              group: ae({}, C, {
                value: dr
              }),
              groupCollapsed: ae({}, C, {
                value: cs
              }),
              groupEnd: ae({}, C, {
                value: fs
              })
            });
          }
          co < 0 && Oe("disabledDepth fell below zero. This is a bug in React. Please file an issue.");
        }
      }
      var Ya = Ot.ReactCurrentDispatcher, pr;
      function vo(C, M, X) {
        {
          if (pr === void 0)
            try {
              throw Error();
            } catch (de) {
              var J = de.stack.trim().match(/\n( *(at )?)/);
              pr = J && J[1] || "";
            }
          return `
` + pr + C;
        }
      }
      var ho = !1, mo;
      {
        var Wl = typeof WeakMap == "function" ? WeakMap : Map;
        mo = new Wl();
      }
      function Ql(C, M) {
        if (!C || ho)
          return "";
        {
          var X = mo.get(C);
          if (X !== void 0)
            return X;
        }
        var J;
        ho = !0;
        var de = Error.prepareStackTrace;
        Error.prepareStackTrace = void 0;
        var Xe;
        Xe = Ya.current, Ya.current = null, Yl();
        try {
          if (M) {
            var be = function() {
              throw Error();
            };
            if (Object.defineProperty(be.prototype, "props", {
              set: function() {
                throw Error();
              }
            }), typeof Reflect == "object" && Reflect.construct) {
              try {
                Reflect.construct(be, []);
              } catch (wt) {
                J = wt;
              }
              Reflect.construct(C, [], be);
            } else {
              try {
                be.call();
              } catch (wt) {
                J = wt;
              }
              C.call(be.prototype);
            }
          } else {
            try {
              throw Error();
            } catch (wt) {
              J = wt;
            }
            C();
          }
        } catch (wt) {
          if (wt && J && typeof wt.stack == "string") {
            for (var Ge = wt.stack.split(`
`), pt = J.stack.split(`
`), Dt = Ge.length - 1, Wt = pt.length - 1; Dt >= 1 && Wt >= 0 && Ge[Dt] !== pt[Wt]; )
              Wt--;
            for (; Dt >= 1 && Wt >= 0; Dt--, Wt--)
              if (Ge[Dt] !== pt[Wt]) {
                if (Dt !== 1 || Wt !== 1)
                  do
                    if (Dt--, Wt--, Wt < 0 || Ge[Dt] !== pt[Wt]) {
                      var $t = `
` + Ge[Dt].replace(" at new ", " at ");
                      return C.displayName && $t.includes("<anonymous>") && ($t = $t.replace("<anonymous>", C.displayName)), typeof C == "function" && mo.set(C, $t), $t;
                    }
                  while (Dt >= 1 && Wt >= 0);
                break;
              }
          }
        } finally {
          ho = !1, Ya.current = Xe, po(), Error.prepareStackTrace = de;
        }
        var Qt = C ? C.displayName || C.name : "", Xt = Qt ? vo(Qt) : "";
        return typeof C == "function" && mo.set(C, Xt), Xt;
      }
      function _i(C, M, X) {
        return Ql(C, !1);
      }
      function xd(C) {
        var M = C.prototype;
        return !!(M && M.isReactComponent);
      }
      function oi(C, M, X) {
        if (C == null)
          return "";
        if (typeof C == "function")
          return Ql(C, xd(C));
        if (typeof C == "string")
          return vo(C);
        switch (C) {
          case L:
            return vo("Suspense");
          case j:
            return vo("SuspenseList");
        }
        if (typeof C == "object")
          switch (C.$$typeof) {
            case A:
              return _i(C.render);
            case F:
              return oi(C.type, M, X);
            case $: {
              var J = C, de = J._payload, Xe = J._init;
              try {
                return oi(Xe(de), M, X);
              } catch {
              }
            }
          }
        return "";
      }
      var Nt = {}, Gl = Ot.ReactDebugCurrentFrame;
      function Jo(C) {
        if (C) {
          var M = C._owner, X = oi(C.type, C._source, M ? M.type : null);
          Gl.setExtraStackFrame(X);
        } else
          Gl.setExtraStackFrame(null);
      }
      function ql(C, M, X, J, de) {
        {
          var Xe = Function.call.bind(Dr);
          for (var be in C)
            if (Xe(C, be)) {
              var Ge = void 0;
              try {
                if (typeof C[be] != "function") {
                  var pt = Error((J || "React class") + ": " + X + " type `" + be + "` is invalid; it must be a function, usually from the `prop-types` package, but received `" + typeof C[be] + "`.This often happens because of typos such as `PropTypes.function` instead of `PropTypes.func`.");
                  throw pt.name = "Invariant Violation", pt;
                }
                Ge = C[be](M, be, J, X, null, "SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED");
              } catch (Dt) {
                Ge = Dt;
              }
              Ge && !(Ge instanceof Error) && (Jo(de), Oe("%s: type specification of %s `%s` is invalid; the type checker function must return `null` or an `Error` but returned a %s. You may have forgotten to pass an argument to the type checker creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and shape all require an argument).", J || "React class", X, be, typeof Ge), Jo(null)), Ge instanceof Error && !(Ge.message in Nt) && (Nt[Ge.message] = !0, Jo(de), Oe("Failed %s type: %s", X, Ge.message), Jo(null));
            }
        }
      }
      function Ct(C) {
        if (C) {
          var M = C._owner, X = oi(C.type, C._source, M ? M.type : null);
          Et(X);
        } else
          Et(null);
      }
      var Xl;
      Xl = !1;
      function Kl() {
        if (Ce.current) {
          var C = or(Ce.current.type);
          if (C)
            return `

Check the render method of \`` + C + "`.";
        }
        return "";
      }
      function it(C) {
        if (C !== void 0) {
          var M = C.fileName.replace(/^.*[\\\/]/, ""), X = C.lineNumber;
          return `

Check your code at ` + M + ":" + X + ".";
        }
        return "";
      }
      function el(C) {
        return C != null ? it(C.__source) : "";
      }
      var cn = {};
      function Ar(C) {
        var M = Kl();
        if (!M) {
          var X = typeof C == "string" ? C : C.displayName || C.name;
          X && (M = `

Check the top-level render call using <` + X + ">.");
        }
        return M;
      }
      function vr(C, M) {
        if (!(!C._store || C._store.validated || C.key != null)) {
          C._store.validated = !0;
          var X = Ar(M);
          if (!cn[X]) {
            cn[X] = !0;
            var J = "";
            C && C._owner && C._owner !== Ce.current && (J = " It was passed a child from " + or(C._owner.type) + "."), Ct(C), Oe('Each child in a list should have a unique "key" prop.%s%s See https://reactjs.org/link/warning-keys for more information.', X, J), Ct(null);
          }
        }
      }
      function yo(C, M) {
        if (typeof C == "object") {
          if (bn(C))
            for (var X = 0; X < C.length; X++) {
              var J = C[X];
              Bt(J) && vr(J, M);
            }
          else if (Bt(C))
            C._store && (C._store.validated = !0);
          else if (C) {
            var de = ve(C);
            if (typeof de == "function" && de !== C.entries)
              for (var Xe = de.call(C), be; !(be = Xe.next()).done; )
                Bt(be.value) && vr(be.value, M);
          }
        }
      }
      function hn(C) {
        {
          var M = C.type;
          if (M == null || typeof M == "string")
            return;
          var X;
          if (typeof M == "function")
            X = M.propTypes;
          else if (typeof M == "object" && (M.$$typeof === A || // Note: Memo only checks outer props here.
          // Inner props are checked in the reconciler.
          M.$$typeof === F))
            X = M.propTypes;
          else
            return;
          if (X) {
            var J = or(M);
            ql(X, C.props, "prop", J, C);
          } else if (M.PropTypes !== void 0 && !Xl) {
            Xl = !0;
            var de = or(M);
            Oe("Component %s declared `PropTypes` instead of `propTypes`. Did you misspell the property assignment?", de || "Unknown");
          }
          typeof M.getDefaultProps == "function" && !M.getDefaultProps.isReactClassApproved && Oe("getDefaultProps is only used on classic React.createClass definitions. Use a static property named `defaultProps` instead.");
        }
      }
      function Ft(C) {
        {
          for (var M = Object.keys(C.props), X = 0; X < M.length; X++) {
            var J = M[X];
            if (J !== "children" && J !== "key") {
              Ct(C), Oe("Invalid prop `%s` supplied to `React.Fragment`. React.Fragment can only have `key` and `children` props.", J), Ct(null);
              break;
            }
          }
          C.ref !== null && (Ct(C), Oe("Invalid attribute `ref` supplied to `React.Fragment`."), Ct(null));
        }
      }
      function Tc(C, M, X) {
        var J = Bl(C);
        if (!J) {
          var de = "";
          (C === void 0 || typeof C == "object" && C !== null && Object.keys(C).length === 0) && (de += " You likely forgot to export your component from the file it's defined in, or you might have mixed up default and named imports.");
          var Xe = el(M);
          Xe ? de += Xe : de += Kl();
          var be;
          C === null ? be = "null" : bn(C) ? be = "array" : C !== void 0 && C.$$typeof === p ? (be = "<" + (or(C.type) || "Unknown") + " />", de = " Did you accidentally export a JSX literal instead of a component?") : be = typeof C, Oe("React.createElement: type is invalid -- expected a string (for built-in components) or a class/function (for composite components) but got: %s.%s", be, de);
        }
        var Ge = Ie.apply(this, arguments);
        if (Ge == null)
          return Ge;
        if (J)
          for (var pt = 2; pt < arguments.length; pt++)
            yo(arguments[pt], C);
        return C === E ? Ft(Ge) : hn(Ge), Ge;
      }
      var zr = !1;
      function Hn(C) {
        var M = Tc.bind(null, C);
        return M.type = C, zr || (zr = !0, mt("React.createFactory() is deprecated and will be removed in a future major release. Consider using JSX or use React.createElement() directly instead.")), Object.defineProperty(M, "type", {
          enumerable: !1,
          get: function() {
            return mt("Factory.type is deprecated. Access the class directly before passing it to createFactory."), Object.defineProperty(this, "type", {
              value: C
            }), C;
          }
        }), M;
      }
      function li(C, M, X) {
        for (var J = Vt.apply(this, arguments), de = 2; de < arguments.length; de++)
          yo(arguments[de], J.type);
        return hn(J), J;
      }
      function Rc(C, M) {
        var X = oe.transition;
        oe.transition = {};
        var J = oe.transition;
        oe.transition._updatedFibers = /* @__PURE__ */ new Set();
        try {
          C();
        } finally {
          if (oe.transition = X, X === null && J._updatedFibers) {
            var de = J._updatedFibers.size;
            de > 10 && mt("Detected a large number of updates inside startTransition. If this is due to a subscription please re-write it to use React provided hooks. Otherwise concurrent mode guarantees are off the table."), J._updatedFibers.clear();
          }
        }
      }
      var Di = !1, go = null;
      function xc(C) {
        if (go === null)
          try {
            var M = ("require" + Math.random()).slice(0, 7), X = l && l[M];
            go = X.call(l, "timers").setImmediate;
          } catch {
            go = function(de) {
              Di === !1 && (Di = !0, typeof MessageChannel > "u" && Oe("This browser does not have a MessageChannel implementation, so enqueuing tasks via await act(async () => ...) will fail. Please file an issue at https://github.com/facebook/react/issues if you encounter this warning."));
              var Xe = new MessageChannel();
              Xe.port1.onmessage = de, Xe.port2.postMessage(void 0);
            };
          }
        return go(C);
      }
      var ha = 0, So = !1;
      function Oi(C) {
        {
          var M = ha;
          ha++, ne.current === null && (ne.current = []);
          var X = ne.isBatchingLegacy, J;
          try {
            if (ne.isBatchingLegacy = !0, J = C(), !X && ne.didScheduleLegacyUpdate) {
              var de = ne.current;
              de !== null && (ne.didScheduleLegacyUpdate = !1, bo(de));
            }
          } catch (Qt) {
            throw ma(M), Qt;
          } finally {
            ne.isBatchingLegacy = X;
          }
          if (J !== null && typeof J == "object" && typeof J.then == "function") {
            var Xe = J, be = !1, Ge = {
              then: function(Qt, Xt) {
                be = !0, Xe.then(function(wt) {
                  ma(M), ha === 0 ? Zl(wt, Qt, Xt) : Qt(wt);
                }, function(wt) {
                  ma(M), Xt(wt);
                });
              }
            };
            return !So && typeof Promise < "u" && Promise.resolve().then(function() {
            }).then(function() {
              be || (So = !0, Oe("You called act(async () => ...) without await. This could lead to unexpected testing behaviour, interleaving multiple act calls and mixing their scopes. You should - await act(async () => ...);"));
            }), Ge;
          } else {
            var pt = J;
            if (ma(M), ha === 0) {
              var Dt = ne.current;
              Dt !== null && (bo(Dt), ne.current = null);
              var Wt = {
                then: function(Qt, Xt) {
                  ne.current === null ? (ne.current = [], Zl(pt, Qt, Xt)) : Qt(pt);
                }
              };
              return Wt;
            } else {
              var $t = {
                then: function(Qt, Xt) {
                  Qt(pt);
                }
              };
              return $t;
            }
          }
        }
      }
      function ma(C) {
        C !== ha - 1 && Oe("You seem to have overlapping act() calls, this is not supported. Be sure to await previous act() calls before making a new one. "), ha = C;
      }
      function Zl(C, M, X) {
        {
          var J = ne.current;
          if (J !== null)
            try {
              bo(J), xc(function() {
                J.length === 0 ? (ne.current = null, M(C)) : Zl(C, M, X);
              });
            } catch (de) {
              X(de);
            }
          else
            M(C);
        }
      }
      var Eo = !1;
      function bo(C) {
        if (!Eo) {
          Eo = !0;
          var M = 0;
          try {
            for (; M < C.length; M++) {
              var X = C[M];
              do
                X = X(!0);
              while (X !== null);
            }
            C.length = 0;
          } catch (J) {
            throw C = C.slice(M + 1), J;
          } finally {
            Eo = !1;
          }
        }
      }
      var tl = Tc, Jl = li, ds = Hn, Wa = {
        map: ki,
        forEach: gc,
        count: Vl,
        toArray: Sc,
        only: $l
      };
      s.Children = Wa, s.Component = at, s.Fragment = E, s.Profiler = x, s.PureComponent = bt, s.StrictMode = m, s.Suspense = L, s.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED = Ot, s.act = Oi, s.cloneElement = Jl, s.createContext = os, s.createElement = tl, s.createFactory = ds, s.createRef = xr, s.forwardRef = lo, s.isValidElement = Bt, s.lazy = Ba, s.memo = ls, s.startTransition = Rc, s.unstable_act = Oi, s.useCallback = Cc, s.useContext = va, s.useDebugValue = rn, s.useDeferredValue = Ia, s.useEffect = Ec, s.useId = dt, s.useImperativeHandle = Td, s.useInsertionEffect = bc, s.useLayoutEffect = us, s.useMemo = wd, s.useReducer = so, s.useRef = Qn, s.useState = Zo, s.useSyncExternalStore = ii, s.useTransition = Rd, s.version = c, typeof __REACT_DEVTOOLS_GLOBAL_HOOK__ < "u" && typeof __REACT_DEVTOOLS_GLOBAL_HOOK__.registerInternalModuleStop == "function" && __REACT_DEVTOOLS_GLOBAL_HOOK__.registerInternalModuleStop(new Error());
    }();
  }(Mv, Mv.exports)), Mv.exports;
}
process.env.NODE_ENV === "production" ? A1.exports = F2() : A1.exports = H2();
var R = A1.exports;
const Ny = /* @__PURE__ */ LT(R);
var z1 = { exports: {} }, Ua = {}, Dy = { exports: {} }, E1 = {};
/**
 * @license React
 * scheduler.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var eT;
function j2() {
  return eT || (eT = 1, function(l) {
    function s(he, Be) {
      var Te = he.length;
      he.push(Be);
      e: for (; 0 < Te; ) {
        var V = Te - 1 >>> 1, ae = he[V];
        if (0 < y(ae, Be)) he[V] = Be, he[Te] = ae, Te = V;
        else break e;
      }
    }
    function c(he) {
      return he.length === 0 ? null : he[0];
    }
    function p(he) {
      if (he.length === 0) return null;
      var Be = he[0], Te = he.pop();
      if (Te !== Be) {
        he[0] = Te;
        e: for (var V = 0, ae = he.length, Je = ae >>> 1; V < Je; ) {
          var at = 2 * (V + 1) - 1, ct = he[at], yt = at + 1, Lt = he[yt];
          if (0 > y(ct, Te)) yt < ae && 0 > y(Lt, ct) ? (he[V] = Lt, he[yt] = Te, V = yt) : (he[V] = ct, he[at] = Te, V = at);
          else if (yt < ae && 0 > y(Lt, Te)) he[V] = Lt, he[yt] = Te, V = yt;
          else break e;
        }
      }
      return Be;
    }
    function y(he, Be) {
      var Te = he.sortIndex - Be.sortIndex;
      return Te !== 0 ? Te : he.id - Be.id;
    }
    if (typeof performance == "object" && typeof performance.now == "function") {
      var E = performance;
      l.unstable_now = function() {
        return E.now();
      };
    } else {
      var m = Date, x = m.now();
      l.unstable_now = function() {
        return m.now() - x;
      };
    }
    var T = [], D = [], A = 1, L = null, j = 3, F = !1, $ = !1, H = !1, G = typeof setTimeout == "function" ? setTimeout : null, W = typeof clearTimeout == "function" ? clearTimeout : null, ve = typeof setImmediate < "u" ? setImmediate : null;
    typeof navigator < "u" && navigator.scheduling !== void 0 && navigator.scheduling.isInputPending !== void 0 && navigator.scheduling.isInputPending.bind(navigator.scheduling);
    function fe(he) {
      for (var Be = c(D); Be !== null; ) {
        if (Be.callback === null) p(D);
        else if (Be.startTime <= he) p(D), Be.sortIndex = Be.expirationTime, s(T, Be);
        else break;
        Be = c(D);
      }
    }
    function oe(he) {
      if (H = !1, fe(he), !$) if (c(T) !== null) $ = !0, mt(ne);
      else {
        var Be = c(D);
        Be !== null && Oe(oe, Be.startTime - he);
      }
    }
    function ne(he, Be) {
      $ = !1, H && (H = !1, W(We), We = -1), F = !0;
      var Te = j;
      try {
        for (fe(Be), L = c(T); L !== null && (!(L.expirationTime > Be) || he && !ye()); ) {
          var V = L.callback;
          if (typeof V == "function") {
            L.callback = null, j = L.priorityLevel;
            var ae = V(L.expirationTime <= Be);
            Be = l.unstable_now(), typeof ae == "function" ? L.callback = ae : L === c(T) && p(T), fe(Be);
          } else p(T);
          L = c(T);
        }
        if (L !== null) var Je = !0;
        else {
          var at = c(D);
          at !== null && Oe(oe, at.startTime - Be), Je = !1;
        }
        return Je;
      } finally {
        L = null, j = Te, F = !1;
      }
    }
    var Ce = !1, me = null, We = -1, Et = 5, Rt = -1;
    function ye() {
      return !(l.unstable_now() - Rt < Et);
    }
    function Ue() {
      if (me !== null) {
        var he = l.unstable_now();
        Rt = he;
        var Be = !0;
        try {
          Be = me(!0, he);
        } finally {
          Be ? qe() : (Ce = !1, me = null);
        }
      } else Ce = !1;
    }
    var qe;
    if (typeof ve == "function") qe = function() {
      ve(Ue);
    };
    else if (typeof MessageChannel < "u") {
      var rt = new MessageChannel(), Ot = rt.port2;
      rt.port1.onmessage = Ue, qe = function() {
        Ot.postMessage(null);
      };
    } else qe = function() {
      G(Ue, 0);
    };
    function mt(he) {
      me = he, Ce || (Ce = !0, qe());
    }
    function Oe(he, Be) {
      We = G(function() {
        he(l.unstable_now());
      }, Be);
    }
    l.unstable_IdlePriority = 5, l.unstable_ImmediatePriority = 1, l.unstable_LowPriority = 4, l.unstable_NormalPriority = 3, l.unstable_Profiling = null, l.unstable_UserBlockingPriority = 2, l.unstable_cancelCallback = function(he) {
      he.callback = null;
    }, l.unstable_continueExecution = function() {
      $ || F || ($ = !0, mt(ne));
    }, l.unstable_forceFrameRate = function(he) {
      0 > he || 125 < he ? console.error("forceFrameRate takes a positive int between 0 and 125, forcing frame rates higher than 125 fps is not supported") : Et = 0 < he ? Math.floor(1e3 / he) : 5;
    }, l.unstable_getCurrentPriorityLevel = function() {
      return j;
    }, l.unstable_getFirstCallbackNode = function() {
      return c(T);
    }, l.unstable_next = function(he) {
      switch (j) {
        case 1:
        case 2:
        case 3:
          var Be = 3;
          break;
        default:
          Be = j;
      }
      var Te = j;
      j = Be;
      try {
        return he();
      } finally {
        j = Te;
      }
    }, l.unstable_pauseExecution = function() {
    }, l.unstable_requestPaint = function() {
    }, l.unstable_runWithPriority = function(he, Be) {
      switch (he) {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
          break;
        default:
          he = 3;
      }
      var Te = j;
      j = he;
      try {
        return Be();
      } finally {
        j = Te;
      }
    }, l.unstable_scheduleCallback = function(he, Be, Te) {
      var V = l.unstable_now();
      switch (typeof Te == "object" && Te !== null ? (Te = Te.delay, Te = typeof Te == "number" && 0 < Te ? V + Te : V) : Te = V, he) {
        case 1:
          var ae = -1;
          break;
        case 2:
          ae = 250;
          break;
        case 5:
          ae = 1073741823;
          break;
        case 4:
          ae = 1e4;
          break;
        default:
          ae = 5e3;
      }
      return ae = Te + ae, he = { id: A++, callback: Be, priorityLevel: he, startTime: Te, expirationTime: ae, sortIndex: -1 }, Te > V ? (he.sortIndex = Te, s(D, he), c(T) === null && he === c(D) && (H ? (W(We), We = -1) : H = !0, Oe(oe, Te - V))) : (he.sortIndex = ae, s(T, he), $ || F || ($ = !0, mt(ne))), he;
    }, l.unstable_shouldYield = ye, l.unstable_wrapCallback = function(he) {
      var Be = j;
      return function() {
        var Te = j;
        j = Be;
        try {
          return he.apply(this, arguments);
        } finally {
          j = Te;
        }
      };
    };
  }(E1)), E1;
}
var b1 = {};
/**
 * @license React
 * scheduler.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var tT;
function V2() {
  return tT || (tT = 1, function(l) {
    process.env.NODE_ENV !== "production" && function() {
      typeof __REACT_DEVTOOLS_GLOBAL_HOOK__ < "u" && typeof __REACT_DEVTOOLS_GLOBAL_HOOK__.registerInternalModuleStart == "function" && __REACT_DEVTOOLS_GLOBAL_HOOK__.registerInternalModuleStart(new Error());
      var s = !1, c = !1, p = 5;
      function y(Ee, Ie) {
        var ft = Ee.length;
        Ee.push(Ie), x(Ee, Ie, ft);
      }
      function E(Ee) {
        return Ee.length === 0 ? null : Ee[0];
      }
      function m(Ee) {
        if (Ee.length === 0)
          return null;
        var Ie = Ee[0], ft = Ee.pop();
        return ft !== Ie && (Ee[0] = ft, T(Ee, ft, 0)), Ie;
      }
      function x(Ee, Ie, ft) {
        for (var Vt = ft; Vt > 0; ) {
          var Bt = Vt - 1 >>> 1, Mn = Ee[Bt];
          if (D(Mn, Ie) > 0)
            Ee[Bt] = Ie, Ee[Vt] = Mn, Vt = Bt;
          else
            return;
        }
      }
      function T(Ee, Ie, ft) {
        for (var Vt = ft, Bt = Ee.length, Mn = Bt >>> 1; Vt < Mn; ) {
          var Cn = (Vt + 1) * 2 - 1, Lr = Ee[Cn], Zt = Cn + 1, pa = Ee[Zt];
          if (D(Lr, Ie) < 0)
            Zt < Bt && D(pa, Lr) < 0 ? (Ee[Vt] = pa, Ee[Zt] = Ie, Vt = Zt) : (Ee[Vt] = Lr, Ee[Cn] = Ie, Vt = Cn);
          else if (Zt < Bt && D(pa, Ie) < 0)
            Ee[Vt] = pa, Ee[Zt] = Ie, Vt = Zt;
          else
            return;
        }
      }
      function D(Ee, Ie) {
        var ft = Ee.sortIndex - Ie.sortIndex;
        return ft !== 0 ? ft : Ee.id - Ie.id;
      }
      var A = 1, L = 2, j = 3, F = 4, $ = 5;
      function H(Ee, Ie) {
      }
      var G = typeof performance == "object" && typeof performance.now == "function";
      if (G) {
        var W = performance;
        l.unstable_now = function() {
          return W.now();
        };
      } else {
        var ve = Date, fe = ve.now();
        l.unstable_now = function() {
          return ve.now() - fe;
        };
      }
      var oe = 1073741823, ne = -1, Ce = 250, me = 5e3, We = 1e4, Et = oe, Rt = [], ye = [], Ue = 1, qe = null, rt = j, Ot = !1, mt = !1, Oe = !1, he = typeof setTimeout == "function" ? setTimeout : null, Be = typeof clearTimeout == "function" ? clearTimeout : null, Te = typeof setImmediate < "u" ? setImmediate : null;
      typeof navigator < "u" && navigator.scheduling !== void 0 && navigator.scheduling.isInputPending !== void 0 && navigator.scheduling.isInputPending.bind(navigator.scheduling);
      function V(Ee) {
        for (var Ie = E(ye); Ie !== null; ) {
          if (Ie.callback === null)
            m(ye);
          else if (Ie.startTime <= Ee)
            m(ye), Ie.sortIndex = Ie.expirationTime, y(Rt, Ie);
          else
            return;
          Ie = E(ye);
        }
      }
      function ae(Ee) {
        if (Oe = !1, V(Ee), !mt)
          if (E(Rt) !== null)
            mt = !0, Va(Je);
          else {
            var Ie = E(ye);
            Ie !== null && Wn(ae, Ie.startTime - Ee);
          }
      }
      function Je(Ee, Ie) {
        mt = !1, Oe && (Oe = !1, ea()), Ot = !0;
        var ft = rt;
        try {
          var Vt;
          if (!c) return at(Ee, Ie);
        } finally {
          qe = null, rt = ft, Ot = !1;
        }
      }
      function at(Ee, Ie) {
        var ft = Ie;
        for (V(ft), qe = E(Rt); qe !== null && !s && !(qe.expirationTime > ft && (!Ee || xi())); ) {
          var Vt = qe.callback;
          if (typeof Vt == "function") {
            qe.callback = null, rt = qe.priorityLevel;
            var Bt = qe.expirationTime <= ft, Mn = Vt(Bt);
            ft = l.unstable_now(), typeof Mn == "function" ? qe.callback = Mn : qe === E(Rt) && m(Rt), V(ft);
          } else
            m(Rt);
          qe = E(Rt);
        }
        if (qe !== null)
          return !0;
        var Cn = E(ye);
        return Cn !== null && Wn(ae, Cn.startTime - ft), !1;
      }
      function ct(Ee, Ie) {
        switch (Ee) {
          case A:
          case L:
          case j:
          case F:
          case $:
            break;
          default:
            Ee = j;
        }
        var ft = rt;
        rt = Ee;
        try {
          return Ie();
        } finally {
          rt = ft;
        }
      }
      function yt(Ee) {
        var Ie;
        switch (rt) {
          case A:
          case L:
          case j:
            Ie = j;
            break;
          default:
            Ie = rt;
            break;
        }
        var ft = rt;
        rt = Ie;
        try {
          return Ee();
        } finally {
          rt = ft;
        }
      }
      function Lt(Ee) {
        var Ie = rt;
        return function() {
          var ft = rt;
          rt = Ie;
          try {
            return Ee.apply(this, arguments);
          } finally {
            rt = ft;
          }
        };
      }
      function xt(Ee, Ie, ft) {
        var Vt = l.unstable_now(), Bt;
        if (typeof ft == "object" && ft !== null) {
          var Mn = ft.delay;
          typeof Mn == "number" && Mn > 0 ? Bt = Vt + Mn : Bt = Vt;
        } else
          Bt = Vt;
        var Cn;
        switch (Ee) {
          case A:
            Cn = ne;
            break;
          case L:
            Cn = Ce;
            break;
          case $:
            Cn = Et;
            break;
          case F:
            Cn = We;
            break;
          case j:
          default:
            Cn = me;
            break;
        }
        var Lr = Bt + Cn, Zt = {
          id: Ue++,
          callback: Ie,
          priorityLevel: Ee,
          startTime: Bt,
          expirationTime: Lr,
          sortIndex: -1
        };
        return Bt > Vt ? (Zt.sortIndex = Bt, y(ye, Zt), E(Rt) === null && Zt === E(ye) && (Oe ? ea() : Oe = !0, Wn(ae, Bt - Vt))) : (Zt.sortIndex = Lr, y(Rt, Zt), !mt && !Ot && (mt = !0, Va(Je))), Zt;
      }
      function bt() {
      }
      function En() {
        !mt && !Ot && (mt = !0, Va(Je));
      }
      function xr() {
        return E(Rt);
      }
      function Fa(Ee) {
        Ee.callback = null;
      }
      function bn() {
        return rt;
      }
      var ir = !1, kr = null, _r = -1, Fn = p, Ha = -1;
      function xi() {
        var Ee = l.unstable_now() - Ha;
        return !(Ee < Fn);
      }
      function or() {
      }
      function Dr(Ee) {
        if (Ee < 0 || Ee > 125) {
          console.error("forceFrameRate takes a positive int between 0 and 125, forcing frame rates higher than 125 fps is not supported");
          return;
        }
        Ee > 0 ? Fn = Math.floor(1e3 / Ee) : Fn = p;
      }
      var ni = function() {
        if (kr !== null) {
          var Ee = l.unstable_now();
          Ha = Ee;
          var Ie = !0, ft = !0;
          try {
            ft = kr(Ie, Ee);
          } finally {
            ft ? Or() : (ir = !1, kr = null);
          }
        } else
          ir = !1;
      }, Or;
      if (typeof Te == "function")
        Or = function() {
          Te(ni);
        };
      else if (typeof MessageChannel < "u") {
        var ja = new MessageChannel(), Mr = ja.port2;
        ja.port1.onmessage = ni, Or = function() {
          Mr.postMessage(null);
        };
      } else
        Or = function() {
          he(ni, 0);
        };
      function Va(Ee) {
        kr = Ee, ir || (ir = !0, Or());
      }
      function Wn(Ee, Ie) {
        _r = he(function() {
          Ee(l.unstable_now());
        }, Ie);
      }
      function ea() {
        Be(_r), _r = -1;
      }
      var Xo = or, ri = null;
      l.unstable_IdlePriority = $, l.unstable_ImmediatePriority = A, l.unstable_LowPriority = F, l.unstable_NormalPriority = j, l.unstable_Profiling = ri, l.unstable_UserBlockingPriority = L, l.unstable_cancelCallback = Fa, l.unstable_continueExecution = En, l.unstable_forceFrameRate = Dr, l.unstable_getCurrentPriorityLevel = bn, l.unstable_getFirstCallbackNode = xr, l.unstable_next = yt, l.unstable_pauseExecution = bt, l.unstable_requestPaint = Xo, l.unstable_runWithPriority = ct, l.unstable_scheduleCallback = xt, l.unstable_shouldYield = xi, l.unstable_wrapCallback = Lt, typeof __REACT_DEVTOOLS_GLOBAL_HOOK__ < "u" && typeof __REACT_DEVTOOLS_GLOBAL_HOOK__.registerInternalModuleStop == "function" && __REACT_DEVTOOLS_GLOBAL_HOOK__.registerInternalModuleStop(new Error());
    }();
  }(b1)), b1;
}
var nT;
function NT() {
  return nT || (nT = 1, process.env.NODE_ENV === "production" ? Dy.exports = j2() : Dy.exports = V2()), Dy.exports;
}
/**
 * @license React
 * react-dom.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var rT;
function $2() {
  if (rT) return Ua;
  rT = 1;
  var l = R, s = NT();
  function c(n) {
    for (var r = "https://reactjs.org/docs/error-decoder.html?invariant=" + n, o = 1; o < arguments.length; o++) r += "&args[]=" + encodeURIComponent(arguments[o]);
    return "Minified React error #" + n + "; visit " + r + " for the full message or use the non-minified dev environment for full errors and additional helpful warnings.";
  }
  var p = /* @__PURE__ */ new Set(), y = {};
  function E(n, r) {
    m(n, r), m(n + "Capture", r);
  }
  function m(n, r) {
    for (y[n] = r, n = 0; n < r.length; n++) p.add(r[n]);
  }
  var x = !(typeof window > "u" || typeof window.document > "u" || typeof window.document.createElement > "u"), T = Object.prototype.hasOwnProperty, D = /^[:A-Z_a-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02FF\u0370-\u037D\u037F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD][:A-Z_a-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02FF\u0370-\u037D\u037F-\u1FFF\u200C-\u200D\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD\-.0-9\u00B7\u0300-\u036F\u203F-\u2040]*$/, A = {}, L = {};
  function j(n) {
    return T.call(L, n) ? !0 : T.call(A, n) ? !1 : D.test(n) ? L[n] = !0 : (A[n] = !0, !1);
  }
  function F(n, r, o, f) {
    if (o !== null && o.type === 0) return !1;
    switch (typeof r) {
      case "function":
      case "symbol":
        return !0;
      case "boolean":
        return f ? !1 : o !== null ? !o.acceptsBooleans : (n = n.toLowerCase().slice(0, 5), n !== "data-" && n !== "aria-");
      default:
        return !1;
    }
  }
  function $(n, r, o, f) {
    if (r === null || typeof r > "u" || F(n, r, o, f)) return !0;
    if (f) return !1;
    if (o !== null) switch (o.type) {
      case 3:
        return !r;
      case 4:
        return r === !1;
      case 5:
        return isNaN(r);
      case 6:
        return isNaN(r) || 1 > r;
    }
    return !1;
  }
  function H(n, r, o, f, v, g, w) {
    this.acceptsBooleans = r === 2 || r === 3 || r === 4, this.attributeName = f, this.attributeNamespace = v, this.mustUseProperty = o, this.propertyName = n, this.type = r, this.sanitizeURL = g, this.removeEmptyString = w;
  }
  var G = {};
  "children dangerouslySetInnerHTML defaultValue defaultChecked innerHTML suppressContentEditableWarning suppressHydrationWarning style".split(" ").forEach(function(n) {
    G[n] = new H(n, 0, !1, n, null, !1, !1);
  }), [["acceptCharset", "accept-charset"], ["className", "class"], ["htmlFor", "for"], ["httpEquiv", "http-equiv"]].forEach(function(n) {
    var r = n[0];
    G[r] = new H(r, 1, !1, n[1], null, !1, !1);
  }), ["contentEditable", "draggable", "spellCheck", "value"].forEach(function(n) {
    G[n] = new H(n, 2, !1, n.toLowerCase(), null, !1, !1);
  }), ["autoReverse", "externalResourcesRequired", "focusable", "preserveAlpha"].forEach(function(n) {
    G[n] = new H(n, 2, !1, n, null, !1, !1);
  }), "allowFullScreen async autoFocus autoPlay controls default defer disabled disablePictureInPicture disableRemotePlayback formNoValidate hidden loop noModule noValidate open playsInline readOnly required reversed scoped seamless itemScope".split(" ").forEach(function(n) {
    G[n] = new H(n, 3, !1, n.toLowerCase(), null, !1, !1);
  }), ["checked", "multiple", "muted", "selected"].forEach(function(n) {
    G[n] = new H(n, 3, !0, n, null, !1, !1);
  }), ["capture", "download"].forEach(function(n) {
    G[n] = new H(n, 4, !1, n, null, !1, !1);
  }), ["cols", "rows", "size", "span"].forEach(function(n) {
    G[n] = new H(n, 6, !1, n, null, !1, !1);
  }), ["rowSpan", "start"].forEach(function(n) {
    G[n] = new H(n, 5, !1, n.toLowerCase(), null, !1, !1);
  });
  var W = /[\-:]([a-z])/g;
  function ve(n) {
    return n[1].toUpperCase();
  }
  "accent-height alignment-baseline arabic-form baseline-shift cap-height clip-path clip-rule color-interpolation color-interpolation-filters color-profile color-rendering dominant-baseline enable-background fill-opacity fill-rule flood-color flood-opacity font-family font-size font-size-adjust font-stretch font-style font-variant font-weight glyph-name glyph-orientation-horizontal glyph-orientation-vertical horiz-adv-x horiz-origin-x image-rendering letter-spacing lighting-color marker-end marker-mid marker-start overline-position overline-thickness paint-order panose-1 pointer-events rendering-intent shape-rendering stop-color stop-opacity strikethrough-position strikethrough-thickness stroke-dasharray stroke-dashoffset stroke-linecap stroke-linejoin stroke-miterlimit stroke-opacity stroke-width text-anchor text-decoration text-rendering underline-position underline-thickness unicode-bidi unicode-range units-per-em v-alphabetic v-hanging v-ideographic v-mathematical vector-effect vert-adv-y vert-origin-x vert-origin-y word-spacing writing-mode xmlns:xlink x-height".split(" ").forEach(function(n) {
    var r = n.replace(
      W,
      ve
    );
    G[r] = new H(r, 1, !1, n, null, !1, !1);
  }), "xlink:actuate xlink:arcrole xlink:role xlink:show xlink:title xlink:type".split(" ").forEach(function(n) {
    var r = n.replace(W, ve);
    G[r] = new H(r, 1, !1, n, "http://www.w3.org/1999/xlink", !1, !1);
  }), ["xml:base", "xml:lang", "xml:space"].forEach(function(n) {
    var r = n.replace(W, ve);
    G[r] = new H(r, 1, !1, n, "http://www.w3.org/XML/1998/namespace", !1, !1);
  }), ["tabIndex", "crossOrigin"].forEach(function(n) {
    G[n] = new H(n, 1, !1, n.toLowerCase(), null, !1, !1);
  }), G.xlinkHref = new H("xlinkHref", 1, !1, "xlink:href", "http://www.w3.org/1999/xlink", !0, !1), ["src", "href", "action", "formAction"].forEach(function(n) {
    G[n] = new H(n, 1, !1, n.toLowerCase(), null, !0, !0);
  });
  function fe(n, r, o, f) {
    var v = G.hasOwnProperty(r) ? G[r] : null;
    (v !== null ? v.type !== 0 : f || !(2 < r.length) || r[0] !== "o" && r[0] !== "O" || r[1] !== "n" && r[1] !== "N") && ($(r, o, v, f) && (o = null), f || v === null ? j(r) && (o === null ? n.removeAttribute(r) : n.setAttribute(r, "" + o)) : v.mustUseProperty ? n[v.propertyName] = o === null ? v.type === 3 ? !1 : "" : o : (r = v.attributeName, f = v.attributeNamespace, o === null ? n.removeAttribute(r) : (v = v.type, o = v === 3 || v === 4 && o === !0 ? "" : "" + o, f ? n.setAttributeNS(f, r, o) : n.setAttribute(r, o))));
  }
  var oe = l.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED, ne = Symbol.for("react.element"), Ce = Symbol.for("react.portal"), me = Symbol.for("react.fragment"), We = Symbol.for("react.strict_mode"), Et = Symbol.for("react.profiler"), Rt = Symbol.for("react.provider"), ye = Symbol.for("react.context"), Ue = Symbol.for("react.forward_ref"), qe = Symbol.for("react.suspense"), rt = Symbol.for("react.suspense_list"), Ot = Symbol.for("react.memo"), mt = Symbol.for("react.lazy"), Oe = Symbol.for("react.offscreen"), he = Symbol.iterator;
  function Be(n) {
    return n === null || typeof n != "object" ? null : (n = he && n[he] || n["@@iterator"], typeof n == "function" ? n : null);
  }
  var Te = Object.assign, V;
  function ae(n) {
    if (V === void 0) try {
      throw Error();
    } catch (o) {
      var r = o.stack.trim().match(/\n( *(at )?)/);
      V = r && r[1] || "";
    }
    return `
` + V + n;
  }
  var Je = !1;
  function at(n, r) {
    if (!n || Je) return "";
    Je = !0;
    var o = Error.prepareStackTrace;
    Error.prepareStackTrace = void 0;
    try {
      if (r) if (r = function() {
        throw Error();
      }, Object.defineProperty(r.prototype, "props", { set: function() {
        throw Error();
      } }), typeof Reflect == "object" && Reflect.construct) {
        try {
          Reflect.construct(r, []);
        } catch (K) {
          var f = K;
        }
        Reflect.construct(n, [], r);
      } else {
        try {
          r.call();
        } catch (K) {
          f = K;
        }
        n.call(r.prototype);
      }
      else {
        try {
          throw Error();
        } catch (K) {
          f = K;
        }
        n();
      }
    } catch (K) {
      if (K && f && typeof K.stack == "string") {
        for (var v = K.stack.split(`
`), g = f.stack.split(`
`), w = v.length - 1, O = g.length - 1; 1 <= w && 0 <= O && v[w] !== g[O]; ) O--;
        for (; 1 <= w && 0 <= O; w--, O--) if (v[w] !== g[O]) {
          if (w !== 1 || O !== 1)
            do
              if (w--, O--, 0 > O || v[w] !== g[O]) {
                var N = `
` + v[w].replace(" at new ", " at ");
                return n.displayName && N.includes("<anonymous>") && (N = N.replace("<anonymous>", n.displayName)), N;
              }
            while (1 <= w && 0 <= O);
          break;
        }
      }
    } finally {
      Je = !1, Error.prepareStackTrace = o;
    }
    return (n = n ? n.displayName || n.name : "") ? ae(n) : "";
  }
  function ct(n) {
    switch (n.tag) {
      case 5:
        return ae(n.type);
      case 16:
        return ae("Lazy");
      case 13:
        return ae("Suspense");
      case 19:
        return ae("SuspenseList");
      case 0:
      case 2:
      case 15:
        return n = at(n.type, !1), n;
      case 11:
        return n = at(n.type.render, !1), n;
      case 1:
        return n = at(n.type, !0), n;
      default:
        return "";
    }
  }
  function yt(n) {
    if (n == null) return null;
    if (typeof n == "function") return n.displayName || n.name || null;
    if (typeof n == "string") return n;
    switch (n) {
      case me:
        return "Fragment";
      case Ce:
        return "Portal";
      case Et:
        return "Profiler";
      case We:
        return "StrictMode";
      case qe:
        return "Suspense";
      case rt:
        return "SuspenseList";
    }
    if (typeof n == "object") switch (n.$$typeof) {
      case ye:
        return (n.displayName || "Context") + ".Consumer";
      case Rt:
        return (n._context.displayName || "Context") + ".Provider";
      case Ue:
        var r = n.render;
        return n = n.displayName, n || (n = r.displayName || r.name || "", n = n !== "" ? "ForwardRef(" + n + ")" : "ForwardRef"), n;
      case Ot:
        return r = n.displayName || null, r !== null ? r : yt(n.type) || "Memo";
      case mt:
        r = n._payload, n = n._init;
        try {
          return yt(n(r));
        } catch {
        }
    }
    return null;
  }
  function Lt(n) {
    var r = n.type;
    switch (n.tag) {
      case 24:
        return "Cache";
      case 9:
        return (r.displayName || "Context") + ".Consumer";
      case 10:
        return (r._context.displayName || "Context") + ".Provider";
      case 18:
        return "DehydratedFragment";
      case 11:
        return n = r.render, n = n.displayName || n.name || "", r.displayName || (n !== "" ? "ForwardRef(" + n + ")" : "ForwardRef");
      case 7:
        return "Fragment";
      case 5:
        return r;
      case 4:
        return "Portal";
      case 3:
        return "Root";
      case 6:
        return "Text";
      case 16:
        return yt(r);
      case 8:
        return r === We ? "StrictMode" : "Mode";
      case 22:
        return "Offscreen";
      case 12:
        return "Profiler";
      case 21:
        return "Scope";
      case 13:
        return "Suspense";
      case 19:
        return "SuspenseList";
      case 25:
        return "TracingMarker";
      case 1:
      case 0:
      case 17:
      case 2:
      case 14:
      case 15:
        if (typeof r == "function") return r.displayName || r.name || null;
        if (typeof r == "string") return r;
    }
    return null;
  }
  function xt(n) {
    switch (typeof n) {
      case "boolean":
      case "number":
      case "string":
      case "undefined":
        return n;
      case "object":
        return n;
      default:
        return "";
    }
  }
  function bt(n) {
    var r = n.type;
    return (n = n.nodeName) && n.toLowerCase() === "input" && (r === "checkbox" || r === "radio");
  }
  function En(n) {
    var r = bt(n) ? "checked" : "value", o = Object.getOwnPropertyDescriptor(n.constructor.prototype, r), f = "" + n[r];
    if (!n.hasOwnProperty(r) && typeof o < "u" && typeof o.get == "function" && typeof o.set == "function") {
      var v = o.get, g = o.set;
      return Object.defineProperty(n, r, { configurable: !0, get: function() {
        return v.call(this);
      }, set: function(w) {
        f = "" + w, g.call(this, w);
      } }), Object.defineProperty(n, r, { enumerable: o.enumerable }), { getValue: function() {
        return f;
      }, setValue: function(w) {
        f = "" + w;
      }, stopTracking: function() {
        n._valueTracker = null, delete n[r];
      } };
    }
  }
  function xr(n) {
    n._valueTracker || (n._valueTracker = En(n));
  }
  function Fa(n) {
    if (!n) return !1;
    var r = n._valueTracker;
    if (!r) return !0;
    var o = r.getValue(), f = "";
    return n && (f = bt(n) ? n.checked ? "true" : "false" : n.value), n = f, n !== o ? (r.setValue(n), !0) : !1;
  }
  function bn(n) {
    if (n = n || (typeof document < "u" ? document : void 0), typeof n > "u") return null;
    try {
      return n.activeElement || n.body;
    } catch {
      return n.body;
    }
  }
  function ir(n, r) {
    var o = r.checked;
    return Te({}, r, { defaultChecked: void 0, defaultValue: void 0, value: void 0, checked: o ?? n._wrapperState.initialChecked });
  }
  function kr(n, r) {
    var o = r.defaultValue == null ? "" : r.defaultValue, f = r.checked != null ? r.checked : r.defaultChecked;
    o = xt(r.value != null ? r.value : o), n._wrapperState = { initialChecked: f, initialValue: o, controlled: r.type === "checkbox" || r.type === "radio" ? r.checked != null : r.value != null };
  }
  function _r(n, r) {
    r = r.checked, r != null && fe(n, "checked", r, !1);
  }
  function Fn(n, r) {
    _r(n, r);
    var o = xt(r.value), f = r.type;
    if (o != null) f === "number" ? (o === 0 && n.value === "" || n.value != o) && (n.value = "" + o) : n.value !== "" + o && (n.value = "" + o);
    else if (f === "submit" || f === "reset") {
      n.removeAttribute("value");
      return;
    }
    r.hasOwnProperty("value") ? xi(n, r.type, o) : r.hasOwnProperty("defaultValue") && xi(n, r.type, xt(r.defaultValue)), r.checked == null && r.defaultChecked != null && (n.defaultChecked = !!r.defaultChecked);
  }
  function Ha(n, r, o) {
    if (r.hasOwnProperty("value") || r.hasOwnProperty("defaultValue")) {
      var f = r.type;
      if (!(f !== "submit" && f !== "reset" || r.value !== void 0 && r.value !== null)) return;
      r = "" + n._wrapperState.initialValue, o || r === n.value || (n.value = r), n.defaultValue = r;
    }
    o = n.name, o !== "" && (n.name = ""), n.defaultChecked = !!n._wrapperState.initialChecked, o !== "" && (n.name = o);
  }
  function xi(n, r, o) {
    (r !== "number" || bn(n.ownerDocument) !== n) && (o == null ? n.defaultValue = "" + n._wrapperState.initialValue : n.defaultValue !== "" + o && (n.defaultValue = "" + o));
  }
  var or = Array.isArray;
  function Dr(n, r, o, f) {
    if (n = n.options, r) {
      r = {};
      for (var v = 0; v < o.length; v++) r["$" + o[v]] = !0;
      for (o = 0; o < n.length; o++) v = r.hasOwnProperty("$" + n[o].value), n[o].selected !== v && (n[o].selected = v), v && f && (n[o].defaultSelected = !0);
    } else {
      for (o = "" + xt(o), r = null, v = 0; v < n.length; v++) {
        if (n[v].value === o) {
          n[v].selected = !0, f && (n[v].defaultSelected = !0);
          return;
        }
        r !== null || n[v].disabled || (r = n[v]);
      }
      r !== null && (r.selected = !0);
    }
  }
  function ni(n, r) {
    if (r.dangerouslySetInnerHTML != null) throw Error(c(91));
    return Te({}, r, { value: void 0, defaultValue: void 0, children: "" + n._wrapperState.initialValue });
  }
  function Or(n, r) {
    var o = r.value;
    if (o == null) {
      if (o = r.children, r = r.defaultValue, o != null) {
        if (r != null) throw Error(c(92));
        if (or(o)) {
          if (1 < o.length) throw Error(c(93));
          o = o[0];
        }
        r = o;
      }
      r == null && (r = ""), o = r;
    }
    n._wrapperState = { initialValue: xt(o) };
  }
  function ja(n, r) {
    var o = xt(r.value), f = xt(r.defaultValue);
    o != null && (o = "" + o, o !== n.value && (n.value = o), r.defaultValue == null && n.defaultValue !== o && (n.defaultValue = o)), f != null && (n.defaultValue = "" + f);
  }
  function Mr(n) {
    var r = n.textContent;
    r === n._wrapperState.initialValue && r !== "" && r !== null && (n.value = r);
  }
  function Va(n) {
    switch (n) {
      case "svg":
        return "http://www.w3.org/2000/svg";
      case "math":
        return "http://www.w3.org/1998/Math/MathML";
      default:
        return "http://www.w3.org/1999/xhtml";
    }
  }
  function Wn(n, r) {
    return n == null || n === "http://www.w3.org/1999/xhtml" ? Va(r) : n === "http://www.w3.org/2000/svg" && r === "foreignObject" ? "http://www.w3.org/1999/xhtml" : n;
  }
  var ea, Xo = function(n) {
    return typeof MSApp < "u" && MSApp.execUnsafeLocalFunction ? function(r, o, f, v) {
      MSApp.execUnsafeLocalFunction(function() {
        return n(r, o, f, v);
      });
    } : n;
  }(function(n, r) {
    if (n.namespaceURI !== "http://www.w3.org/2000/svg" || "innerHTML" in n) n.innerHTML = r;
    else {
      for (ea = ea || document.createElement("div"), ea.innerHTML = "<svg>" + r.valueOf().toString() + "</svg>", r = ea.firstChild; n.firstChild; ) n.removeChild(n.firstChild);
      for (; r.firstChild; ) n.appendChild(r.firstChild);
    }
  });
  function ri(n, r) {
    if (r) {
      var o = n.firstChild;
      if (o && o === n.lastChild && o.nodeType === 3) {
        o.nodeValue = r;
        return;
      }
    }
    n.textContent = r;
  }
  var Ee = {
    animationIterationCount: !0,
    aspectRatio: !0,
    borderImageOutset: !0,
    borderImageSlice: !0,
    borderImageWidth: !0,
    boxFlex: !0,
    boxFlexGroup: !0,
    boxOrdinalGroup: !0,
    columnCount: !0,
    columns: !0,
    flex: !0,
    flexGrow: !0,
    flexPositive: !0,
    flexShrink: !0,
    flexNegative: !0,
    flexOrder: !0,
    gridArea: !0,
    gridRow: !0,
    gridRowEnd: !0,
    gridRowSpan: !0,
    gridRowStart: !0,
    gridColumn: !0,
    gridColumnEnd: !0,
    gridColumnSpan: !0,
    gridColumnStart: !0,
    fontWeight: !0,
    lineClamp: !0,
    lineHeight: !0,
    opacity: !0,
    order: !0,
    orphans: !0,
    tabSize: !0,
    widows: !0,
    zIndex: !0,
    zoom: !0,
    fillOpacity: !0,
    floodOpacity: !0,
    stopOpacity: !0,
    strokeDasharray: !0,
    strokeDashoffset: !0,
    strokeMiterlimit: !0,
    strokeOpacity: !0,
    strokeWidth: !0
  }, Ie = ["Webkit", "ms", "Moz", "O"];
  Object.keys(Ee).forEach(function(n) {
    Ie.forEach(function(r) {
      r = r + n.charAt(0).toUpperCase() + n.substring(1), Ee[r] = Ee[n];
    });
  });
  function ft(n, r, o) {
    return r == null || typeof r == "boolean" || r === "" ? "" : o || typeof r != "number" || r === 0 || Ee.hasOwnProperty(n) && Ee[n] ? ("" + r).trim() : r + "px";
  }
  function Vt(n, r) {
    n = n.style;
    for (var o in r) if (r.hasOwnProperty(o)) {
      var f = o.indexOf("--") === 0, v = ft(o, r[o], f);
      o === "float" && (o = "cssFloat"), f ? n.setProperty(o, v) : n[o] = v;
    }
  }
  var Bt = Te({ menuitem: !0 }, { area: !0, base: !0, br: !0, col: !0, embed: !0, hr: !0, img: !0, input: !0, keygen: !0, link: !0, meta: !0, param: !0, source: !0, track: !0, wbr: !0 });
  function Mn(n, r) {
    if (r) {
      if (Bt[n] && (r.children != null || r.dangerouslySetInnerHTML != null)) throw Error(c(137, n));
      if (r.dangerouslySetInnerHTML != null) {
        if (r.children != null) throw Error(c(60));
        if (typeof r.dangerouslySetInnerHTML != "object" || !("__html" in r.dangerouslySetInnerHTML)) throw Error(c(61));
      }
      if (r.style != null && typeof r.style != "object") throw Error(c(62));
    }
  }
  function Cn(n, r) {
    if (n.indexOf("-") === -1) return typeof r.is == "string";
    switch (n) {
      case "annotation-xml":
      case "color-profile":
      case "font-face":
      case "font-face-src":
      case "font-face-uri":
      case "font-face-format":
      case "font-face-name":
      case "missing-glyph":
        return !1;
      default:
        return !0;
    }
  }
  var Lr = null;
  function Zt(n) {
    return n = n.target || n.srcElement || window, n.correspondingUseElement && (n = n.correspondingUseElement), n.nodeType === 3 ? n.parentNode : n;
  }
  var pa = null, sn = null, qt = null;
  function Ko(n) {
    if (n = xs(n)) {
      if (typeof pa != "function") throw Error(c(280));
      var r = n.stateNode;
      r && (r = Ye(r), pa(n.stateNode, n.type, r));
    }
  }
  function ki(n) {
    sn ? qt ? qt.push(n) : qt = [n] : sn = n;
  }
  function Vl() {
    if (sn) {
      var n = sn, r = qt;
      if (qt = sn = null, Ko(n), r) for (n = 0; n < r.length; n++) Ko(r[n]);
    }
  }
  function gc(n, r) {
    return n(r);
  }
  function Sc() {
  }
  var $l = !1;
  function os(n, r, o) {
    if ($l) return n(r, o);
    $l = !0;
    try {
      return gc(n, r, o);
    } finally {
      $l = !1, (sn !== null || qt !== null) && (Sc(), Vl());
    }
  }
  function $a(n, r) {
    var o = n.stateNode;
    if (o === null) return null;
    var f = Ye(o);
    if (f === null) return null;
    o = f[r];
    e: switch (r) {
      case "onClick":
      case "onClickCapture":
      case "onDoubleClick":
      case "onDoubleClickCapture":
      case "onMouseDown":
      case "onMouseDownCapture":
      case "onMouseMove":
      case "onMouseMoveCapture":
      case "onMouseUp":
      case "onMouseUpCapture":
      case "onMouseEnter":
        (f = !f.disabled) || (n = n.type, f = !(n === "button" || n === "input" || n === "select" || n === "textarea")), n = !f;
        break e;
      default:
        n = !1;
    }
    if (n) return null;
    if (o && typeof o != "function") throw Error(c(231, r, typeof o));
    return o;
  }
  var io = !1;
  if (x) try {
    var ai = {};
    Object.defineProperty(ai, "passive", { get: function() {
      io = !0;
    } }), window.addEventListener("test", ai, ai), window.removeEventListener("test", ai, ai);
  } catch {
    io = !1;
  }
  function oo(n, r, o, f, v, g, w, O, N) {
    var K = Array.prototype.slice.call(arguments, 3);
    try {
      r.apply(o, K);
    } catch (ue) {
      this.onError(ue);
    }
  }
  var ta = !1, Ba = null, lo = !1, uo = null, Bl = { onError: function(n) {
    ta = !0, Ba = n;
  } };
  function ls(n, r, o, f, v, g, w, O, N) {
    ta = !1, Ba = null, oo.apply(Bl, arguments);
  }
  function vn(n, r, o, f, v, g, w, O, N) {
    if (ls.apply(this, arguments), ta) {
      if (ta) {
        var K = Ba;
        ta = !1, Ba = null;
      } else throw Error(c(198));
      lo || (lo = !0, uo = K);
    }
  }
  function va(n) {
    var r = n, o = n;
    if (n.alternate) for (; r.return; ) r = r.return;
    else {
      n = r;
      do
        r = n, r.flags & 4098 && (o = r.return), n = r.return;
      while (n);
    }
    return r.tag === 3 ? o : null;
  }
  function Zo(n) {
    if (n.tag === 13) {
      var r = n.memoizedState;
      if (r === null && (n = n.alternate, n !== null && (r = n.memoizedState)), r !== null) return r.dehydrated;
    }
    return null;
  }
  function so(n) {
    if (va(n) !== n) throw Error(c(188));
  }
  function Qn(n) {
    var r = n.alternate;
    if (!r) {
      if (r = va(n), r === null) throw Error(c(188));
      return r !== n ? null : n;
    }
    for (var o = n, f = r; ; ) {
      var v = o.return;
      if (v === null) break;
      var g = v.alternate;
      if (g === null) {
        if (f = v.return, f !== null) {
          o = f;
          continue;
        }
        break;
      }
      if (v.child === g.child) {
        for (g = v.child; g; ) {
          if (g === o) return so(v), n;
          if (g === f) return so(v), r;
          g = g.sibling;
        }
        throw Error(c(188));
      }
      if (o.return !== f.return) o = v, f = g;
      else {
        for (var w = !1, O = v.child; O; ) {
          if (O === o) {
            w = !0, o = v, f = g;
            break;
          }
          if (O === f) {
            w = !0, f = v, o = g;
            break;
          }
          O = O.sibling;
        }
        if (!w) {
          for (O = g.child; O; ) {
            if (O === o) {
              w = !0, o = g, f = v;
              break;
            }
            if (O === f) {
              w = !0, f = g, o = v;
              break;
            }
            O = O.sibling;
          }
          if (!w) throw Error(c(189));
        }
      }
      if (o.alternate !== f) throw Error(c(190));
    }
    if (o.tag !== 3) throw Error(c(188));
    return o.stateNode.current === o ? n : r;
  }
  function Ec(n) {
    return n = Qn(n), n !== null ? bc(n) : null;
  }
  function bc(n) {
    if (n.tag === 5 || n.tag === 6) return n;
    for (n = n.child; n !== null; ) {
      var r = bc(n);
      if (r !== null) return r;
      n = n.sibling;
    }
    return null;
  }
  var us = s.unstable_scheduleCallback, Cc = s.unstable_cancelCallback, wd = s.unstable_shouldYield, Td = s.unstable_requestPaint, rn = s.unstable_now, Rd = s.unstable_getCurrentPriorityLevel, Ia = s.unstable_ImmediatePriority, dt = s.unstable_UserBlockingPriority, ii = s.unstable_NormalPriority, co = s.unstable_LowPriority, Il = s.unstable_IdlePriority, fo = null, Nr = null;
  function ss(n) {
    if (Nr && typeof Nr.onCommitFiberRoot == "function") try {
      Nr.onCommitFiberRoot(fo, n, void 0, (n.current.flags & 128) === 128);
    } catch {
    }
  }
  var dr = Math.clz32 ? Math.clz32 : wc, cs = Math.log, fs = Math.LN2;
  function wc(n) {
    return n >>>= 0, n === 0 ? 32 : 31 - (cs(n) / fs | 0) | 0;
  }
  var Yl = 64, po = 4194304;
  function Ya(n) {
    switch (n & -n) {
      case 1:
        return 1;
      case 2:
        return 2;
      case 4:
        return 4;
      case 8:
        return 8;
      case 16:
        return 16;
      case 32:
        return 32;
      case 64:
      case 128:
      case 256:
      case 512:
      case 1024:
      case 2048:
      case 4096:
      case 8192:
      case 16384:
      case 32768:
      case 65536:
      case 131072:
      case 262144:
      case 524288:
      case 1048576:
      case 2097152:
        return n & 4194240;
      case 4194304:
      case 8388608:
      case 16777216:
      case 33554432:
      case 67108864:
        return n & 130023424;
      case 134217728:
        return 134217728;
      case 268435456:
        return 268435456;
      case 536870912:
        return 536870912;
      case 1073741824:
        return 1073741824;
      default:
        return n;
    }
  }
  function pr(n, r) {
    var o = n.pendingLanes;
    if (o === 0) return 0;
    var f = 0, v = n.suspendedLanes, g = n.pingedLanes, w = o & 268435455;
    if (w !== 0) {
      var O = w & ~v;
      O !== 0 ? f = Ya(O) : (g &= w, g !== 0 && (f = Ya(g)));
    } else w = o & ~v, w !== 0 ? f = Ya(w) : g !== 0 && (f = Ya(g));
    if (f === 0) return 0;
    if (r !== 0 && r !== f && !(r & v) && (v = f & -f, g = r & -r, v >= g || v === 16 && (g & 4194240) !== 0)) return r;
    if (f & 4 && (f |= o & 16), r = n.entangledLanes, r !== 0) for (n = n.entanglements, r &= f; 0 < r; ) o = 31 - dr(r), v = 1 << o, f |= n[o], r &= ~v;
    return f;
  }
  function vo(n, r) {
    switch (n) {
      case 1:
      case 2:
      case 4:
        return r + 250;
      case 8:
      case 16:
      case 32:
      case 64:
      case 128:
      case 256:
      case 512:
      case 1024:
      case 2048:
      case 4096:
      case 8192:
      case 16384:
      case 32768:
      case 65536:
      case 131072:
      case 262144:
      case 524288:
      case 1048576:
      case 2097152:
        return r + 5e3;
      case 4194304:
      case 8388608:
      case 16777216:
      case 33554432:
      case 67108864:
        return -1;
      case 134217728:
      case 268435456:
      case 536870912:
      case 1073741824:
        return -1;
      default:
        return -1;
    }
  }
  function ho(n, r) {
    for (var o = n.suspendedLanes, f = n.pingedLanes, v = n.expirationTimes, g = n.pendingLanes; 0 < g; ) {
      var w = 31 - dr(g), O = 1 << w, N = v[w];
      N === -1 ? (!(O & o) || O & f) && (v[w] = vo(O, r)) : N <= r && (n.expiredLanes |= O), g &= ~O;
    }
  }
  function mo(n) {
    return n = n.pendingLanes & -1073741825, n !== 0 ? n : n & 1073741824 ? 1073741824 : 0;
  }
  function Wl() {
    var n = Yl;
    return Yl <<= 1, !(Yl & 4194240) && (Yl = 64), n;
  }
  function Ql(n) {
    for (var r = [], o = 0; 31 > o; o++) r.push(n);
    return r;
  }
  function _i(n, r, o) {
    n.pendingLanes |= r, r !== 536870912 && (n.suspendedLanes = 0, n.pingedLanes = 0), n = n.eventTimes, r = 31 - dr(r), n[r] = o;
  }
  function xd(n, r) {
    var o = n.pendingLanes & ~r;
    n.pendingLanes = r, n.suspendedLanes = 0, n.pingedLanes = 0, n.expiredLanes &= r, n.mutableReadLanes &= r, n.entangledLanes &= r, r = n.entanglements;
    var f = n.eventTimes;
    for (n = n.expirationTimes; 0 < o; ) {
      var v = 31 - dr(o), g = 1 << v;
      r[v] = 0, f[v] = -1, n[v] = -1, o &= ~g;
    }
  }
  function oi(n, r) {
    var o = n.entangledLanes |= r;
    for (n = n.entanglements; o; ) {
      var f = 31 - dr(o), v = 1 << f;
      v & r | n[f] & r && (n[f] |= r), o &= ~v;
    }
  }
  var Nt = 0;
  function Gl(n) {
    return n &= -n, 1 < n ? 4 < n ? n & 268435455 ? 16 : 536870912 : 4 : 1;
  }
  var Jo, ql, Ct, Xl, Kl, it = !1, el = [], cn = null, Ar = null, vr = null, yo = /* @__PURE__ */ new Map(), hn = /* @__PURE__ */ new Map(), Ft = [], Tc = "mousedown mouseup touchcancel touchend touchstart auxclick dblclick pointercancel pointerdown pointerup dragend dragstart drop compositionend compositionstart keydown keypress keyup input textInput copy cut paste click change contextmenu reset submit".split(" ");
  function zr(n, r) {
    switch (n) {
      case "focusin":
      case "focusout":
        cn = null;
        break;
      case "dragenter":
      case "dragleave":
        Ar = null;
        break;
      case "mouseover":
      case "mouseout":
        vr = null;
        break;
      case "pointerover":
      case "pointerout":
        yo.delete(r.pointerId);
        break;
      case "gotpointercapture":
      case "lostpointercapture":
        hn.delete(r.pointerId);
    }
  }
  function Hn(n, r, o, f, v, g) {
    return n === null || n.nativeEvent !== g ? (n = { blockedOn: r, domEventName: o, eventSystemFlags: f, nativeEvent: g, targetContainers: [v] }, r !== null && (r = xs(r), r !== null && ql(r)), n) : (n.eventSystemFlags |= f, r = n.targetContainers, v !== null && r.indexOf(v) === -1 && r.push(v), n);
  }
  function li(n, r, o, f, v) {
    switch (r) {
      case "focusin":
        return cn = Hn(cn, n, r, o, f, v), !0;
      case "dragenter":
        return Ar = Hn(Ar, n, r, o, f, v), !0;
      case "mouseover":
        return vr = Hn(vr, n, r, o, f, v), !0;
      case "pointerover":
        var g = v.pointerId;
        return yo.set(g, Hn(yo.get(g) || null, n, r, o, f, v)), !0;
      case "gotpointercapture":
        return g = v.pointerId, hn.set(g, Hn(hn.get(g) || null, n, r, o, f, v)), !0;
    }
    return !1;
  }
  function Rc(n) {
    var r = ga(n.target);
    if (r !== null) {
      var o = va(r);
      if (o !== null) {
        if (r = o.tag, r === 13) {
          if (r = Zo(o), r !== null) {
            n.blockedOn = r, Kl(n.priority, function() {
              Ct(o);
            });
            return;
          }
        } else if (r === 3 && o.stateNode.current.memoizedState.isDehydrated) {
          n.blockedOn = o.tag === 3 ? o.stateNode.containerInfo : null;
          return;
        }
      }
    }
    n.blockedOn = null;
  }
  function Di(n) {
    if (n.blockedOn !== null) return !1;
    for (var r = n.targetContainers; 0 < r.length; ) {
      var o = Jl(n.domEventName, n.eventSystemFlags, r[0], n.nativeEvent);
      if (o === null) {
        o = n.nativeEvent;
        var f = new o.constructor(o.type, o);
        Lr = f, o.target.dispatchEvent(f), Lr = null;
      } else return r = xs(o), r !== null && ql(r), n.blockedOn = o, !1;
      r.shift();
    }
    return !0;
  }
  function go(n, r, o) {
    Di(n) && o.delete(r);
  }
  function xc() {
    it = !1, cn !== null && Di(cn) && (cn = null), Ar !== null && Di(Ar) && (Ar = null), vr !== null && Di(vr) && (vr = null), yo.forEach(go), hn.forEach(go);
  }
  function ha(n, r) {
    n.blockedOn === r && (n.blockedOn = null, it || (it = !0, s.unstable_scheduleCallback(s.unstable_NormalPriority, xc)));
  }
  function So(n) {
    function r(v) {
      return ha(v, n);
    }
    if (0 < el.length) {
      ha(el[0], n);
      for (var o = 1; o < el.length; o++) {
        var f = el[o];
        f.blockedOn === n && (f.blockedOn = null);
      }
    }
    for (cn !== null && ha(cn, n), Ar !== null && ha(Ar, n), vr !== null && ha(vr, n), yo.forEach(r), hn.forEach(r), o = 0; o < Ft.length; o++) f = Ft[o], f.blockedOn === n && (f.blockedOn = null);
    for (; 0 < Ft.length && (o = Ft[0], o.blockedOn === null); ) Rc(o), o.blockedOn === null && Ft.shift();
  }
  var Oi = oe.ReactCurrentBatchConfig, ma = !0;
  function Zl(n, r, o, f) {
    var v = Nt, g = Oi.transition;
    Oi.transition = null;
    try {
      Nt = 1, bo(n, r, o, f);
    } finally {
      Nt = v, Oi.transition = g;
    }
  }
  function Eo(n, r, o, f) {
    var v = Nt, g = Oi.transition;
    Oi.transition = null;
    try {
      Nt = 4, bo(n, r, o, f);
    } finally {
      Nt = v, Oi.transition = g;
    }
  }
  function bo(n, r, o, f) {
    if (ma) {
      var v = Jl(n, r, o, f);
      if (v === null) zc(n, r, f, tl, o), zr(n, f);
      else if (li(v, n, r, o, f)) f.stopPropagation();
      else if (zr(n, f), r & 4 && -1 < Tc.indexOf(n)) {
        for (; v !== null; ) {
          var g = xs(v);
          if (g !== null && Jo(g), g = Jl(n, r, o, f), g === null && zc(n, r, f, tl, o), g === v) break;
          v = g;
        }
        v !== null && f.stopPropagation();
      } else zc(n, r, f, null, o);
    }
  }
  var tl = null;
  function Jl(n, r, o, f) {
    if (tl = null, n = Zt(f), n = ga(n), n !== null) if (r = va(n), r === null) n = null;
    else if (o = r.tag, o === 13) {
      if (n = Zo(r), n !== null) return n;
      n = null;
    } else if (o === 3) {
      if (r.stateNode.current.memoizedState.isDehydrated) return r.tag === 3 ? r.stateNode.containerInfo : null;
      n = null;
    } else r !== n && (n = null);
    return tl = n, null;
  }
  function ds(n) {
    switch (n) {
      case "cancel":
      case "click":
      case "close":
      case "contextmenu":
      case "copy":
      case "cut":
      case "auxclick":
      case "dblclick":
      case "dragend":
      case "dragstart":
      case "drop":
      case "focusin":
      case "focusout":
      case "input":
      case "invalid":
      case "keydown":
      case "keypress":
      case "keyup":
      case "mousedown":
      case "mouseup":
      case "paste":
      case "pause":
      case "play":
      case "pointercancel":
      case "pointerdown":
      case "pointerup":
      case "ratechange":
      case "reset":
      case "resize":
      case "seeked":
      case "submit":
      case "touchcancel":
      case "touchend":
      case "touchstart":
      case "volumechange":
      case "change":
      case "selectionchange":
      case "textInput":
      case "compositionstart":
      case "compositionend":
      case "compositionupdate":
      case "beforeblur":
      case "afterblur":
      case "beforeinput":
      case "blur":
      case "fullscreenchange":
      case "focus":
      case "hashchange":
      case "popstate":
      case "select":
      case "selectstart":
        return 1;
      case "drag":
      case "dragenter":
      case "dragexit":
      case "dragleave":
      case "dragover":
      case "mousemove":
      case "mouseout":
      case "mouseover":
      case "pointermove":
      case "pointerout":
      case "pointerover":
      case "scroll":
      case "toggle":
      case "touchmove":
      case "wheel":
      case "mouseenter":
      case "mouseleave":
      case "pointerenter":
      case "pointerleave":
        return 4;
      case "message":
        switch (Rd()) {
          case Ia:
            return 1;
          case dt:
            return 4;
          case ii:
          case co:
            return 16;
          case Il:
            return 536870912;
          default:
            return 16;
        }
      default:
        return 16;
    }
  }
  var Wa = null, C = null, M = null;
  function X() {
    if (M) return M;
    var n, r = C, o = r.length, f, v = "value" in Wa ? Wa.value : Wa.textContent, g = v.length;
    for (n = 0; n < o && r[n] === v[n]; n++) ;
    var w = o - n;
    for (f = 1; f <= w && r[o - f] === v[g - f]; f++) ;
    return M = v.slice(n, 1 < f ? 1 - f : void 0);
  }
  function J(n) {
    var r = n.keyCode;
    return "charCode" in n ? (n = n.charCode, n === 0 && r === 13 && (n = 13)) : n = r, n === 10 && (n = 13), 32 <= n || n === 13 ? n : 0;
  }
  function de() {
    return !0;
  }
  function Xe() {
    return !1;
  }
  function be(n) {
    function r(o, f, v, g, w) {
      this._reactName = o, this._targetInst = v, this.type = f, this.nativeEvent = g, this.target = w, this.currentTarget = null;
      for (var O in n) n.hasOwnProperty(O) && (o = n[O], this[O] = o ? o(g) : g[O]);
      return this.isDefaultPrevented = (g.defaultPrevented != null ? g.defaultPrevented : g.returnValue === !1) ? de : Xe, this.isPropagationStopped = Xe, this;
    }
    return Te(r.prototype, { preventDefault: function() {
      this.defaultPrevented = !0;
      var o = this.nativeEvent;
      o && (o.preventDefault ? o.preventDefault() : typeof o.returnValue != "unknown" && (o.returnValue = !1), this.isDefaultPrevented = de);
    }, stopPropagation: function() {
      var o = this.nativeEvent;
      o && (o.stopPropagation ? o.stopPropagation() : typeof o.cancelBubble != "unknown" && (o.cancelBubble = !0), this.isPropagationStopped = de);
    }, persist: function() {
    }, isPersistent: de }), r;
  }
  var Ge = { eventPhase: 0, bubbles: 0, cancelable: 0, timeStamp: function(n) {
    return n.timeStamp || Date.now();
  }, defaultPrevented: 0, isTrusted: 0 }, pt = be(Ge), Dt = Te({}, Ge, { view: 0, detail: 0 }), Wt = be(Dt), $t, Qt, Xt, wt = Te({}, Dt, { screenX: 0, screenY: 0, clientX: 0, clientY: 0, pageX: 0, pageY: 0, ctrlKey: 0, shiftKey: 0, altKey: 0, metaKey: 0, getModifierState: Md, button: 0, buttons: 0, relatedTarget: function(n) {
    return n.relatedTarget === void 0 ? n.fromElement === n.srcElement ? n.toElement : n.fromElement : n.relatedTarget;
  }, movementX: function(n) {
    return "movementX" in n ? n.movementX : (n !== Xt && (Xt && n.type === "mousemove" ? ($t = n.screenX - Xt.screenX, Qt = n.screenY - Xt.screenY) : Qt = $t = 0, Xt = n), $t);
  }, movementY: function(n) {
    return "movementY" in n ? n.movementY : Qt;
  } }), Mi = be(wt), eu = Te({}, wt, { dataTransfer: 0 }), ps = be(eu), kd = Te({}, Dt, { relatedTarget: 0 }), Qa = be(kd), vs = Te({}, Ge, { animationName: 0, elapsedTime: 0, pseudoElement: 0 }), hs = be(vs), _d = Te({}, Ge, { clipboardData: function(n) {
    return "clipboardData" in n ? n.clipboardData : window.clipboardData;
  } }), Ky = be(_d), Zy = Te({}, Ge, { data: 0 }), Dd = be(Zy), Od = {
    Esc: "Escape",
    Spacebar: " ",
    Left: "ArrowLeft",
    Up: "ArrowUp",
    Right: "ArrowRight",
    Down: "ArrowDown",
    Del: "Delete",
    Win: "OS",
    Menu: "ContextMenu",
    Apps: "ContextMenu",
    Scroll: "ScrollLock",
    MozPrintableKey: "Unidentified"
  }, jv = {
    8: "Backspace",
    9: "Tab",
    12: "Clear",
    13: "Enter",
    16: "Shift",
    17: "Control",
    18: "Alt",
    19: "Pause",
    20: "CapsLock",
    27: "Escape",
    32: " ",
    33: "PageUp",
    34: "PageDown",
    35: "End",
    36: "Home",
    37: "ArrowLeft",
    38: "ArrowUp",
    39: "ArrowRight",
    40: "ArrowDown",
    45: "Insert",
    46: "Delete",
    112: "F1",
    113: "F2",
    114: "F3",
    115: "F4",
    116: "F5",
    117: "F6",
    118: "F7",
    119: "F8",
    120: "F9",
    121: "F10",
    122: "F11",
    123: "F12",
    144: "NumLock",
    145: "ScrollLock",
    224: "Meta"
  }, Vv = { Alt: "altKey", Control: "ctrlKey", Meta: "metaKey", Shift: "shiftKey" };
  function $v(n) {
    var r = this.nativeEvent;
    return r.getModifierState ? r.getModifierState(n) : (n = Vv[n]) ? !!r[n] : !1;
  }
  function Md() {
    return $v;
  }
  var Li = Te({}, Dt, { key: function(n) {
    if (n.key) {
      var r = Od[n.key] || n.key;
      if (r !== "Unidentified") return r;
    }
    return n.type === "keypress" ? (n = J(n), n === 13 ? "Enter" : String.fromCharCode(n)) : n.type === "keydown" || n.type === "keyup" ? jv[n.keyCode] || "Unidentified" : "";
  }, code: 0, location: 0, ctrlKey: 0, shiftKey: 0, altKey: 0, metaKey: 0, repeat: 0, locale: 0, getModifierState: Md, charCode: function(n) {
    return n.type === "keypress" ? J(n) : 0;
  }, keyCode: function(n) {
    return n.type === "keydown" || n.type === "keyup" ? n.keyCode : 0;
  }, which: function(n) {
    return n.type === "keypress" ? J(n) : n.type === "keydown" || n.type === "keyup" ? n.keyCode : 0;
  } }), Jy = be(Li), Ld = Te({}, wt, { pointerId: 0, width: 0, height: 0, pressure: 0, tangentialPressure: 0, tiltX: 0, tiltY: 0, twist: 0, pointerType: 0, isPrimary: 0 }), kc = be(Ld), Nd = Te({}, Dt, { touches: 0, targetTouches: 0, changedTouches: 0, altKey: 0, metaKey: 0, ctrlKey: 0, shiftKey: 0, getModifierState: Md }), eg = be(Nd), _c = Te({}, Ge, { propertyName: 0, elapsedTime: 0, pseudoElement: 0 }), Bv = be(_c), Ur = Te({}, wt, {
    deltaX: function(n) {
      return "deltaX" in n ? n.deltaX : "wheelDeltaX" in n ? -n.wheelDeltaX : 0;
    },
    deltaY: function(n) {
      return "deltaY" in n ? n.deltaY : "wheelDeltaY" in n ? -n.wheelDeltaY : "wheelDelta" in n ? -n.wheelDelta : 0;
    },
    deltaZ: 0,
    deltaMode: 0
  }), Ni = be(Ur), kn = [9, 13, 27, 32], Ga = x && "CompositionEvent" in window, nl = null;
  x && "documentMode" in document && (nl = document.documentMode);
  var Dc = x && "TextEvent" in window && !nl, Iv = x && (!Ga || nl && 8 < nl && 11 >= nl), tu = " ", Yv = !1;
  function Wv(n, r) {
    switch (n) {
      case "keyup":
        return kn.indexOf(r.keyCode) !== -1;
      case "keydown":
        return r.keyCode !== 229;
      case "keypress":
      case "mousedown":
      case "focusout":
        return !0;
      default:
        return !1;
    }
  }
  function Oc(n) {
    return n = n.detail, typeof n == "object" && "data" in n ? n.data : null;
  }
  var nu = !1;
  function tg(n, r) {
    switch (n) {
      case "compositionend":
        return Oc(r);
      case "keypress":
        return r.which !== 32 ? null : (Yv = !0, tu);
      case "textInput":
        return n = r.data, n === tu && Yv ? null : n;
      default:
        return null;
    }
  }
  function ng(n, r) {
    if (nu) return n === "compositionend" || !Ga && Wv(n, r) ? (n = X(), M = C = Wa = null, nu = !1, n) : null;
    switch (n) {
      case "paste":
        return null;
      case "keypress":
        if (!(r.ctrlKey || r.altKey || r.metaKey) || r.ctrlKey && r.altKey) {
          if (r.char && 1 < r.char.length) return r.char;
          if (r.which) return String.fromCharCode(r.which);
        }
        return null;
      case "compositionend":
        return Iv && r.locale !== "ko" ? null : r.data;
      default:
        return null;
    }
  }
  var Qv = { color: !0, date: !0, datetime: !0, "datetime-local": !0, email: !0, month: !0, number: !0, password: !0, range: !0, search: !0, tel: !0, text: !0, time: !0, url: !0, week: !0 };
  function Gv(n) {
    var r = n && n.nodeName && n.nodeName.toLowerCase();
    return r === "input" ? !!Qv[n.type] : r === "textarea";
  }
  function qv(n, r, o, f) {
    ki(f), r = ws(r, "onChange"), 0 < r.length && (o = new pt("onChange", "change", null, o, f), n.push({ event: o, listeners: r }));
  }
  var ms = null, ru = null;
  function au(n) {
    Ac(n, 0);
  }
  function iu(n) {
    var r = lu(n);
    if (Fa(r)) return n;
  }
  function Xv(n, r) {
    if (n === "change") return r;
  }
  var Ad = !1;
  if (x) {
    var zd;
    if (x) {
      var Ud = "oninput" in document;
      if (!Ud) {
        var Kv = document.createElement("div");
        Kv.setAttribute("oninput", "return;"), Ud = typeof Kv.oninput == "function";
      }
      zd = Ud;
    } else zd = !1;
    Ad = zd && (!document.documentMode || 9 < document.documentMode);
  }
  function Zv() {
    ms && (ms.detachEvent("onpropertychange", Jv), ru = ms = null);
  }
  function Jv(n) {
    if (n.propertyName === "value" && iu(ru)) {
      var r = [];
      qv(r, ru, n, Zt(n)), os(au, r);
    }
  }
  function rg(n, r, o) {
    n === "focusin" ? (Zv(), ms = r, ru = o, ms.attachEvent("onpropertychange", Jv)) : n === "focusout" && Zv();
  }
  function ag(n) {
    if (n === "selectionchange" || n === "keyup" || n === "keydown") return iu(ru);
  }
  function ig(n, r) {
    if (n === "click") return iu(r);
  }
  function eh(n, r) {
    if (n === "input" || n === "change") return iu(r);
  }
  function og(n, r) {
    return n === r && (n !== 0 || 1 / n === 1 / r) || n !== n && r !== r;
  }
  var ya = typeof Object.is == "function" ? Object.is : og;
  function ys(n, r) {
    if (ya(n, r)) return !0;
    if (typeof n != "object" || n === null || typeof r != "object" || r === null) return !1;
    var o = Object.keys(n), f = Object.keys(r);
    if (o.length !== f.length) return !1;
    for (f = 0; f < o.length; f++) {
      var v = o[f];
      if (!T.call(r, v) || !ya(n[v], r[v])) return !1;
    }
    return !0;
  }
  function th(n) {
    for (; n && n.firstChild; ) n = n.firstChild;
    return n;
  }
  function nh(n, r) {
    var o = th(n);
    n = 0;
    for (var f; o; ) {
      if (o.nodeType === 3) {
        if (f = n + o.textContent.length, n <= r && f >= r) return { node: o, offset: r - n };
        n = f;
      }
      e: {
        for (; o; ) {
          if (o.nextSibling) {
            o = o.nextSibling;
            break e;
          }
          o = o.parentNode;
        }
        o = void 0;
      }
      o = th(o);
    }
  }
  function rh(n, r) {
    return n && r ? n === r ? !0 : n && n.nodeType === 3 ? !1 : r && r.nodeType === 3 ? rh(n, r.parentNode) : "contains" in n ? n.contains(r) : n.compareDocumentPosition ? !!(n.compareDocumentPosition(r) & 16) : !1 : !1;
  }
  function Mc() {
    for (var n = window, r = bn(); r instanceof n.HTMLIFrameElement; ) {
      try {
        var o = typeof r.contentWindow.location.href == "string";
      } catch {
        o = !1;
      }
      if (o) n = r.contentWindow;
      else break;
      r = bn(n.document);
    }
    return r;
  }
  function Ai(n) {
    var r = n && n.nodeName && n.nodeName.toLowerCase();
    return r && (r === "input" && (n.type === "text" || n.type === "search" || n.type === "tel" || n.type === "url" || n.type === "password") || r === "textarea" || n.contentEditable === "true");
  }
  function Lc(n) {
    var r = Mc(), o = n.focusedElem, f = n.selectionRange;
    if (r !== o && o && o.ownerDocument && rh(o.ownerDocument.documentElement, o)) {
      if (f !== null && Ai(o)) {
        if (r = f.start, n = f.end, n === void 0 && (n = r), "selectionStart" in o) o.selectionStart = r, o.selectionEnd = Math.min(n, o.value.length);
        else if (n = (r = o.ownerDocument || document) && r.defaultView || window, n.getSelection) {
          n = n.getSelection();
          var v = o.textContent.length, g = Math.min(f.start, v);
          f = f.end === void 0 ? g : Math.min(f.end, v), !n.extend && g > f && (v = f, f = g, g = v), v = nh(o, g);
          var w = nh(
            o,
            f
          );
          v && w && (n.rangeCount !== 1 || n.anchorNode !== v.node || n.anchorOffset !== v.offset || n.focusNode !== w.node || n.focusOffset !== w.offset) && (r = r.createRange(), r.setStart(v.node, v.offset), n.removeAllRanges(), g > f ? (n.addRange(r), n.extend(w.node, w.offset)) : (r.setEnd(w.node, w.offset), n.addRange(r)));
        }
      }
      for (r = [], n = o; n = n.parentNode; ) n.nodeType === 1 && r.push({ element: n, left: n.scrollLeft, top: n.scrollTop });
      for (typeof o.focus == "function" && o.focus(), o = 0; o < r.length; o++) n = r[o], n.element.scrollLeft = n.left, n.element.scrollTop = n.top;
    }
  }
  var ah = x && "documentMode" in document && 11 >= document.documentMode, qa = null, Pd = null, gs = null, Fd = !1;
  function ih(n, r, o) {
    var f = o.window === o ? o.document : o.nodeType === 9 ? o : o.ownerDocument;
    Fd || qa == null || qa !== bn(f) || (f = qa, "selectionStart" in f && Ai(f) ? f = { start: f.selectionStart, end: f.selectionEnd } : (f = (f.ownerDocument && f.ownerDocument.defaultView || window).getSelection(), f = { anchorNode: f.anchorNode, anchorOffset: f.anchorOffset, focusNode: f.focusNode, focusOffset: f.focusOffset }), gs && ys(gs, f) || (gs = f, f = ws(Pd, "onSelect"), 0 < f.length && (r = new pt("onSelect", "select", null, r, o), n.push({ event: r, listeners: f }), r.target = qa)));
  }
  function Nc(n, r) {
    var o = {};
    return o[n.toLowerCase()] = r.toLowerCase(), o["Webkit" + n] = "webkit" + r, o["Moz" + n] = "moz" + r, o;
  }
  var rl = { animationend: Nc("Animation", "AnimationEnd"), animationiteration: Nc("Animation", "AnimationIteration"), animationstart: Nc("Animation", "AnimationStart"), transitionend: Nc("Transition", "TransitionEnd") }, Hd = {}, jd = {};
  x && (jd = document.createElement("div").style, "AnimationEvent" in window || (delete rl.animationend.animation, delete rl.animationiteration.animation, delete rl.animationstart.animation), "TransitionEvent" in window || delete rl.transitionend.transition);
  function jn(n) {
    if (Hd[n]) return Hd[n];
    if (!rl[n]) return n;
    var r = rl[n], o;
    for (o in r) if (r.hasOwnProperty(o) && o in jd) return Hd[n] = r[o];
    return n;
  }
  var Vd = jn("animationend"), oh = jn("animationiteration"), lh = jn("animationstart"), uh = jn("transitionend"), sh = /* @__PURE__ */ new Map(), ch = "abort auxClick cancel canPlay canPlayThrough click close contextMenu copy cut drag dragEnd dragEnter dragExit dragLeave dragOver dragStart drop durationChange emptied encrypted ended error gotPointerCapture input invalid keyDown keyPress keyUp load loadedData loadedMetadata loadStart lostPointerCapture mouseDown mouseMove mouseOut mouseOver mouseUp paste pause play playing pointerCancel pointerDown pointerMove pointerOut pointerOver pointerUp progress rateChange reset resize seeked seeking stalled submit suspend timeUpdate touchCancel touchEnd touchStart volumeChange scroll toggle touchMove waiting wheel".split(" ");
  function zi(n, r) {
    sh.set(n, r), E(r, [n]);
  }
  for (var Ss = 0; Ss < ch.length; Ss++) {
    var al = ch[Ss], lg = al.toLowerCase(), Es = al[0].toUpperCase() + al.slice(1);
    zi(lg, "on" + Es);
  }
  zi(Vd, "onAnimationEnd"), zi(oh, "onAnimationIteration"), zi(lh, "onAnimationStart"), zi("dblclick", "onDoubleClick"), zi("focusin", "onFocus"), zi("focusout", "onBlur"), zi(uh, "onTransitionEnd"), m("onMouseEnter", ["mouseout", "mouseover"]), m("onMouseLeave", ["mouseout", "mouseover"]), m("onPointerEnter", ["pointerout", "pointerover"]), m("onPointerLeave", ["pointerout", "pointerover"]), E("onChange", "change click focusin focusout input keydown keyup selectionchange".split(" ")), E("onSelect", "focusout contextmenu dragend focusin keydown keyup mousedown mouseup selectionchange".split(" ")), E("onBeforeInput", ["compositionend", "keypress", "textInput", "paste"]), E("onCompositionEnd", "compositionend focusout keydown keypress keyup mousedown".split(" ")), E("onCompositionStart", "compositionstart focusout keydown keypress keyup mousedown".split(" ")), E("onCompositionUpdate", "compositionupdate focusout keydown keypress keyup mousedown".split(" "));
  var bs = "abort canplay canplaythrough durationchange emptied encrypted ended error loadeddata loadedmetadata loadstart pause play playing progress ratechange resize seeked seeking stalled suspend timeupdate volumechange waiting".split(" "), ug = new Set("cancel close invalid load scroll toggle".split(" ").concat(bs));
  function fh(n, r, o) {
    var f = n.type || "unknown-event";
    n.currentTarget = o, vn(f, r, void 0, n), n.currentTarget = null;
  }
  function Ac(n, r) {
    r = (r & 4) !== 0;
    for (var o = 0; o < n.length; o++) {
      var f = n[o], v = f.event;
      f = f.listeners;
      e: {
        var g = void 0;
        if (r) for (var w = f.length - 1; 0 <= w; w--) {
          var O = f[w], N = O.instance, K = O.currentTarget;
          if (O = O.listener, N !== g && v.isPropagationStopped()) break e;
          fh(v, O, K), g = N;
        }
        else for (w = 0; w < f.length; w++) {
          if (O = f[w], N = O.instance, K = O.currentTarget, O = O.listener, N !== g && v.isPropagationStopped()) break e;
          fh(v, O, K), g = N;
        }
      }
    }
    if (lo) throw n = uo, lo = !1, uo = null, n;
  }
  function Gt(n, r) {
    var o = r[Gd];
    o === void 0 && (o = r[Gd] = /* @__PURE__ */ new Set());
    var f = n + "__bubble";
    o.has(f) || (dh(r, n, 2, !1), o.add(f));
  }
  function Co(n, r, o) {
    var f = 0;
    r && (f |= 4), dh(o, n, f, r);
  }
  var Ui = "_reactListening" + Math.random().toString(36).slice(2);
  function ou(n) {
    if (!n[Ui]) {
      n[Ui] = !0, p.forEach(function(o) {
        o !== "selectionchange" && (ug.has(o) || Co(o, !1, n), Co(o, !0, n));
      });
      var r = n.nodeType === 9 ? n : n.ownerDocument;
      r === null || r[Ui] || (r[Ui] = !0, Co("selectionchange", !1, r));
    }
  }
  function dh(n, r, o, f) {
    switch (ds(r)) {
      case 1:
        var v = Zl;
        break;
      case 4:
        v = Eo;
        break;
      default:
        v = bo;
    }
    o = v.bind(null, r, o, n), v = void 0, !io || r !== "touchstart" && r !== "touchmove" && r !== "wheel" || (v = !0), f ? v !== void 0 ? n.addEventListener(r, o, { capture: !0, passive: v }) : n.addEventListener(r, o, !0) : v !== void 0 ? n.addEventListener(r, o, { passive: v }) : n.addEventListener(r, o, !1);
  }
  function zc(n, r, o, f, v) {
    var g = f;
    if (!(r & 1) && !(r & 2) && f !== null) e: for (; ; ) {
      if (f === null) return;
      var w = f.tag;
      if (w === 3 || w === 4) {
        var O = f.stateNode.containerInfo;
        if (O === v || O.nodeType === 8 && O.parentNode === v) break;
        if (w === 4) for (w = f.return; w !== null; ) {
          var N = w.tag;
          if ((N === 3 || N === 4) && (N = w.stateNode.containerInfo, N === v || N.nodeType === 8 && N.parentNode === v)) return;
          w = w.return;
        }
        for (; O !== null; ) {
          if (w = ga(O), w === null) return;
          if (N = w.tag, N === 5 || N === 6) {
            f = g = w;
            continue e;
          }
          O = O.parentNode;
        }
      }
      f = f.return;
    }
    os(function() {
      var K = g, ue = Zt(o), se = [];
      e: {
        var le = sh.get(n);
        if (le !== void 0) {
          var xe = pt, Le = n;
          switch (n) {
            case "keypress":
              if (J(o) === 0) break e;
            case "keydown":
            case "keyup":
              xe = Jy;
              break;
            case "focusin":
              Le = "focus", xe = Qa;
              break;
            case "focusout":
              Le = "blur", xe = Qa;
              break;
            case "beforeblur":
            case "afterblur":
              xe = Qa;
              break;
            case "click":
              if (o.button === 2) break e;
            case "auxclick":
            case "dblclick":
            case "mousedown":
            case "mousemove":
            case "mouseup":
            case "mouseout":
            case "mouseover":
            case "contextmenu":
              xe = Mi;
              break;
            case "drag":
            case "dragend":
            case "dragenter":
            case "dragexit":
            case "dragleave":
            case "dragover":
            case "dragstart":
            case "drop":
              xe = ps;
              break;
            case "touchcancel":
            case "touchend":
            case "touchmove":
            case "touchstart":
              xe = eg;
              break;
            case Vd:
            case oh:
            case lh:
              xe = hs;
              break;
            case uh:
              xe = Bv;
              break;
            case "scroll":
              xe = Wt;
              break;
            case "wheel":
              xe = Ni;
              break;
            case "copy":
            case "cut":
            case "paste":
              xe = Ky;
              break;
            case "gotpointercapture":
            case "lostpointercapture":
            case "pointercancel":
            case "pointerdown":
            case "pointermove":
            case "pointerout":
            case "pointerover":
            case "pointerup":
              xe = kc;
          }
          var ze = (r & 4) !== 0, Rn = !ze && n === "scroll", B = ze ? le !== null ? le + "Capture" : null : le;
          ze = [];
          for (var U = K, Q; U !== null; ) {
            Q = U;
            var pe = Q.stateNode;
            if (Q.tag === 5 && pe !== null && (Q = pe, B !== null && (pe = $a(U, B), pe != null && ze.push(Cs(U, pe, Q)))), Rn) break;
            U = U.return;
          }
          0 < ze.length && (le = new xe(le, Le, null, o, ue), se.push({ event: le, listeners: ze }));
        }
      }
      if (!(r & 7)) {
        e: {
          if (le = n === "mouseover" || n === "pointerover", xe = n === "mouseout" || n === "pointerout", le && o !== Lr && (Le = o.relatedTarget || o.fromElement) && (ga(Le) || Le[Pi])) break e;
          if ((xe || le) && (le = ue.window === ue ? ue : (le = ue.ownerDocument) ? le.defaultView || le.parentWindow : window, xe ? (Le = o.relatedTarget || o.toElement, xe = K, Le = Le ? ga(Le) : null, Le !== null && (Rn = va(Le), Le !== Rn || Le.tag !== 5 && Le.tag !== 6) && (Le = null)) : (xe = null, Le = K), xe !== Le)) {
            if (ze = Mi, pe = "onMouseLeave", B = "onMouseEnter", U = "mouse", (n === "pointerout" || n === "pointerover") && (ze = kc, pe = "onPointerLeave", B = "onPointerEnter", U = "pointer"), Rn = xe == null ? le : lu(xe), Q = Le == null ? le : lu(Le), le = new ze(pe, U + "leave", xe, o, ue), le.target = Rn, le.relatedTarget = Q, pe = null, ga(ue) === K && (ze = new ze(B, U + "enter", Le, o, ue), ze.target = Q, ze.relatedTarget = Rn, pe = ze), Rn = pe, xe && Le) t: {
              for (ze = xe, B = Le, U = 0, Q = ze; Q; Q = il(Q)) U++;
              for (Q = 0, pe = B; pe; pe = il(pe)) Q++;
              for (; 0 < U - Q; ) ze = il(ze), U--;
              for (; 0 < Q - U; ) B = il(B), Q--;
              for (; U--; ) {
                if (ze === B || B !== null && ze === B.alternate) break t;
                ze = il(ze), B = il(B);
              }
              ze = null;
            }
            else ze = null;
            xe !== null && $d(se, le, xe, ze, !1), Le !== null && Rn !== null && $d(se, Rn, Le, ze, !0);
          }
        }
        e: {
          if (le = K ? lu(K) : window, xe = le.nodeName && le.nodeName.toLowerCase(), xe === "select" || xe === "input" && le.type === "file") var He = Xv;
          else if (Gv(le)) if (Ad) He = eh;
          else {
            He = ag;
            var Ke = rg;
          }
          else (xe = le.nodeName) && xe.toLowerCase() === "input" && (le.type === "checkbox" || le.type === "radio") && (He = ig);
          if (He && (He = He(n, K))) {
            qv(se, He, o, ue);
            break e;
          }
          Ke && Ke(n, le, K), n === "focusout" && (Ke = le._wrapperState) && Ke.controlled && le.type === "number" && xi(le, "number", le.value);
        }
        switch (Ke = K ? lu(K) : window, n) {
          case "focusin":
            (Gv(Ke) || Ke.contentEditable === "true") && (qa = Ke, Pd = K, gs = null);
            break;
          case "focusout":
            gs = Pd = qa = null;
            break;
          case "mousedown":
            Fd = !0;
            break;
          case "contextmenu":
          case "mouseup":
          case "dragend":
            Fd = !1, ih(se, o, ue);
            break;
          case "selectionchange":
            if (ah) break;
          case "keydown":
          case "keyup":
            ih(se, o, ue);
        }
        var Ne;
        if (Ga) e: {
          switch (n) {
            case "compositionstart":
              var Ze = "onCompositionStart";
              break e;
            case "compositionend":
              Ze = "onCompositionEnd";
              break e;
            case "compositionupdate":
              Ze = "onCompositionUpdate";
              break e;
          }
          Ze = void 0;
        }
        else nu ? Wv(n, o) && (Ze = "onCompositionEnd") : n === "keydown" && o.keyCode === 229 && (Ze = "onCompositionStart");
        Ze && (Iv && o.locale !== "ko" && (nu || Ze !== "onCompositionStart" ? Ze === "onCompositionEnd" && nu && (Ne = X()) : (Wa = ue, C = "value" in Wa ? Wa.value : Wa.textContent, nu = !0)), Ke = ws(K, Ze), 0 < Ke.length && (Ze = new Dd(Ze, n, null, o, ue), se.push({ event: Ze, listeners: Ke }), Ne ? Ze.data = Ne : (Ne = Oc(o), Ne !== null && (Ze.data = Ne)))), (Ne = Dc ? tg(n, o) : ng(n, o)) && (K = ws(K, "onBeforeInput"), 0 < K.length && (ue = new Dd("onBeforeInput", "beforeinput", null, o, ue), se.push({ event: ue, listeners: K }), ue.data = Ne));
      }
      Ac(se, r);
    });
  }
  function Cs(n, r, o) {
    return { instance: n, listener: r, currentTarget: o };
  }
  function ws(n, r) {
    for (var o = r + "Capture", f = []; n !== null; ) {
      var v = n, g = v.stateNode;
      v.tag === 5 && g !== null && (v = g, g = $a(n, o), g != null && f.unshift(Cs(n, g, v)), g = $a(n, r), g != null && f.push(Cs(n, g, v))), n = n.return;
    }
    return f;
  }
  function il(n) {
    if (n === null) return null;
    do
      n = n.return;
    while (n && n.tag !== 5);
    return n || null;
  }
  function $d(n, r, o, f, v) {
    for (var g = r._reactName, w = []; o !== null && o !== f; ) {
      var O = o, N = O.alternate, K = O.stateNode;
      if (N !== null && N === f) break;
      O.tag === 5 && K !== null && (O = K, v ? (N = $a(o, g), N != null && w.unshift(Cs(o, N, O))) : v || (N = $a(o, g), N != null && w.push(Cs(o, N, O)))), o = o.return;
    }
    w.length !== 0 && n.push({ event: r, listeners: w });
  }
  var Bd = /\r\n?/g, sg = /\u0000|\uFFFD/g;
  function Id(n) {
    return (typeof n == "string" ? n : "" + n).replace(Bd, `
`).replace(sg, "");
  }
  function Uc(n, r, o) {
    if (r = Id(r), Id(n) !== r && o) throw Error(c(425));
  }
  function Pc() {
  }
  var Yd = null, ol = null;
  function Ts(n, r) {
    return n === "textarea" || n === "noscript" || typeof r.children == "string" || typeof r.children == "number" || typeof r.dangerouslySetInnerHTML == "object" && r.dangerouslySetInnerHTML !== null && r.dangerouslySetInnerHTML.__html != null;
  }
  var ll = typeof setTimeout == "function" ? setTimeout : void 0, ph = typeof clearTimeout == "function" ? clearTimeout : void 0, Wd = typeof Promise == "function" ? Promise : void 0, Qd = typeof queueMicrotask == "function" ? queueMicrotask : typeof Wd < "u" ? function(n) {
    return Wd.resolve(null).then(n).catch(cg);
  } : ll;
  function cg(n) {
    setTimeout(function() {
      throw n;
    });
  }
  function wo(n, r) {
    var o = r, f = 0;
    do {
      var v = o.nextSibling;
      if (n.removeChild(o), v && v.nodeType === 8) if (o = v.data, o === "/$") {
        if (f === 0) {
          n.removeChild(v), So(r);
          return;
        }
        f--;
      } else o !== "$" && o !== "$?" && o !== "$!" || f++;
      o = v;
    } while (o);
    So(r);
  }
  function Xa(n) {
    for (; n != null; n = n.nextSibling) {
      var r = n.nodeType;
      if (r === 1 || r === 3) break;
      if (r === 8) {
        if (r = n.data, r === "$" || r === "$!" || r === "$?") break;
        if (r === "/$") return null;
      }
    }
    return n;
  }
  function Rs(n) {
    n = n.previousSibling;
    for (var r = 0; n; ) {
      if (n.nodeType === 8) {
        var o = n.data;
        if (o === "$" || o === "$!" || o === "$?") {
          if (r === 0) return n;
          r--;
        } else o === "/$" && r++;
      }
      n = n.previousSibling;
    }
    return null;
  }
  var To = Math.random().toString(36).slice(2), ui = "__reactFiber$" + To, ul = "__reactProps$" + To, Pi = "__reactContainer$" + To, Gd = "__reactEvents$" + To, fg = "__reactListeners$" + To, qd = "__reactHandles$" + To;
  function ga(n) {
    var r = n[ui];
    if (r) return r;
    for (var o = n.parentNode; o; ) {
      if (r = o[Pi] || o[ui]) {
        if (o = r.alternate, r.child !== null || o !== null && o.child !== null) for (n = Rs(n); n !== null; ) {
          if (o = n[ui]) return o;
          n = Rs(n);
        }
        return r;
      }
      n = o, o = n.parentNode;
    }
    return null;
  }
  function xs(n) {
    return n = n[ui] || n[Pi], !n || n.tag !== 5 && n.tag !== 6 && n.tag !== 13 && n.tag !== 3 ? null : n;
  }
  function lu(n) {
    if (n.tag === 5 || n.tag === 6) return n.stateNode;
    throw Error(c(33));
  }
  function Ye(n) {
    return n[ul] || null;
  }
  var Ro = [], Jt = -1;
  function st(n) {
    return { current: n };
  }
  function Pt(n) {
    0 > Jt || (n.current = Ro[Jt], Ro[Jt] = null, Jt--);
  }
  function Ht(n, r) {
    Jt++, Ro[Jt] = n.current, n.current = r;
  }
  var si = {}, tt = st(si), mn = st(!1), Pr = si;
  function Sa(n, r) {
    var o = n.type.contextTypes;
    if (!o) return si;
    var f = n.stateNode;
    if (f && f.__reactInternalMemoizedUnmaskedChildContext === r) return f.__reactInternalMemoizedMaskedChildContext;
    var v = {}, g;
    for (g in o) v[g] = r[g];
    return f && (n = n.stateNode, n.__reactInternalMemoizedUnmaskedChildContext = r, n.__reactInternalMemoizedMaskedChildContext = v), v;
  }
  function an(n) {
    return n = n.childContextTypes, n != null;
  }
  function Ea() {
    Pt(mn), Pt(tt);
  }
  function xo(n, r, o) {
    if (tt.current !== si) throw Error(c(168));
    Ht(tt, r), Ht(mn, o);
  }
  function ks(n, r, o) {
    var f = n.stateNode;
    if (r = r.childContextTypes, typeof f.getChildContext != "function") return o;
    f = f.getChildContext();
    for (var v in f) if (!(v in r)) throw Error(c(108, Lt(n) || "Unknown", v));
    return Te({}, o, f);
  }
  function Fc(n) {
    return n = (n = n.stateNode) && n.__reactInternalMemoizedMergedChildContext || si, Pr = tt.current, Ht(tt, n), Ht(mn, mn.current), !0;
  }
  function vh(n, r, o) {
    var f = n.stateNode;
    if (!f) throw Error(c(169));
    o ? (n = ks(n, r, Pr), f.__reactInternalMemoizedMergedChildContext = n, Pt(mn), Pt(tt), Ht(tt, n)) : Pt(mn), Ht(mn, o);
  }
  var na = null, Vn = !1, _s = !1;
  function Xd(n) {
    na === null ? na = [n] : na.push(n);
  }
  function Kd(n) {
    Vn = !0, Xd(n);
  }
  function Fr() {
    if (!_s && na !== null) {
      _s = !0;
      var n = 0, r = Nt;
      try {
        var o = na;
        for (Nt = 1; n < o.length; n++) {
          var f = o[n];
          do
            f = f(!0);
          while (f !== null);
        }
        na = null, Vn = !1;
      } catch (v) {
        throw na !== null && (na = na.slice(n + 1)), us(Ia, Fr), v;
      } finally {
        Nt = r, _s = !1;
      }
    }
    return null;
  }
  var ko = [], Hr = 0, sl = null, uu = 0, jr = [], lr = 0, ba = null, Gn = 1, Fi = "";
  function ra(n, r) {
    ko[Hr++] = uu, ko[Hr++] = sl, sl = n, uu = r;
  }
  function Zd(n, r, o) {
    jr[lr++] = Gn, jr[lr++] = Fi, jr[lr++] = ba, ba = n;
    var f = Gn;
    n = Fi;
    var v = 32 - dr(f) - 1;
    f &= ~(1 << v), o += 1;
    var g = 32 - dr(r) + v;
    if (30 < g) {
      var w = v - v % 5;
      g = (f & (1 << w) - 1).toString(32), f >>= w, v -= w, Gn = 1 << 32 - dr(r) + v | o << v | f, Fi = g + n;
    } else Gn = 1 << g | o << v | f, Fi = n;
  }
  function Hc(n) {
    n.return !== null && (ra(n, 1), Zd(n, 1, 0));
  }
  function Jd(n) {
    for (; n === sl; ) sl = ko[--Hr], ko[Hr] = null, uu = ko[--Hr], ko[Hr] = null;
    for (; n === ba; ) ba = jr[--lr], jr[lr] = null, Fi = jr[--lr], jr[lr] = null, Gn = jr[--lr], jr[lr] = null;
  }
  var aa = null, Vr = null, en = !1, Ca = null;
  function ep(n, r) {
    var o = Oa(5, null, null, 0);
    o.elementType = "DELETED", o.stateNode = r, o.return = n, r = n.deletions, r === null ? (n.deletions = [o], n.flags |= 16) : r.push(o);
  }
  function hh(n, r) {
    switch (n.tag) {
      case 5:
        var o = n.type;
        return r = r.nodeType !== 1 || o.toLowerCase() !== r.nodeName.toLowerCase() ? null : r, r !== null ? (n.stateNode = r, aa = n, Vr = Xa(r.firstChild), !0) : !1;
      case 6:
        return r = n.pendingProps === "" || r.nodeType !== 3 ? null : r, r !== null ? (n.stateNode = r, aa = n, Vr = null, !0) : !1;
      case 13:
        return r = r.nodeType !== 8 ? null : r, r !== null ? (o = ba !== null ? { id: Gn, overflow: Fi } : null, n.memoizedState = { dehydrated: r, treeContext: o, retryLane: 1073741824 }, o = Oa(18, null, null, 0), o.stateNode = r, o.return = n, n.child = o, aa = n, Vr = null, !0) : !1;
      default:
        return !1;
    }
  }
  function jc(n) {
    return (n.mode & 1) !== 0 && (n.flags & 128) === 0;
  }
  function Vc(n) {
    if (en) {
      var r = Vr;
      if (r) {
        var o = r;
        if (!hh(n, r)) {
          if (jc(n)) throw Error(c(418));
          r = Xa(o.nextSibling);
          var f = aa;
          r && hh(n, r) ? ep(f, o) : (n.flags = n.flags & -4097 | 2, en = !1, aa = n);
        }
      } else {
        if (jc(n)) throw Error(c(418));
        n.flags = n.flags & -4097 | 2, en = !1, aa = n;
      }
    }
  }
  function mh(n) {
    for (n = n.return; n !== null && n.tag !== 5 && n.tag !== 3 && n.tag !== 13; ) n = n.return;
    aa = n;
  }
  function $c(n) {
    if (n !== aa) return !1;
    if (!en) return mh(n), en = !0, !1;
    var r;
    if ((r = n.tag !== 3) && !(r = n.tag !== 5) && (r = n.type, r = r !== "head" && r !== "body" && !Ts(n.type, n.memoizedProps)), r && (r = Vr)) {
      if (jc(n)) throw yh(), Error(c(418));
      for (; r; ) ep(n, r), r = Xa(r.nextSibling);
    }
    if (mh(n), n.tag === 13) {
      if (n = n.memoizedState, n = n !== null ? n.dehydrated : null, !n) throw Error(c(317));
      e: {
        for (n = n.nextSibling, r = 0; n; ) {
          if (n.nodeType === 8) {
            var o = n.data;
            if (o === "/$") {
              if (r === 0) {
                Vr = Xa(n.nextSibling);
                break e;
              }
              r--;
            } else o !== "$" && o !== "$!" && o !== "$?" || r++;
          }
          n = n.nextSibling;
        }
        Vr = null;
      }
    } else Vr = aa ? Xa(n.stateNode.nextSibling) : null;
    return !0;
  }
  function yh() {
    for (var n = Vr; n; ) n = Xa(n.nextSibling);
  }
  function fn() {
    Vr = aa = null, en = !1;
  }
  function tp(n) {
    Ca === null ? Ca = [n] : Ca.push(n);
  }
  var Bc = oe.ReactCurrentBatchConfig;
  function cl(n, r, o) {
    if (n = o.ref, n !== null && typeof n != "function" && typeof n != "object") {
      if (o._owner) {
        if (o = o._owner, o) {
          if (o.tag !== 1) throw Error(c(309));
          var f = o.stateNode;
        }
        if (!f) throw Error(c(147, n));
        var v = f, g = "" + n;
        return r !== null && r.ref !== null && typeof r.ref == "function" && r.ref._stringRef === g ? r.ref : (r = function(w) {
          var O = v.refs;
          w === null ? delete O[g] : O[g] = w;
        }, r._stringRef = g, r);
      }
      if (typeof n != "string") throw Error(c(284));
      if (!o._owner) throw Error(c(290, n));
    }
    return n;
  }
  function ci(n, r) {
    throw n = Object.prototype.toString.call(r), Error(c(31, n === "[object Object]" ? "object with keys {" + Object.keys(r).join(", ") + "}" : n));
  }
  function gh(n) {
    var r = n._init;
    return r(n._payload);
  }
  function Ic(n) {
    function r(B, U) {
      if (n) {
        var Q = B.deletions;
        Q === null ? (B.deletions = [U], B.flags |= 16) : Q.push(U);
      }
    }
    function o(B, U) {
      if (!n) return null;
      for (; U !== null; ) r(B, U), U = U.sibling;
      return null;
    }
    function f(B, U) {
      for (B = /* @__PURE__ */ new Map(); U !== null; ) U.key !== null ? B.set(U.key, U) : B.set(U.index, U), U = U.sibling;
      return B;
    }
    function v(B, U) {
      return B = zo(B, U), B.index = 0, B.sibling = null, B;
    }
    function g(B, U, Q) {
      return B.index = Q, n ? (Q = B.alternate, Q !== null ? (Q = Q.index, Q < U ? (B.flags |= 2, U) : Q) : (B.flags |= 2, U)) : (B.flags |= 1048576, U);
    }
    function w(B) {
      return n && B.alternate === null && (B.flags |= 2), B;
    }
    function O(B, U, Q, pe) {
      return U === null || U.tag !== 6 ? (U = Lf(Q, B.mode, pe), U.return = B, U) : (U = v(U, Q), U.return = B, U);
    }
    function N(B, U, Q, pe) {
      var He = Q.type;
      return He === me ? ue(B, U, Q.props.children, pe, Q.key) : U !== null && (U.elementType === He || typeof He == "object" && He !== null && He.$$typeof === mt && gh(He) === U.type) ? (pe = v(U, Q.props), pe.ref = cl(B, U, Q), pe.return = B, pe) : (pe = Of(Q.type, Q.key, Q.props, null, B.mode, pe), pe.ref = cl(B, U, Q), pe.return = B, pe);
    }
    function K(B, U, Q, pe) {
      return U === null || U.tag !== 4 || U.stateNode.containerInfo !== Q.containerInfo || U.stateNode.implementation !== Q.implementation ? (U = Ws(Q, B.mode, pe), U.return = B, U) : (U = v(U, Q.children || []), U.return = B, U);
    }
    function ue(B, U, Q, pe, He) {
      return U === null || U.tag !== 7 ? (U = Rl(Q, B.mode, pe, He), U.return = B, U) : (U = v(U, Q), U.return = B, U);
    }
    function se(B, U, Q) {
      if (typeof U == "string" && U !== "" || typeof U == "number") return U = Lf("" + U, B.mode, Q), U.return = B, U;
      if (typeof U == "object" && U !== null) {
        switch (U.$$typeof) {
          case ne:
            return Q = Of(U.type, U.key, U.props, null, B.mode, Q), Q.ref = cl(B, null, U), Q.return = B, Q;
          case Ce:
            return U = Ws(U, B.mode, Q), U.return = B, U;
          case mt:
            var pe = U._init;
            return se(B, pe(U._payload), Q);
        }
        if (or(U) || Be(U)) return U = Rl(U, B.mode, Q, null), U.return = B, U;
        ci(B, U);
      }
      return null;
    }
    function le(B, U, Q, pe) {
      var He = U !== null ? U.key : null;
      if (typeof Q == "string" && Q !== "" || typeof Q == "number") return He !== null ? null : O(B, U, "" + Q, pe);
      if (typeof Q == "object" && Q !== null) {
        switch (Q.$$typeof) {
          case ne:
            return Q.key === He ? N(B, U, Q, pe) : null;
          case Ce:
            return Q.key === He ? K(B, U, Q, pe) : null;
          case mt:
            return He = Q._init, le(
              B,
              U,
              He(Q._payload),
              pe
            );
        }
        if (or(Q) || Be(Q)) return He !== null ? null : ue(B, U, Q, pe, null);
        ci(B, Q);
      }
      return null;
    }
    function xe(B, U, Q, pe, He) {
      if (typeof pe == "string" && pe !== "" || typeof pe == "number") return B = B.get(Q) || null, O(U, B, "" + pe, He);
      if (typeof pe == "object" && pe !== null) {
        switch (pe.$$typeof) {
          case ne:
            return B = B.get(pe.key === null ? Q : pe.key) || null, N(U, B, pe, He);
          case Ce:
            return B = B.get(pe.key === null ? Q : pe.key) || null, K(U, B, pe, He);
          case mt:
            var Ke = pe._init;
            return xe(B, U, Q, Ke(pe._payload), He);
        }
        if (or(pe) || Be(pe)) return B = B.get(Q) || null, ue(U, B, pe, He, null);
        ci(U, pe);
      }
      return null;
    }
    function Le(B, U, Q, pe) {
      for (var He = null, Ke = null, Ne = U, Ze = U = 0, Un = null; Ne !== null && Ze < Q.length; Ze++) {
        Ne.index > Ze ? (Un = Ne, Ne = null) : Un = Ne.sibling;
        var At = le(B, Ne, Q[Ze], pe);
        if (At === null) {
          Ne === null && (Ne = Un);
          break;
        }
        n && Ne && At.alternate === null && r(B, Ne), U = g(At, U, Ze), Ke === null ? He = At : Ke.sibling = At, Ke = At, Ne = Un;
      }
      if (Ze === Q.length) return o(B, Ne), en && ra(B, Ze), He;
      if (Ne === null) {
        for (; Ze < Q.length; Ze++) Ne = se(B, Q[Ze], pe), Ne !== null && (U = g(Ne, U, Ze), Ke === null ? He = Ne : Ke.sibling = Ne, Ke = Ne);
        return en && ra(B, Ze), He;
      }
      for (Ne = f(B, Ne); Ze < Q.length; Ze++) Un = xe(Ne, B, Ze, Q[Ze], pe), Un !== null && (n && Un.alternate !== null && Ne.delete(Un.key === null ? Ze : Un.key), U = g(Un, U, Ze), Ke === null ? He = Un : Ke.sibling = Un, Ke = Un);
      return n && Ne.forEach(function(Yi) {
        return r(B, Yi);
      }), en && ra(B, Ze), He;
    }
    function ze(B, U, Q, pe) {
      var He = Be(Q);
      if (typeof He != "function") throw Error(c(150));
      if (Q = He.call(Q), Q == null) throw Error(c(151));
      for (var Ke = He = null, Ne = U, Ze = U = 0, Un = null, At = Q.next(); Ne !== null && !At.done; Ze++, At = Q.next()) {
        Ne.index > Ze ? (Un = Ne, Ne = null) : Un = Ne.sibling;
        var Yi = le(B, Ne, At.value, pe);
        if (Yi === null) {
          Ne === null && (Ne = Un);
          break;
        }
        n && Ne && Yi.alternate === null && r(B, Ne), U = g(Yi, U, Ze), Ke === null ? He = Yi : Ke.sibling = Yi, Ke = Yi, Ne = Un;
      }
      if (At.done) return o(
        B,
        Ne
      ), en && ra(B, Ze), He;
      if (Ne === null) {
        for (; !At.done; Ze++, At = Q.next()) At = se(B, At.value, pe), At !== null && (U = g(At, U, Ze), Ke === null ? He = At : Ke.sibling = At, Ke = At);
        return en && ra(B, Ze), He;
      }
      for (Ne = f(B, Ne); !At.done; Ze++, At = Q.next()) At = xe(Ne, B, Ze, At.value, pe), At !== null && (n && At.alternate !== null && Ne.delete(At.key === null ? Ze : At.key), U = g(At, U, Ze), Ke === null ? He = At : Ke.sibling = At, Ke = At);
      return n && Ne.forEach(function(Dg) {
        return r(B, Dg);
      }), en && ra(B, Ze), He;
    }
    function Rn(B, U, Q, pe) {
      if (typeof Q == "object" && Q !== null && Q.type === me && Q.key === null && (Q = Q.props.children), typeof Q == "object" && Q !== null) {
        switch (Q.$$typeof) {
          case ne:
            e: {
              for (var He = Q.key, Ke = U; Ke !== null; ) {
                if (Ke.key === He) {
                  if (He = Q.type, He === me) {
                    if (Ke.tag === 7) {
                      o(B, Ke.sibling), U = v(Ke, Q.props.children), U.return = B, B = U;
                      break e;
                    }
                  } else if (Ke.elementType === He || typeof He == "object" && He !== null && He.$$typeof === mt && gh(He) === Ke.type) {
                    o(B, Ke.sibling), U = v(Ke, Q.props), U.ref = cl(B, Ke, Q), U.return = B, B = U;
                    break e;
                  }
                  o(B, Ke);
                  break;
                } else r(B, Ke);
                Ke = Ke.sibling;
              }
              Q.type === me ? (U = Rl(Q.props.children, B.mode, pe, Q.key), U.return = B, B = U) : (pe = Of(Q.type, Q.key, Q.props, null, B.mode, pe), pe.ref = cl(B, U, Q), pe.return = B, B = pe);
            }
            return w(B);
          case Ce:
            e: {
              for (Ke = Q.key; U !== null; ) {
                if (U.key === Ke) if (U.tag === 4 && U.stateNode.containerInfo === Q.containerInfo && U.stateNode.implementation === Q.implementation) {
                  o(B, U.sibling), U = v(U, Q.children || []), U.return = B, B = U;
                  break e;
                } else {
                  o(B, U);
                  break;
                }
                else r(B, U);
                U = U.sibling;
              }
              U = Ws(Q, B.mode, pe), U.return = B, B = U;
            }
            return w(B);
          case mt:
            return Ke = Q._init, Rn(B, U, Ke(Q._payload), pe);
        }
        if (or(Q)) return Le(B, U, Q, pe);
        if (Be(Q)) return ze(B, U, Q, pe);
        ci(B, Q);
      }
      return typeof Q == "string" && Q !== "" || typeof Q == "number" ? (Q = "" + Q, U !== null && U.tag === 6 ? (o(B, U.sibling), U = v(U, Q), U.return = B, B = U) : (o(B, U), U = Lf(Q, B.mode, pe), U.return = B, B = U), w(B)) : o(B, U);
    }
    return Rn;
  }
  var su = Ic(!0), Sh = Ic(!1), Hi = st(null), Ln = null, ge = null, wa = null;
  function ia() {
    wa = ge = Ln = null;
  }
  function np(n) {
    var r = Hi.current;
    Pt(Hi), n._currentValue = r;
  }
  function rp(n, r, o) {
    for (; n !== null; ) {
      var f = n.alternate;
      if ((n.childLanes & r) !== r ? (n.childLanes |= r, f !== null && (f.childLanes |= r)) : f !== null && (f.childLanes & r) !== r && (f.childLanes |= r), n === o) break;
      n = n.return;
    }
  }
  function cu(n, r) {
    Ln = n, wa = ge = null, n = n.dependencies, n !== null && n.firstContext !== null && (n.lanes & r && (Ir = !0), n.firstContext = null);
  }
  function Ta(n) {
    var r = n._currentValue;
    if (wa !== n) if (n = { context: n, memoizedValue: r, next: null }, ge === null) {
      if (Ln === null) throw Error(c(308));
      ge = n, Ln.dependencies = { lanes: 0, firstContext: n };
    } else ge = ge.next = n;
    return r;
  }
  var fl = null;
  function _n(n) {
    fl === null ? fl = [n] : fl.push(n);
  }
  function Eh(n, r, o, f) {
    var v = r.interleaved;
    return v === null ? (o.next = o, _n(r)) : (o.next = v.next, v.next = o), r.interleaved = o, ji(n, f);
  }
  function ji(n, r) {
    n.lanes |= r;
    var o = n.alternate;
    for (o !== null && (o.lanes |= r), o = n, n = n.return; n !== null; ) n.childLanes |= r, o = n.alternate, o !== null && (o.childLanes |= r), o = n, n = n.return;
    return o.tag === 3 ? o.stateNode : null;
  }
  var _o = !1;
  function Yc(n) {
    n.updateQueue = { baseState: n.memoizedState, firstBaseUpdate: null, lastBaseUpdate: null, shared: { pending: null, interleaved: null, lanes: 0 }, effects: null };
  }
  function fu(n, r) {
    n = n.updateQueue, r.updateQueue === n && (r.updateQueue = { baseState: n.baseState, firstBaseUpdate: n.firstBaseUpdate, lastBaseUpdate: n.lastBaseUpdate, shared: n.shared, effects: n.effects });
  }
  function $r(n, r) {
    return { eventTime: n, lane: r, tag: 0, payload: null, callback: null, next: null };
  }
  function Do(n, r, o) {
    var f = n.updateQueue;
    if (f === null) return null;
    if (f = f.shared, gt & 2) {
      var v = f.pending;
      return v === null ? r.next = r : (r.next = v.next, v.next = r), f.pending = r, ji(n, o);
    }
    return v = f.interleaved, v === null ? (r.next = r, _n(f)) : (r.next = v.next, v.next = r), f.interleaved = r, ji(n, o);
  }
  function Wc(n, r, o) {
    if (r = r.updateQueue, r !== null && (r = r.shared, (o & 4194240) !== 0)) {
      var f = r.lanes;
      f &= n.pendingLanes, o |= f, r.lanes = o, oi(n, o);
    }
  }
  function bh(n, r) {
    var o = n.updateQueue, f = n.alternate;
    if (f !== null && (f = f.updateQueue, o === f)) {
      var v = null, g = null;
      if (o = o.firstBaseUpdate, o !== null) {
        do {
          var w = { eventTime: o.eventTime, lane: o.lane, tag: o.tag, payload: o.payload, callback: o.callback, next: null };
          g === null ? v = g = w : g = g.next = w, o = o.next;
        } while (o !== null);
        g === null ? v = g = r : g = g.next = r;
      } else v = g = r;
      o = { baseState: f.baseState, firstBaseUpdate: v, lastBaseUpdate: g, shared: f.shared, effects: f.effects }, n.updateQueue = o;
      return;
    }
    n = o.lastBaseUpdate, n === null ? o.firstBaseUpdate = r : n.next = r, o.lastBaseUpdate = r;
  }
  function Qc(n, r, o, f) {
    var v = n.updateQueue;
    _o = !1;
    var g = v.firstBaseUpdate, w = v.lastBaseUpdate, O = v.shared.pending;
    if (O !== null) {
      v.shared.pending = null;
      var N = O, K = N.next;
      N.next = null, w === null ? g = K : w.next = K, w = N;
      var ue = n.alternate;
      ue !== null && (ue = ue.updateQueue, O = ue.lastBaseUpdate, O !== w && (O === null ? ue.firstBaseUpdate = K : O.next = K, ue.lastBaseUpdate = N));
    }
    if (g !== null) {
      var se = v.baseState;
      w = 0, ue = K = N = null, O = g;
      do {
        var le = O.lane, xe = O.eventTime;
        if ((f & le) === le) {
          ue !== null && (ue = ue.next = {
            eventTime: xe,
            lane: 0,
            tag: O.tag,
            payload: O.payload,
            callback: O.callback,
            next: null
          });
          e: {
            var Le = n, ze = O;
            switch (le = r, xe = o, ze.tag) {
              case 1:
                if (Le = ze.payload, typeof Le == "function") {
                  se = Le.call(xe, se, le);
                  break e;
                }
                se = Le;
                break e;
              case 3:
                Le.flags = Le.flags & -65537 | 128;
              case 0:
                if (Le = ze.payload, le = typeof Le == "function" ? Le.call(xe, se, le) : Le, le == null) break e;
                se = Te({}, se, le);
                break e;
              case 2:
                _o = !0;
            }
          }
          O.callback !== null && O.lane !== 0 && (n.flags |= 64, le = v.effects, le === null ? v.effects = [O] : le.push(O));
        } else xe = { eventTime: xe, lane: le, tag: O.tag, payload: O.payload, callback: O.callback, next: null }, ue === null ? (K = ue = xe, N = se) : ue = ue.next = xe, w |= le;
        if (O = O.next, O === null) {
          if (O = v.shared.pending, O === null) break;
          le = O, O = le.next, le.next = null, v.lastBaseUpdate = le, v.shared.pending = null;
        }
      } while (!0);
      if (ue === null && (N = se), v.baseState = N, v.firstBaseUpdate = K, v.lastBaseUpdate = ue, r = v.shared.interleaved, r !== null) {
        v = r;
        do
          w |= v.lane, v = v.next;
        while (v !== r);
      } else g === null && (v.shared.lanes = 0);
      bl |= w, n.lanes = w, n.memoizedState = se;
    }
  }
  function Ch(n, r, o) {
    if (n = r.effects, r.effects = null, n !== null) for (r = 0; r < n.length; r++) {
      var f = n[r], v = f.callback;
      if (v !== null) {
        if (f.callback = null, f = o, typeof v != "function") throw Error(c(191, v));
        v.call(f);
      }
    }
  }
  var Ds = {}, Ka = st(Ds), du = st(Ds), Os = st(Ds);
  function dl(n) {
    if (n === Ds) throw Error(c(174));
    return n;
  }
  function ap(n, r) {
    switch (Ht(Os, r), Ht(du, n), Ht(Ka, Ds), n = r.nodeType, n) {
      case 9:
      case 11:
        r = (r = r.documentElement) ? r.namespaceURI : Wn(null, "");
        break;
      default:
        n = n === 8 ? r.parentNode : r, r = n.namespaceURI || null, n = n.tagName, r = Wn(r, n);
    }
    Pt(Ka), Ht(Ka, r);
  }
  function pu() {
    Pt(Ka), Pt(du), Pt(Os);
  }
  function wh(n) {
    dl(Os.current);
    var r = dl(Ka.current), o = Wn(r, n.type);
    r !== o && (Ht(du, n), Ht(Ka, o));
  }
  function ip(n) {
    du.current === n && (Pt(Ka), Pt(du));
  }
  var on = st(0);
  function Gc(n) {
    for (var r = n; r !== null; ) {
      if (r.tag === 13) {
        var o = r.memoizedState;
        if (o !== null && (o = o.dehydrated, o === null || o.data === "$?" || o.data === "$!")) return r;
      } else if (r.tag === 19 && r.memoizedProps.revealOrder !== void 0) {
        if (r.flags & 128) return r;
      } else if (r.child !== null) {
        r.child.return = r, r = r.child;
        continue;
      }
      if (r === n) break;
      for (; r.sibling === null; ) {
        if (r.return === null || r.return === n) return null;
        r = r.return;
      }
      r.sibling.return = r.return, r = r.sibling;
    }
    return null;
  }
  var qc = [];
  function op() {
    for (var n = 0; n < qc.length; n++) qc[n]._workInProgressVersionPrimary = null;
    qc.length = 0;
  }
  var Xc = oe.ReactCurrentDispatcher, Ms = oe.ReactCurrentBatchConfig, Fe = 0, je = null, nt = null, vt = null, oa = !1, vu = !1, Ls = 0, dg = 0;
  function ur() {
    throw Error(c(321));
  }
  function Ns(n, r) {
    if (r === null) return !1;
    for (var o = 0; o < r.length && o < n.length; o++) if (!ya(n[o], r[o])) return !1;
    return !0;
  }
  function ie(n, r, o, f, v, g) {
    if (Fe = g, je = r, r.memoizedState = null, r.updateQueue = null, r.lanes = 0, Xc.current = n === null || n.memoizedState === null ? pg : Kt, n = o(f, v), vu) {
      g = 0;
      do {
        if (vu = !1, Ls = 0, 25 <= g) throw Error(c(301));
        g += 1, vt = nt = null, r.updateQueue = null, Xc.current = df, n = o(f, v);
      } while (vu);
    }
    if (Xc.current = sr, r = nt !== null && nt.next !== null, Fe = 0, vt = nt = je = null, oa = !1, r) throw Error(c(300));
    return n;
  }
  function Dn() {
    var n = Ls !== 0;
    return Ls = 0, n;
  }
  function $e() {
    var n = { memoizedState: null, baseState: null, baseQueue: null, queue: null, next: null };
    return vt === null ? je.memoizedState = vt = n : vt = vt.next = n, vt;
  }
  function qn() {
    if (nt === null) {
      var n = je.alternate;
      n = n !== null ? n.memoizedState : null;
    } else n = nt.next;
    var r = vt === null ? je.memoizedState : vt.next;
    if (r !== null) vt = r, nt = n;
    else {
      if (n === null) throw Error(c(310));
      nt = n, n = { memoizedState: nt.memoizedState, baseState: nt.baseState, baseQueue: nt.baseQueue, queue: nt.queue, next: null }, vt === null ? je.memoizedState = vt = n : vt = vt.next = n;
    }
    return vt;
  }
  function la(n, r) {
    return typeof r == "function" ? r(n) : r;
  }
  function Vi(n) {
    var r = qn(), o = r.queue;
    if (o === null) throw Error(c(311));
    o.lastRenderedReducer = n;
    var f = nt, v = f.baseQueue, g = o.pending;
    if (g !== null) {
      if (v !== null) {
        var w = v.next;
        v.next = g.next, g.next = w;
      }
      f.baseQueue = v = g, o.pending = null;
    }
    if (v !== null) {
      g = v.next, f = f.baseState;
      var O = w = null, N = null, K = g;
      do {
        var ue = K.lane;
        if ((Fe & ue) === ue) N !== null && (N = N.next = { lane: 0, action: K.action, hasEagerState: K.hasEagerState, eagerState: K.eagerState, next: null }), f = K.hasEagerState ? K.eagerState : n(f, K.action);
        else {
          var se = {
            lane: ue,
            action: K.action,
            hasEagerState: K.hasEagerState,
            eagerState: K.eagerState,
            next: null
          };
          N === null ? (O = N = se, w = f) : N = N.next = se, je.lanes |= ue, bl |= ue;
        }
        K = K.next;
      } while (K !== null && K !== g);
      N === null ? w = f : N.next = O, ya(f, r.memoizedState) || (Ir = !0), r.memoizedState = f, r.baseState = w, r.baseQueue = N, o.lastRenderedState = f;
    }
    if (n = o.interleaved, n !== null) {
      v = n;
      do
        g = v.lane, je.lanes |= g, bl |= g, v = v.next;
      while (v !== n);
    } else v === null && (o.lanes = 0);
    return [r.memoizedState, o.dispatch];
  }
  function Ra(n) {
    var r = qn(), o = r.queue;
    if (o === null) throw Error(c(311));
    o.lastRenderedReducer = n;
    var f = o.dispatch, v = o.pending, g = r.memoizedState;
    if (v !== null) {
      o.pending = null;
      var w = v = v.next;
      do
        g = n(g, w.action), w = w.next;
      while (w !== v);
      ya(g, r.memoizedState) || (Ir = !0), r.memoizedState = g, r.baseQueue === null && (r.baseState = g), o.lastRenderedState = g;
    }
    return [g, f];
  }
  function hu() {
  }
  function pl(n, r) {
    var o = je, f = qn(), v = r(), g = !ya(f.memoizedState, v);
    if (g && (f.memoizedState = v, Ir = !0), f = f.queue, As(Zc.bind(null, o, f, n), [n]), f.getSnapshot !== r || g || vt !== null && vt.memoizedState.tag & 1) {
      if (o.flags |= 2048, vl(9, Kc.bind(null, o, f, v, r), void 0, null), gn === null) throw Error(c(349));
      Fe & 30 || mu(o, r, v);
    }
    return v;
  }
  function mu(n, r, o) {
    n.flags |= 16384, n = { getSnapshot: r, value: o }, r = je.updateQueue, r === null ? (r = { lastEffect: null, stores: null }, je.updateQueue = r, r.stores = [n]) : (o = r.stores, o === null ? r.stores = [n] : o.push(n));
  }
  function Kc(n, r, o, f) {
    r.value = o, r.getSnapshot = f, Jc(r) && ef(n);
  }
  function Zc(n, r, o) {
    return o(function() {
      Jc(r) && ef(n);
    });
  }
  function Jc(n) {
    var r = n.getSnapshot;
    n = n.value;
    try {
      var o = r();
      return !ya(n, o);
    } catch {
      return !0;
    }
  }
  function ef(n) {
    var r = ji(n, 1);
    r !== null && dn(r, n, 1, -1);
  }
  function tf(n) {
    var r = $e();
    return typeof n == "function" && (n = n()), r.memoizedState = r.baseState = n, n = { pending: null, interleaved: null, lanes: 0, dispatch: null, lastRenderedReducer: la, lastRenderedState: n }, r.queue = n, n = n.dispatch = zs.bind(null, je, n), [r.memoizedState, n];
  }
  function vl(n, r, o, f) {
    return n = { tag: n, create: r, destroy: o, deps: f, next: null }, r = je.updateQueue, r === null ? (r = { lastEffect: null, stores: null }, je.updateQueue = r, r.lastEffect = n.next = n) : (o = r.lastEffect, o === null ? r.lastEffect = n.next = n : (f = o.next, o.next = n, n.next = f, r.lastEffect = n)), n;
  }
  function nf() {
    return qn().memoizedState;
  }
  function yu(n, r, o, f) {
    var v = $e();
    je.flags |= n, v.memoizedState = vl(1 | r, o, void 0, f === void 0 ? null : f);
  }
  function gu(n, r, o, f) {
    var v = qn();
    f = f === void 0 ? null : f;
    var g = void 0;
    if (nt !== null) {
      var w = nt.memoizedState;
      if (g = w.destroy, f !== null && Ns(f, w.deps)) {
        v.memoizedState = vl(r, o, g, f);
        return;
      }
    }
    je.flags |= n, v.memoizedState = vl(1 | r, o, g, f);
  }
  function rf(n, r) {
    return yu(8390656, 8, n, r);
  }
  function As(n, r) {
    return gu(2048, 8, n, r);
  }
  function af(n, r) {
    return gu(4, 2, n, r);
  }
  function of(n, r) {
    return gu(4, 4, n, r);
  }
  function lf(n, r) {
    if (typeof r == "function") return n = n(), r(n), function() {
      r(null);
    };
    if (r != null) return n = n(), r.current = n, function() {
      r.current = null;
    };
  }
  function uf(n, r, o) {
    return o = o != null ? o.concat([n]) : null, gu(4, 4, lf.bind(null, r, n), o);
  }
  function Su() {
  }
  function hl(n, r) {
    var o = qn();
    r = r === void 0 ? null : r;
    var f = o.memoizedState;
    return f !== null && r !== null && Ns(r, f[1]) ? f[0] : (o.memoizedState = [n, r], n);
  }
  function sf(n, r) {
    var o = qn();
    r = r === void 0 ? null : r;
    var f = o.memoizedState;
    return f !== null && r !== null && Ns(r, f[1]) ? f[0] : (n = n(), o.memoizedState = [n, r], n);
  }
  function cf(n, r, o) {
    return Fe & 21 ? (ya(o, r) || (o = Wl(), je.lanes |= o, bl |= o, n.baseState = !0), r) : (n.baseState && (n.baseState = !1, Ir = !0), n.memoizedState = o);
  }
  function lp(n, r) {
    var o = Nt;
    Nt = o !== 0 && 4 > o ? o : 4, n(!0);
    var f = Ms.transition;
    Ms.transition = {};
    try {
      n(!1), r();
    } finally {
      Nt = o, Ms.transition = f;
    }
  }
  function ff() {
    return qn().memoizedState;
  }
  function Th(n, r, o) {
    var f = Ii(n);
    if (o = { lane: f, action: o, hasEagerState: !1, eagerState: null, next: null }, up(n)) Eu(r, o);
    else if (o = Eh(n, r, o, f), o !== null) {
      var v = In();
      dn(o, n, f, v), Oo(o, r, f);
    }
  }
  function zs(n, r, o) {
    var f = Ii(n), v = { lane: f, action: o, hasEagerState: !1, eagerState: null, next: null };
    if (up(n)) Eu(r, v);
    else {
      var g = n.alternate;
      if (n.lanes === 0 && (g === null || g.lanes === 0) && (g = r.lastRenderedReducer, g !== null)) try {
        var w = r.lastRenderedState, O = g(w, o);
        if (v.hasEagerState = !0, v.eagerState = O, ya(O, w)) {
          var N = r.interleaved;
          N === null ? (v.next = v, _n(r)) : (v.next = N.next, N.next = v), r.interleaved = v;
          return;
        }
      } catch {
      } finally {
      }
      o = Eh(n, r, v, f), o !== null && (v = In(), dn(o, n, f, v), Oo(o, r, f));
    }
  }
  function up(n) {
    var r = n.alternate;
    return n === je || r !== null && r === je;
  }
  function Eu(n, r) {
    vu = oa = !0;
    var o = n.pending;
    o === null ? r.next = r : (r.next = o.next, o.next = r), n.pending = r;
  }
  function Oo(n, r, o) {
    if (o & 4194240) {
      var f = r.lanes;
      f &= n.pendingLanes, o |= f, r.lanes = o, oi(n, o);
    }
  }
  var sr = { readContext: Ta, useCallback: ur, useContext: ur, useEffect: ur, useImperativeHandle: ur, useInsertionEffect: ur, useLayoutEffect: ur, useMemo: ur, useReducer: ur, useRef: ur, useState: ur, useDebugValue: ur, useDeferredValue: ur, useTransition: ur, useMutableSource: ur, useSyncExternalStore: ur, useId: ur, unstable_isNewReconciler: !1 }, pg = { readContext: Ta, useCallback: function(n, r) {
    return $e().memoizedState = [n, r === void 0 ? null : r], n;
  }, useContext: Ta, useEffect: rf, useImperativeHandle: function(n, r, o) {
    return o = o != null ? o.concat([n]) : null, yu(
      4194308,
      4,
      lf.bind(null, r, n),
      o
    );
  }, useLayoutEffect: function(n, r) {
    return yu(4194308, 4, n, r);
  }, useInsertionEffect: function(n, r) {
    return yu(4, 2, n, r);
  }, useMemo: function(n, r) {
    var o = $e();
    return r = r === void 0 ? null : r, n = n(), o.memoizedState = [n, r], n;
  }, useReducer: function(n, r, o) {
    var f = $e();
    return r = o !== void 0 ? o(r) : r, f.memoizedState = f.baseState = r, n = { pending: null, interleaved: null, lanes: 0, dispatch: null, lastRenderedReducer: n, lastRenderedState: r }, f.queue = n, n = n.dispatch = Th.bind(null, je, n), [f.memoizedState, n];
  }, useRef: function(n) {
    var r = $e();
    return n = { current: n }, r.memoizedState = n;
  }, useState: tf, useDebugValue: Su, useDeferredValue: function(n) {
    return $e().memoizedState = n;
  }, useTransition: function() {
    var n = tf(!1), r = n[0];
    return n = lp.bind(null, n[1]), $e().memoizedState = n, [r, n];
  }, useMutableSource: function() {
  }, useSyncExternalStore: function(n, r, o) {
    var f = je, v = $e();
    if (en) {
      if (o === void 0) throw Error(c(407));
      o = o();
    } else {
      if (o = r(), gn === null) throw Error(c(349));
      Fe & 30 || mu(f, r, o);
    }
    v.memoizedState = o;
    var g = { value: o, getSnapshot: r };
    return v.queue = g, rf(Zc.bind(
      null,
      f,
      g,
      n
    ), [n]), f.flags |= 2048, vl(9, Kc.bind(null, f, g, o, r), void 0, null), o;
  }, useId: function() {
    var n = $e(), r = gn.identifierPrefix;
    if (en) {
      var o = Fi, f = Gn;
      o = (f & ~(1 << 32 - dr(f) - 1)).toString(32) + o, r = ":" + r + "R" + o, o = Ls++, 0 < o && (r += "H" + o.toString(32)), r += ":";
    } else o = dg++, r = ":" + r + "r" + o.toString(32) + ":";
    return n.memoizedState = r;
  }, unstable_isNewReconciler: !1 }, Kt = {
    readContext: Ta,
    useCallback: hl,
    useContext: Ta,
    useEffect: As,
    useImperativeHandle: uf,
    useInsertionEffect: af,
    useLayoutEffect: of,
    useMemo: sf,
    useReducer: Vi,
    useRef: nf,
    useState: function() {
      return Vi(la);
    },
    useDebugValue: Su,
    useDeferredValue: function(n) {
      var r = qn();
      return cf(r, nt.memoizedState, n);
    },
    useTransition: function() {
      var n = Vi(la)[0], r = qn().memoizedState;
      return [n, r];
    },
    useMutableSource: hu,
    useSyncExternalStore: pl,
    useId: ff,
    unstable_isNewReconciler: !1
  }, df = { readContext: Ta, useCallback: hl, useContext: Ta, useEffect: As, useImperativeHandle: uf, useInsertionEffect: af, useLayoutEffect: of, useMemo: sf, useReducer: Ra, useRef: nf, useState: function() {
    return Ra(la);
  }, useDebugValue: Su, useDeferredValue: function(n) {
    var r = qn();
    return nt === null ? r.memoizedState = n : cf(r, nt.memoizedState, n);
  }, useTransition: function() {
    var n = Ra(la)[0], r = qn().memoizedState;
    return [n, r];
  }, useMutableSource: hu, useSyncExternalStore: pl, useId: ff, unstable_isNewReconciler: !1 };
  function Br(n, r) {
    if (n && n.defaultProps) {
      r = Te({}, r), n = n.defaultProps;
      for (var o in n) r[o] === void 0 && (r[o] = n[o]);
      return r;
    }
    return r;
  }
  function ml(n, r, o, f) {
    r = n.memoizedState, o = o(f, r), o = o == null ? r : Te({}, r, o), n.memoizedState = o, n.lanes === 0 && (n.updateQueue.baseState = o);
  }
  var yl = { isMounted: function(n) {
    return (n = n._reactInternals) ? va(n) === n : !1;
  }, enqueueSetState: function(n, r, o) {
    n = n._reactInternals;
    var f = In(), v = Ii(n), g = $r(f, v);
    g.payload = r, o != null && (g.callback = o), r = Do(n, g, v), r !== null && (dn(r, n, v, f), Wc(r, n, v));
  }, enqueueReplaceState: function(n, r, o) {
    n = n._reactInternals;
    var f = In(), v = Ii(n), g = $r(f, v);
    g.tag = 1, g.payload = r, o != null && (g.callback = o), r = Do(n, g, v), r !== null && (dn(r, n, v, f), Wc(r, n, v));
  }, enqueueForceUpdate: function(n, r) {
    n = n._reactInternals;
    var o = In(), f = Ii(n), v = $r(o, f);
    v.tag = 2, r != null && (v.callback = r), r = Do(n, v, f), r !== null && (dn(r, n, f, o), Wc(r, n, f));
  } };
  function Rh(n, r, o, f, v, g, w) {
    return n = n.stateNode, typeof n.shouldComponentUpdate == "function" ? n.shouldComponentUpdate(f, g, w) : r.prototype && r.prototype.isPureReactComponent ? !ys(o, f) || !ys(v, g) : !0;
  }
  function xh(n, r, o) {
    var f = !1, v = si, g = r.contextType;
    return typeof g == "object" && g !== null ? g = Ta(g) : (v = an(r) ? Pr : tt.current, f = r.contextTypes, g = (f = f != null) ? Sa(n, v) : si), r = new r(o, g), n.memoizedState = r.state !== null && r.state !== void 0 ? r.state : null, r.updater = yl, n.stateNode = r, r._reactInternals = n, f && (n = n.stateNode, n.__reactInternalMemoizedUnmaskedChildContext = v, n.__reactInternalMemoizedMaskedChildContext = g), r;
  }
  function kh(n, r, o, f) {
    n = r.state, typeof r.componentWillReceiveProps == "function" && r.componentWillReceiveProps(o, f), typeof r.UNSAFE_componentWillReceiveProps == "function" && r.UNSAFE_componentWillReceiveProps(o, f), r.state !== n && yl.enqueueReplaceState(r, r.state, null);
  }
  function sp(n, r, o, f) {
    var v = n.stateNode;
    v.props = o, v.state = n.memoizedState, v.refs = {}, Yc(n);
    var g = r.contextType;
    typeof g == "object" && g !== null ? v.context = Ta(g) : (g = an(r) ? Pr : tt.current, v.context = Sa(n, g)), v.state = n.memoizedState, g = r.getDerivedStateFromProps, typeof g == "function" && (ml(n, r, g, o), v.state = n.memoizedState), typeof r.getDerivedStateFromProps == "function" || typeof v.getSnapshotBeforeUpdate == "function" || typeof v.UNSAFE_componentWillMount != "function" && typeof v.componentWillMount != "function" || (r = v.state, typeof v.componentWillMount == "function" && v.componentWillMount(), typeof v.UNSAFE_componentWillMount == "function" && v.UNSAFE_componentWillMount(), r !== v.state && yl.enqueueReplaceState(v, v.state, null), Qc(n, o, v, f), v.state = n.memoizedState), typeof v.componentDidMount == "function" && (n.flags |= 4194308);
  }
  function Mo(n, r) {
    try {
      var o = "", f = r;
      do
        o += ct(f), f = f.return;
      while (f);
      var v = o;
    } catch (g) {
      v = `
Error generating stack: ` + g.message + `
` + g.stack;
    }
    return { value: n, source: r, stack: v, digest: null };
  }
  function cp(n, r, o) {
    return { value: n, source: null, stack: o ?? null, digest: r ?? null };
  }
  function Us(n, r) {
    try {
      console.error(r.value);
    } catch (o) {
      setTimeout(function() {
        throw o;
      });
    }
  }
  var _h = typeof WeakMap == "function" ? WeakMap : Map;
  function Dh(n, r, o) {
    o = $r(-1, o), o.tag = 3, o.payload = { element: null };
    var f = r.value;
    return o.callback = function() {
      Tf || (Tf = !0, Sp = f), Us(n, r);
    }, o;
  }
  function Oh(n, r, o) {
    o = $r(-1, o), o.tag = 3;
    var f = n.type.getDerivedStateFromError;
    if (typeof f == "function") {
      var v = r.value;
      o.payload = function() {
        return f(v);
      }, o.callback = function() {
        Us(n, r);
      };
    }
    var g = n.stateNode;
    return g !== null && typeof g.componentDidCatch == "function" && (o.callback = function() {
      Us(n, r), typeof f != "function" && (_a === null ? _a = /* @__PURE__ */ new Set([this]) : _a.add(this));
      var w = r.stack;
      this.componentDidCatch(r.value, { componentStack: w !== null ? w : "" });
    }), o;
  }
  function Ps(n, r, o) {
    var f = n.pingCache;
    if (f === null) {
      f = n.pingCache = new _h();
      var v = /* @__PURE__ */ new Set();
      f.set(r, v);
    } else v = f.get(r), v === void 0 && (v = /* @__PURE__ */ new Set(), f.set(r, v));
    v.has(o) || (v.add(o), n = wg.bind(null, n, r, o), r.then(n, n));
  }
  function Mh(n) {
    do {
      var r;
      if ((r = n.tag === 13) && (r = n.memoizedState, r = r !== null ? r.dehydrated !== null : !0), r) return n;
      n = n.return;
    } while (n !== null);
    return null;
  }
  function fp(n, r, o, f, v) {
    return n.mode & 1 ? (n.flags |= 65536, n.lanes = v, n) : (n === r ? n.flags |= 65536 : (n.flags |= 128, o.flags |= 131072, o.flags &= -52805, o.tag === 1 && (o.alternate === null ? o.tag = 17 : (r = $r(-1, 1), r.tag = 2, Do(o, r, 1))), o.lanes |= 1), n);
  }
  var Lh = oe.ReactCurrentOwner, Ir = !1;
  function wn(n, r, o, f) {
    r.child = n === null ? Sh(r, null, o, f) : su(r, n.child, o, f);
  }
  function bu(n, r, o, f, v) {
    o = o.render;
    var g = r.ref;
    return cu(r, v), f = ie(n, r, o, f, g, v), o = Dn(), n !== null && !Ir ? (r.updateQueue = n.updateQueue, r.flags &= -2053, n.lanes &= ~v, Tn(n, r, v)) : (en && o && Hc(r), r.flags |= 1, wn(n, r, f, v), r.child);
  }
  function Lo(n, r, o, f, v) {
    if (n === null) {
      var g = o.type;
      return typeof g == "function" && !Tp(g) && g.defaultProps === void 0 && o.compare === null && o.defaultProps === void 0 ? (r.tag = 15, r.type = g, pf(n, r, g, f, v)) : (n = Of(o.type, null, f, r, r.mode, v), n.ref = r.ref, n.return = r, r.child = n);
    }
    if (g = n.child, !(n.lanes & v)) {
      var w = g.memoizedProps;
      if (o = o.compare, o = o !== null ? o : ys, o(w, f) && n.ref === r.ref) return Tn(n, r, v);
    }
    return r.flags |= 1, n = zo(g, f), n.ref = r.ref, n.return = r, r.child = n;
  }
  function pf(n, r, o, f, v) {
    if (n !== null) {
      var g = n.memoizedProps;
      if (ys(g, f) && n.ref === r.ref) if (Ir = !1, r.pendingProps = f = g, (n.lanes & v) !== 0) n.flags & 131072 && (Ir = !0);
      else return r.lanes = n.lanes, Tn(n, r, v);
    }
    return ut(n, r, o, f, v);
  }
  function Yr(n, r, o) {
    var f = r.pendingProps, v = f.children, g = n !== null ? n.memoizedState : null;
    if (f.mode === "hidden") if (!(r.mode & 1)) r.memoizedState = { baseLanes: 0, cachePool: null, transitions: null }, Ht(Mu, Wr), Wr |= o;
    else {
      if (!(o & 1073741824)) return n = g !== null ? g.baseLanes | o : o, r.lanes = r.childLanes = 1073741824, r.memoizedState = { baseLanes: n, cachePool: null, transitions: null }, r.updateQueue = null, Ht(Mu, Wr), Wr |= n, null;
      r.memoizedState = { baseLanes: 0, cachePool: null, transitions: null }, f = g !== null ? g.baseLanes : o, Ht(Mu, Wr), Wr |= f;
    }
    else g !== null ? (f = g.baseLanes | o, r.memoizedState = null) : f = o, Ht(Mu, Wr), Wr |= f;
    return wn(n, r, v, o), r.child;
  }
  function gl(n, r) {
    var o = r.ref;
    (n === null && o !== null || n !== null && n.ref !== o) && (r.flags |= 512, r.flags |= 2097152);
  }
  function ut(n, r, o, f, v) {
    var g = an(o) ? Pr : tt.current;
    return g = Sa(r, g), cu(r, v), o = ie(n, r, o, f, g, v), f = Dn(), n !== null && !Ir ? (r.updateQueue = n.updateQueue, r.flags &= -2053, n.lanes &= ~v, Tn(n, r, v)) : (en && f && Hc(r), r.flags |= 1, wn(n, r, o, v), r.child);
  }
  function Fs(n, r, o, f, v) {
    if (an(o)) {
      var g = !0;
      Fc(r);
    } else g = !1;
    if (cu(r, v), r.stateNode === null) js(n, r), xh(r, o, f), sp(r, o, f, v), f = !0;
    else if (n === null) {
      var w = r.stateNode, O = r.memoizedProps;
      w.props = O;
      var N = w.context, K = o.contextType;
      typeof K == "object" && K !== null ? K = Ta(K) : (K = an(o) ? Pr : tt.current, K = Sa(r, K));
      var ue = o.getDerivedStateFromProps, se = typeof ue == "function" || typeof w.getSnapshotBeforeUpdate == "function";
      se || typeof w.UNSAFE_componentWillReceiveProps != "function" && typeof w.componentWillReceiveProps != "function" || (O !== f || N !== K) && kh(r, w, f, K), _o = !1;
      var le = r.memoizedState;
      w.state = le, Qc(r, f, w, v), N = r.memoizedState, O !== f || le !== N || mn.current || _o ? (typeof ue == "function" && (ml(r, o, ue, f), N = r.memoizedState), (O = _o || Rh(r, o, O, f, le, N, K)) ? (se || typeof w.UNSAFE_componentWillMount != "function" && typeof w.componentWillMount != "function" || (typeof w.componentWillMount == "function" && w.componentWillMount(), typeof w.UNSAFE_componentWillMount == "function" && w.UNSAFE_componentWillMount()), typeof w.componentDidMount == "function" && (r.flags |= 4194308)) : (typeof w.componentDidMount == "function" && (r.flags |= 4194308), r.memoizedProps = f, r.memoizedState = N), w.props = f, w.state = N, w.context = K, f = O) : (typeof w.componentDidMount == "function" && (r.flags |= 4194308), f = !1);
    } else {
      w = r.stateNode, fu(n, r), O = r.memoizedProps, K = r.type === r.elementType ? O : Br(r.type, O), w.props = K, se = r.pendingProps, le = w.context, N = o.contextType, typeof N == "object" && N !== null ? N = Ta(N) : (N = an(o) ? Pr : tt.current, N = Sa(r, N));
      var xe = o.getDerivedStateFromProps;
      (ue = typeof xe == "function" || typeof w.getSnapshotBeforeUpdate == "function") || typeof w.UNSAFE_componentWillReceiveProps != "function" && typeof w.componentWillReceiveProps != "function" || (O !== se || le !== N) && kh(r, w, f, N), _o = !1, le = r.memoizedState, w.state = le, Qc(r, f, w, v);
      var Le = r.memoizedState;
      O !== se || le !== Le || mn.current || _o ? (typeof xe == "function" && (ml(r, o, xe, f), Le = r.memoizedState), (K = _o || Rh(r, o, K, f, le, Le, N) || !1) ? (ue || typeof w.UNSAFE_componentWillUpdate != "function" && typeof w.componentWillUpdate != "function" || (typeof w.componentWillUpdate == "function" && w.componentWillUpdate(f, Le, N), typeof w.UNSAFE_componentWillUpdate == "function" && w.UNSAFE_componentWillUpdate(f, Le, N)), typeof w.componentDidUpdate == "function" && (r.flags |= 4), typeof w.getSnapshotBeforeUpdate == "function" && (r.flags |= 1024)) : (typeof w.componentDidUpdate != "function" || O === n.memoizedProps && le === n.memoizedState || (r.flags |= 4), typeof w.getSnapshotBeforeUpdate != "function" || O === n.memoizedProps && le === n.memoizedState || (r.flags |= 1024), r.memoizedProps = f, r.memoizedState = Le), w.props = f, w.state = Le, w.context = N, f = K) : (typeof w.componentDidUpdate != "function" || O === n.memoizedProps && le === n.memoizedState || (r.flags |= 4), typeof w.getSnapshotBeforeUpdate != "function" || O === n.memoizedProps && le === n.memoizedState || (r.flags |= 1024), f = !1);
    }
    return vf(n, r, o, f, g, v);
  }
  function vf(n, r, o, f, v, g) {
    gl(n, r);
    var w = (r.flags & 128) !== 0;
    if (!f && !w) return v && vh(r, o, !1), Tn(n, r, g);
    f = r.stateNode, Lh.current = r;
    var O = w && typeof o.getDerivedStateFromError != "function" ? null : f.render();
    return r.flags |= 1, n !== null && w ? (r.child = su(r, n.child, null, g), r.child = su(r, null, O, g)) : wn(n, r, O, g), r.memoizedState = f.state, v && vh(r, o, !0), r.child;
  }
  function vg(n) {
    var r = n.stateNode;
    r.pendingContext ? xo(n, r.pendingContext, r.pendingContext !== r.context) : r.context && xo(n, r.context, !1), ap(n, r.containerInfo);
  }
  function Nh(n, r, o, f, v) {
    return fn(), tp(v), r.flags |= 256, wn(n, r, o, f), r.child;
  }
  var Hs = { dehydrated: null, treeContext: null, retryLane: 0 };
  function Sl(n) {
    return { baseLanes: n, cachePool: null, transitions: null };
  }
  function Ah(n, r, o) {
    var f = r.pendingProps, v = on.current, g = !1, w = (r.flags & 128) !== 0, O;
    if ((O = w) || (O = n !== null && n.memoizedState === null ? !1 : (v & 2) !== 0), O ? (g = !0, r.flags &= -129) : (n === null || n.memoizedState !== null) && (v |= 1), Ht(on, v & 1), n === null)
      return Vc(r), n = r.memoizedState, n !== null && (n = n.dehydrated, n !== null) ? (r.mode & 1 ? n.data === "$!" ? r.lanes = 8 : r.lanes = 1073741824 : r.lanes = 1, null) : (w = f.children, n = f.fallback, g ? (f = r.mode, g = r.child, w = { mode: "hidden", children: w }, !(f & 1) && g !== null ? (g.childLanes = 0, g.pendingProps = w) : g = Mf(w, f, 0, null), n = Rl(n, f, o, null), g.return = r, n.return = r, g.sibling = n, r.child = g, r.child.memoizedState = Sl(o), r.memoizedState = Hs, n) : hf(r, w));
    if (v = n.memoizedState, v !== null && (O = v.dehydrated, O !== null)) return dp(n, r, w, f, O, v, o);
    if (g) {
      g = f.fallback, w = r.mode, v = n.child, O = v.sibling;
      var N = { mode: "hidden", children: f.children };
      return !(w & 1) && r.child !== v ? (f = r.child, f.childLanes = 0, f.pendingProps = N, r.deletions = null) : (f = zo(v, N), f.subtreeFlags = v.subtreeFlags & 14680064), O !== null ? g = zo(O, g) : (g = Rl(g, w, o, null), g.flags |= 2), g.return = r, f.return = r, f.sibling = g, r.child = f, f = g, g = r.child, w = n.child.memoizedState, w = w === null ? Sl(o) : { baseLanes: w.baseLanes | o, cachePool: null, transitions: w.transitions }, g.memoizedState = w, g.childLanes = n.childLanes & ~o, r.memoizedState = Hs, f;
    }
    return g = n.child, n = g.sibling, f = zo(g, { mode: "visible", children: f.children }), !(r.mode & 1) && (f.lanes = o), f.return = r, f.sibling = null, n !== null && (o = r.deletions, o === null ? (r.deletions = [n], r.flags |= 16) : o.push(n)), r.child = f, r.memoizedState = null, f;
  }
  function hf(n, r) {
    return r = Mf({ mode: "visible", children: r }, n.mode, 0, null), r.return = n, n.child = r;
  }
  function mf(n, r, o, f) {
    return f !== null && tp(f), su(r, n.child, null, o), n = hf(r, r.pendingProps.children), n.flags |= 2, r.memoizedState = null, n;
  }
  function dp(n, r, o, f, v, g, w) {
    if (o)
      return r.flags & 256 ? (r.flags &= -257, f = cp(Error(c(422))), mf(n, r, w, f)) : r.memoizedState !== null ? (r.child = n.child, r.flags |= 128, null) : (g = f.fallback, v = r.mode, f = Mf({ mode: "visible", children: f.children }, v, 0, null), g = Rl(g, v, w, null), g.flags |= 2, f.return = r, g.return = r, f.sibling = g, r.child = f, r.mode & 1 && su(r, n.child, null, w), r.child.memoizedState = Sl(w), r.memoizedState = Hs, g);
    if (!(r.mode & 1)) return mf(n, r, w, null);
    if (v.data === "$!") {
      if (f = v.nextSibling && v.nextSibling.dataset, f) var O = f.dgst;
      return f = O, g = Error(c(419)), f = cp(g, f, void 0), mf(n, r, w, f);
    }
    if (O = (w & n.childLanes) !== 0, Ir || O) {
      if (f = gn, f !== null) {
        switch (w & -w) {
          case 4:
            v = 2;
            break;
          case 16:
            v = 8;
            break;
          case 64:
          case 128:
          case 256:
          case 512:
          case 1024:
          case 2048:
          case 4096:
          case 8192:
          case 16384:
          case 32768:
          case 65536:
          case 131072:
          case 262144:
          case 524288:
          case 1048576:
          case 2097152:
          case 4194304:
          case 8388608:
          case 16777216:
          case 33554432:
          case 67108864:
            v = 32;
            break;
          case 536870912:
            v = 268435456;
            break;
          default:
            v = 0;
        }
        v = v & (f.suspendedLanes | w) ? 0 : v, v !== 0 && v !== g.retryLane && (g.retryLane = v, ji(n, v), dn(f, n, v, -1));
      }
      return Ys(), f = cp(Error(c(421))), mf(n, r, w, f);
    }
    return v.data === "$?" ? (r.flags |= 128, r.child = n.child, r = wp.bind(null, n), v._reactRetry = r, null) : (n = g.treeContext, Vr = Xa(v.nextSibling), aa = r, en = !0, Ca = null, n !== null && (jr[lr++] = Gn, jr[lr++] = Fi, jr[lr++] = ba, Gn = n.id, Fi = n.overflow, ba = r), r = hf(r, f.children), r.flags |= 4096, r);
  }
  function zh(n, r, o) {
    n.lanes |= r;
    var f = n.alternate;
    f !== null && (f.lanes |= r), rp(n.return, r, o);
  }
  function yf(n, r, o, f, v) {
    var g = n.memoizedState;
    g === null ? n.memoizedState = { isBackwards: r, rendering: null, renderingStartTime: 0, last: f, tail: o, tailMode: v } : (g.isBackwards = r, g.rendering = null, g.renderingStartTime = 0, g.last = f, g.tail = o, g.tailMode = v);
  }
  function pp(n, r, o) {
    var f = r.pendingProps, v = f.revealOrder, g = f.tail;
    if (wn(n, r, f.children, o), f = on.current, f & 2) f = f & 1 | 2, r.flags |= 128;
    else {
      if (n !== null && n.flags & 128) e: for (n = r.child; n !== null; ) {
        if (n.tag === 13) n.memoizedState !== null && zh(n, o, r);
        else if (n.tag === 19) zh(n, o, r);
        else if (n.child !== null) {
          n.child.return = n, n = n.child;
          continue;
        }
        if (n === r) break e;
        for (; n.sibling === null; ) {
          if (n.return === null || n.return === r) break e;
          n = n.return;
        }
        n.sibling.return = n.return, n = n.sibling;
      }
      f &= 1;
    }
    if (Ht(on, f), !(r.mode & 1)) r.memoizedState = null;
    else switch (v) {
      case "forwards":
        for (o = r.child, v = null; o !== null; ) n = o.alternate, n !== null && Gc(n) === null && (v = o), o = o.sibling;
        o = v, o === null ? (v = r.child, r.child = null) : (v = o.sibling, o.sibling = null), yf(r, !1, v, o, g);
        break;
      case "backwards":
        for (o = null, v = r.child, r.child = null; v !== null; ) {
          if (n = v.alternate, n !== null && Gc(n) === null) {
            r.child = v;
            break;
          }
          n = v.sibling, v.sibling = o, o = v, v = n;
        }
        yf(r, !0, o, null, g);
        break;
      case "together":
        yf(r, !1, null, null, void 0);
        break;
      default:
        r.memoizedState = null;
    }
    return r.child;
  }
  function js(n, r) {
    !(r.mode & 1) && n !== null && (n.alternate = null, r.alternate = null, r.flags |= 2);
  }
  function Tn(n, r, o) {
    if (n !== null && (r.dependencies = n.dependencies), bl |= r.lanes, !(o & r.childLanes)) return null;
    if (n !== null && r.child !== n.child) throw Error(c(153));
    if (r.child !== null) {
      for (n = r.child, o = zo(n, n.pendingProps), r.child = o, o.return = r; n.sibling !== null; ) n = n.sibling, o = o.sibling = zo(n, n.pendingProps), o.return = r;
      o.sibling = null;
    }
    return r.child;
  }
  function $i(n, r, o) {
    switch (r.tag) {
      case 3:
        vg(r), fn();
        break;
      case 5:
        wh(r);
        break;
      case 1:
        an(r.type) && Fc(r);
        break;
      case 4:
        ap(r, r.stateNode.containerInfo);
        break;
      case 10:
        var f = r.type._context, v = r.memoizedProps.value;
        Ht(Hi, f._currentValue), f._currentValue = v;
        break;
      case 13:
        if (f = r.memoizedState, f !== null)
          return f.dehydrated !== null ? (Ht(on, on.current & 1), r.flags |= 128, null) : o & r.child.childLanes ? Ah(n, r, o) : (Ht(on, on.current & 1), n = Tn(n, r, o), n !== null ? n.sibling : null);
        Ht(on, on.current & 1);
        break;
      case 19:
        if (f = (o & r.childLanes) !== 0, n.flags & 128) {
          if (f) return pp(n, r, o);
          r.flags |= 128;
        }
        if (v = r.memoizedState, v !== null && (v.rendering = null, v.tail = null, v.lastEffect = null), Ht(on, on.current), f) break;
        return null;
      case 22:
      case 23:
        return r.lanes = 0, Yr(n, r, o);
    }
    return Tn(n, r, o);
  }
  var fi, Cu, wu, xa;
  fi = function(n, r) {
    for (var o = r.child; o !== null; ) {
      if (o.tag === 5 || o.tag === 6) n.appendChild(o.stateNode);
      else if (o.tag !== 4 && o.child !== null) {
        o.child.return = o, o = o.child;
        continue;
      }
      if (o === r) break;
      for (; o.sibling === null; ) {
        if (o.return === null || o.return === r) return;
        o = o.return;
      }
      o.sibling.return = o.return, o = o.sibling;
    }
  }, Cu = function() {
  }, wu = function(n, r, o, f) {
    var v = n.memoizedProps;
    if (v !== f) {
      n = r.stateNode, dl(Ka.current);
      var g = null;
      switch (o) {
        case "input":
          v = ir(n, v), f = ir(n, f), g = [];
          break;
        case "select":
          v = Te({}, v, { value: void 0 }), f = Te({}, f, { value: void 0 }), g = [];
          break;
        case "textarea":
          v = ni(n, v), f = ni(n, f), g = [];
          break;
        default:
          typeof v.onClick != "function" && typeof f.onClick == "function" && (n.onclick = Pc);
      }
      Mn(o, f);
      var w;
      o = null;
      for (K in v) if (!f.hasOwnProperty(K) && v.hasOwnProperty(K) && v[K] != null) if (K === "style") {
        var O = v[K];
        for (w in O) O.hasOwnProperty(w) && (o || (o = {}), o[w] = "");
      } else K !== "dangerouslySetInnerHTML" && K !== "children" && K !== "suppressContentEditableWarning" && K !== "suppressHydrationWarning" && K !== "autoFocus" && (y.hasOwnProperty(K) ? g || (g = []) : (g = g || []).push(K, null));
      for (K in f) {
        var N = f[K];
        if (O = v != null ? v[K] : void 0, f.hasOwnProperty(K) && N !== O && (N != null || O != null)) if (K === "style") if (O) {
          for (w in O) !O.hasOwnProperty(w) || N && N.hasOwnProperty(w) || (o || (o = {}), o[w] = "");
          for (w in N) N.hasOwnProperty(w) && O[w] !== N[w] && (o || (o = {}), o[w] = N[w]);
        } else o || (g || (g = []), g.push(
          K,
          o
        )), o = N;
        else K === "dangerouslySetInnerHTML" ? (N = N ? N.__html : void 0, O = O ? O.__html : void 0, N != null && O !== N && (g = g || []).push(K, N)) : K === "children" ? typeof N != "string" && typeof N != "number" || (g = g || []).push(K, "" + N) : K !== "suppressContentEditableWarning" && K !== "suppressHydrationWarning" && (y.hasOwnProperty(K) ? (N != null && K === "onScroll" && Gt("scroll", n), g || O === N || (g = [])) : (g = g || []).push(K, N));
      }
      o && (g = g || []).push("style", o);
      var K = g;
      (r.updateQueue = K) && (r.flags |= 4);
    }
  }, xa = function(n, r, o, f) {
    o !== f && (r.flags |= 4);
  };
  function yn(n, r) {
    if (!en) switch (n.tailMode) {
      case "hidden":
        r = n.tail;
        for (var o = null; r !== null; ) r.alternate !== null && (o = r), r = r.sibling;
        o === null ? n.tail = null : o.sibling = null;
        break;
      case "collapsed":
        o = n.tail;
        for (var f = null; o !== null; ) o.alternate !== null && (f = o), o = o.sibling;
        f === null ? r || n.tail === null ? n.tail = null : n.tail.sibling = null : f.sibling = null;
    }
  }
  function cr(n) {
    var r = n.alternate !== null && n.alternate.child === n.child, o = 0, f = 0;
    if (r) for (var v = n.child; v !== null; ) o |= v.lanes | v.childLanes, f |= v.subtreeFlags & 14680064, f |= v.flags & 14680064, v.return = n, v = v.sibling;
    else for (v = n.child; v !== null; ) o |= v.lanes | v.childLanes, f |= v.subtreeFlags, f |= v.flags, v.return = n, v = v.sibling;
    return n.subtreeFlags |= f, n.childLanes = o, r;
  }
  function hg(n, r, o) {
    var f = r.pendingProps;
    switch (Jd(r), r.tag) {
      case 2:
      case 16:
      case 15:
      case 0:
      case 11:
      case 7:
      case 8:
      case 12:
      case 9:
      case 14:
        return cr(r), null;
      case 1:
        return an(r.type) && Ea(), cr(r), null;
      case 3:
        return f = r.stateNode, pu(), Pt(mn), Pt(tt), op(), f.pendingContext && (f.context = f.pendingContext, f.pendingContext = null), (n === null || n.child === null) && ($c(r) ? r.flags |= 4 : n === null || n.memoizedState.isDehydrated && !(r.flags & 256) || (r.flags |= 1024, Ca !== null && (Ep(Ca), Ca = null))), Cu(n, r), cr(r), null;
      case 5:
        ip(r);
        var v = dl(Os.current);
        if (o = r.type, n !== null && r.stateNode != null) wu(n, r, o, f, v), n.ref !== r.ref && (r.flags |= 512, r.flags |= 2097152);
        else {
          if (!f) {
            if (r.stateNode === null) throw Error(c(166));
            return cr(r), null;
          }
          if (n = dl(Ka.current), $c(r)) {
            f = r.stateNode, o = r.type;
            var g = r.memoizedProps;
            switch (f[ui] = r, f[ul] = g, n = (r.mode & 1) !== 0, o) {
              case "dialog":
                Gt("cancel", f), Gt("close", f);
                break;
              case "iframe":
              case "object":
              case "embed":
                Gt("load", f);
                break;
              case "video":
              case "audio":
                for (v = 0; v < bs.length; v++) Gt(bs[v], f);
                break;
              case "source":
                Gt("error", f);
                break;
              case "img":
              case "image":
              case "link":
                Gt(
                  "error",
                  f
                ), Gt("load", f);
                break;
              case "details":
                Gt("toggle", f);
                break;
              case "input":
                kr(f, g), Gt("invalid", f);
                break;
              case "select":
                f._wrapperState = { wasMultiple: !!g.multiple }, Gt("invalid", f);
                break;
              case "textarea":
                Or(f, g), Gt("invalid", f);
            }
            Mn(o, g), v = null;
            for (var w in g) if (g.hasOwnProperty(w)) {
              var O = g[w];
              w === "children" ? typeof O == "string" ? f.textContent !== O && (g.suppressHydrationWarning !== !0 && Uc(f.textContent, O, n), v = ["children", O]) : typeof O == "number" && f.textContent !== "" + O && (g.suppressHydrationWarning !== !0 && Uc(
                f.textContent,
                O,
                n
              ), v = ["children", "" + O]) : y.hasOwnProperty(w) && O != null && w === "onScroll" && Gt("scroll", f);
            }
            switch (o) {
              case "input":
                xr(f), Ha(f, g, !0);
                break;
              case "textarea":
                xr(f), Mr(f);
                break;
              case "select":
              case "option":
                break;
              default:
                typeof g.onClick == "function" && (f.onclick = Pc);
            }
            f = v, r.updateQueue = f, f !== null && (r.flags |= 4);
          } else {
            w = v.nodeType === 9 ? v : v.ownerDocument, n === "http://www.w3.org/1999/xhtml" && (n = Va(o)), n === "http://www.w3.org/1999/xhtml" ? o === "script" ? (n = w.createElement("div"), n.innerHTML = "<script><\/script>", n = n.removeChild(n.firstChild)) : typeof f.is == "string" ? n = w.createElement(o, { is: f.is }) : (n = w.createElement(o), o === "select" && (w = n, f.multiple ? w.multiple = !0 : f.size && (w.size = f.size))) : n = w.createElementNS(n, o), n[ui] = r, n[ul] = f, fi(n, r, !1, !1), r.stateNode = n;
            e: {
              switch (w = Cn(o, f), o) {
                case "dialog":
                  Gt("cancel", n), Gt("close", n), v = f;
                  break;
                case "iframe":
                case "object":
                case "embed":
                  Gt("load", n), v = f;
                  break;
                case "video":
                case "audio":
                  for (v = 0; v < bs.length; v++) Gt(bs[v], n);
                  v = f;
                  break;
                case "source":
                  Gt("error", n), v = f;
                  break;
                case "img":
                case "image":
                case "link":
                  Gt(
                    "error",
                    n
                  ), Gt("load", n), v = f;
                  break;
                case "details":
                  Gt("toggle", n), v = f;
                  break;
                case "input":
                  kr(n, f), v = ir(n, f), Gt("invalid", n);
                  break;
                case "option":
                  v = f;
                  break;
                case "select":
                  n._wrapperState = { wasMultiple: !!f.multiple }, v = Te({}, f, { value: void 0 }), Gt("invalid", n);
                  break;
                case "textarea":
                  Or(n, f), v = ni(n, f), Gt("invalid", n);
                  break;
                default:
                  v = f;
              }
              Mn(o, v), O = v;
              for (g in O) if (O.hasOwnProperty(g)) {
                var N = O[g];
                g === "style" ? Vt(n, N) : g === "dangerouslySetInnerHTML" ? (N = N ? N.__html : void 0, N != null && Xo(n, N)) : g === "children" ? typeof N == "string" ? (o !== "textarea" || N !== "") && ri(n, N) : typeof N == "number" && ri(n, "" + N) : g !== "suppressContentEditableWarning" && g !== "suppressHydrationWarning" && g !== "autoFocus" && (y.hasOwnProperty(g) ? N != null && g === "onScroll" && Gt("scroll", n) : N != null && fe(n, g, N, w));
              }
              switch (o) {
                case "input":
                  xr(n), Ha(n, f, !1);
                  break;
                case "textarea":
                  xr(n), Mr(n);
                  break;
                case "option":
                  f.value != null && n.setAttribute("value", "" + xt(f.value));
                  break;
                case "select":
                  n.multiple = !!f.multiple, g = f.value, g != null ? Dr(n, !!f.multiple, g, !1) : f.defaultValue != null && Dr(
                    n,
                    !!f.multiple,
                    f.defaultValue,
                    !0
                  );
                  break;
                default:
                  typeof v.onClick == "function" && (n.onclick = Pc);
              }
              switch (o) {
                case "button":
                case "input":
                case "select":
                case "textarea":
                  f = !!f.autoFocus;
                  break e;
                case "img":
                  f = !0;
                  break e;
                default:
                  f = !1;
              }
            }
            f && (r.flags |= 4);
          }
          r.ref !== null && (r.flags |= 512, r.flags |= 2097152);
        }
        return cr(r), null;
      case 6:
        if (n && r.stateNode != null) xa(n, r, n.memoizedProps, f);
        else {
          if (typeof f != "string" && r.stateNode === null) throw Error(c(166));
          if (o = dl(Os.current), dl(Ka.current), $c(r)) {
            if (f = r.stateNode, o = r.memoizedProps, f[ui] = r, (g = f.nodeValue !== o) && (n = aa, n !== null)) switch (n.tag) {
              case 3:
                Uc(f.nodeValue, o, (n.mode & 1) !== 0);
                break;
              case 5:
                n.memoizedProps.suppressHydrationWarning !== !0 && Uc(f.nodeValue, o, (n.mode & 1) !== 0);
            }
            g && (r.flags |= 4);
          } else f = (o.nodeType === 9 ? o : o.ownerDocument).createTextNode(f), f[ui] = r, r.stateNode = f;
        }
        return cr(r), null;
      case 13:
        if (Pt(on), f = r.memoizedState, n === null || n.memoizedState !== null && n.memoizedState.dehydrated !== null) {
          if (en && Vr !== null && r.mode & 1 && !(r.flags & 128)) yh(), fn(), r.flags |= 98560, g = !1;
          else if (g = $c(r), f !== null && f.dehydrated !== null) {
            if (n === null) {
              if (!g) throw Error(c(318));
              if (g = r.memoizedState, g = g !== null ? g.dehydrated : null, !g) throw Error(c(317));
              g[ui] = r;
            } else fn(), !(r.flags & 128) && (r.memoizedState = null), r.flags |= 4;
            cr(r), g = !1;
          } else Ca !== null && (Ep(Ca), Ca = null), g = !0;
          if (!g) return r.flags & 65536 ? r : null;
        }
        return r.flags & 128 ? (r.lanes = o, r) : (f = f !== null, f !== (n !== null && n.memoizedState !== null) && f && (r.child.flags |= 8192, r.mode & 1 && (n === null || on.current & 1 ? An === 0 && (An = 3) : Ys())), r.updateQueue !== null && (r.flags |= 4), cr(r), null);
      case 4:
        return pu(), Cu(n, r), n === null && ou(r.stateNode.containerInfo), cr(r), null;
      case 10:
        return np(r.type._context), cr(r), null;
      case 17:
        return an(r.type) && Ea(), cr(r), null;
      case 19:
        if (Pt(on), g = r.memoizedState, g === null) return cr(r), null;
        if (f = (r.flags & 128) !== 0, w = g.rendering, w === null) if (f) yn(g, !1);
        else {
          if (An !== 0 || n !== null && n.flags & 128) for (n = r.child; n !== null; ) {
            if (w = Gc(n), w !== null) {
              for (r.flags |= 128, yn(g, !1), f = w.updateQueue, f !== null && (r.updateQueue = f, r.flags |= 4), r.subtreeFlags = 0, f = o, o = r.child; o !== null; ) g = o, n = f, g.flags &= 14680066, w = g.alternate, w === null ? (g.childLanes = 0, g.lanes = n, g.child = null, g.subtreeFlags = 0, g.memoizedProps = null, g.memoizedState = null, g.updateQueue = null, g.dependencies = null, g.stateNode = null) : (g.childLanes = w.childLanes, g.lanes = w.lanes, g.child = w.child, g.subtreeFlags = 0, g.deletions = null, g.memoizedProps = w.memoizedProps, g.memoizedState = w.memoizedState, g.updateQueue = w.updateQueue, g.type = w.type, n = w.dependencies, g.dependencies = n === null ? null : { lanes: n.lanes, firstContext: n.firstContext }), o = o.sibling;
              return Ht(on, on.current & 1 | 2), r.child;
            }
            n = n.sibling;
          }
          g.tail !== null && rn() > Nu && (r.flags |= 128, f = !0, yn(g, !1), r.lanes = 4194304);
        }
        else {
          if (!f) if (n = Gc(w), n !== null) {
            if (r.flags |= 128, f = !0, o = n.updateQueue, o !== null && (r.updateQueue = o, r.flags |= 4), yn(g, !0), g.tail === null && g.tailMode === "hidden" && !w.alternate && !en) return cr(r), null;
          } else 2 * rn() - g.renderingStartTime > Nu && o !== 1073741824 && (r.flags |= 128, f = !0, yn(g, !1), r.lanes = 4194304);
          g.isBackwards ? (w.sibling = r.child, r.child = w) : (o = g.last, o !== null ? o.sibling = w : r.child = w, g.last = w);
        }
        return g.tail !== null ? (r = g.tail, g.rendering = r, g.tail = r.sibling, g.renderingStartTime = rn(), r.sibling = null, o = on.current, Ht(on, f ? o & 1 | 2 : o & 1), r) : (cr(r), null);
      case 22:
      case 23:
        return _f(), f = r.memoizedState !== null, n !== null && n.memoizedState !== null !== f && (r.flags |= 8192), f && r.mode & 1 ? Wr & 1073741824 && (cr(r), r.subtreeFlags & 6 && (r.flags |= 8192)) : cr(r), null;
      case 24:
        return null;
      case 25:
        return null;
    }
    throw Error(c(156, r.tag));
  }
  function mg(n, r) {
    switch (Jd(r), r.tag) {
      case 1:
        return an(r.type) && Ea(), n = r.flags, n & 65536 ? (r.flags = n & -65537 | 128, r) : null;
      case 3:
        return pu(), Pt(mn), Pt(tt), op(), n = r.flags, n & 65536 && !(n & 128) ? (r.flags = n & -65537 | 128, r) : null;
      case 5:
        return ip(r), null;
      case 13:
        if (Pt(on), n = r.memoizedState, n !== null && n.dehydrated !== null) {
          if (r.alternate === null) throw Error(c(340));
          fn();
        }
        return n = r.flags, n & 65536 ? (r.flags = n & -65537 | 128, r) : null;
      case 19:
        return Pt(on), null;
      case 4:
        return pu(), null;
      case 10:
        return np(r.type._context), null;
      case 22:
      case 23:
        return _f(), null;
      case 24:
        return null;
      default:
        return null;
    }
  }
  var Tu = !1, Xn = !1, gf = typeof WeakSet == "function" ? WeakSet : Set, Me = null;
  function Ru(n, r) {
    var o = n.ref;
    if (o !== null) if (typeof o == "function") try {
      o(null);
    } catch (f) {
      Sn(n, r, f);
    }
    else o.current = null;
  }
  function vp(n, r, o) {
    try {
      o();
    } catch (f) {
      Sn(n, r, f);
    }
  }
  var Sf = !1;
  function yg(n, r) {
    if (Yd = ma, n = Mc(), Ai(n)) {
      if ("selectionStart" in n) var o = { start: n.selectionStart, end: n.selectionEnd };
      else e: {
        o = (o = n.ownerDocument) && o.defaultView || window;
        var f = o.getSelection && o.getSelection();
        if (f && f.rangeCount !== 0) {
          o = f.anchorNode;
          var v = f.anchorOffset, g = f.focusNode;
          f = f.focusOffset;
          try {
            o.nodeType, g.nodeType;
          } catch {
            o = null;
            break e;
          }
          var w = 0, O = -1, N = -1, K = 0, ue = 0, se = n, le = null;
          t: for (; ; ) {
            for (var xe; se !== o || v !== 0 && se.nodeType !== 3 || (O = w + v), se !== g || f !== 0 && se.nodeType !== 3 || (N = w + f), se.nodeType === 3 && (w += se.nodeValue.length), (xe = se.firstChild) !== null; )
              le = se, se = xe;
            for (; ; ) {
              if (se === n) break t;
              if (le === o && ++K === v && (O = w), le === g && ++ue === f && (N = w), (xe = se.nextSibling) !== null) break;
              se = le, le = se.parentNode;
            }
            se = xe;
          }
          o = O === -1 || N === -1 ? null : { start: O, end: N };
        } else o = null;
      }
      o = o || { start: 0, end: 0 };
    } else o = null;
    for (ol = { focusedElem: n, selectionRange: o }, ma = !1, Me = r; Me !== null; ) if (r = Me, n = r.child, (r.subtreeFlags & 1028) !== 0 && n !== null) n.return = r, Me = n;
    else for (; Me !== null; ) {
      r = Me;
      try {
        var Le = r.alternate;
        if (r.flags & 1024) switch (r.tag) {
          case 0:
          case 11:
          case 15:
            break;
          case 1:
            if (Le !== null) {
              var ze = Le.memoizedProps, Rn = Le.memoizedState, B = r.stateNode, U = B.getSnapshotBeforeUpdate(r.elementType === r.type ? ze : Br(r.type, ze), Rn);
              B.__reactInternalSnapshotBeforeUpdate = U;
            }
            break;
          case 3:
            var Q = r.stateNode.containerInfo;
            Q.nodeType === 1 ? Q.textContent = "" : Q.nodeType === 9 && Q.documentElement && Q.removeChild(Q.documentElement);
            break;
          case 5:
          case 6:
          case 4:
          case 17:
            break;
          default:
            throw Error(c(163));
        }
      } catch (pe) {
        Sn(r, r.return, pe);
      }
      if (n = r.sibling, n !== null) {
        n.return = r.return, Me = n;
        break;
      }
      Me = r.return;
    }
    return Le = Sf, Sf = !1, Le;
  }
  function xu(n, r, o) {
    var f = r.updateQueue;
    if (f = f !== null ? f.lastEffect : null, f !== null) {
      var v = f = f.next;
      do {
        if ((v.tag & n) === n) {
          var g = v.destroy;
          v.destroy = void 0, g !== void 0 && vp(r, o, g);
        }
        v = v.next;
      } while (v !== f);
    }
  }
  function Ef(n, r) {
    if (r = r.updateQueue, r = r !== null ? r.lastEffect : null, r !== null) {
      var o = r = r.next;
      do {
        if ((o.tag & n) === n) {
          var f = o.create;
          o.destroy = f();
        }
        o = o.next;
      } while (o !== r);
    }
  }
  function bf(n) {
    var r = n.ref;
    if (r !== null) {
      var o = n.stateNode;
      switch (n.tag) {
        case 5:
          n = o;
          break;
        default:
          n = o;
      }
      typeof r == "function" ? r(n) : r.current = n;
    }
  }
  function Uh(n) {
    var r = n.alternate;
    r !== null && (n.alternate = null, Uh(r)), n.child = null, n.deletions = null, n.sibling = null, n.tag === 5 && (r = n.stateNode, r !== null && (delete r[ui], delete r[ul], delete r[Gd], delete r[fg], delete r[qd])), n.stateNode = null, n.return = null, n.dependencies = null, n.memoizedProps = null, n.memoizedState = null, n.pendingProps = null, n.stateNode = null, n.updateQueue = null;
  }
  function hp(n) {
    return n.tag === 5 || n.tag === 3 || n.tag === 4;
  }
  function Ph(n) {
    e: for (; ; ) {
      for (; n.sibling === null; ) {
        if (n.return === null || hp(n.return)) return null;
        n = n.return;
      }
      for (n.sibling.return = n.return, n = n.sibling; n.tag !== 5 && n.tag !== 6 && n.tag !== 18; ) {
        if (n.flags & 2 || n.child === null || n.tag === 4) continue e;
        n.child.return = n, n = n.child;
      }
      if (!(n.flags & 2)) return n.stateNode;
    }
  }
  function Vs(n, r, o) {
    var f = n.tag;
    if (f === 5 || f === 6) n = n.stateNode, r ? o.nodeType === 8 ? o.parentNode.insertBefore(n, r) : o.insertBefore(n, r) : (o.nodeType === 8 ? (r = o.parentNode, r.insertBefore(n, o)) : (r = o, r.appendChild(n)), o = o._reactRootContainer, o != null || r.onclick !== null || (r.onclick = Pc));
    else if (f !== 4 && (n = n.child, n !== null)) for (Vs(n, r, o), n = n.sibling; n !== null; ) Vs(n, r, o), n = n.sibling;
  }
  function ku(n, r, o) {
    var f = n.tag;
    if (f === 5 || f === 6) n = n.stateNode, r ? o.insertBefore(n, r) : o.appendChild(n);
    else if (f !== 4 && (n = n.child, n !== null)) for (ku(n, r, o), n = n.sibling; n !== null; ) ku(n, r, o), n = n.sibling;
  }
  var ln = null, $n = !1;
  function hr(n, r, o) {
    for (o = o.child; o !== null; ) _u(n, r, o), o = o.sibling;
  }
  function _u(n, r, o) {
    if (Nr && typeof Nr.onCommitFiberUnmount == "function") try {
      Nr.onCommitFiberUnmount(fo, o);
    } catch {
    }
    switch (o.tag) {
      case 5:
        Xn || Ru(o, r);
      case 6:
        var f = ln, v = $n;
        ln = null, hr(n, r, o), ln = f, $n = v, ln !== null && ($n ? (n = ln, o = o.stateNode, n.nodeType === 8 ? n.parentNode.removeChild(o) : n.removeChild(o)) : ln.removeChild(o.stateNode));
        break;
      case 18:
        ln !== null && ($n ? (n = ln, o = o.stateNode, n.nodeType === 8 ? wo(n.parentNode, o) : n.nodeType === 1 && wo(n, o), So(n)) : wo(ln, o.stateNode));
        break;
      case 4:
        f = ln, v = $n, ln = o.stateNode.containerInfo, $n = !0, hr(n, r, o), ln = f, $n = v;
        break;
      case 0:
      case 11:
      case 14:
      case 15:
        if (!Xn && (f = o.updateQueue, f !== null && (f = f.lastEffect, f !== null))) {
          v = f = f.next;
          do {
            var g = v, w = g.destroy;
            g = g.tag, w !== void 0 && (g & 2 || g & 4) && vp(o, r, w), v = v.next;
          } while (v !== f);
        }
        hr(n, r, o);
        break;
      case 1:
        if (!Xn && (Ru(o, r), f = o.stateNode, typeof f.componentWillUnmount == "function")) try {
          f.props = o.memoizedProps, f.state = o.memoizedState, f.componentWillUnmount();
        } catch (O) {
          Sn(o, r, O);
        }
        hr(n, r, o);
        break;
      case 21:
        hr(n, r, o);
        break;
      case 22:
        o.mode & 1 ? (Xn = (f = Xn) || o.memoizedState !== null, hr(n, r, o), Xn = f) : hr(n, r, o);
        break;
      default:
        hr(n, r, o);
    }
  }
  function Du(n) {
    var r = n.updateQueue;
    if (r !== null) {
      n.updateQueue = null;
      var o = n.stateNode;
      o === null && (o = n.stateNode = new gf()), r.forEach(function(f) {
        var v = Tg.bind(null, n, f);
        o.has(f) || (o.add(f), f.then(v, v));
      });
    }
  }
  function Bn(n, r) {
    var o = r.deletions;
    if (o !== null) for (var f = 0; f < o.length; f++) {
      var v = o[f];
      try {
        var g = n, w = r, O = w;
        e: for (; O !== null; ) {
          switch (O.tag) {
            case 5:
              ln = O.stateNode, $n = !1;
              break e;
            case 3:
              ln = O.stateNode.containerInfo, $n = !0;
              break e;
            case 4:
              ln = O.stateNode.containerInfo, $n = !0;
              break e;
          }
          O = O.return;
        }
        if (ln === null) throw Error(c(160));
        _u(g, w, v), ln = null, $n = !1;
        var N = v.alternate;
        N !== null && (N.return = null), v.return = null;
      } catch (K) {
        Sn(v, r, K);
      }
    }
    if (r.subtreeFlags & 12854) for (r = r.child; r !== null; ) Fh(r, n), r = r.sibling;
  }
  function Fh(n, r) {
    var o = n.alternate, f = n.flags;
    switch (n.tag) {
      case 0:
      case 11:
      case 14:
      case 15:
        if (Bn(r, n), di(n), f & 4) {
          try {
            xu(3, n, n.return), Ef(3, n);
          } catch (ze) {
            Sn(n, n.return, ze);
          }
          try {
            xu(5, n, n.return);
          } catch (ze) {
            Sn(n, n.return, ze);
          }
        }
        break;
      case 1:
        Bn(r, n), di(n), f & 512 && o !== null && Ru(o, o.return);
        break;
      case 5:
        if (Bn(r, n), di(n), f & 512 && o !== null && Ru(o, o.return), n.flags & 32) {
          var v = n.stateNode;
          try {
            ri(v, "");
          } catch (ze) {
            Sn(n, n.return, ze);
          }
        }
        if (f & 4 && (v = n.stateNode, v != null)) {
          var g = n.memoizedProps, w = o !== null ? o.memoizedProps : g, O = n.type, N = n.updateQueue;
          if (n.updateQueue = null, N !== null) try {
            O === "input" && g.type === "radio" && g.name != null && _r(v, g), Cn(O, w);
            var K = Cn(O, g);
            for (w = 0; w < N.length; w += 2) {
              var ue = N[w], se = N[w + 1];
              ue === "style" ? Vt(v, se) : ue === "dangerouslySetInnerHTML" ? Xo(v, se) : ue === "children" ? ri(v, se) : fe(v, ue, se, K);
            }
            switch (O) {
              case "input":
                Fn(v, g);
                break;
              case "textarea":
                ja(v, g);
                break;
              case "select":
                var le = v._wrapperState.wasMultiple;
                v._wrapperState.wasMultiple = !!g.multiple;
                var xe = g.value;
                xe != null ? Dr(v, !!g.multiple, xe, !1) : le !== !!g.multiple && (g.defaultValue != null ? Dr(
                  v,
                  !!g.multiple,
                  g.defaultValue,
                  !0
                ) : Dr(v, !!g.multiple, g.multiple ? [] : "", !1));
            }
            v[ul] = g;
          } catch (ze) {
            Sn(n, n.return, ze);
          }
        }
        break;
      case 6:
        if (Bn(r, n), di(n), f & 4) {
          if (n.stateNode === null) throw Error(c(162));
          v = n.stateNode, g = n.memoizedProps;
          try {
            v.nodeValue = g;
          } catch (ze) {
            Sn(n, n.return, ze);
          }
        }
        break;
      case 3:
        if (Bn(r, n), di(n), f & 4 && o !== null && o.memoizedState.isDehydrated) try {
          So(r.containerInfo);
        } catch (ze) {
          Sn(n, n.return, ze);
        }
        break;
      case 4:
        Bn(r, n), di(n);
        break;
      case 13:
        Bn(r, n), di(n), v = n.child, v.flags & 8192 && (g = v.memoizedState !== null, v.stateNode.isHidden = g, !g || v.alternate !== null && v.alternate.memoizedState !== null || (wf = rn())), f & 4 && Du(n);
        break;
      case 22:
        if (ue = o !== null && o.memoizedState !== null, n.mode & 1 ? (Xn = (K = Xn) || ue, Bn(r, n), Xn = K) : Bn(r, n), di(n), f & 8192) {
          if (K = n.memoizedState !== null, (n.stateNode.isHidden = K) && !ue && n.mode & 1) for (Me = n, ue = n.child; ue !== null; ) {
            for (se = Me = ue; Me !== null; ) {
              switch (le = Me, xe = le.child, le.tag) {
                case 0:
                case 11:
                case 14:
                case 15:
                  xu(4, le, le.return);
                  break;
                case 1:
                  Ru(le, le.return);
                  var Le = le.stateNode;
                  if (typeof Le.componentWillUnmount == "function") {
                    f = le, o = le.return;
                    try {
                      r = f, Le.props = r.memoizedProps, Le.state = r.memoizedState, Le.componentWillUnmount();
                    } catch (ze) {
                      Sn(f, o, ze);
                    }
                  }
                  break;
                case 5:
                  Ru(le, le.return);
                  break;
                case 22:
                  if (le.memoizedState !== null) {
                    Hh(se);
                    continue;
                  }
              }
              xe !== null ? (xe.return = le, Me = xe) : Hh(se);
            }
            ue = ue.sibling;
          }
          e: for (ue = null, se = n; ; ) {
            if (se.tag === 5) {
              if (ue === null) {
                ue = se;
                try {
                  v = se.stateNode, K ? (g = v.style, typeof g.setProperty == "function" ? g.setProperty("display", "none", "important") : g.display = "none") : (O = se.stateNode, N = se.memoizedProps.style, w = N != null && N.hasOwnProperty("display") ? N.display : null, O.style.display = ft("display", w));
                } catch (ze) {
                  Sn(n, n.return, ze);
                }
              }
            } else if (se.tag === 6) {
              if (ue === null) try {
                se.stateNode.nodeValue = K ? "" : se.memoizedProps;
              } catch (ze) {
                Sn(n, n.return, ze);
              }
            } else if ((se.tag !== 22 && se.tag !== 23 || se.memoizedState === null || se === n) && se.child !== null) {
              se.child.return = se, se = se.child;
              continue;
            }
            if (se === n) break e;
            for (; se.sibling === null; ) {
              if (se.return === null || se.return === n) break e;
              ue === se && (ue = null), se = se.return;
            }
            ue === se && (ue = null), se.sibling.return = se.return, se = se.sibling;
          }
        }
        break;
      case 19:
        Bn(r, n), di(n), f & 4 && Du(n);
        break;
      case 21:
        break;
      default:
        Bn(
          r,
          n
        ), di(n);
    }
  }
  function di(n) {
    var r = n.flags;
    if (r & 2) {
      try {
        e: {
          for (var o = n.return; o !== null; ) {
            if (hp(o)) {
              var f = o;
              break e;
            }
            o = o.return;
          }
          throw Error(c(160));
        }
        switch (f.tag) {
          case 5:
            var v = f.stateNode;
            f.flags & 32 && (ri(v, ""), f.flags &= -33);
            var g = Ph(n);
            ku(n, g, v);
            break;
          case 3:
          case 4:
            var w = f.stateNode.containerInfo, O = Ph(n);
            Vs(n, O, w);
            break;
          default:
            throw Error(c(161));
        }
      } catch (N) {
        Sn(n, n.return, N);
      }
      n.flags &= -3;
    }
    r & 4096 && (n.flags &= -4097);
  }
  function gg(n, r, o) {
    Me = n, mp(n);
  }
  function mp(n, r, o) {
    for (var f = (n.mode & 1) !== 0; Me !== null; ) {
      var v = Me, g = v.child;
      if (v.tag === 22 && f) {
        var w = v.memoizedState !== null || Tu;
        if (!w) {
          var O = v.alternate, N = O !== null && O.memoizedState !== null || Xn;
          O = Tu;
          var K = Xn;
          if (Tu = w, (Xn = N) && !K) for (Me = v; Me !== null; ) w = Me, N = w.child, w.tag === 22 && w.memoizedState !== null ? yp(v) : N !== null ? (N.return = w, Me = N) : yp(v);
          for (; g !== null; ) Me = g, mp(g), g = g.sibling;
          Me = v, Tu = O, Xn = K;
        }
        Ou(n);
      } else v.subtreeFlags & 8772 && g !== null ? (g.return = v, Me = g) : Ou(n);
    }
  }
  function Ou(n) {
    for (; Me !== null; ) {
      var r = Me;
      if (r.flags & 8772) {
        var o = r.alternate;
        try {
          if (r.flags & 8772) switch (r.tag) {
            case 0:
            case 11:
            case 15:
              Xn || Ef(5, r);
              break;
            case 1:
              var f = r.stateNode;
              if (r.flags & 4 && !Xn) if (o === null) f.componentDidMount();
              else {
                var v = r.elementType === r.type ? o.memoizedProps : Br(r.type, o.memoizedProps);
                f.componentDidUpdate(v, o.memoizedState, f.__reactInternalSnapshotBeforeUpdate);
              }
              var g = r.updateQueue;
              g !== null && Ch(r, g, f);
              break;
            case 3:
              var w = r.updateQueue;
              if (w !== null) {
                if (o = null, r.child !== null) switch (r.child.tag) {
                  case 5:
                    o = r.child.stateNode;
                    break;
                  case 1:
                    o = r.child.stateNode;
                }
                Ch(r, w, o);
              }
              break;
            case 5:
              var O = r.stateNode;
              if (o === null && r.flags & 4) {
                o = O;
                var N = r.memoizedProps;
                switch (r.type) {
                  case "button":
                  case "input":
                  case "select":
                  case "textarea":
                    N.autoFocus && o.focus();
                    break;
                  case "img":
                    N.src && (o.src = N.src);
                }
              }
              break;
            case 6:
              break;
            case 4:
              break;
            case 12:
              break;
            case 13:
              if (r.memoizedState === null) {
                var K = r.alternate;
                if (K !== null) {
                  var ue = K.memoizedState;
                  if (ue !== null) {
                    var se = ue.dehydrated;
                    se !== null && So(se);
                  }
                }
              }
              break;
            case 19:
            case 17:
            case 21:
            case 22:
            case 23:
            case 25:
              break;
            default:
              throw Error(c(163));
          }
          Xn || r.flags & 512 && bf(r);
        } catch (le) {
          Sn(r, r.return, le);
        }
      }
      if (r === n) {
        Me = null;
        break;
      }
      if (o = r.sibling, o !== null) {
        o.return = r.return, Me = o;
        break;
      }
      Me = r.return;
    }
  }
  function Hh(n) {
    for (; Me !== null; ) {
      var r = Me;
      if (r === n) {
        Me = null;
        break;
      }
      var o = r.sibling;
      if (o !== null) {
        o.return = r.return, Me = o;
        break;
      }
      Me = r.return;
    }
  }
  function yp(n) {
    for (; Me !== null; ) {
      var r = Me;
      try {
        switch (r.tag) {
          case 0:
          case 11:
          case 15:
            var o = r.return;
            try {
              Ef(4, r);
            } catch (N) {
              Sn(r, o, N);
            }
            break;
          case 1:
            var f = r.stateNode;
            if (typeof f.componentDidMount == "function") {
              var v = r.return;
              try {
                f.componentDidMount();
              } catch (N) {
                Sn(r, v, N);
              }
            }
            var g = r.return;
            try {
              bf(r);
            } catch (N) {
              Sn(r, g, N);
            }
            break;
          case 5:
            var w = r.return;
            try {
              bf(r);
            } catch (N) {
              Sn(r, w, N);
            }
        }
      } catch (N) {
        Sn(r, r.return, N);
      }
      if (r === n) {
        Me = null;
        break;
      }
      var O = r.sibling;
      if (O !== null) {
        O.return = r.return, Me = O;
        break;
      }
      Me = r.return;
    }
  }
  var Sg = Math.ceil, El = oe.ReactCurrentDispatcher, Cf = oe.ReactCurrentOwner, ka = oe.ReactCurrentBatchConfig, gt = 0, gn = null, tn = null, Nn = 0, Wr = 0, Mu = st(0), An = 0, $s = null, bl = 0, Lu = 0, gp = 0, No = null, fr = null, wf = 0, Nu = 1 / 0, Bi = null, Tf = !1, Sp = null, _a = null, Au = !1, Da = null, Rf = 0, Bs = 0, xf = null, Is = -1, Cl = 0;
  function In() {
    return gt & 6 ? rn() : Is !== -1 ? Is : Is = rn();
  }
  function Ii(n) {
    return n.mode & 1 ? gt & 2 && Nn !== 0 ? Nn & -Nn : Bc.transition !== null ? (Cl === 0 && (Cl = Wl()), Cl) : (n = Nt, n !== 0 || (n = window.event, n = n === void 0 ? 16 : ds(n.type)), n) : 1;
  }
  function dn(n, r, o, f) {
    if (50 < Bs) throw Bs = 0, xf = null, Error(c(185));
    _i(n, o, f), (!(gt & 2) || n !== gn) && (n === gn && (!(gt & 2) && (Lu |= o), An === 4 && pi(n, Nn)), zn(n, f), o === 1 && gt === 0 && !(r.mode & 1) && (Nu = rn() + 500, Vn && Fr()));
  }
  function zn(n, r) {
    var o = n.callbackNode;
    ho(n, r);
    var f = pr(n, n === gn ? Nn : 0);
    if (f === 0) o !== null && Cc(o), n.callbackNode = null, n.callbackPriority = 0;
    else if (r = f & -f, n.callbackPriority !== r) {
      if (o != null && Cc(o), r === 1) n.tag === 0 ? Kd(zu.bind(null, n)) : Xd(zu.bind(null, n)), Qd(function() {
        !(gt & 6) && Fr();
      }), o = null;
      else {
        switch (Gl(f)) {
          case 1:
            o = Ia;
            break;
          case 4:
            o = dt;
            break;
          case 16:
            o = ii;
            break;
          case 536870912:
            o = Il;
            break;
          default:
            o = ii;
        }
        o = Qh(o, kf.bind(null, n));
      }
      n.callbackPriority = r, n.callbackNode = o;
    }
  }
  function kf(n, r) {
    if (Is = -1, Cl = 0, gt & 6) throw Error(c(327));
    var o = n.callbackNode;
    if (Uu() && n.callbackNode !== o) return null;
    var f = pr(n, n === gn ? Nn : 0);
    if (f === 0) return null;
    if (f & 30 || f & n.expiredLanes || r) r = Df(n, f);
    else {
      r = f;
      var v = gt;
      gt |= 2;
      var g = Vh();
      (gn !== n || Nn !== r) && (Bi = null, Nu = rn() + 500, Tl(n, r));
      do
        try {
          bg();
          break;
        } catch (O) {
          jh(n, O);
        }
      while (!0);
      ia(), El.current = g, gt = v, tn !== null ? r = 0 : (gn = null, Nn = 0, r = An);
    }
    if (r !== 0) {
      if (r === 2 && (v = mo(n), v !== 0 && (f = v, r = wl(n, v))), r === 1) throw o = $s, Tl(n, 0), pi(n, f), zn(n, rn()), o;
      if (r === 6) pi(n, f);
      else {
        if (v = n.current.alternate, !(f & 30) && !bp(v) && (r = Df(n, f), r === 2 && (g = mo(n), g !== 0 && (f = g, r = wl(n, g))), r === 1)) throw o = $s, Tl(n, 0), pi(n, f), zn(n, rn()), o;
        switch (n.finishedWork = v, n.finishedLanes = f, r) {
          case 0:
          case 1:
            throw Error(c(345));
          case 2:
            Ao(n, fr, Bi);
            break;
          case 3:
            if (pi(n, f), (f & 130023424) === f && (r = wf + 500 - rn(), 10 < r)) {
              if (pr(n, 0) !== 0) break;
              if (v = n.suspendedLanes, (v & f) !== f) {
                In(), n.pingedLanes |= n.suspendedLanes & v;
                break;
              }
              n.timeoutHandle = ll(Ao.bind(null, n, fr, Bi), r);
              break;
            }
            Ao(n, fr, Bi);
            break;
          case 4:
            if (pi(n, f), (f & 4194240) === f) break;
            for (r = n.eventTimes, v = -1; 0 < f; ) {
              var w = 31 - dr(f);
              g = 1 << w, w = r[w], w > v && (v = w), f &= ~g;
            }
            if (f = v, f = rn() - f, f = (120 > f ? 120 : 480 > f ? 480 : 1080 > f ? 1080 : 1920 > f ? 1920 : 3e3 > f ? 3e3 : 4320 > f ? 4320 : 1960 * Sg(f / 1960)) - f, 10 < f) {
              n.timeoutHandle = ll(Ao.bind(null, n, fr, Bi), f);
              break;
            }
            Ao(n, fr, Bi);
            break;
          case 5:
            Ao(n, fr, Bi);
            break;
          default:
            throw Error(c(329));
        }
      }
    }
    return zn(n, rn()), n.callbackNode === o ? kf.bind(null, n) : null;
  }
  function wl(n, r) {
    var o = No;
    return n.current.memoizedState.isDehydrated && (Tl(n, r).flags |= 256), n = Df(n, r), n !== 2 && (r = fr, fr = o, r !== null && Ep(r)), n;
  }
  function Ep(n) {
    fr === null ? fr = n : fr.push.apply(fr, n);
  }
  function bp(n) {
    for (var r = n; ; ) {
      if (r.flags & 16384) {
        var o = r.updateQueue;
        if (o !== null && (o = o.stores, o !== null)) for (var f = 0; f < o.length; f++) {
          var v = o[f], g = v.getSnapshot;
          v = v.value;
          try {
            if (!ya(g(), v)) return !1;
          } catch {
            return !1;
          }
        }
      }
      if (o = r.child, r.subtreeFlags & 16384 && o !== null) o.return = r, r = o;
      else {
        if (r === n) break;
        for (; r.sibling === null; ) {
          if (r.return === null || r.return === n) return !0;
          r = r.return;
        }
        r.sibling.return = r.return, r = r.sibling;
      }
    }
    return !0;
  }
  function pi(n, r) {
    for (r &= ~gp, r &= ~Lu, n.suspendedLanes |= r, n.pingedLanes &= ~r, n = n.expirationTimes; 0 < r; ) {
      var o = 31 - dr(r), f = 1 << o;
      n[o] = -1, r &= ~f;
    }
  }
  function zu(n) {
    if (gt & 6) throw Error(c(327));
    Uu();
    var r = pr(n, 0);
    if (!(r & 1)) return zn(n, rn()), null;
    var o = Df(n, r);
    if (n.tag !== 0 && o === 2) {
      var f = mo(n);
      f !== 0 && (r = f, o = wl(n, f));
    }
    if (o === 1) throw o = $s, Tl(n, 0), pi(n, r), zn(n, rn()), o;
    if (o === 6) throw Error(c(345));
    return n.finishedWork = n.current.alternate, n.finishedLanes = r, Ao(n, fr, Bi), zn(n, rn()), null;
  }
  function Cp(n, r) {
    var o = gt;
    gt |= 1;
    try {
      return n(r);
    } finally {
      gt = o, gt === 0 && (Nu = rn() + 500, Vn && Fr());
    }
  }
  function vi(n) {
    Da !== null && Da.tag === 0 && !(gt & 6) && Uu();
    var r = gt;
    gt |= 1;
    var o = ka.transition, f = Nt;
    try {
      if (ka.transition = null, Nt = 1, n) return n();
    } finally {
      Nt = f, ka.transition = o, gt = r, !(gt & 6) && Fr();
    }
  }
  function _f() {
    Wr = Mu.current, Pt(Mu);
  }
  function Tl(n, r) {
    n.finishedWork = null, n.finishedLanes = 0;
    var o = n.timeoutHandle;
    if (o !== -1 && (n.timeoutHandle = -1, ph(o)), tn !== null) for (o = tn.return; o !== null; ) {
      var f = o;
      switch (Jd(f), f.tag) {
        case 1:
          f = f.type.childContextTypes, f != null && Ea();
          break;
        case 3:
          pu(), Pt(mn), Pt(tt), op();
          break;
        case 5:
          ip(f);
          break;
        case 4:
          pu();
          break;
        case 13:
          Pt(on);
          break;
        case 19:
          Pt(on);
          break;
        case 10:
          np(f.type._context);
          break;
        case 22:
        case 23:
          _f();
      }
      o = o.return;
    }
    if (gn = n, tn = n = zo(n.current, null), Nn = Wr = r, An = 0, $s = null, gp = Lu = bl = 0, fr = No = null, fl !== null) {
      for (r = 0; r < fl.length; r++) if (o = fl[r], f = o.interleaved, f !== null) {
        o.interleaved = null;
        var v = f.next, g = o.pending;
        if (g !== null) {
          var w = g.next;
          g.next = v, f.next = w;
        }
        o.pending = f;
      }
      fl = null;
    }
    return n;
  }
  function jh(n, r) {
    do {
      var o = tn;
      try {
        if (ia(), Xc.current = sr, oa) {
          for (var f = je.memoizedState; f !== null; ) {
            var v = f.queue;
            v !== null && (v.pending = null), f = f.next;
          }
          oa = !1;
        }
        if (Fe = 0, vt = nt = je = null, vu = !1, Ls = 0, Cf.current = null, o === null || o.return === null) {
          An = 1, $s = r, tn = null;
          break;
        }
        e: {
          var g = n, w = o.return, O = o, N = r;
          if (r = Nn, O.flags |= 32768, N !== null && typeof N == "object" && typeof N.then == "function") {
            var K = N, ue = O, se = ue.tag;
            if (!(ue.mode & 1) && (se === 0 || se === 11 || se === 15)) {
              var le = ue.alternate;
              le ? (ue.updateQueue = le.updateQueue, ue.memoizedState = le.memoizedState, ue.lanes = le.lanes) : (ue.updateQueue = null, ue.memoizedState = null);
            }
            var xe = Mh(w);
            if (xe !== null) {
              xe.flags &= -257, fp(xe, w, O, g, r), xe.mode & 1 && Ps(g, K, r), r = xe, N = K;
              var Le = r.updateQueue;
              if (Le === null) {
                var ze = /* @__PURE__ */ new Set();
                ze.add(N), r.updateQueue = ze;
              } else Le.add(N);
              break e;
            } else {
              if (!(r & 1)) {
                Ps(g, K, r), Ys();
                break e;
              }
              N = Error(c(426));
            }
          } else if (en && O.mode & 1) {
            var Rn = Mh(w);
            if (Rn !== null) {
              !(Rn.flags & 65536) && (Rn.flags |= 256), fp(Rn, w, O, g, r), tp(Mo(N, O));
              break e;
            }
          }
          g = N = Mo(N, O), An !== 4 && (An = 2), No === null ? No = [g] : No.push(g), g = w;
          do {
            switch (g.tag) {
              case 3:
                g.flags |= 65536, r &= -r, g.lanes |= r;
                var B = Dh(g, N, r);
                bh(g, B);
                break e;
              case 1:
                O = N;
                var U = g.type, Q = g.stateNode;
                if (!(g.flags & 128) && (typeof U.getDerivedStateFromError == "function" || Q !== null && typeof Q.componentDidCatch == "function" && (_a === null || !_a.has(Q)))) {
                  g.flags |= 65536, r &= -r, g.lanes |= r;
                  var pe = Oh(g, O, r);
                  bh(g, pe);
                  break e;
                }
            }
            g = g.return;
          } while (g !== null);
        }
        Bh(o);
      } catch (He) {
        r = He, tn === o && o !== null && (tn = o = o.return);
        continue;
      }
      break;
    } while (!0);
  }
  function Vh() {
    var n = El.current;
    return El.current = sr, n === null ? sr : n;
  }
  function Ys() {
    (An === 0 || An === 3 || An === 2) && (An = 4), gn === null || !(bl & 268435455) && !(Lu & 268435455) || pi(gn, Nn);
  }
  function Df(n, r) {
    var o = gt;
    gt |= 2;
    var f = Vh();
    (gn !== n || Nn !== r) && (Bi = null, Tl(n, r));
    do
      try {
        Eg();
        break;
      } catch (v) {
        jh(n, v);
      }
    while (!0);
    if (ia(), gt = o, El.current = f, tn !== null) throw Error(c(261));
    return gn = null, Nn = 0, An;
  }
  function Eg() {
    for (; tn !== null; ) $h(tn);
  }
  function bg() {
    for (; tn !== null && !wd(); ) $h(tn);
  }
  function $h(n) {
    var r = Wh(n.alternate, n, Wr);
    n.memoizedProps = n.pendingProps, r === null ? Bh(n) : tn = r, Cf.current = null;
  }
  function Bh(n) {
    var r = n;
    do {
      var o = r.alternate;
      if (n = r.return, r.flags & 32768) {
        if (o = mg(o, r), o !== null) {
          o.flags &= 32767, tn = o;
          return;
        }
        if (n !== null) n.flags |= 32768, n.subtreeFlags = 0, n.deletions = null;
        else {
          An = 6, tn = null;
          return;
        }
      } else if (o = hg(o, r, Wr), o !== null) {
        tn = o;
        return;
      }
      if (r = r.sibling, r !== null) {
        tn = r;
        return;
      }
      tn = r = n;
    } while (r !== null);
    An === 0 && (An = 5);
  }
  function Ao(n, r, o) {
    var f = Nt, v = ka.transition;
    try {
      ka.transition = null, Nt = 1, Cg(n, r, o, f);
    } finally {
      ka.transition = v, Nt = f;
    }
    return null;
  }
  function Cg(n, r, o, f) {
    do
      Uu();
    while (Da !== null);
    if (gt & 6) throw Error(c(327));
    o = n.finishedWork;
    var v = n.finishedLanes;
    if (o === null) return null;
    if (n.finishedWork = null, n.finishedLanes = 0, o === n.current) throw Error(c(177));
    n.callbackNode = null, n.callbackPriority = 0;
    var g = o.lanes | o.childLanes;
    if (xd(n, g), n === gn && (tn = gn = null, Nn = 0), !(o.subtreeFlags & 2064) && !(o.flags & 2064) || Au || (Au = !0, Qh(ii, function() {
      return Uu(), null;
    })), g = (o.flags & 15990) !== 0, o.subtreeFlags & 15990 || g) {
      g = ka.transition, ka.transition = null;
      var w = Nt;
      Nt = 1;
      var O = gt;
      gt |= 4, Cf.current = null, yg(n, o), Fh(o, n), Lc(ol), ma = !!Yd, ol = Yd = null, n.current = o, gg(o), Td(), gt = O, Nt = w, ka.transition = g;
    } else n.current = o;
    if (Au && (Au = !1, Da = n, Rf = v), g = n.pendingLanes, g === 0 && (_a = null), ss(o.stateNode), zn(n, rn()), r !== null) for (f = n.onRecoverableError, o = 0; o < r.length; o++) v = r[o], f(v.value, { componentStack: v.stack, digest: v.digest });
    if (Tf) throw Tf = !1, n = Sp, Sp = null, n;
    return Rf & 1 && n.tag !== 0 && Uu(), g = n.pendingLanes, g & 1 ? n === xf ? Bs++ : (Bs = 0, xf = n) : Bs = 0, Fr(), null;
  }
  function Uu() {
    if (Da !== null) {
      var n = Gl(Rf), r = ka.transition, o = Nt;
      try {
        if (ka.transition = null, Nt = 16 > n ? 16 : n, Da === null) var f = !1;
        else {
          if (n = Da, Da = null, Rf = 0, gt & 6) throw Error(c(331));
          var v = gt;
          for (gt |= 4, Me = n.current; Me !== null; ) {
            var g = Me, w = g.child;
            if (Me.flags & 16) {
              var O = g.deletions;
              if (O !== null) {
                for (var N = 0; N < O.length; N++) {
                  var K = O[N];
                  for (Me = K; Me !== null; ) {
                    var ue = Me;
                    switch (ue.tag) {
                      case 0:
                      case 11:
                      case 15:
                        xu(8, ue, g);
                    }
                    var se = ue.child;
                    if (se !== null) se.return = ue, Me = se;
                    else for (; Me !== null; ) {
                      ue = Me;
                      var le = ue.sibling, xe = ue.return;
                      if (Uh(ue), ue === K) {
                        Me = null;
                        break;
                      }
                      if (le !== null) {
                        le.return = xe, Me = le;
                        break;
                      }
                      Me = xe;
                    }
                  }
                }
                var Le = g.alternate;
                if (Le !== null) {
                  var ze = Le.child;
                  if (ze !== null) {
                    Le.child = null;
                    do {
                      var Rn = ze.sibling;
                      ze.sibling = null, ze = Rn;
                    } while (ze !== null);
                  }
                }
                Me = g;
              }
            }
            if (g.subtreeFlags & 2064 && w !== null) w.return = g, Me = w;
            else e: for (; Me !== null; ) {
              if (g = Me, g.flags & 2048) switch (g.tag) {
                case 0:
                case 11:
                case 15:
                  xu(9, g, g.return);
              }
              var B = g.sibling;
              if (B !== null) {
                B.return = g.return, Me = B;
                break e;
              }
              Me = g.return;
            }
          }
          var U = n.current;
          for (Me = U; Me !== null; ) {
            w = Me;
            var Q = w.child;
            if (w.subtreeFlags & 2064 && Q !== null) Q.return = w, Me = Q;
            else e: for (w = U; Me !== null; ) {
              if (O = Me, O.flags & 2048) try {
                switch (O.tag) {
                  case 0:
                  case 11:
                  case 15:
                    Ef(9, O);
                }
              } catch (He) {
                Sn(O, O.return, He);
              }
              if (O === w) {
                Me = null;
                break e;
              }
              var pe = O.sibling;
              if (pe !== null) {
                pe.return = O.return, Me = pe;
                break e;
              }
              Me = O.return;
            }
          }
          if (gt = v, Fr(), Nr && typeof Nr.onPostCommitFiberRoot == "function") try {
            Nr.onPostCommitFiberRoot(fo, n);
          } catch {
          }
          f = !0;
        }
        return f;
      } finally {
        Nt = o, ka.transition = r;
      }
    }
    return !1;
  }
  function Ih(n, r, o) {
    r = Mo(o, r), r = Dh(n, r, 1), n = Do(n, r, 1), r = In(), n !== null && (_i(n, 1, r), zn(n, r));
  }
  function Sn(n, r, o) {
    if (n.tag === 3) Ih(n, n, o);
    else for (; r !== null; ) {
      if (r.tag === 3) {
        Ih(r, n, o);
        break;
      } else if (r.tag === 1) {
        var f = r.stateNode;
        if (typeof r.type.getDerivedStateFromError == "function" || typeof f.componentDidCatch == "function" && (_a === null || !_a.has(f))) {
          n = Mo(o, n), n = Oh(r, n, 1), r = Do(r, n, 1), n = In(), r !== null && (_i(r, 1, n), zn(r, n));
          break;
        }
      }
      r = r.return;
    }
  }
  function wg(n, r, o) {
    var f = n.pingCache;
    f !== null && f.delete(r), r = In(), n.pingedLanes |= n.suspendedLanes & o, gn === n && (Nn & o) === o && (An === 4 || An === 3 && (Nn & 130023424) === Nn && 500 > rn() - wf ? Tl(n, 0) : gp |= o), zn(n, r);
  }
  function Yh(n, r) {
    r === 0 && (n.mode & 1 ? (r = po, po <<= 1, !(po & 130023424) && (po = 4194304)) : r = 1);
    var o = In();
    n = ji(n, r), n !== null && (_i(n, r, o), zn(n, o));
  }
  function wp(n) {
    var r = n.memoizedState, o = 0;
    r !== null && (o = r.retryLane), Yh(n, o);
  }
  function Tg(n, r) {
    var o = 0;
    switch (n.tag) {
      case 13:
        var f = n.stateNode, v = n.memoizedState;
        v !== null && (o = v.retryLane);
        break;
      case 19:
        f = n.stateNode;
        break;
      default:
        throw Error(c(314));
    }
    f !== null && f.delete(r), Yh(n, o);
  }
  var Wh;
  Wh = function(n, r, o) {
    if (n !== null) if (n.memoizedProps !== r.pendingProps || mn.current) Ir = !0;
    else {
      if (!(n.lanes & o) && !(r.flags & 128)) return Ir = !1, $i(n, r, o);
      Ir = !!(n.flags & 131072);
    }
    else Ir = !1, en && r.flags & 1048576 && Zd(r, uu, r.index);
    switch (r.lanes = 0, r.tag) {
      case 2:
        var f = r.type;
        js(n, r), n = r.pendingProps;
        var v = Sa(r, tt.current);
        cu(r, o), v = ie(null, r, f, n, v, o);
        var g = Dn();
        return r.flags |= 1, typeof v == "object" && v !== null && typeof v.render == "function" && v.$$typeof === void 0 ? (r.tag = 1, r.memoizedState = null, r.updateQueue = null, an(f) ? (g = !0, Fc(r)) : g = !1, r.memoizedState = v.state !== null && v.state !== void 0 ? v.state : null, Yc(r), v.updater = yl, r.stateNode = v, v._reactInternals = r, sp(r, f, n, o), r = vf(null, r, f, !0, g, o)) : (r.tag = 0, en && g && Hc(r), wn(null, r, v, o), r = r.child), r;
      case 16:
        f = r.elementType;
        e: {
          switch (js(n, r), n = r.pendingProps, v = f._init, f = v(f._payload), r.type = f, v = r.tag = Rg(f), n = Br(f, n), v) {
            case 0:
              r = ut(null, r, f, n, o);
              break e;
            case 1:
              r = Fs(null, r, f, n, o);
              break e;
            case 11:
              r = bu(null, r, f, n, o);
              break e;
            case 14:
              r = Lo(null, r, f, Br(f.type, n), o);
              break e;
          }
          throw Error(c(
            306,
            f,
            ""
          ));
        }
        return r;
      case 0:
        return f = r.type, v = r.pendingProps, v = r.elementType === f ? v : Br(f, v), ut(n, r, f, v, o);
      case 1:
        return f = r.type, v = r.pendingProps, v = r.elementType === f ? v : Br(f, v), Fs(n, r, f, v, o);
      case 3:
        e: {
          if (vg(r), n === null) throw Error(c(387));
          f = r.pendingProps, g = r.memoizedState, v = g.element, fu(n, r), Qc(r, f, null, o);
          var w = r.memoizedState;
          if (f = w.element, g.isDehydrated) if (g = { element: f, isDehydrated: !1, cache: w.cache, pendingSuspenseBoundaries: w.pendingSuspenseBoundaries, transitions: w.transitions }, r.updateQueue.baseState = g, r.memoizedState = g, r.flags & 256) {
            v = Mo(Error(c(423)), r), r = Nh(n, r, f, o, v);
            break e;
          } else if (f !== v) {
            v = Mo(Error(c(424)), r), r = Nh(n, r, f, o, v);
            break e;
          } else for (Vr = Xa(r.stateNode.containerInfo.firstChild), aa = r, en = !0, Ca = null, o = Sh(r, null, f, o), r.child = o; o; ) o.flags = o.flags & -3 | 4096, o = o.sibling;
          else {
            if (fn(), f === v) {
              r = Tn(n, r, o);
              break e;
            }
            wn(n, r, f, o);
          }
          r = r.child;
        }
        return r;
      case 5:
        return wh(r), n === null && Vc(r), f = r.type, v = r.pendingProps, g = n !== null ? n.memoizedProps : null, w = v.children, Ts(f, v) ? w = null : g !== null && Ts(f, g) && (r.flags |= 32), gl(n, r), wn(n, r, w, o), r.child;
      case 6:
        return n === null && Vc(r), null;
      case 13:
        return Ah(n, r, o);
      case 4:
        return ap(r, r.stateNode.containerInfo), f = r.pendingProps, n === null ? r.child = su(r, null, f, o) : wn(n, r, f, o), r.child;
      case 11:
        return f = r.type, v = r.pendingProps, v = r.elementType === f ? v : Br(f, v), bu(n, r, f, v, o);
      case 7:
        return wn(n, r, r.pendingProps, o), r.child;
      case 8:
        return wn(n, r, r.pendingProps.children, o), r.child;
      case 12:
        return wn(n, r, r.pendingProps.children, o), r.child;
      case 10:
        e: {
          if (f = r.type._context, v = r.pendingProps, g = r.memoizedProps, w = v.value, Ht(Hi, f._currentValue), f._currentValue = w, g !== null) if (ya(g.value, w)) {
            if (g.children === v.children && !mn.current) {
              r = Tn(n, r, o);
              break e;
            }
          } else for (g = r.child, g !== null && (g.return = r); g !== null; ) {
            var O = g.dependencies;
            if (O !== null) {
              w = g.child;
              for (var N = O.firstContext; N !== null; ) {
                if (N.context === f) {
                  if (g.tag === 1) {
                    N = $r(-1, o & -o), N.tag = 2;
                    var K = g.updateQueue;
                    if (K !== null) {
                      K = K.shared;
                      var ue = K.pending;
                      ue === null ? N.next = N : (N.next = ue.next, ue.next = N), K.pending = N;
                    }
                  }
                  g.lanes |= o, N = g.alternate, N !== null && (N.lanes |= o), rp(
                    g.return,
                    o,
                    r
                  ), O.lanes |= o;
                  break;
                }
                N = N.next;
              }
            } else if (g.tag === 10) w = g.type === r.type ? null : g.child;
            else if (g.tag === 18) {
              if (w = g.return, w === null) throw Error(c(341));
              w.lanes |= o, O = w.alternate, O !== null && (O.lanes |= o), rp(w, o, r), w = g.sibling;
            } else w = g.child;
            if (w !== null) w.return = g;
            else for (w = g; w !== null; ) {
              if (w === r) {
                w = null;
                break;
              }
              if (g = w.sibling, g !== null) {
                g.return = w.return, w = g;
                break;
              }
              w = w.return;
            }
            g = w;
          }
          wn(n, r, v.children, o), r = r.child;
        }
        return r;
      case 9:
        return v = r.type, f = r.pendingProps.children, cu(r, o), v = Ta(v), f = f(v), r.flags |= 1, wn(n, r, f, o), r.child;
      case 14:
        return f = r.type, v = Br(f, r.pendingProps), v = Br(f.type, v), Lo(n, r, f, v, o);
      case 15:
        return pf(n, r, r.type, r.pendingProps, o);
      case 17:
        return f = r.type, v = r.pendingProps, v = r.elementType === f ? v : Br(f, v), js(n, r), r.tag = 1, an(f) ? (n = !0, Fc(r)) : n = !1, cu(r, o), xh(r, f, v), sp(r, f, v, o), vf(null, r, f, !0, n, o);
      case 19:
        return pp(n, r, o);
      case 22:
        return Yr(n, r, o);
    }
    throw Error(c(156, r.tag));
  };
  function Qh(n, r) {
    return us(n, r);
  }
  function Gh(n, r, o, f) {
    this.tag = n, this.key = o, this.sibling = this.child = this.return = this.stateNode = this.type = this.elementType = null, this.index = 0, this.ref = null, this.pendingProps = r, this.dependencies = this.memoizedState = this.updateQueue = this.memoizedProps = null, this.mode = f, this.subtreeFlags = this.flags = 0, this.deletions = null, this.childLanes = this.lanes = 0, this.alternate = null;
  }
  function Oa(n, r, o, f) {
    return new Gh(n, r, o, f);
  }
  function Tp(n) {
    return n = n.prototype, !(!n || !n.isReactComponent);
  }
  function Rg(n) {
    if (typeof n == "function") return Tp(n) ? 1 : 0;
    if (n != null) {
      if (n = n.$$typeof, n === Ue) return 11;
      if (n === Ot) return 14;
    }
    return 2;
  }
  function zo(n, r) {
    var o = n.alternate;
    return o === null ? (o = Oa(n.tag, r, n.key, n.mode), o.elementType = n.elementType, o.type = n.type, o.stateNode = n.stateNode, o.alternate = n, n.alternate = o) : (o.pendingProps = r, o.type = n.type, o.flags = 0, o.subtreeFlags = 0, o.deletions = null), o.flags = n.flags & 14680064, o.childLanes = n.childLanes, o.lanes = n.lanes, o.child = n.child, o.memoizedProps = n.memoizedProps, o.memoizedState = n.memoizedState, o.updateQueue = n.updateQueue, r = n.dependencies, o.dependencies = r === null ? null : { lanes: r.lanes, firstContext: r.firstContext }, o.sibling = n.sibling, o.index = n.index, o.ref = n.ref, o;
  }
  function Of(n, r, o, f, v, g) {
    var w = 2;
    if (f = n, typeof n == "function") Tp(n) && (w = 1);
    else if (typeof n == "string") w = 5;
    else e: switch (n) {
      case me:
        return Rl(o.children, v, g, r);
      case We:
        w = 8, v |= 8;
        break;
      case Et:
        return n = Oa(12, o, r, v | 2), n.elementType = Et, n.lanes = g, n;
      case qe:
        return n = Oa(13, o, r, v), n.elementType = qe, n.lanes = g, n;
      case rt:
        return n = Oa(19, o, r, v), n.elementType = rt, n.lanes = g, n;
      case Oe:
        return Mf(o, v, g, r);
      default:
        if (typeof n == "object" && n !== null) switch (n.$$typeof) {
          case Rt:
            w = 10;
            break e;
          case ye:
            w = 9;
            break e;
          case Ue:
            w = 11;
            break e;
          case Ot:
            w = 14;
            break e;
          case mt:
            w = 16, f = null;
            break e;
        }
        throw Error(c(130, n == null ? n : typeof n, ""));
    }
    return r = Oa(w, o, r, v), r.elementType = n, r.type = f, r.lanes = g, r;
  }
  function Rl(n, r, o, f) {
    return n = Oa(7, n, f, r), n.lanes = o, n;
  }
  function Mf(n, r, o, f) {
    return n = Oa(22, n, f, r), n.elementType = Oe, n.lanes = o, n.stateNode = { isHidden: !1 }, n;
  }
  function Lf(n, r, o) {
    return n = Oa(6, n, null, r), n.lanes = o, n;
  }
  function Ws(n, r, o) {
    return r = Oa(4, n.children !== null ? n.children : [], n.key, r), r.lanes = o, r.stateNode = { containerInfo: n.containerInfo, pendingChildren: null, implementation: n.implementation }, r;
  }
  function Qs(n, r, o, f, v) {
    this.tag = r, this.containerInfo = n, this.finishedWork = this.pingCache = this.current = this.pendingChildren = null, this.timeoutHandle = -1, this.callbackNode = this.pendingContext = this.context = null, this.callbackPriority = 0, this.eventTimes = Ql(0), this.expirationTimes = Ql(-1), this.entangledLanes = this.finishedLanes = this.mutableReadLanes = this.expiredLanes = this.pingedLanes = this.suspendedLanes = this.pendingLanes = 0, this.entanglements = Ql(0), this.identifierPrefix = f, this.onRecoverableError = v, this.mutableSourceEagerHydrationData = null;
  }
  function Rp(n, r, o, f, v, g, w, O, N) {
    return n = new Qs(n, r, o, O, N), r === 1 ? (r = 1, g === !0 && (r |= 8)) : r = 0, g = Oa(3, null, null, r), n.current = g, g.stateNode = n, g.memoizedState = { element: f, isDehydrated: o, cache: null, transitions: null, pendingSuspenseBoundaries: null }, Yc(g), n;
  }
  function qh(n, r, o) {
    var f = 3 < arguments.length && arguments[3] !== void 0 ? arguments[3] : null;
    return { $$typeof: Ce, key: f == null ? null : "" + f, children: n, containerInfo: r, implementation: o };
  }
  function xp(n) {
    if (!n) return si;
    n = n._reactInternals;
    e: {
      if (va(n) !== n || n.tag !== 1) throw Error(c(170));
      var r = n;
      do {
        switch (r.tag) {
          case 3:
            r = r.stateNode.context;
            break e;
          case 1:
            if (an(r.type)) {
              r = r.stateNode.__reactInternalMemoizedMergedChildContext;
              break e;
            }
        }
        r = r.return;
      } while (r !== null);
      throw Error(c(171));
    }
    if (n.tag === 1) {
      var o = n.type;
      if (an(o)) return ks(n, o, r);
    }
    return r;
  }
  function kp(n, r, o, f, v, g, w, O, N) {
    return n = Rp(o, f, !0, n, v, g, w, O, N), n.context = xp(null), o = n.current, f = In(), v = Ii(o), g = $r(f, v), g.callback = r ?? null, Do(o, g, v), n.current.lanes = v, _i(n, v, f), zn(n, f), n;
  }
  function Nf(n, r, o, f) {
    var v = r.current, g = In(), w = Ii(v);
    return o = xp(o), r.context === null ? r.context = o : r.pendingContext = o, r = $r(g, w), r.payload = { element: n }, f = f === void 0 ? null : f, f !== null && (r.callback = f), n = Do(v, r, w), n !== null && (dn(n, v, w, g), Wc(n, v, w)), w;
  }
  function Gs(n) {
    if (n = n.current, !n.child) return null;
    switch (n.child.tag) {
      case 5:
        return n.child.stateNode;
      default:
        return n.child.stateNode;
    }
  }
  function Xh(n, r) {
    if (n = n.memoizedState, n !== null && n.dehydrated !== null) {
      var o = n.retryLane;
      n.retryLane = o !== 0 && o < r ? o : r;
    }
  }
  function _p(n, r) {
    Xh(n, r), (n = n.alternate) && Xh(n, r);
  }
  function xg() {
    return null;
  }
  var Dp = typeof reportError == "function" ? reportError : function(n) {
    console.error(n);
  };
  function Af(n) {
    this._internalRoot = n;
  }
  qs.prototype.render = Af.prototype.render = function(n) {
    var r = this._internalRoot;
    if (r === null) throw Error(c(409));
    Nf(n, r, null, null);
  }, qs.prototype.unmount = Af.prototype.unmount = function() {
    var n = this._internalRoot;
    if (n !== null) {
      this._internalRoot = null;
      var r = n.containerInfo;
      vi(function() {
        Nf(null, n, null, null);
      }), r[Pi] = null;
    }
  };
  function qs(n) {
    this._internalRoot = n;
  }
  qs.prototype.unstable_scheduleHydration = function(n) {
    if (n) {
      var r = Xl();
      n = { blockedOn: null, target: n, priority: r };
      for (var o = 0; o < Ft.length && r !== 0 && r < Ft[o].priority; o++) ;
      Ft.splice(o, 0, n), o === 0 && Rc(n);
    }
  };
  function Uo(n) {
    return !(!n || n.nodeType !== 1 && n.nodeType !== 9 && n.nodeType !== 11);
  }
  function zf(n) {
    return !(!n || n.nodeType !== 1 && n.nodeType !== 9 && n.nodeType !== 11 && (n.nodeType !== 8 || n.nodeValue !== " react-mount-point-unstable "));
  }
  function Kh() {
  }
  function kg(n, r, o, f, v) {
    if (v) {
      if (typeof f == "function") {
        var g = f;
        f = function() {
          var K = Gs(w);
          g.call(K);
        };
      }
      var w = kp(r, f, n, 0, null, !1, !1, "", Kh);
      return n._reactRootContainer = w, n[Pi] = w.current, ou(n.nodeType === 8 ? n.parentNode : n), vi(), w;
    }
    for (; v = n.lastChild; ) n.removeChild(v);
    if (typeof f == "function") {
      var O = f;
      f = function() {
        var K = Gs(N);
        O.call(K);
      };
    }
    var N = Rp(n, 0, !1, null, null, !1, !1, "", Kh);
    return n._reactRootContainer = N, n[Pi] = N.current, ou(n.nodeType === 8 ? n.parentNode : n), vi(function() {
      Nf(r, N, o, f);
    }), N;
  }
  function Uf(n, r, o, f, v) {
    var g = o._reactRootContainer;
    if (g) {
      var w = g;
      if (typeof v == "function") {
        var O = v;
        v = function() {
          var N = Gs(w);
          O.call(N);
        };
      }
      Nf(r, w, n, v);
    } else w = kg(o, r, n, v, f);
    return Gs(w);
  }
  Jo = function(n) {
    switch (n.tag) {
      case 3:
        var r = n.stateNode;
        if (r.current.memoizedState.isDehydrated) {
          var o = Ya(r.pendingLanes);
          o !== 0 && (oi(r, o | 1), zn(r, rn()), !(gt & 6) && (Nu = rn() + 500, Fr()));
        }
        break;
      case 13:
        vi(function() {
          var f = ji(n, 1);
          if (f !== null) {
            var v = In();
            dn(f, n, 1, v);
          }
        }), _p(n, 1);
    }
  }, ql = function(n) {
    if (n.tag === 13) {
      var r = ji(n, 134217728);
      if (r !== null) {
        var o = In();
        dn(r, n, 134217728, o);
      }
      _p(n, 134217728);
    }
  }, Ct = function(n) {
    if (n.tag === 13) {
      var r = Ii(n), o = ji(n, r);
      if (o !== null) {
        var f = In();
        dn(o, n, r, f);
      }
      _p(n, r);
    }
  }, Xl = function() {
    return Nt;
  }, Kl = function(n, r) {
    var o = Nt;
    try {
      return Nt = n, r();
    } finally {
      Nt = o;
    }
  }, pa = function(n, r, o) {
    switch (r) {
      case "input":
        if (Fn(n, o), r = o.name, o.type === "radio" && r != null) {
          for (o = n; o.parentNode; ) o = o.parentNode;
          for (o = o.querySelectorAll("input[name=" + JSON.stringify("" + r) + '][type="radio"]'), r = 0; r < o.length; r++) {
            var f = o[r];
            if (f !== n && f.form === n.form) {
              var v = Ye(f);
              if (!v) throw Error(c(90));
              Fa(f), Fn(f, v);
            }
          }
        }
        break;
      case "textarea":
        ja(n, o);
        break;
      case "select":
        r = o.value, r != null && Dr(n, !!o.multiple, r, !1);
    }
  }, gc = Cp, Sc = vi;
  var _g = { usingClientEntryPoint: !1, Events: [xs, lu, Ye, ki, Vl, Cp] }, Xs = { findFiberByHostInstance: ga, bundleType: 0, version: "18.3.1", rendererPackageName: "react-dom" }, Zh = { bundleType: Xs.bundleType, version: Xs.version, rendererPackageName: Xs.rendererPackageName, rendererConfig: Xs.rendererConfig, overrideHookState: null, overrideHookStateDeletePath: null, overrideHookStateRenamePath: null, overrideProps: null, overridePropsDeletePath: null, overridePropsRenamePath: null, setErrorHandler: null, setSuspenseHandler: null, scheduleUpdate: null, currentDispatcherRef: oe.ReactCurrentDispatcher, findHostInstanceByFiber: function(n) {
    return n = Ec(n), n === null ? null : n.stateNode;
  }, findFiberByHostInstance: Xs.findFiberByHostInstance || xg, findHostInstancesForRefresh: null, scheduleRefresh: null, scheduleRoot: null, setRefreshHandler: null, getCurrentFiber: null, reconcilerVersion: "18.3.1-next-f1338f8080-20240426" };
  if (typeof __REACT_DEVTOOLS_GLOBAL_HOOK__ < "u") {
    var Pf = __REACT_DEVTOOLS_GLOBAL_HOOK__;
    if (!Pf.isDisabled && Pf.supportsFiber) try {
      fo = Pf.inject(Zh), Nr = Pf;
    } catch {
    }
  }
  return Ua.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED = _g, Ua.createPortal = function(n, r) {
    var o = 2 < arguments.length && arguments[2] !== void 0 ? arguments[2] : null;
    if (!Uo(r)) throw Error(c(200));
    return qh(n, r, null, o);
  }, Ua.createRoot = function(n, r) {
    if (!Uo(n)) throw Error(c(299));
    var o = !1, f = "", v = Dp;
    return r != null && (r.unstable_strictMode === !0 && (o = !0), r.identifierPrefix !== void 0 && (f = r.identifierPrefix), r.onRecoverableError !== void 0 && (v = r.onRecoverableError)), r = Rp(n, 1, !1, null, null, o, !1, f, v), n[Pi] = r.current, ou(n.nodeType === 8 ? n.parentNode : n), new Af(r);
  }, Ua.findDOMNode = function(n) {
    if (n == null) return null;
    if (n.nodeType === 1) return n;
    var r = n._reactInternals;
    if (r === void 0)
      throw typeof n.render == "function" ? Error(c(188)) : (n = Object.keys(n).join(","), Error(c(268, n)));
    return n = Ec(r), n = n === null ? null : n.stateNode, n;
  }, Ua.flushSync = function(n) {
    return vi(n);
  }, Ua.hydrate = function(n, r, o) {
    if (!zf(r)) throw Error(c(200));
    return Uf(null, n, r, !0, o);
  }, Ua.hydrateRoot = function(n, r, o) {
    if (!Uo(n)) throw Error(c(405));
    var f = o != null && o.hydratedSources || null, v = !1, g = "", w = Dp;
    if (o != null && (o.unstable_strictMode === !0 && (v = !0), o.identifierPrefix !== void 0 && (g = o.identifierPrefix), o.onRecoverableError !== void 0 && (w = o.onRecoverableError)), r = kp(r, null, n, 1, o ?? null, v, !1, g, w), n[Pi] = r.current, ou(n), f) for (n = 0; n < f.length; n++) o = f[n], v = o._getVersion, v = v(o._source), r.mutableSourceEagerHydrationData == null ? r.mutableSourceEagerHydrationData = [o, v] : r.mutableSourceEagerHydrationData.push(
      o,
      v
    );
    return new qs(r);
  }, Ua.render = function(n, r, o) {
    if (!zf(r)) throw Error(c(200));
    return Uf(null, n, r, !1, o);
  }, Ua.unmountComponentAtNode = function(n) {
    if (!zf(n)) throw Error(c(40));
    return n._reactRootContainer ? (vi(function() {
      Uf(null, null, n, !1, function() {
        n._reactRootContainer = null, n[Pi] = null;
      });
    }), !0) : !1;
  }, Ua.unstable_batchedUpdates = Cp, Ua.unstable_renderSubtreeIntoContainer = function(n, r, o, f) {
    if (!zf(o)) throw Error(c(200));
    if (n == null || n._reactInternals === void 0) throw Error(c(38));
    return Uf(n, r, o, !1, f);
  }, Ua.version = "18.3.1-next-f1338f8080-20240426", Ua;
}
var Pa = {};
/**
 * @license React
 * react-dom.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var aT;
function B2() {
  return aT || (aT = 1, process.env.NODE_ENV !== "production" && function() {
    typeof __REACT_DEVTOOLS_GLOBAL_HOOK__ < "u" && typeof __REACT_DEVTOOLS_GLOBAL_HOOK__.registerInternalModuleStart == "function" && __REACT_DEVTOOLS_GLOBAL_HOOK__.registerInternalModuleStart(new Error());
    var l = R, s = NT(), c = l.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED, p = !1;
    function y(e) {
      p = e;
    }
    function E(e) {
      if (!p) {
        for (var t = arguments.length, a = new Array(t > 1 ? t - 1 : 0), i = 1; i < t; i++)
          a[i - 1] = arguments[i];
        x("warn", e, a);
      }
    }
    function m(e) {
      if (!p) {
        for (var t = arguments.length, a = new Array(t > 1 ? t - 1 : 0), i = 1; i < t; i++)
          a[i - 1] = arguments[i];
        x("error", e, a);
      }
    }
    function x(e, t, a) {
      {
        var i = c.ReactDebugCurrentFrame, u = i.getStackAddendum();
        u !== "" && (t += "%s", a = a.concat([u]));
        var d = a.map(function(h) {
          return String(h);
        });
        d.unshift("Warning: " + t), Function.prototype.apply.call(console[e], console, d);
      }
    }
    var T = 0, D = 1, A = 2, L = 3, j = 4, F = 5, $ = 6, H = 7, G = 8, W = 9, ve = 10, fe = 11, oe = 12, ne = 13, Ce = 14, me = 15, We = 16, Et = 17, Rt = 18, ye = 19, Ue = 21, qe = 22, rt = 23, Ot = 24, mt = 25, Oe = !0, he = !1, Be = !1, Te = !1, V = !1, ae = !0, Je = !1, at = !0, ct = !0, yt = !0, Lt = !0, xt = /* @__PURE__ */ new Set(), bt = {}, En = {};
    function xr(e, t) {
      Fa(e, t), Fa(e + "Capture", t);
    }
    function Fa(e, t) {
      bt[e] && m("EventRegistry: More than one plugin attempted to publish the same registration name, `%s`.", e), bt[e] = t;
      {
        var a = e.toLowerCase();
        En[a] = e, e === "onDoubleClick" && (En.ondblclick = e);
      }
      for (var i = 0; i < t.length; i++)
        xt.add(t[i]);
    }
    var bn = typeof window < "u" && typeof window.document < "u" && typeof window.document.createElement < "u", ir = Object.prototype.hasOwnProperty;
    function kr(e) {
      {
        var t = typeof Symbol == "function" && Symbol.toStringTag, a = t && e[Symbol.toStringTag] || e.constructor.name || "Object";
        return a;
      }
    }
    function _r(e) {
      try {
        return Fn(e), !1;
      } catch {
        return !0;
      }
    }
    function Fn(e) {
      return "" + e;
    }
    function Ha(e, t) {
      if (_r(e))
        return m("The provided `%s` attribute is an unsupported type %s. This value must be coerced to a string before before using it here.", t, kr(e)), Fn(e);
    }
    function xi(e) {
      if (_r(e))
        return m("The provided key is an unsupported type %s. This value must be coerced to a string before before using it here.", kr(e)), Fn(e);
    }
    function or(e, t) {
      if (_r(e))
        return m("The provided `%s` prop is an unsupported type %s. This value must be coerced to a string before before using it here.", t, kr(e)), Fn(e);
    }
    function Dr(e, t) {
      if (_r(e))
        return m("The provided `%s` CSS property is an unsupported type %s. This value must be coerced to a string before before using it here.", t, kr(e)), Fn(e);
    }
    function ni(e) {
      if (_r(e))
        return m("The provided HTML markup uses a value of unsupported type %s. This value must be coerced to a string before before using it here.", kr(e)), Fn(e);
    }
    function Or(e) {
      if (_r(e))
        return m("Form field values (value, checked, defaultValue, or defaultChecked props) must be strings, not %s. This value must be coerced to a string before before using it here.", kr(e)), Fn(e);
    }
    var ja = 0, Mr = 1, Va = 2, Wn = 3, ea = 4, Xo = 5, ri = 6, Ee = ":A-Z_a-z\\u00C0-\\u00D6\\u00D8-\\u00F6\\u00F8-\\u02FF\\u0370-\\u037D\\u037F-\\u1FFF\\u200C-\\u200D\\u2070-\\u218F\\u2C00-\\u2FEF\\u3001-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFFD", Ie = Ee + "\\-.0-9\\u00B7\\u0300-\\u036F\\u203F-\\u2040", ft = new RegExp("^[" + Ee + "][" + Ie + "]*$"), Vt = {}, Bt = {};
    function Mn(e) {
      return ir.call(Bt, e) ? !0 : ir.call(Vt, e) ? !1 : ft.test(e) ? (Bt[e] = !0, !0) : (Vt[e] = !0, m("Invalid attribute name: `%s`", e), !1);
    }
    function Cn(e, t, a) {
      return t !== null ? t.type === ja : a ? !1 : e.length > 2 && (e[0] === "o" || e[0] === "O") && (e[1] === "n" || e[1] === "N");
    }
    function Lr(e, t, a, i) {
      if (a !== null && a.type === ja)
        return !1;
      switch (typeof t) {
        case "function":
        case "symbol":
          return !0;
        case "boolean": {
          if (i)
            return !1;
          if (a !== null)
            return !a.acceptsBooleans;
          var u = e.toLowerCase().slice(0, 5);
          return u !== "data-" && u !== "aria-";
        }
        default:
          return !1;
      }
    }
    function Zt(e, t, a, i) {
      if (t === null || typeof t > "u" || Lr(e, t, a, i))
        return !0;
      if (i)
        return !1;
      if (a !== null)
        switch (a.type) {
          case Wn:
            return !t;
          case ea:
            return t === !1;
          case Xo:
            return isNaN(t);
          case ri:
            return isNaN(t) || t < 1;
        }
      return !1;
    }
    function pa(e) {
      return qt.hasOwnProperty(e) ? qt[e] : null;
    }
    function sn(e, t, a, i, u, d, h) {
      this.acceptsBooleans = t === Va || t === Wn || t === ea, this.attributeName = i, this.attributeNamespace = u, this.mustUseProperty = a, this.propertyName = e, this.type = t, this.sanitizeURL = d, this.removeEmptyString = h;
    }
    var qt = {}, Ko = [
      "children",
      "dangerouslySetInnerHTML",
      // TODO: This prevents the assignment of defaultValue to regular
      // elements (not just inputs). Now that ReactDOMInput assigns to the
      // defaultValue property -- do we need this?
      "defaultValue",
      "defaultChecked",
      "innerHTML",
      "suppressContentEditableWarning",
      "suppressHydrationWarning",
      "style"
    ];
    Ko.forEach(function(e) {
      qt[e] = new sn(
        e,
        ja,
        !1,
        // mustUseProperty
        e,
        // attributeName
        null,
        // attributeNamespace
        !1,
        // sanitizeURL
        !1
      );
    }), [["acceptCharset", "accept-charset"], ["className", "class"], ["htmlFor", "for"], ["httpEquiv", "http-equiv"]].forEach(function(e) {
      var t = e[0], a = e[1];
      qt[t] = new sn(
        t,
        Mr,
        !1,
        // mustUseProperty
        a,
        // attributeName
        null,
        // attributeNamespace
        !1,
        // sanitizeURL
        !1
      );
    }), ["contentEditable", "draggable", "spellCheck", "value"].forEach(function(e) {
      qt[e] = new sn(
        e,
        Va,
        !1,
        // mustUseProperty
        e.toLowerCase(),
        // attributeName
        null,
        // attributeNamespace
        !1,
        // sanitizeURL
        !1
      );
    }), ["autoReverse", "externalResourcesRequired", "focusable", "preserveAlpha"].forEach(function(e) {
      qt[e] = new sn(
        e,
        Va,
        !1,
        // mustUseProperty
        e,
        // attributeName
        null,
        // attributeNamespace
        !1,
        // sanitizeURL
        !1
      );
    }), [
      "allowFullScreen",
      "async",
      // Note: there is a special case that prevents it from being written to the DOM
      // on the client side because the browsers are inconsistent. Instead we call focus().
      "autoFocus",
      "autoPlay",
      "controls",
      "default",
      "defer",
      "disabled",
      "disablePictureInPicture",
      "disableRemotePlayback",
      "formNoValidate",
      "hidden",
      "loop",
      "noModule",
      "noValidate",
      "open",
      "playsInline",
      "readOnly",
      "required",
      "reversed",
      "scoped",
      "seamless",
      // Microdata
      "itemScope"
    ].forEach(function(e) {
      qt[e] = new sn(
        e,
        Wn,
        !1,
        // mustUseProperty
        e.toLowerCase(),
        // attributeName
        null,
        // attributeNamespace
        !1,
        // sanitizeURL
        !1
      );
    }), [
      "checked",
      // Note: `option.selected` is not updated if `select.multiple` is
      // disabled with `removeAttribute`. We have special logic for handling this.
      "multiple",
      "muted",
      "selected"
      // NOTE: if you add a camelCased prop to this list,
      // you'll need to set attributeName to name.toLowerCase()
      // instead in the assignment below.
    ].forEach(function(e) {
      qt[e] = new sn(
        e,
        Wn,
        !0,
        // mustUseProperty
        e,
        // attributeName
        null,
        // attributeNamespace
        !1,
        // sanitizeURL
        !1
      );
    }), [
      "capture",
      "download"
      // NOTE: if you add a camelCased prop to this list,
      // you'll need to set attributeName to name.toLowerCase()
      // instead in the assignment below.
    ].forEach(function(e) {
      qt[e] = new sn(
        e,
        ea,
        !1,
        // mustUseProperty
        e,
        // attributeName
        null,
        // attributeNamespace
        !1,
        // sanitizeURL
        !1
      );
    }), [
      "cols",
      "rows",
      "size",
      "span"
      // NOTE: if you add a camelCased prop to this list,
      // you'll need to set attributeName to name.toLowerCase()
      // instead in the assignment below.
    ].forEach(function(e) {
      qt[e] = new sn(
        e,
        ri,
        !1,
        // mustUseProperty
        e,
        // attributeName
        null,
        // attributeNamespace
        !1,
        // sanitizeURL
        !1
      );
    }), ["rowSpan", "start"].forEach(function(e) {
      qt[e] = new sn(
        e,
        Xo,
        !1,
        // mustUseProperty
        e.toLowerCase(),
        // attributeName
        null,
        // attributeNamespace
        !1,
        // sanitizeURL
        !1
      );
    });
    var ki = /[\-\:]([a-z])/g, Vl = function(e) {
      return e[1].toUpperCase();
    };
    [
      "accent-height",
      "alignment-baseline",
      "arabic-form",
      "baseline-shift",
      "cap-height",
      "clip-path",
      "clip-rule",
      "color-interpolation",
      "color-interpolation-filters",
      "color-profile",
      "color-rendering",
      "dominant-baseline",
      "enable-background",
      "fill-opacity",
      "fill-rule",
      "flood-color",
      "flood-opacity",
      "font-family",
      "font-size",
      "font-size-adjust",
      "font-stretch",
      "font-style",
      "font-variant",
      "font-weight",
      "glyph-name",
      "glyph-orientation-horizontal",
      "glyph-orientation-vertical",
      "horiz-adv-x",
      "horiz-origin-x",
      "image-rendering",
      "letter-spacing",
      "lighting-color",
      "marker-end",
      "marker-mid",
      "marker-start",
      "overline-position",
      "overline-thickness",
      "paint-order",
      "panose-1",
      "pointer-events",
      "rendering-intent",
      "shape-rendering",
      "stop-color",
      "stop-opacity",
      "strikethrough-position",
      "strikethrough-thickness",
      "stroke-dasharray",
      "stroke-dashoffset",
      "stroke-linecap",
      "stroke-linejoin",
      "stroke-miterlimit",
      "stroke-opacity",
      "stroke-width",
      "text-anchor",
      "text-decoration",
      "text-rendering",
      "underline-position",
      "underline-thickness",
      "unicode-bidi",
      "unicode-range",
      "units-per-em",
      "v-alphabetic",
      "v-hanging",
      "v-ideographic",
      "v-mathematical",
      "vector-effect",
      "vert-adv-y",
      "vert-origin-x",
      "vert-origin-y",
      "word-spacing",
      "writing-mode",
      "xmlns:xlink",
      "x-height"
      // NOTE: if you add a camelCased prop to this list,
      // you'll need to set attributeName to name.toLowerCase()
      // instead in the assignment below.
    ].forEach(function(e) {
      var t = e.replace(ki, Vl);
      qt[t] = new sn(
        t,
        Mr,
        !1,
        // mustUseProperty
        e,
        null,
        // attributeNamespace
        !1,
        // sanitizeURL
        !1
      );
    }), [
      "xlink:actuate",
      "xlink:arcrole",
      "xlink:role",
      "xlink:show",
      "xlink:title",
      "xlink:type"
      // NOTE: if you add a camelCased prop to this list,
      // you'll need to set attributeName to name.toLowerCase()
      // instead in the assignment below.
    ].forEach(function(e) {
      var t = e.replace(ki, Vl);
      qt[t] = new sn(
        t,
        Mr,
        !1,
        // mustUseProperty
        e,
        "http://www.w3.org/1999/xlink",
        !1,
        // sanitizeURL
        !1
      );
    }), [
      "xml:base",
      "xml:lang",
      "xml:space"
      // NOTE: if you add a camelCased prop to this list,
      // you'll need to set attributeName to name.toLowerCase()
      // instead in the assignment below.
    ].forEach(function(e) {
      var t = e.replace(ki, Vl);
      qt[t] = new sn(
        t,
        Mr,
        !1,
        // mustUseProperty
        e,
        "http://www.w3.org/XML/1998/namespace",
        !1,
        // sanitizeURL
        !1
      );
    }), ["tabIndex", "crossOrigin"].forEach(function(e) {
      qt[e] = new sn(
        e,
        Mr,
        !1,
        // mustUseProperty
        e.toLowerCase(),
        // attributeName
        null,
        // attributeNamespace
        !1,
        // sanitizeURL
        !1
      );
    });
    var gc = "xlinkHref";
    qt[gc] = new sn(
      "xlinkHref",
      Mr,
      !1,
      // mustUseProperty
      "xlink:href",
      "http://www.w3.org/1999/xlink",
      !0,
      // sanitizeURL
      !1
    ), ["src", "href", "action", "formAction"].forEach(function(e) {
      qt[e] = new sn(
        e,
        Mr,
        !1,
        // mustUseProperty
        e.toLowerCase(),
        // attributeName
        null,
        // attributeNamespace
        !0,
        // sanitizeURL
        !0
      );
    });
    var Sc = /^[\u0000-\u001F ]*j[\r\n\t]*a[\r\n\t]*v[\r\n\t]*a[\r\n\t]*s[\r\n\t]*c[\r\n\t]*r[\r\n\t]*i[\r\n\t]*p[\r\n\t]*t[\r\n\t]*\:/i, $l = !1;
    function os(e) {
      !$l && Sc.test(e) && ($l = !0, m("A future version of React will block javascript: URLs as a security precaution. Use event handlers instead if you can. If you need to generate unsafe HTML try using dangerouslySetInnerHTML instead. React was passed %s.", JSON.stringify(e)));
    }
    function $a(e, t, a, i) {
      if (i.mustUseProperty) {
        var u = i.propertyName;
        return e[u];
      } else {
        Ha(a, t), i.sanitizeURL && os("" + a);
        var d = i.attributeName, h = null;
        if (i.type === ea) {
          if (e.hasAttribute(d)) {
            var S = e.getAttribute(d);
            return S === "" ? !0 : Zt(t, a, i, !1) ? S : S === "" + a ? a : S;
          }
        } else if (e.hasAttribute(d)) {
          if (Zt(t, a, i, !1))
            return e.getAttribute(d);
          if (i.type === Wn)
            return a;
          h = e.getAttribute(d);
        }
        return Zt(t, a, i, !1) ? h === null ? a : h : h === "" + a ? a : h;
      }
    }
    function io(e, t, a, i) {
      {
        if (!Mn(t))
          return;
        if (!e.hasAttribute(t))
          return a === void 0 ? void 0 : null;
        var u = e.getAttribute(t);
        return Ha(a, t), u === "" + a ? a : u;
      }
    }
    function ai(e, t, a, i) {
      var u = pa(t);
      if (!Cn(t, u, i)) {
        if (Zt(t, a, u, i) && (a = null), i || u === null) {
          if (Mn(t)) {
            var d = t;
            a === null ? e.removeAttribute(d) : (Ha(a, t), e.setAttribute(d, "" + a));
          }
          return;
        }
        var h = u.mustUseProperty;
        if (h) {
          var S = u.propertyName;
          if (a === null) {
            var b = u.type;
            e[S] = b === Wn ? !1 : "";
          } else
            e[S] = a;
          return;
        }
        var k = u.attributeName, _ = u.attributeNamespace;
        if (a === null)
          e.removeAttribute(k);
        else {
          var P = u.type, z;
          P === Wn || P === ea && a === !0 ? z = "" : (Ha(a, k), z = "" + a, u.sanitizeURL && os(z.toString())), _ ? e.setAttributeNS(_, k, z) : e.setAttribute(k, z);
        }
      }
    }
    var oo = Symbol.for("react.element"), ta = Symbol.for("react.portal"), Ba = Symbol.for("react.fragment"), lo = Symbol.for("react.strict_mode"), uo = Symbol.for("react.profiler"), Bl = Symbol.for("react.provider"), ls = Symbol.for("react.context"), vn = Symbol.for("react.forward_ref"), va = Symbol.for("react.suspense"), Zo = Symbol.for("react.suspense_list"), so = Symbol.for("react.memo"), Qn = Symbol.for("react.lazy"), Ec = Symbol.for("react.scope"), bc = Symbol.for("react.debug_trace_mode"), us = Symbol.for("react.offscreen"), Cc = Symbol.for("react.legacy_hidden"), wd = Symbol.for("react.cache"), Td = Symbol.for("react.tracing_marker"), rn = Symbol.iterator, Rd = "@@iterator";
    function Ia(e) {
      if (e === null || typeof e != "object")
        return null;
      var t = rn && e[rn] || e[Rd];
      return typeof t == "function" ? t : null;
    }
    var dt = Object.assign, ii = 0, co, Il, fo, Nr, ss, dr, cs;
    function fs() {
    }
    fs.__reactDisabledLog = !0;
    function wc() {
      {
        if (ii === 0) {
          co = console.log, Il = console.info, fo = console.warn, Nr = console.error, ss = console.group, dr = console.groupCollapsed, cs = console.groupEnd;
          var e = {
            configurable: !0,
            enumerable: !0,
            value: fs,
            writable: !0
          };
          Object.defineProperties(console, {
            info: e,
            log: e,
            warn: e,
            error: e,
            group: e,
            groupCollapsed: e,
            groupEnd: e
          });
        }
        ii++;
      }
    }
    function Yl() {
      {
        if (ii--, ii === 0) {
          var e = {
            configurable: !0,
            enumerable: !0,
            writable: !0
          };
          Object.defineProperties(console, {
            log: dt({}, e, {
              value: co
            }),
            info: dt({}, e, {
              value: Il
            }),
            warn: dt({}, e, {
              value: fo
            }),
            error: dt({}, e, {
              value: Nr
            }),
            group: dt({}, e, {
              value: ss
            }),
            groupCollapsed: dt({}, e, {
              value: dr
            }),
            groupEnd: dt({}, e, {
              value: cs
            })
          });
        }
        ii < 0 && m("disabledDepth fell below zero. This is a bug in React. Please file an issue.");
      }
    }
    var po = c.ReactCurrentDispatcher, Ya;
    function pr(e, t, a) {
      {
        if (Ya === void 0)
          try {
            throw Error();
          } catch (u) {
            var i = u.stack.trim().match(/\n( *(at )?)/);
            Ya = i && i[1] || "";
          }
        return `
` + Ya + e;
      }
    }
    var vo = !1, ho;
    {
      var mo = typeof WeakMap == "function" ? WeakMap : Map;
      ho = new mo();
    }
    function Wl(e, t) {
      if (!e || vo)
        return "";
      {
        var a = ho.get(e);
        if (a !== void 0)
          return a;
      }
      var i;
      vo = !0;
      var u = Error.prepareStackTrace;
      Error.prepareStackTrace = void 0;
      var d;
      d = po.current, po.current = null, wc();
      try {
        if (t) {
          var h = function() {
            throw Error();
          };
          if (Object.defineProperty(h.prototype, "props", {
            set: function() {
              throw Error();
            }
          }), typeof Reflect == "object" && Reflect.construct) {
            try {
              Reflect.construct(h, []);
            } catch (Z) {
              i = Z;
            }
            Reflect.construct(e, [], h);
          } else {
            try {
              h.call();
            } catch (Z) {
              i = Z;
            }
            e.call(h.prototype);
          }
        } else {
          try {
            throw Error();
          } catch (Z) {
            i = Z;
          }
          e();
        }
      } catch (Z) {
        if (Z && i && typeof Z.stack == "string") {
          for (var S = Z.stack.split(`
`), b = i.stack.split(`
`), k = S.length - 1, _ = b.length - 1; k >= 1 && _ >= 0 && S[k] !== b[_]; )
            _--;
          for (; k >= 1 && _ >= 0; k--, _--)
            if (S[k] !== b[_]) {
              if (k !== 1 || _ !== 1)
                do
                  if (k--, _--, _ < 0 || S[k] !== b[_]) {
                    var P = `
` + S[k].replace(" at new ", " at ");
                    return e.displayName && P.includes("<anonymous>") && (P = P.replace("<anonymous>", e.displayName)), typeof e == "function" && ho.set(e, P), P;
                  }
                while (k >= 1 && _ >= 0);
              break;
            }
        }
      } finally {
        vo = !1, po.current = d, Yl(), Error.prepareStackTrace = u;
      }
      var z = e ? e.displayName || e.name : "", q = z ? pr(z) : "";
      return typeof e == "function" && ho.set(e, q), q;
    }
    function Ql(e, t, a) {
      return Wl(e, !0);
    }
    function _i(e, t, a) {
      return Wl(e, !1);
    }
    function xd(e) {
      var t = e.prototype;
      return !!(t && t.isReactComponent);
    }
    function oi(e, t, a) {
      if (e == null)
        return "";
      if (typeof e == "function")
        return Wl(e, xd(e));
      if (typeof e == "string")
        return pr(e);
      switch (e) {
        case va:
          return pr("Suspense");
        case Zo:
          return pr("SuspenseList");
      }
      if (typeof e == "object")
        switch (e.$$typeof) {
          case vn:
            return _i(e.render);
          case so:
            return oi(e.type, t, a);
          case Qn: {
            var i = e, u = i._payload, d = i._init;
            try {
              return oi(d(u), t, a);
            } catch {
            }
          }
        }
      return "";
    }
    function Nt(e) {
      switch (e._debugOwner && e._debugOwner.type, e._debugSource, e.tag) {
        case F:
          return pr(e.type);
        case We:
          return pr("Lazy");
        case ne:
          return pr("Suspense");
        case ye:
          return pr("SuspenseList");
        case T:
        case A:
        case me:
          return _i(e.type);
        case fe:
          return _i(e.type.render);
        case D:
          return Ql(e.type);
        default:
          return "";
      }
    }
    function Gl(e) {
      try {
        var t = "", a = e;
        do
          t += Nt(a), a = a.return;
        while (a);
        return t;
      } catch (i) {
        return `
Error generating stack: ` + i.message + `
` + i.stack;
      }
    }
    function Jo(e, t, a) {
      var i = e.displayName;
      if (i)
        return i;
      var u = t.displayName || t.name || "";
      return u !== "" ? a + "(" + u + ")" : a;
    }
    function ql(e) {
      return e.displayName || "Context";
    }
    function Ct(e) {
      if (e == null)
        return null;
      if (typeof e.tag == "number" && m("Received an unexpected object in getComponentNameFromType(). This is likely a bug in React. Please file an issue."), typeof e == "function")
        return e.displayName || e.name || null;
      if (typeof e == "string")
        return e;
      switch (e) {
        case Ba:
          return "Fragment";
        case ta:
          return "Portal";
        case uo:
          return "Profiler";
        case lo:
          return "StrictMode";
        case va:
          return "Suspense";
        case Zo:
          return "SuspenseList";
      }
      if (typeof e == "object")
        switch (e.$$typeof) {
          case ls:
            var t = e;
            return ql(t) + ".Consumer";
          case Bl:
            var a = e;
            return ql(a._context) + ".Provider";
          case vn:
            return Jo(e, e.render, "ForwardRef");
          case so:
            var i = e.displayName || null;
            return i !== null ? i : Ct(e.type) || "Memo";
          case Qn: {
            var u = e, d = u._payload, h = u._init;
            try {
              return Ct(h(d));
            } catch {
              return null;
            }
          }
        }
      return null;
    }
    function Xl(e, t, a) {
      var i = t.displayName || t.name || "";
      return e.displayName || (i !== "" ? a + "(" + i + ")" : a);
    }
    function Kl(e) {
      return e.displayName || "Context";
    }
    function it(e) {
      var t = e.tag, a = e.type;
      switch (t) {
        case Ot:
          return "Cache";
        case W:
          var i = a;
          return Kl(i) + ".Consumer";
        case ve:
          var u = a;
          return Kl(u._context) + ".Provider";
        case Rt:
          return "DehydratedFragment";
        case fe:
          return Xl(a, a.render, "ForwardRef");
        case H:
          return "Fragment";
        case F:
          return a;
        case j:
          return "Portal";
        case L:
          return "Root";
        case $:
          return "Text";
        case We:
          return Ct(a);
        case G:
          return a === lo ? "StrictMode" : "Mode";
        case qe:
          return "Offscreen";
        case oe:
          return "Profiler";
        case Ue:
          return "Scope";
        case ne:
          return "Suspense";
        case ye:
          return "SuspenseList";
        case mt:
          return "TracingMarker";
        case D:
        case T:
        case Et:
        case A:
        case Ce:
        case me:
          if (typeof a == "function")
            return a.displayName || a.name || null;
          if (typeof a == "string")
            return a;
          break;
      }
      return null;
    }
    var el = c.ReactDebugCurrentFrame, cn = null, Ar = !1;
    function vr() {
      {
        if (cn === null)
          return null;
        var e = cn._debugOwner;
        if (e !== null && typeof e < "u")
          return it(e);
      }
      return null;
    }
    function yo() {
      return cn === null ? "" : Gl(cn);
    }
    function hn() {
      el.getCurrentStack = null, cn = null, Ar = !1;
    }
    function Ft(e) {
      el.getCurrentStack = e === null ? null : yo, cn = e, Ar = !1;
    }
    function Tc() {
      return cn;
    }
    function zr(e) {
      Ar = e;
    }
    function Hn(e) {
      return "" + e;
    }
    function li(e) {
      switch (typeof e) {
        case "boolean":
        case "number":
        case "string":
        case "undefined":
          return e;
        case "object":
          return Or(e), e;
        default:
          return "";
      }
    }
    var Rc = {
      button: !0,
      checkbox: !0,
      image: !0,
      hidden: !0,
      radio: !0,
      reset: !0,
      submit: !0
    };
    function Di(e, t) {
      Rc[t.type] || t.onChange || t.onInput || t.readOnly || t.disabled || t.value == null || m("You provided a `value` prop to a form field without an `onChange` handler. This will render a read-only field. If the field should be mutable use `defaultValue`. Otherwise, set either `onChange` or `readOnly`."), t.onChange || t.readOnly || t.disabled || t.checked == null || m("You provided a `checked` prop to a form field without an `onChange` handler. This will render a read-only field. If the field should be mutable use `defaultChecked`. Otherwise, set either `onChange` or `readOnly`.");
    }
    function go(e) {
      var t = e.type, a = e.nodeName;
      return a && a.toLowerCase() === "input" && (t === "checkbox" || t === "radio");
    }
    function xc(e) {
      return e._valueTracker;
    }
    function ha(e) {
      e._valueTracker = null;
    }
    function So(e) {
      var t = "";
      return e && (go(e) ? t = e.checked ? "true" : "false" : t = e.value), t;
    }
    function Oi(e) {
      var t = go(e) ? "checked" : "value", a = Object.getOwnPropertyDescriptor(e.constructor.prototype, t);
      Or(e[t]);
      var i = "" + e[t];
      if (!(e.hasOwnProperty(t) || typeof a > "u" || typeof a.get != "function" || typeof a.set != "function")) {
        var u = a.get, d = a.set;
        Object.defineProperty(e, t, {
          configurable: !0,
          get: function() {
            return u.call(this);
          },
          set: function(S) {
            Or(S), i = "" + S, d.call(this, S);
          }
        }), Object.defineProperty(e, t, {
          enumerable: a.enumerable
        });
        var h = {
          getValue: function() {
            return i;
          },
          setValue: function(S) {
            Or(S), i = "" + S;
          },
          stopTracking: function() {
            ha(e), delete e[t];
          }
        };
        return h;
      }
    }
    function ma(e) {
      xc(e) || (e._valueTracker = Oi(e));
    }
    function Zl(e) {
      if (!e)
        return !1;
      var t = xc(e);
      if (!t)
        return !0;
      var a = t.getValue(), i = So(e);
      return i !== a ? (t.setValue(i), !0) : !1;
    }
    function Eo(e) {
      if (e = e || (typeof document < "u" ? document : void 0), typeof e > "u")
        return null;
      try {
        return e.activeElement || e.body;
      } catch {
        return e.body;
      }
    }
    var bo = !1, tl = !1, Jl = !1, ds = !1;
    function Wa(e) {
      var t = e.type === "checkbox" || e.type === "radio";
      return t ? e.checked != null : e.value != null;
    }
    function C(e, t) {
      var a = e, i = t.checked, u = dt({}, t, {
        defaultChecked: void 0,
        defaultValue: void 0,
        value: void 0,
        checked: i ?? a._wrapperState.initialChecked
      });
      return u;
    }
    function M(e, t) {
      Di("input", t), t.checked !== void 0 && t.defaultChecked !== void 0 && !tl && (m("%s contains an input of type %s with both checked and defaultChecked props. Input elements must be either controlled or uncontrolled (specify either the checked prop, or the defaultChecked prop, but not both). Decide between using a controlled or uncontrolled input element and remove one of these props. More info: https://reactjs.org/link/controlled-components", vr() || "A component", t.type), tl = !0), t.value !== void 0 && t.defaultValue !== void 0 && !bo && (m("%s contains an input of type %s with both value and defaultValue props. Input elements must be either controlled or uncontrolled (specify either the value prop, or the defaultValue prop, but not both). Decide between using a controlled or uncontrolled input element and remove one of these props. More info: https://reactjs.org/link/controlled-components", vr() || "A component", t.type), bo = !0);
      var a = e, i = t.defaultValue == null ? "" : t.defaultValue;
      a._wrapperState = {
        initialChecked: t.checked != null ? t.checked : t.defaultChecked,
        initialValue: li(t.value != null ? t.value : i),
        controlled: Wa(t)
      };
    }
    function X(e, t) {
      var a = e, i = t.checked;
      i != null && ai(a, "checked", i, !1);
    }
    function J(e, t) {
      var a = e;
      {
        var i = Wa(t);
        !a._wrapperState.controlled && i && !ds && (m("A component is changing an uncontrolled input to be controlled. This is likely caused by the value changing from undefined to a defined value, which should not happen. Decide between using a controlled or uncontrolled input element for the lifetime of the component. More info: https://reactjs.org/link/controlled-components"), ds = !0), a._wrapperState.controlled && !i && !Jl && (m("A component is changing a controlled input to be uncontrolled. This is likely caused by the value changing from a defined to undefined, which should not happen. Decide between using a controlled or uncontrolled input element for the lifetime of the component. More info: https://reactjs.org/link/controlled-components"), Jl = !0);
      }
      X(e, t);
      var u = li(t.value), d = t.type;
      if (u != null)
        d === "number" ? (u === 0 && a.value === "" || // We explicitly want to coerce to number here if possible.
        // eslint-disable-next-line
        a.value != u) && (a.value = Hn(u)) : a.value !== Hn(u) && (a.value = Hn(u));
      else if (d === "submit" || d === "reset") {
        a.removeAttribute("value");
        return;
      }
      t.hasOwnProperty("value") ? Ge(a, t.type, u) : t.hasOwnProperty("defaultValue") && Ge(a, t.type, li(t.defaultValue)), t.checked == null && t.defaultChecked != null && (a.defaultChecked = !!t.defaultChecked);
    }
    function de(e, t, a) {
      var i = e;
      if (t.hasOwnProperty("value") || t.hasOwnProperty("defaultValue")) {
        var u = t.type, d = u === "submit" || u === "reset";
        if (d && (t.value === void 0 || t.value === null))
          return;
        var h = Hn(i._wrapperState.initialValue);
        a || h !== i.value && (i.value = h), i.defaultValue = h;
      }
      var S = i.name;
      S !== "" && (i.name = ""), i.defaultChecked = !i.defaultChecked, i.defaultChecked = !!i._wrapperState.initialChecked, S !== "" && (i.name = S);
    }
    function Xe(e, t) {
      var a = e;
      J(a, t), be(a, t);
    }
    function be(e, t) {
      var a = t.name;
      if (t.type === "radio" && a != null) {
        for (var i = e; i.parentNode; )
          i = i.parentNode;
        Ha(a, "name");
        for (var u = i.querySelectorAll("input[name=" + JSON.stringify("" + a) + '][type="radio"]'), d = 0; d < u.length; d++) {
          var h = u[d];
          if (!(h === e || h.form !== e.form)) {
            var S = hm(h);
            if (!S)
              throw new Error("ReactDOMInput: Mixing React and non-React radio inputs with the same `name` is not supported.");
            Zl(h), J(h, S);
          }
        }
      }
    }
    function Ge(e, t, a) {
      // Focused number inputs synchronize on blur. See ChangeEventPlugin.js
      (t !== "number" || Eo(e.ownerDocument) !== e) && (a == null ? e.defaultValue = Hn(e._wrapperState.initialValue) : e.defaultValue !== Hn(a) && (e.defaultValue = Hn(a)));
    }
    var pt = !1, Dt = !1, Wt = !1;
    function $t(e, t) {
      t.value == null && (typeof t.children == "object" && t.children !== null ? l.Children.forEach(t.children, function(a) {
        a != null && (typeof a == "string" || typeof a == "number" || Dt || (Dt = !0, m("Cannot infer the option value of complex children. Pass a `value` prop or use a plain string as children to <option>.")));
      }) : t.dangerouslySetInnerHTML != null && (Wt || (Wt = !0, m("Pass a `value` prop if you set dangerouslyInnerHTML so React knows which value should be selected.")))), t.selected != null && !pt && (m("Use the `defaultValue` or `value` props on <select> instead of setting `selected` on <option>."), pt = !0);
    }
    function Qt(e, t) {
      t.value != null && e.setAttribute("value", Hn(li(t.value)));
    }
    var Xt = Array.isArray;
    function wt(e) {
      return Xt(e);
    }
    var Mi;
    Mi = !1;
    function eu() {
      var e = vr();
      return e ? `

Check the render method of \`` + e + "`." : "";
    }
    var ps = ["value", "defaultValue"];
    function kd(e) {
      {
        Di("select", e);
        for (var t = 0; t < ps.length; t++) {
          var a = ps[t];
          if (e[a] != null) {
            var i = wt(e[a]);
            e.multiple && !i ? m("The `%s` prop supplied to <select> must be an array if `multiple` is true.%s", a, eu()) : !e.multiple && i && m("The `%s` prop supplied to <select> must be a scalar value if `multiple` is false.%s", a, eu());
          }
        }
      }
    }
    function Qa(e, t, a, i) {
      var u = e.options;
      if (t) {
        for (var d = a, h = {}, S = 0; S < d.length; S++)
          h["$" + d[S]] = !0;
        for (var b = 0; b < u.length; b++) {
          var k = h.hasOwnProperty("$" + u[b].value);
          u[b].selected !== k && (u[b].selected = k), k && i && (u[b].defaultSelected = !0);
        }
      } else {
        for (var _ = Hn(li(a)), P = null, z = 0; z < u.length; z++) {
          if (u[z].value === _) {
            u[z].selected = !0, i && (u[z].defaultSelected = !0);
            return;
          }
          P === null && !u[z].disabled && (P = u[z]);
        }
        P !== null && (P.selected = !0);
      }
    }
    function vs(e, t) {
      return dt({}, t, {
        value: void 0
      });
    }
    function hs(e, t) {
      var a = e;
      kd(t), a._wrapperState = {
        wasMultiple: !!t.multiple
      }, t.value !== void 0 && t.defaultValue !== void 0 && !Mi && (m("Select elements must be either controlled or uncontrolled (specify either the value prop, or the defaultValue prop, but not both). Decide between using a controlled or uncontrolled select element and remove one of these props. More info: https://reactjs.org/link/controlled-components"), Mi = !0);
    }
    function _d(e, t) {
      var a = e;
      a.multiple = !!t.multiple;
      var i = t.value;
      i != null ? Qa(a, !!t.multiple, i, !1) : t.defaultValue != null && Qa(a, !!t.multiple, t.defaultValue, !0);
    }
    function Ky(e, t) {
      var a = e, i = a._wrapperState.wasMultiple;
      a._wrapperState.wasMultiple = !!t.multiple;
      var u = t.value;
      u != null ? Qa(a, !!t.multiple, u, !1) : i !== !!t.multiple && (t.defaultValue != null ? Qa(a, !!t.multiple, t.defaultValue, !0) : Qa(a, !!t.multiple, t.multiple ? [] : "", !1));
    }
    function Zy(e, t) {
      var a = e, i = t.value;
      i != null && Qa(a, !!t.multiple, i, !1);
    }
    var Dd = !1;
    function Od(e, t) {
      var a = e;
      if (t.dangerouslySetInnerHTML != null)
        throw new Error("`dangerouslySetInnerHTML` does not make sense on <textarea>.");
      var i = dt({}, t, {
        value: void 0,
        defaultValue: void 0,
        children: Hn(a._wrapperState.initialValue)
      });
      return i;
    }
    function jv(e, t) {
      var a = e;
      Di("textarea", t), t.value !== void 0 && t.defaultValue !== void 0 && !Dd && (m("%s contains a textarea with both value and defaultValue props. Textarea elements must be either controlled or uncontrolled (specify either the value prop, or the defaultValue prop, but not both). Decide between using a controlled or uncontrolled textarea and remove one of these props. More info: https://reactjs.org/link/controlled-components", vr() || "A component"), Dd = !0);
      var i = t.value;
      if (i == null) {
        var u = t.children, d = t.defaultValue;
        if (u != null) {
          m("Use the `defaultValue` or `value` props instead of setting children on <textarea>.");
          {
            if (d != null)
              throw new Error("If you supply `defaultValue` on a <textarea>, do not pass children.");
            if (wt(u)) {
              if (u.length > 1)
                throw new Error("<textarea> can only have at most one child.");
              u = u[0];
            }
            d = u;
          }
        }
        d == null && (d = ""), i = d;
      }
      a._wrapperState = {
        initialValue: li(i)
      };
    }
    function Vv(e, t) {
      var a = e, i = li(t.value), u = li(t.defaultValue);
      if (i != null) {
        var d = Hn(i);
        d !== a.value && (a.value = d), t.defaultValue == null && a.defaultValue !== d && (a.defaultValue = d);
      }
      u != null && (a.defaultValue = Hn(u));
    }
    function $v(e, t) {
      var a = e, i = a.textContent;
      i === a._wrapperState.initialValue && i !== "" && i !== null && (a.value = i);
    }
    function Md(e, t) {
      Vv(e, t);
    }
    var Li = "http://www.w3.org/1999/xhtml", Jy = "http://www.w3.org/1998/Math/MathML", Ld = "http://www.w3.org/2000/svg";
    function kc(e) {
      switch (e) {
        case "svg":
          return Ld;
        case "math":
          return Jy;
        default:
          return Li;
      }
    }
    function Nd(e, t) {
      return e == null || e === Li ? kc(t) : e === Ld && t === "foreignObject" ? Li : e;
    }
    var eg = function(e) {
      return typeof MSApp < "u" && MSApp.execUnsafeLocalFunction ? function(t, a, i, u) {
        MSApp.execUnsafeLocalFunction(function() {
          return e(t, a, i, u);
        });
      } : e;
    }, _c, Bv = eg(function(e, t) {
      if (e.namespaceURI === Ld && !("innerHTML" in e)) {
        _c = _c || document.createElement("div"), _c.innerHTML = "<svg>" + t.valueOf().toString() + "</svg>";
        for (var a = _c.firstChild; e.firstChild; )
          e.removeChild(e.firstChild);
        for (; a.firstChild; )
          e.appendChild(a.firstChild);
        return;
      }
      e.innerHTML = t;
    }), Ur = 1, Ni = 3, kn = 8, Ga = 9, nl = 11, Dc = function(e, t) {
      if (t) {
        var a = e.firstChild;
        if (a && a === e.lastChild && a.nodeType === Ni) {
          a.nodeValue = t;
          return;
        }
      }
      e.textContent = t;
    }, Iv = {
      animation: ["animationDelay", "animationDirection", "animationDuration", "animationFillMode", "animationIterationCount", "animationName", "animationPlayState", "animationTimingFunction"],
      background: ["backgroundAttachment", "backgroundClip", "backgroundColor", "backgroundImage", "backgroundOrigin", "backgroundPositionX", "backgroundPositionY", "backgroundRepeat", "backgroundSize"],
      backgroundPosition: ["backgroundPositionX", "backgroundPositionY"],
      border: ["borderBottomColor", "borderBottomStyle", "borderBottomWidth", "borderImageOutset", "borderImageRepeat", "borderImageSlice", "borderImageSource", "borderImageWidth", "borderLeftColor", "borderLeftStyle", "borderLeftWidth", "borderRightColor", "borderRightStyle", "borderRightWidth", "borderTopColor", "borderTopStyle", "borderTopWidth"],
      borderBlockEnd: ["borderBlockEndColor", "borderBlockEndStyle", "borderBlockEndWidth"],
      borderBlockStart: ["borderBlockStartColor", "borderBlockStartStyle", "borderBlockStartWidth"],
      borderBottom: ["borderBottomColor", "borderBottomStyle", "borderBottomWidth"],
      borderColor: ["borderBottomColor", "borderLeftColor", "borderRightColor", "borderTopColor"],
      borderImage: ["borderImageOutset", "borderImageRepeat", "borderImageSlice", "borderImageSource", "borderImageWidth"],
      borderInlineEnd: ["borderInlineEndColor", "borderInlineEndStyle", "borderInlineEndWidth"],
      borderInlineStart: ["borderInlineStartColor", "borderInlineStartStyle", "borderInlineStartWidth"],
      borderLeft: ["borderLeftColor", "borderLeftStyle", "borderLeftWidth"],
      borderRadius: ["borderBottomLeftRadius", "borderBottomRightRadius", "borderTopLeftRadius", "borderTopRightRadius"],
      borderRight: ["borderRightColor", "borderRightStyle", "borderRightWidth"],
      borderStyle: ["borderBottomStyle", "borderLeftStyle", "borderRightStyle", "borderTopStyle"],
      borderTop: ["borderTopColor", "borderTopStyle", "borderTopWidth"],
      borderWidth: ["borderBottomWidth", "borderLeftWidth", "borderRightWidth", "borderTopWidth"],
      columnRule: ["columnRuleColor", "columnRuleStyle", "columnRuleWidth"],
      columns: ["columnCount", "columnWidth"],
      flex: ["flexBasis", "flexGrow", "flexShrink"],
      flexFlow: ["flexDirection", "flexWrap"],
      font: ["fontFamily", "fontFeatureSettings", "fontKerning", "fontLanguageOverride", "fontSize", "fontSizeAdjust", "fontStretch", "fontStyle", "fontVariant", "fontVariantAlternates", "fontVariantCaps", "fontVariantEastAsian", "fontVariantLigatures", "fontVariantNumeric", "fontVariantPosition", "fontWeight", "lineHeight"],
      fontVariant: ["fontVariantAlternates", "fontVariantCaps", "fontVariantEastAsian", "fontVariantLigatures", "fontVariantNumeric", "fontVariantPosition"],
      gap: ["columnGap", "rowGap"],
      grid: ["gridAutoColumns", "gridAutoFlow", "gridAutoRows", "gridTemplateAreas", "gridTemplateColumns", "gridTemplateRows"],
      gridArea: ["gridColumnEnd", "gridColumnStart", "gridRowEnd", "gridRowStart"],
      gridColumn: ["gridColumnEnd", "gridColumnStart"],
      gridColumnGap: ["columnGap"],
      gridGap: ["columnGap", "rowGap"],
      gridRow: ["gridRowEnd", "gridRowStart"],
      gridRowGap: ["rowGap"],
      gridTemplate: ["gridTemplateAreas", "gridTemplateColumns", "gridTemplateRows"],
      listStyle: ["listStyleImage", "listStylePosition", "listStyleType"],
      margin: ["marginBottom", "marginLeft", "marginRight", "marginTop"],
      marker: ["markerEnd", "markerMid", "markerStart"],
      mask: ["maskClip", "maskComposite", "maskImage", "maskMode", "maskOrigin", "maskPositionX", "maskPositionY", "maskRepeat", "maskSize"],
      maskPosition: ["maskPositionX", "maskPositionY"],
      outline: ["outlineColor", "outlineStyle", "outlineWidth"],
      overflow: ["overflowX", "overflowY"],
      padding: ["paddingBottom", "paddingLeft", "paddingRight", "paddingTop"],
      placeContent: ["alignContent", "justifyContent"],
      placeItems: ["alignItems", "justifyItems"],
      placeSelf: ["alignSelf", "justifySelf"],
      textDecoration: ["textDecorationColor", "textDecorationLine", "textDecorationStyle"],
      textEmphasis: ["textEmphasisColor", "textEmphasisStyle"],
      transition: ["transitionDelay", "transitionDuration", "transitionProperty", "transitionTimingFunction"],
      wordWrap: ["overflowWrap"]
    }, tu = {
      animationIterationCount: !0,
      aspectRatio: !0,
      borderImageOutset: !0,
      borderImageSlice: !0,
      borderImageWidth: !0,
      boxFlex: !0,
      boxFlexGroup: !0,
      boxOrdinalGroup: !0,
      columnCount: !0,
      columns: !0,
      flex: !0,
      flexGrow: !0,
      flexPositive: !0,
      flexShrink: !0,
      flexNegative: !0,
      flexOrder: !0,
      gridArea: !0,
      gridRow: !0,
      gridRowEnd: !0,
      gridRowSpan: !0,
      gridRowStart: !0,
      gridColumn: !0,
      gridColumnEnd: !0,
      gridColumnSpan: !0,
      gridColumnStart: !0,
      fontWeight: !0,
      lineClamp: !0,
      lineHeight: !0,
      opacity: !0,
      order: !0,
      orphans: !0,
      tabSize: !0,
      widows: !0,
      zIndex: !0,
      zoom: !0,
      // SVG-related properties
      fillOpacity: !0,
      floodOpacity: !0,
      stopOpacity: !0,
      strokeDasharray: !0,
      strokeDashoffset: !0,
      strokeMiterlimit: !0,
      strokeOpacity: !0,
      strokeWidth: !0
    };
    function Yv(e, t) {
      return e + t.charAt(0).toUpperCase() + t.substring(1);
    }
    var Wv = ["Webkit", "ms", "Moz", "O"];
    Object.keys(tu).forEach(function(e) {
      Wv.forEach(function(t) {
        tu[Yv(t, e)] = tu[e];
      });
    });
    function Oc(e, t, a) {
      var i = t == null || typeof t == "boolean" || t === "";
      return i ? "" : !a && typeof t == "number" && t !== 0 && !(tu.hasOwnProperty(e) && tu[e]) ? t + "px" : (Dr(t, e), ("" + t).trim());
    }
    var nu = /([A-Z])/g, tg = /^ms-/;
    function ng(e) {
      return e.replace(nu, "-$1").toLowerCase().replace(tg, "-ms-");
    }
    var Qv = function() {
    };
    {
      var Gv = /^(?:webkit|moz|o)[A-Z]/, qv = /^-ms-/, ms = /-(.)/g, ru = /;\s*$/, au = {}, iu = {}, Xv = !1, Ad = !1, zd = function(e) {
        return e.replace(ms, function(t, a) {
          return a.toUpperCase();
        });
      }, Ud = function(e) {
        au.hasOwnProperty(e) && au[e] || (au[e] = !0, m(
          "Unsupported style property %s. Did you mean %s?",
          e,
          // As Andi Smith suggests
          // (http://www.andismith.com/blog/2012/02/modernizr-prefixed/), an `-ms` prefix
          // is converted to lowercase `ms`.
          zd(e.replace(qv, "ms-"))
        ));
      }, Kv = function(e) {
        au.hasOwnProperty(e) && au[e] || (au[e] = !0, m("Unsupported vendor-prefixed style property %s. Did you mean %s?", e, e.charAt(0).toUpperCase() + e.slice(1)));
      }, Zv = function(e, t) {
        iu.hasOwnProperty(t) && iu[t] || (iu[t] = !0, m(`Style property values shouldn't contain a semicolon. Try "%s: %s" instead.`, e, t.replace(ru, "")));
      }, Jv = function(e, t) {
        Xv || (Xv = !0, m("`NaN` is an invalid value for the `%s` css style property.", e));
      }, rg = function(e, t) {
        Ad || (Ad = !0, m("`Infinity` is an invalid value for the `%s` css style property.", e));
      };
      Qv = function(e, t) {
        e.indexOf("-") > -1 ? Ud(e) : Gv.test(e) ? Kv(e) : ru.test(t) && Zv(e, t), typeof t == "number" && (isNaN(t) ? Jv(e, t) : isFinite(t) || rg(e, t));
      };
    }
    var ag = Qv;
    function ig(e) {
      {
        var t = "", a = "";
        for (var i in e)
          if (e.hasOwnProperty(i)) {
            var u = e[i];
            if (u != null) {
              var d = i.indexOf("--") === 0;
              t += a + (d ? i : ng(i)) + ":", t += Oc(i, u, d), a = ";";
            }
          }
        return t || null;
      }
    }
    function eh(e, t) {
      var a = e.style;
      for (var i in t)
        if (t.hasOwnProperty(i)) {
          var u = i.indexOf("--") === 0;
          u || ag(i, t[i]);
          var d = Oc(i, t[i], u);
          i === "float" && (i = "cssFloat"), u ? a.setProperty(i, d) : a[i] = d;
        }
    }
    function og(e) {
      return e == null || typeof e == "boolean" || e === "";
    }
    function ya(e) {
      var t = {};
      for (var a in e)
        for (var i = Iv[a] || [a], u = 0; u < i.length; u++)
          t[i[u]] = a;
      return t;
    }
    function ys(e, t) {
      {
        if (!t)
          return;
        var a = ya(e), i = ya(t), u = {};
        for (var d in a) {
          var h = a[d], S = i[d];
          if (S && h !== S) {
            var b = h + "," + S;
            if (u[b])
              continue;
            u[b] = !0, m("%s a style property during rerender (%s) when a conflicting property is set (%s) can lead to styling bugs. To avoid this, don't mix shorthand and non-shorthand properties for the same value; instead, replace the shorthand with separate values.", og(e[h]) ? "Removing" : "Updating", h, S);
          }
        }
      }
    }
    var th = {
      area: !0,
      base: !0,
      br: !0,
      col: !0,
      embed: !0,
      hr: !0,
      img: !0,
      input: !0,
      keygen: !0,
      link: !0,
      meta: !0,
      param: !0,
      source: !0,
      track: !0,
      wbr: !0
      // NOTE: menuitem's close tag should be omitted, but that causes problems.
    }, nh = dt({
      menuitem: !0
    }, th), rh = "__html";
    function Mc(e, t) {
      if (t) {
        if (nh[e] && (t.children != null || t.dangerouslySetInnerHTML != null))
          throw new Error(e + " is a void element tag and must neither have `children` nor use `dangerouslySetInnerHTML`.");
        if (t.dangerouslySetInnerHTML != null) {
          if (t.children != null)
            throw new Error("Can only set one of `children` or `props.dangerouslySetInnerHTML`.");
          if (typeof t.dangerouslySetInnerHTML != "object" || !(rh in t.dangerouslySetInnerHTML))
            throw new Error("`props.dangerouslySetInnerHTML` must be in the form `{__html: ...}`. Please visit https://reactjs.org/link/dangerously-set-inner-html for more information.");
        }
        if (!t.suppressContentEditableWarning && t.contentEditable && t.children != null && m("A component is `contentEditable` and contains `children` managed by React. It is now your responsibility to guarantee that none of those nodes are unexpectedly modified or duplicated. This is probably not intentional."), t.style != null && typeof t.style != "object")
          throw new Error("The `style` prop expects a mapping from style properties to values, not a string. For example, style={{marginRight: spacing + 'em'}} when using JSX.");
      }
    }
    function Ai(e, t) {
      if (e.indexOf("-") === -1)
        return typeof t.is == "string";
      switch (e) {
        case "annotation-xml":
        case "color-profile":
        case "font-face":
        case "font-face-src":
        case "font-face-uri":
        case "font-face-format":
        case "font-face-name":
        case "missing-glyph":
          return !1;
        default:
          return !0;
      }
    }
    var Lc = {
      // HTML
      accept: "accept",
      acceptcharset: "acceptCharset",
      "accept-charset": "acceptCharset",
      accesskey: "accessKey",
      action: "action",
      allowfullscreen: "allowFullScreen",
      alt: "alt",
      as: "as",
      async: "async",
      autocapitalize: "autoCapitalize",
      autocomplete: "autoComplete",
      autocorrect: "autoCorrect",
      autofocus: "autoFocus",
      autoplay: "autoPlay",
      autosave: "autoSave",
      capture: "capture",
      cellpadding: "cellPadding",
      cellspacing: "cellSpacing",
      challenge: "challenge",
      charset: "charSet",
      checked: "checked",
      children: "children",
      cite: "cite",
      class: "className",
      classid: "classID",
      classname: "className",
      cols: "cols",
      colspan: "colSpan",
      content: "content",
      contenteditable: "contentEditable",
      contextmenu: "contextMenu",
      controls: "controls",
      controlslist: "controlsList",
      coords: "coords",
      crossorigin: "crossOrigin",
      dangerouslysetinnerhtml: "dangerouslySetInnerHTML",
      data: "data",
      datetime: "dateTime",
      default: "default",
      defaultchecked: "defaultChecked",
      defaultvalue: "defaultValue",
      defer: "defer",
      dir: "dir",
      disabled: "disabled",
      disablepictureinpicture: "disablePictureInPicture",
      disableremoteplayback: "disableRemotePlayback",
      download: "download",
      draggable: "draggable",
      enctype: "encType",
      enterkeyhint: "enterKeyHint",
      for: "htmlFor",
      form: "form",
      formmethod: "formMethod",
      formaction: "formAction",
      formenctype: "formEncType",
      formnovalidate: "formNoValidate",
      formtarget: "formTarget",
      frameborder: "frameBorder",
      headers: "headers",
      height: "height",
      hidden: "hidden",
      high: "high",
      href: "href",
      hreflang: "hrefLang",
      htmlfor: "htmlFor",
      httpequiv: "httpEquiv",
      "http-equiv": "httpEquiv",
      icon: "icon",
      id: "id",
      imagesizes: "imageSizes",
      imagesrcset: "imageSrcSet",
      innerhtml: "innerHTML",
      inputmode: "inputMode",
      integrity: "integrity",
      is: "is",
      itemid: "itemID",
      itemprop: "itemProp",
      itemref: "itemRef",
      itemscope: "itemScope",
      itemtype: "itemType",
      keyparams: "keyParams",
      keytype: "keyType",
      kind: "kind",
      label: "label",
      lang: "lang",
      list: "list",
      loop: "loop",
      low: "low",
      manifest: "manifest",
      marginwidth: "marginWidth",
      marginheight: "marginHeight",
      max: "max",
      maxlength: "maxLength",
      media: "media",
      mediagroup: "mediaGroup",
      method: "method",
      min: "min",
      minlength: "minLength",
      multiple: "multiple",
      muted: "muted",
      name: "name",
      nomodule: "noModule",
      nonce: "nonce",
      novalidate: "noValidate",
      open: "open",
      optimum: "optimum",
      pattern: "pattern",
      placeholder: "placeholder",
      playsinline: "playsInline",
      poster: "poster",
      preload: "preload",
      profile: "profile",
      radiogroup: "radioGroup",
      readonly: "readOnly",
      referrerpolicy: "referrerPolicy",
      rel: "rel",
      required: "required",
      reversed: "reversed",
      role: "role",
      rows: "rows",
      rowspan: "rowSpan",
      sandbox: "sandbox",
      scope: "scope",
      scoped: "scoped",
      scrolling: "scrolling",
      seamless: "seamless",
      selected: "selected",
      shape: "shape",
      size: "size",
      sizes: "sizes",
      span: "span",
      spellcheck: "spellCheck",
      src: "src",
      srcdoc: "srcDoc",
      srclang: "srcLang",
      srcset: "srcSet",
      start: "start",
      step: "step",
      style: "style",
      summary: "summary",
      tabindex: "tabIndex",
      target: "target",
      title: "title",
      type: "type",
      usemap: "useMap",
      value: "value",
      width: "width",
      wmode: "wmode",
      wrap: "wrap",
      // SVG
      about: "about",
      accentheight: "accentHeight",
      "accent-height": "accentHeight",
      accumulate: "accumulate",
      additive: "additive",
      alignmentbaseline: "alignmentBaseline",
      "alignment-baseline": "alignmentBaseline",
      allowreorder: "allowReorder",
      alphabetic: "alphabetic",
      amplitude: "amplitude",
      arabicform: "arabicForm",
      "arabic-form": "arabicForm",
      ascent: "ascent",
      attributename: "attributeName",
      attributetype: "attributeType",
      autoreverse: "autoReverse",
      azimuth: "azimuth",
      basefrequency: "baseFrequency",
      baselineshift: "baselineShift",
      "baseline-shift": "baselineShift",
      baseprofile: "baseProfile",
      bbox: "bbox",
      begin: "begin",
      bias: "bias",
      by: "by",
      calcmode: "calcMode",
      capheight: "capHeight",
      "cap-height": "capHeight",
      clip: "clip",
      clippath: "clipPath",
      "clip-path": "clipPath",
      clippathunits: "clipPathUnits",
      cliprule: "clipRule",
      "clip-rule": "clipRule",
      color: "color",
      colorinterpolation: "colorInterpolation",
      "color-interpolation": "colorInterpolation",
      colorinterpolationfilters: "colorInterpolationFilters",
      "color-interpolation-filters": "colorInterpolationFilters",
      colorprofile: "colorProfile",
      "color-profile": "colorProfile",
      colorrendering: "colorRendering",
      "color-rendering": "colorRendering",
      contentscripttype: "contentScriptType",
      contentstyletype: "contentStyleType",
      cursor: "cursor",
      cx: "cx",
      cy: "cy",
      d: "d",
      datatype: "datatype",
      decelerate: "decelerate",
      descent: "descent",
      diffuseconstant: "diffuseConstant",
      direction: "direction",
      display: "display",
      divisor: "divisor",
      dominantbaseline: "dominantBaseline",
      "dominant-baseline": "dominantBaseline",
      dur: "dur",
      dx: "dx",
      dy: "dy",
      edgemode: "edgeMode",
      elevation: "elevation",
      enablebackground: "enableBackground",
      "enable-background": "enableBackground",
      end: "end",
      exponent: "exponent",
      externalresourcesrequired: "externalResourcesRequired",
      fill: "fill",
      fillopacity: "fillOpacity",
      "fill-opacity": "fillOpacity",
      fillrule: "fillRule",
      "fill-rule": "fillRule",
      filter: "filter",
      filterres: "filterRes",
      filterunits: "filterUnits",
      floodopacity: "floodOpacity",
      "flood-opacity": "floodOpacity",
      floodcolor: "floodColor",
      "flood-color": "floodColor",
      focusable: "focusable",
      fontfamily: "fontFamily",
      "font-family": "fontFamily",
      fontsize: "fontSize",
      "font-size": "fontSize",
      fontsizeadjust: "fontSizeAdjust",
      "font-size-adjust": "fontSizeAdjust",
      fontstretch: "fontStretch",
      "font-stretch": "fontStretch",
      fontstyle: "fontStyle",
      "font-style": "fontStyle",
      fontvariant: "fontVariant",
      "font-variant": "fontVariant",
      fontweight: "fontWeight",
      "font-weight": "fontWeight",
      format: "format",
      from: "from",
      fx: "fx",
      fy: "fy",
      g1: "g1",
      g2: "g2",
      glyphname: "glyphName",
      "glyph-name": "glyphName",
      glyphorientationhorizontal: "glyphOrientationHorizontal",
      "glyph-orientation-horizontal": "glyphOrientationHorizontal",
      glyphorientationvertical: "glyphOrientationVertical",
      "glyph-orientation-vertical": "glyphOrientationVertical",
      glyphref: "glyphRef",
      gradienttransform: "gradientTransform",
      gradientunits: "gradientUnits",
      hanging: "hanging",
      horizadvx: "horizAdvX",
      "horiz-adv-x": "horizAdvX",
      horizoriginx: "horizOriginX",
      "horiz-origin-x": "horizOriginX",
      ideographic: "ideographic",
      imagerendering: "imageRendering",
      "image-rendering": "imageRendering",
      in2: "in2",
      in: "in",
      inlist: "inlist",
      intercept: "intercept",
      k1: "k1",
      k2: "k2",
      k3: "k3",
      k4: "k4",
      k: "k",
      kernelmatrix: "kernelMatrix",
      kernelunitlength: "kernelUnitLength",
      kerning: "kerning",
      keypoints: "keyPoints",
      keysplines: "keySplines",
      keytimes: "keyTimes",
      lengthadjust: "lengthAdjust",
      letterspacing: "letterSpacing",
      "letter-spacing": "letterSpacing",
      lightingcolor: "lightingColor",
      "lighting-color": "lightingColor",
      limitingconeangle: "limitingConeAngle",
      local: "local",
      markerend: "markerEnd",
      "marker-end": "markerEnd",
      markerheight: "markerHeight",
      markermid: "markerMid",
      "marker-mid": "markerMid",
      markerstart: "markerStart",
      "marker-start": "markerStart",
      markerunits: "markerUnits",
      markerwidth: "markerWidth",
      mask: "mask",
      maskcontentunits: "maskContentUnits",
      maskunits: "maskUnits",
      mathematical: "mathematical",
      mode: "mode",
      numoctaves: "numOctaves",
      offset: "offset",
      opacity: "opacity",
      operator: "operator",
      order: "order",
      orient: "orient",
      orientation: "orientation",
      origin: "origin",
      overflow: "overflow",
      overlineposition: "overlinePosition",
      "overline-position": "overlinePosition",
      overlinethickness: "overlineThickness",
      "overline-thickness": "overlineThickness",
      paintorder: "paintOrder",
      "paint-order": "paintOrder",
      panose1: "panose1",
      "panose-1": "panose1",
      pathlength: "pathLength",
      patterncontentunits: "patternContentUnits",
      patterntransform: "patternTransform",
      patternunits: "patternUnits",
      pointerevents: "pointerEvents",
      "pointer-events": "pointerEvents",
      points: "points",
      pointsatx: "pointsAtX",
      pointsaty: "pointsAtY",
      pointsatz: "pointsAtZ",
      prefix: "prefix",
      preservealpha: "preserveAlpha",
      preserveaspectratio: "preserveAspectRatio",
      primitiveunits: "primitiveUnits",
      property: "property",
      r: "r",
      radius: "radius",
      refx: "refX",
      refy: "refY",
      renderingintent: "renderingIntent",
      "rendering-intent": "renderingIntent",
      repeatcount: "repeatCount",
      repeatdur: "repeatDur",
      requiredextensions: "requiredExtensions",
      requiredfeatures: "requiredFeatures",
      resource: "resource",
      restart: "restart",
      result: "result",
      results: "results",
      rotate: "rotate",
      rx: "rx",
      ry: "ry",
      scale: "scale",
      security: "security",
      seed: "seed",
      shaperendering: "shapeRendering",
      "shape-rendering": "shapeRendering",
      slope: "slope",
      spacing: "spacing",
      specularconstant: "specularConstant",
      specularexponent: "specularExponent",
      speed: "speed",
      spreadmethod: "spreadMethod",
      startoffset: "startOffset",
      stddeviation: "stdDeviation",
      stemh: "stemh",
      stemv: "stemv",
      stitchtiles: "stitchTiles",
      stopcolor: "stopColor",
      "stop-color": "stopColor",
      stopopacity: "stopOpacity",
      "stop-opacity": "stopOpacity",
      strikethroughposition: "strikethroughPosition",
      "strikethrough-position": "strikethroughPosition",
      strikethroughthickness: "strikethroughThickness",
      "strikethrough-thickness": "strikethroughThickness",
      string: "string",
      stroke: "stroke",
      strokedasharray: "strokeDasharray",
      "stroke-dasharray": "strokeDasharray",
      strokedashoffset: "strokeDashoffset",
      "stroke-dashoffset": "strokeDashoffset",
      strokelinecap: "strokeLinecap",
      "stroke-linecap": "strokeLinecap",
      strokelinejoin: "strokeLinejoin",
      "stroke-linejoin": "strokeLinejoin",
      strokemiterlimit: "strokeMiterlimit",
      "stroke-miterlimit": "strokeMiterlimit",
      strokewidth: "strokeWidth",
      "stroke-width": "strokeWidth",
      strokeopacity: "strokeOpacity",
      "stroke-opacity": "strokeOpacity",
      suppresscontenteditablewarning: "suppressContentEditableWarning",
      suppresshydrationwarning: "suppressHydrationWarning",
      surfacescale: "surfaceScale",
      systemlanguage: "systemLanguage",
      tablevalues: "tableValues",
      targetx: "targetX",
      targety: "targetY",
      textanchor: "textAnchor",
      "text-anchor": "textAnchor",
      textdecoration: "textDecoration",
      "text-decoration": "textDecoration",
      textlength: "textLength",
      textrendering: "textRendering",
      "text-rendering": "textRendering",
      to: "to",
      transform: "transform",
      typeof: "typeof",
      u1: "u1",
      u2: "u2",
      underlineposition: "underlinePosition",
      "underline-position": "underlinePosition",
      underlinethickness: "underlineThickness",
      "underline-thickness": "underlineThickness",
      unicode: "unicode",
      unicodebidi: "unicodeBidi",
      "unicode-bidi": "unicodeBidi",
      unicoderange: "unicodeRange",
      "unicode-range": "unicodeRange",
      unitsperem: "unitsPerEm",
      "units-per-em": "unitsPerEm",
      unselectable: "unselectable",
      valphabetic: "vAlphabetic",
      "v-alphabetic": "vAlphabetic",
      values: "values",
      vectoreffect: "vectorEffect",
      "vector-effect": "vectorEffect",
      version: "version",
      vertadvy: "vertAdvY",
      "vert-adv-y": "vertAdvY",
      vertoriginx: "vertOriginX",
      "vert-origin-x": "vertOriginX",
      vertoriginy: "vertOriginY",
      "vert-origin-y": "vertOriginY",
      vhanging: "vHanging",
      "v-hanging": "vHanging",
      videographic: "vIdeographic",
      "v-ideographic": "vIdeographic",
      viewbox: "viewBox",
      viewtarget: "viewTarget",
      visibility: "visibility",
      vmathematical: "vMathematical",
      "v-mathematical": "vMathematical",
      vocab: "vocab",
      widths: "widths",
      wordspacing: "wordSpacing",
      "word-spacing": "wordSpacing",
      writingmode: "writingMode",
      "writing-mode": "writingMode",
      x1: "x1",
      x2: "x2",
      x: "x",
      xchannelselector: "xChannelSelector",
      xheight: "xHeight",
      "x-height": "xHeight",
      xlinkactuate: "xlinkActuate",
      "xlink:actuate": "xlinkActuate",
      xlinkarcrole: "xlinkArcrole",
      "xlink:arcrole": "xlinkArcrole",
      xlinkhref: "xlinkHref",
      "xlink:href": "xlinkHref",
      xlinkrole: "xlinkRole",
      "xlink:role": "xlinkRole",
      xlinkshow: "xlinkShow",
      "xlink:show": "xlinkShow",
      xlinktitle: "xlinkTitle",
      "xlink:title": "xlinkTitle",
      xlinktype: "xlinkType",
      "xlink:type": "xlinkType",
      xmlbase: "xmlBase",
      "xml:base": "xmlBase",
      xmllang: "xmlLang",
      "xml:lang": "xmlLang",
      xmlns: "xmlns",
      "xml:space": "xmlSpace",
      xmlnsxlink: "xmlnsXlink",
      "xmlns:xlink": "xmlnsXlink",
      xmlspace: "xmlSpace",
      y1: "y1",
      y2: "y2",
      y: "y",
      ychannelselector: "yChannelSelector",
      z: "z",
      zoomandpan: "zoomAndPan"
    }, ah = {
      "aria-current": 0,
      // state
      "aria-description": 0,
      "aria-details": 0,
      "aria-disabled": 0,
      // state
      "aria-hidden": 0,
      // state
      "aria-invalid": 0,
      // state
      "aria-keyshortcuts": 0,
      "aria-label": 0,
      "aria-roledescription": 0,
      // Widget Attributes
      "aria-autocomplete": 0,
      "aria-checked": 0,
      "aria-expanded": 0,
      "aria-haspopup": 0,
      "aria-level": 0,
      "aria-modal": 0,
      "aria-multiline": 0,
      "aria-multiselectable": 0,
      "aria-orientation": 0,
      "aria-placeholder": 0,
      "aria-pressed": 0,
      "aria-readonly": 0,
      "aria-required": 0,
      "aria-selected": 0,
      "aria-sort": 0,
      "aria-valuemax": 0,
      "aria-valuemin": 0,
      "aria-valuenow": 0,
      "aria-valuetext": 0,
      // Live Region Attributes
      "aria-atomic": 0,
      "aria-busy": 0,
      "aria-live": 0,
      "aria-relevant": 0,
      // Drag-and-Drop Attributes
      "aria-dropeffect": 0,
      "aria-grabbed": 0,
      // Relationship Attributes
      "aria-activedescendant": 0,
      "aria-colcount": 0,
      "aria-colindex": 0,
      "aria-colspan": 0,
      "aria-controls": 0,
      "aria-describedby": 0,
      "aria-errormessage": 0,
      "aria-flowto": 0,
      "aria-labelledby": 0,
      "aria-owns": 0,
      "aria-posinset": 0,
      "aria-rowcount": 0,
      "aria-rowindex": 0,
      "aria-rowspan": 0,
      "aria-setsize": 0
    }, qa = {}, Pd = new RegExp("^(aria)-[" + Ie + "]*$"), gs = new RegExp("^(aria)[A-Z][" + Ie + "]*$");
    function Fd(e, t) {
      {
        if (ir.call(qa, t) && qa[t])
          return !0;
        if (gs.test(t)) {
          var a = "aria-" + t.slice(4).toLowerCase(), i = ah.hasOwnProperty(a) ? a : null;
          if (i == null)
            return m("Invalid ARIA attribute `%s`. ARIA attributes follow the pattern aria-* and must be lowercase.", t), qa[t] = !0, !0;
          if (t !== i)
            return m("Invalid ARIA attribute `%s`. Did you mean `%s`?", t, i), qa[t] = !0, !0;
        }
        if (Pd.test(t)) {
          var u = t.toLowerCase(), d = ah.hasOwnProperty(u) ? u : null;
          if (d == null)
            return qa[t] = !0, !1;
          if (t !== d)
            return m("Unknown ARIA attribute `%s`. Did you mean `%s`?", t, d), qa[t] = !0, !0;
        }
      }
      return !0;
    }
    function ih(e, t) {
      {
        var a = [];
        for (var i in t) {
          var u = Fd(e, i);
          u || a.push(i);
        }
        var d = a.map(function(h) {
          return "`" + h + "`";
        }).join(", ");
        a.length === 1 ? m("Invalid aria prop %s on <%s> tag. For details, see https://reactjs.org/link/invalid-aria-props", d, e) : a.length > 1 && m("Invalid aria props %s on <%s> tag. For details, see https://reactjs.org/link/invalid-aria-props", d, e);
      }
    }
    function Nc(e, t) {
      Ai(e, t) || ih(e, t);
    }
    var rl = !1;
    function Hd(e, t) {
      {
        if (e !== "input" && e !== "textarea" && e !== "select")
          return;
        t != null && t.value === null && !rl && (rl = !0, e === "select" && t.multiple ? m("`value` prop on `%s` should not be null. Consider using an empty array when `multiple` is set to `true` to clear the component or `undefined` for uncontrolled components.", e) : m("`value` prop on `%s` should not be null. Consider using an empty string to clear the component or `undefined` for uncontrolled components.", e));
      }
    }
    var jd = function() {
    };
    {
      var jn = {}, Vd = /^on./, oh = /^on[^A-Z]/, lh = new RegExp("^(aria)-[" + Ie + "]*$"), uh = new RegExp("^(aria)[A-Z][" + Ie + "]*$");
      jd = function(e, t, a, i) {
        if (ir.call(jn, t) && jn[t])
          return !0;
        var u = t.toLowerCase();
        if (u === "onfocusin" || u === "onfocusout")
          return m("React uses onFocus and onBlur instead of onFocusIn and onFocusOut. All React events are normalized to bubble, so onFocusIn and onFocusOut are not needed/supported by React."), jn[t] = !0, !0;
        if (i != null) {
          var d = i.registrationNameDependencies, h = i.possibleRegistrationNames;
          if (d.hasOwnProperty(t))
            return !0;
          var S = h.hasOwnProperty(u) ? h[u] : null;
          if (S != null)
            return m("Invalid event handler property `%s`. Did you mean `%s`?", t, S), jn[t] = !0, !0;
          if (Vd.test(t))
            return m("Unknown event handler property `%s`. It will be ignored.", t), jn[t] = !0, !0;
        } else if (Vd.test(t))
          return oh.test(t) && m("Invalid event handler property `%s`. React events use the camelCase naming convention, for example `onClick`.", t), jn[t] = !0, !0;
        if (lh.test(t) || uh.test(t))
          return !0;
        if (u === "innerhtml")
          return m("Directly setting property `innerHTML` is not permitted. For more information, lookup documentation on `dangerouslySetInnerHTML`."), jn[t] = !0, !0;
        if (u === "aria")
          return m("The `aria` attribute is reserved for future use in React. Pass individual `aria-` attributes instead."), jn[t] = !0, !0;
        if (u === "is" && a !== null && a !== void 0 && typeof a != "string")
          return m("Received a `%s` for a string attribute `is`. If this is expected, cast the value to a string.", typeof a), jn[t] = !0, !0;
        if (typeof a == "number" && isNaN(a))
          return m("Received NaN for the `%s` attribute. If this is expected, cast the value to a string.", t), jn[t] = !0, !0;
        var b = pa(t), k = b !== null && b.type === ja;
        if (Lc.hasOwnProperty(u)) {
          var _ = Lc[u];
          if (_ !== t)
            return m("Invalid DOM property `%s`. Did you mean `%s`?", t, _), jn[t] = !0, !0;
        } else if (!k && t !== u)
          return m("React does not recognize the `%s` prop on a DOM element. If you intentionally want it to appear in the DOM as a custom attribute, spell it as lowercase `%s` instead. If you accidentally passed it from a parent component, remove it from the DOM element.", t, u), jn[t] = !0, !0;
        return typeof a == "boolean" && Lr(t, a, b, !1) ? (a ? m('Received `%s` for a non-boolean attribute `%s`.\n\nIf you want to write it to the DOM, pass a string instead: %s="%s" or %s={value.toString()}.', a, t, t, a, t) : m('Received `%s` for a non-boolean attribute `%s`.\n\nIf you want to write it to the DOM, pass a string instead: %s="%s" or %s={value.toString()}.\n\nIf you used to conditionally omit it with %s={condition && value}, pass %s={condition ? value : undefined} instead.', a, t, t, a, t, t, t), jn[t] = !0, !0) : k ? !0 : Lr(t, a, b, !1) ? (jn[t] = !0, !1) : ((a === "false" || a === "true") && b !== null && b.type === Wn && (m("Received the string `%s` for the boolean attribute `%s`. %s Did you mean %s={%s}?", a, t, a === "false" ? "The browser will interpret it as a truthy value." : 'Although this works, it will not work as expected if you pass the string "false".', t, a), jn[t] = !0), !0);
      };
    }
    var sh = function(e, t, a) {
      {
        var i = [];
        for (var u in t) {
          var d = jd(e, u, t[u], a);
          d || i.push(u);
        }
        var h = i.map(function(S) {
          return "`" + S + "`";
        }).join(", ");
        i.length === 1 ? m("Invalid value for prop %s on <%s> tag. Either remove it from the element, or pass a string or number value to keep it in the DOM. For details, see https://reactjs.org/link/attribute-behavior ", h, e) : i.length > 1 && m("Invalid values for props %s on <%s> tag. Either remove them from the element, or pass a string or number value to keep them in the DOM. For details, see https://reactjs.org/link/attribute-behavior ", h, e);
      }
    };
    function ch(e, t, a) {
      Ai(e, t) || sh(e, t, a);
    }
    var zi = 1, Ss = 2, al = 4, lg = zi | Ss | al, Es = null;
    function bs(e) {
      Es !== null && m("Expected currently replaying event to be null. This error is likely caused by a bug in React. Please file an issue."), Es = e;
    }
    function ug() {
      Es === null && m("Expected currently replaying event to not be null. This error is likely caused by a bug in React. Please file an issue."), Es = null;
    }
    function fh(e) {
      return e === Es;
    }
    function Ac(e) {
      var t = e.target || e.srcElement || window;
      return t.correspondingUseElement && (t = t.correspondingUseElement), t.nodeType === Ni ? t.parentNode : t;
    }
    var Gt = null, Co = null, Ui = null;
    function ou(e) {
      var t = Hu(e);
      if (t) {
        if (typeof Gt != "function")
          throw new Error("setRestoreImplementation() needs to be called to handle a target for controlled events. This error is likely caused by a bug in React. Please file an issue.");
        var a = t.stateNode;
        if (a) {
          var i = hm(a);
          Gt(t.stateNode, t.type, i);
        }
      }
    }
    function dh(e) {
      Gt = e;
    }
    function zc(e) {
      Co ? Ui ? Ui.push(e) : Ui = [e] : Co = e;
    }
    function Cs() {
      return Co !== null || Ui !== null;
    }
    function ws() {
      if (Co) {
        var e = Co, t = Ui;
        if (Co = null, Ui = null, ou(e), t)
          for (var a = 0; a < t.length; a++)
            ou(t[a]);
      }
    }
    var il = function(e, t) {
      return e(t);
    }, $d = function() {
    }, Bd = !1;
    function sg() {
      var e = Cs();
      e && ($d(), ws());
    }
    function Id(e, t, a) {
      if (Bd)
        return e(t, a);
      Bd = !0;
      try {
        return il(e, t, a);
      } finally {
        Bd = !1, sg();
      }
    }
    function Uc(e, t, a) {
      il = e, $d = a;
    }
    function Pc(e) {
      return e === "button" || e === "input" || e === "select" || e === "textarea";
    }
    function Yd(e, t, a) {
      switch (e) {
        case "onClick":
        case "onClickCapture":
        case "onDoubleClick":
        case "onDoubleClickCapture":
        case "onMouseDown":
        case "onMouseDownCapture":
        case "onMouseMove":
        case "onMouseMoveCapture":
        case "onMouseUp":
        case "onMouseUpCapture":
        case "onMouseEnter":
          return !!(a.disabled && Pc(t));
        default:
          return !1;
      }
    }
    function ol(e, t) {
      var a = e.stateNode;
      if (a === null)
        return null;
      var i = hm(a);
      if (i === null)
        return null;
      var u = i[t];
      if (Yd(t, e.type, i))
        return null;
      if (u && typeof u != "function")
        throw new Error("Expected `" + t + "` listener to be a function, instead got a value of `" + typeof u + "` type.");
      return u;
    }
    var Ts = !1;
    if (bn)
      try {
        var ll = {};
        Object.defineProperty(ll, "passive", {
          get: function() {
            Ts = !0;
          }
        }), window.addEventListener("test", ll, ll), window.removeEventListener("test", ll, ll);
      } catch {
        Ts = !1;
      }
    function ph(e, t, a, i, u, d, h, S, b) {
      var k = Array.prototype.slice.call(arguments, 3);
      try {
        t.apply(a, k);
      } catch (_) {
        this.onError(_);
      }
    }
    var Wd = ph;
    if (typeof window < "u" && typeof window.dispatchEvent == "function" && typeof document < "u" && typeof document.createEvent == "function") {
      var Qd = document.createElement("react");
      Wd = function(t, a, i, u, d, h, S, b, k) {
        if (typeof document > "u" || document === null)
          throw new Error("The `document` global was defined when React was initialized, but is not defined anymore. This can happen in a test environment if a component schedules an update from an asynchronous callback, but the test has already finished running. To solve this, you can either unmount the component at the end of your test (and ensure that any asynchronous operations get canceled in `componentWillUnmount`), or you can change the test itself to be asynchronous.");
        var _ = document.createEvent("Event"), P = !1, z = !0, q = window.event, Z = Object.getOwnPropertyDescriptor(window, "event");
        function ee() {
          Qd.removeEventListener(te, Qe, !1), typeof window.event < "u" && window.hasOwnProperty("event") && (window.event = q);
        }
        var we = Array.prototype.slice.call(arguments, 3);
        function Qe() {
          P = !0, ee(), a.apply(i, we), z = !1;
        }
        var Ve, _t = !1, St = !1;
        function I(Y) {
          if (Ve = Y.error, _t = !0, Ve === null && Y.colno === 0 && Y.lineno === 0 && (St = !0), Y.defaultPrevented && Ve != null && typeof Ve == "object")
            try {
              Ve._suppressLogging = !0;
            } catch {
            }
        }
        var te = "react-" + (t || "invokeguardedcallback");
        if (window.addEventListener("error", I), Qd.addEventListener(te, Qe, !1), _.initEvent(te, !1, !1), Qd.dispatchEvent(_), Z && Object.defineProperty(window, "event", Z), P && z && (_t ? St && (Ve = new Error("A cross-origin error was thrown. React doesn't have access to the actual error object in development. See https://reactjs.org/link/crossorigin-error for more information.")) : Ve = new Error(`An error was thrown inside one of your components, but React doesn't know what it was. This is likely due to browser flakiness. React does its best to preserve the "Pause on exceptions" behavior of the DevTools, which requires some DEV-mode only tricks. It's possible that these don't work in your browser. Try triggering the error in production mode, or switching to a modern browser. If you suspect that this is actually an issue with React, please file an issue.`), this.onError(Ve)), window.removeEventListener("error", I), !P)
          return ee(), ph.apply(this, arguments);
      };
    }
    var cg = Wd, wo = !1, Xa = null, Rs = !1, To = null, ui = {
      onError: function(e) {
        wo = !0, Xa = e;
      }
    };
    function ul(e, t, a, i, u, d, h, S, b) {
      wo = !1, Xa = null, cg.apply(ui, arguments);
    }
    function Pi(e, t, a, i, u, d, h, S, b) {
      if (ul.apply(this, arguments), wo) {
        var k = qd();
        Rs || (Rs = !0, To = k);
      }
    }
    function Gd() {
      if (Rs) {
        var e = To;
        throw Rs = !1, To = null, e;
      }
    }
    function fg() {
      return wo;
    }
    function qd() {
      if (wo) {
        var e = Xa;
        return wo = !1, Xa = null, e;
      } else
        throw new Error("clearCaughtError was called but no error was captured. This error is likely caused by a bug in React. Please file an issue.");
    }
    function ga(e) {
      return e._reactInternals;
    }
    function xs(e) {
      return e._reactInternals !== void 0;
    }
    function lu(e, t) {
      e._reactInternals = t;
    }
    var Ye = (
      /*                      */
      0
    ), Ro = (
      /*                */
      1
    ), Jt = (
      /*                    */
      2
    ), st = (
      /*                       */
      4
    ), Pt = (
      /*                */
      16
    ), Ht = (
      /*                 */
      32
    ), si = (
      /*                     */
      64
    ), tt = (
      /*                   */
      128
    ), mn = (
      /*            */
      256
    ), Pr = (
      /*                          */
      512
    ), Sa = (
      /*                     */
      1024
    ), an = (
      /*                      */
      2048
    ), Ea = (
      /*                    */
      4096
    ), xo = (
      /*                   */
      8192
    ), ks = (
      /*             */
      16384
    ), Fc = an | st | si | Pr | Sa | ks, vh = (
      /*               */
      32767
    ), na = (
      /*                   */
      32768
    ), Vn = (
      /*                */
      65536
    ), _s = (
      /* */
      131072
    ), Xd = (
      /*                       */
      1048576
    ), Kd = (
      /*                    */
      2097152
    ), Fr = (
      /*                 */
      4194304
    ), ko = (
      /*                */
      8388608
    ), Hr = (
      /*               */
      16777216
    ), sl = (
      /*              */
      33554432
    ), uu = (
      // TODO: Remove Update flag from before mutation phase by re-landing Visibility
      // flag logic (see #20043)
      st | Sa | 0
    ), jr = Jt | st | Pt | Ht | Pr | Ea | xo, lr = st | si | Pr | xo, ba = an | Pt, Gn = Fr | ko | Kd, Fi = c.ReactCurrentOwner;
    function ra(e) {
      var t = e, a = e;
      if (e.alternate)
        for (; t.return; )
          t = t.return;
      else {
        var i = t;
        do
          t = i, (t.flags & (Jt | Ea)) !== Ye && (a = t.return), i = t.return;
        while (i);
      }
      return t.tag === L ? a : null;
    }
    function Zd(e) {
      if (e.tag === ne) {
        var t = e.memoizedState;
        if (t === null) {
          var a = e.alternate;
          a !== null && (t = a.memoizedState);
        }
        if (t !== null)
          return t.dehydrated;
      }
      return null;
    }
    function Hc(e) {
      return e.tag === L ? e.stateNode.containerInfo : null;
    }
    function Jd(e) {
      return ra(e) === e;
    }
    function aa(e) {
      {
        var t = Fi.current;
        if (t !== null && t.tag === D) {
          var a = t, i = a.stateNode;
          i._warnedAboutRefsInRender || m("%s is accessing isMounted inside its render() function. render() should be a pure function of props and state. It should never access something that requires stale data from the previous render, such as refs. Move this logic to componentDidMount and componentDidUpdate instead.", it(a) || "A component"), i._warnedAboutRefsInRender = !0;
        }
      }
      var u = ga(e);
      return u ? ra(u) === u : !1;
    }
    function Vr(e) {
      if (ra(e) !== e)
        throw new Error("Unable to find node on an unmounted component.");
    }
    function en(e) {
      var t = e.alternate;
      if (!t) {
        var a = ra(e);
        if (a === null)
          throw new Error("Unable to find node on an unmounted component.");
        return a !== e ? null : e;
      }
      for (var i = e, u = t; ; ) {
        var d = i.return;
        if (d === null)
          break;
        var h = d.alternate;
        if (h === null) {
          var S = d.return;
          if (S !== null) {
            i = u = S;
            continue;
          }
          break;
        }
        if (d.child === h.child) {
          for (var b = d.child; b; ) {
            if (b === i)
              return Vr(d), e;
            if (b === u)
              return Vr(d), t;
            b = b.sibling;
          }
          throw new Error("Unable to find node on an unmounted component.");
        }
        if (i.return !== u.return)
          i = d, u = h;
        else {
          for (var k = !1, _ = d.child; _; ) {
            if (_ === i) {
              k = !0, i = d, u = h;
              break;
            }
            if (_ === u) {
              k = !0, u = d, i = h;
              break;
            }
            _ = _.sibling;
          }
          if (!k) {
            for (_ = h.child; _; ) {
              if (_ === i) {
                k = !0, i = h, u = d;
                break;
              }
              if (_ === u) {
                k = !0, u = h, i = d;
                break;
              }
              _ = _.sibling;
            }
            if (!k)
              throw new Error("Child was not found in either parent set. This indicates a bug in React related to the return pointer. Please file an issue.");
          }
        }
        if (i.alternate !== u)
          throw new Error("Return fibers should always be each others' alternates. This error is likely caused by a bug in React. Please file an issue.");
      }
      if (i.tag !== L)
        throw new Error("Unable to find node on an unmounted component.");
      return i.stateNode.current === i ? e : t;
    }
    function Ca(e) {
      var t = en(e);
      return t !== null ? ep(t) : null;
    }
    function ep(e) {
      if (e.tag === F || e.tag === $)
        return e;
      for (var t = e.child; t !== null; ) {
        var a = ep(t);
        if (a !== null)
          return a;
        t = t.sibling;
      }
      return null;
    }
    function hh(e) {
      var t = en(e);
      return t !== null ? jc(t) : null;
    }
    function jc(e) {
      if (e.tag === F || e.tag === $)
        return e;
      for (var t = e.child; t !== null; ) {
        if (t.tag !== j) {
          var a = jc(t);
          if (a !== null)
            return a;
        }
        t = t.sibling;
      }
      return null;
    }
    var Vc = s.unstable_scheduleCallback, mh = s.unstable_cancelCallback, $c = s.unstable_shouldYield, yh = s.unstable_requestPaint, fn = s.unstable_now, tp = s.unstable_getCurrentPriorityLevel, Bc = s.unstable_ImmediatePriority, cl = s.unstable_UserBlockingPriority, ci = s.unstable_NormalPriority, gh = s.unstable_LowPriority, Ic = s.unstable_IdlePriority, su = s.unstable_yieldValue, Sh = s.unstable_setDisableYieldValue, Hi = null, Ln = null, ge = null, wa = !1, ia = typeof __REACT_DEVTOOLS_GLOBAL_HOOK__ < "u";
    function np(e) {
      if (typeof __REACT_DEVTOOLS_GLOBAL_HOOK__ > "u")
        return !1;
      var t = __REACT_DEVTOOLS_GLOBAL_HOOK__;
      if (t.isDisabled)
        return !0;
      if (!t.supportsFiber)
        return m("The installed version of React DevTools is too old and will not work with the current version of React. Please update React DevTools. https://reactjs.org/link/react-devtools"), !0;
      try {
        ct && (e = dt({}, e, {
          getLaneLabelMap: ji,
          injectProfilingHooks: Eh
        })), Hi = t.inject(e), Ln = t;
      } catch (a) {
        m("React instrumentation encountered an error: %s.", a);
      }
      return !!t.checkDCE;
    }
    function rp(e, t) {
      if (Ln && typeof Ln.onScheduleFiberRoot == "function")
        try {
          Ln.onScheduleFiberRoot(Hi, e, t);
        } catch (a) {
          wa || (wa = !0, m("React instrumentation encountered an error: %s", a));
        }
    }
    function cu(e, t) {
      if (Ln && typeof Ln.onCommitFiberRoot == "function")
        try {
          var a = (e.current.flags & tt) === tt;
          if (yt) {
            var i;
            switch (t) {
              case Tn:
                i = Bc;
                break;
              case $i:
                i = cl;
                break;
              case fi:
                i = ci;
                break;
              case Cu:
                i = Ic;
                break;
              default:
                i = ci;
                break;
            }
            Ln.onCommitFiberRoot(Hi, e, i, a);
          }
        } catch (u) {
          wa || (wa = !0, m("React instrumentation encountered an error: %s", u));
        }
    }
    function Ta(e) {
      if (Ln && typeof Ln.onPostCommitFiberRoot == "function")
        try {
          Ln.onPostCommitFiberRoot(Hi, e);
        } catch (t) {
          wa || (wa = !0, m("React instrumentation encountered an error: %s", t));
        }
    }
    function fl(e) {
      if (Ln && typeof Ln.onCommitFiberUnmount == "function")
        try {
          Ln.onCommitFiberUnmount(Hi, e);
        } catch (t) {
          wa || (wa = !0, m("React instrumentation encountered an error: %s", t));
        }
    }
    function _n(e) {
      if (typeof su == "function" && (Sh(e), y(e)), Ln && typeof Ln.setStrictMode == "function")
        try {
          Ln.setStrictMode(Hi, e);
        } catch (t) {
          wa || (wa = !0, m("React instrumentation encountered an error: %s", t));
        }
    }
    function Eh(e) {
      ge = e;
    }
    function ji() {
      {
        for (var e = /* @__PURE__ */ new Map(), t = 1, a = 0; a < Ns; a++) {
          var i = pg(t);
          e.set(t, i), t *= 2;
        }
        return e;
      }
    }
    function _o(e) {
      ge !== null && typeof ge.markCommitStarted == "function" && ge.markCommitStarted(e);
    }
    function Yc() {
      ge !== null && typeof ge.markCommitStopped == "function" && ge.markCommitStopped();
    }
    function fu(e) {
      ge !== null && typeof ge.markComponentRenderStarted == "function" && ge.markComponentRenderStarted(e);
    }
    function $r() {
      ge !== null && typeof ge.markComponentRenderStopped == "function" && ge.markComponentRenderStopped();
    }
    function Do(e) {
      ge !== null && typeof ge.markComponentPassiveEffectMountStarted == "function" && ge.markComponentPassiveEffectMountStarted(e);
    }
    function Wc() {
      ge !== null && typeof ge.markComponentPassiveEffectMountStopped == "function" && ge.markComponentPassiveEffectMountStopped();
    }
    function bh(e) {
      ge !== null && typeof ge.markComponentPassiveEffectUnmountStarted == "function" && ge.markComponentPassiveEffectUnmountStarted(e);
    }
    function Qc() {
      ge !== null && typeof ge.markComponentPassiveEffectUnmountStopped == "function" && ge.markComponentPassiveEffectUnmountStopped();
    }
    function Ch(e) {
      ge !== null && typeof ge.markComponentLayoutEffectMountStarted == "function" && ge.markComponentLayoutEffectMountStarted(e);
    }
    function Ds() {
      ge !== null && typeof ge.markComponentLayoutEffectMountStopped == "function" && ge.markComponentLayoutEffectMountStopped();
    }
    function Ka(e) {
      ge !== null && typeof ge.markComponentLayoutEffectUnmountStarted == "function" && ge.markComponentLayoutEffectUnmountStarted(e);
    }
    function du() {
      ge !== null && typeof ge.markComponentLayoutEffectUnmountStopped == "function" && ge.markComponentLayoutEffectUnmountStopped();
    }
    function Os(e, t, a) {
      ge !== null && typeof ge.markComponentErrored == "function" && ge.markComponentErrored(e, t, a);
    }
    function dl(e, t, a) {
      ge !== null && typeof ge.markComponentSuspended == "function" && ge.markComponentSuspended(e, t, a);
    }
    function ap(e) {
      ge !== null && typeof ge.markLayoutEffectsStarted == "function" && ge.markLayoutEffectsStarted(e);
    }
    function pu() {
      ge !== null && typeof ge.markLayoutEffectsStopped == "function" && ge.markLayoutEffectsStopped();
    }
    function wh(e) {
      ge !== null && typeof ge.markPassiveEffectsStarted == "function" && ge.markPassiveEffectsStarted(e);
    }
    function ip() {
      ge !== null && typeof ge.markPassiveEffectsStopped == "function" && ge.markPassiveEffectsStopped();
    }
    function on(e) {
      ge !== null && typeof ge.markRenderStarted == "function" && ge.markRenderStarted(e);
    }
    function Gc() {
      ge !== null && typeof ge.markRenderYielded == "function" && ge.markRenderYielded();
    }
    function qc() {
      ge !== null && typeof ge.markRenderStopped == "function" && ge.markRenderStopped();
    }
    function op(e) {
      ge !== null && typeof ge.markRenderScheduled == "function" && ge.markRenderScheduled(e);
    }
    function Xc(e, t) {
      ge !== null && typeof ge.markForceUpdateScheduled == "function" && ge.markForceUpdateScheduled(e, t);
    }
    function Ms(e, t) {
      ge !== null && typeof ge.markStateUpdateScheduled == "function" && ge.markStateUpdateScheduled(e, t);
    }
    var Fe = (
      /*                         */
      0
    ), je = (
      /*                 */
      1
    ), nt = (
      /*                    */
      2
    ), vt = (
      /*               */
      8
    ), oa = (
      /*              */
      16
    ), vu = Math.clz32 ? Math.clz32 : ur, Ls = Math.log, dg = Math.LN2;
    function ur(e) {
      var t = e >>> 0;
      return t === 0 ? 32 : 31 - (Ls(t) / dg | 0) | 0;
    }
    var Ns = 31, ie = (
      /*                        */
      0
    ), Dn = (
      /*                          */
      0
    ), $e = (
      /*                        */
      1
    ), qn = (
      /*    */
      2
    ), la = (
      /*             */
      4
    ), Vi = (
      /*            */
      8
    ), Ra = (
      /*                     */
      16
    ), hu = (
      /*                */
      32
    ), pl = (
      /*                       */
      4194240
    ), mu = (
      /*                        */
      64
    ), Kc = (
      /*                        */
      128
    ), Zc = (
      /*                        */
      256
    ), Jc = (
      /*                        */
      512
    ), ef = (
      /*                        */
      1024
    ), tf = (
      /*                        */
      2048
    ), vl = (
      /*                        */
      4096
    ), nf = (
      /*                        */
      8192
    ), yu = (
      /*                        */
      16384
    ), gu = (
      /*                       */
      32768
    ), rf = (
      /*                       */
      65536
    ), As = (
      /*                       */
      131072
    ), af = (
      /*                       */
      262144
    ), of = (
      /*                       */
      524288
    ), lf = (
      /*                       */
      1048576
    ), uf = (
      /*                       */
      2097152
    ), Su = (
      /*                            */
      130023424
    ), hl = (
      /*                             */
      4194304
    ), sf = (
      /*                             */
      8388608
    ), cf = (
      /*                             */
      16777216
    ), lp = (
      /*                             */
      33554432
    ), ff = (
      /*                             */
      67108864
    ), Th = hl, zs = (
      /*          */
      134217728
    ), up = (
      /*                          */
      268435455
    ), Eu = (
      /*               */
      268435456
    ), Oo = (
      /*                        */
      536870912
    ), sr = (
      /*                   */
      1073741824
    );
    function pg(e) {
      {
        if (e & $e)
          return "Sync";
        if (e & qn)
          return "InputContinuousHydration";
        if (e & la)
          return "InputContinuous";
        if (e & Vi)
          return "DefaultHydration";
        if (e & Ra)
          return "Default";
        if (e & hu)
          return "TransitionHydration";
        if (e & pl)
          return "Transition";
        if (e & Su)
          return "Retry";
        if (e & zs)
          return "SelectiveHydration";
        if (e & Eu)
          return "IdleHydration";
        if (e & Oo)
          return "Idle";
        if (e & sr)
          return "Offscreen";
      }
    }
    var Kt = -1, df = mu, Br = hl;
    function ml(e) {
      switch (wn(e)) {
        case $e:
          return $e;
        case qn:
          return qn;
        case la:
          return la;
        case Vi:
          return Vi;
        case Ra:
          return Ra;
        case hu:
          return hu;
        case mu:
        case Kc:
        case Zc:
        case Jc:
        case ef:
        case tf:
        case vl:
        case nf:
        case yu:
        case gu:
        case rf:
        case As:
        case af:
        case of:
        case lf:
        case uf:
          return e & pl;
        case hl:
        case sf:
        case cf:
        case lp:
        case ff:
          return e & Su;
        case zs:
          return zs;
        case Eu:
          return Eu;
        case Oo:
          return Oo;
        case sr:
          return sr;
        default:
          return m("Should have found matching lanes. This is a bug in React."), e;
      }
    }
    function yl(e, t) {
      var a = e.pendingLanes;
      if (a === ie)
        return ie;
      var i = ie, u = e.suspendedLanes, d = e.pingedLanes, h = a & up;
      if (h !== ie) {
        var S = h & ~u;
        if (S !== ie)
          i = ml(S);
        else {
          var b = h & d;
          b !== ie && (i = ml(b));
        }
      } else {
        var k = a & ~u;
        k !== ie ? i = ml(k) : d !== ie && (i = ml(d));
      }
      if (i === ie)
        return ie;
      if (t !== ie && t !== i && // If we already suspended with a delay, then interrupting is fine. Don't
      // bother waiting until the root is complete.
      (t & u) === ie) {
        var _ = wn(i), P = wn(t);
        if (
          // Tests whether the next lane is equal or lower priority than the wip
          // one. This works because the bits decrease in priority as you go left.
          _ >= P || // Default priority updates should not interrupt transition updates. The
          // only difference between default updates and transition updates is that
          // default updates do not support refresh transitions.
          _ === Ra && (P & pl) !== ie
        )
          return t;
      }
      (i & la) !== ie && (i |= a & Ra);
      var z = e.entangledLanes;
      if (z !== ie)
        for (var q = e.entanglements, Z = i & z; Z > 0; ) {
          var ee = Lo(Z), we = 1 << ee;
          i |= q[ee], Z &= ~we;
        }
      return i;
    }
    function Rh(e, t) {
      for (var a = e.eventTimes, i = Kt; t > 0; ) {
        var u = Lo(t), d = 1 << u, h = a[u];
        h > i && (i = h), t &= ~d;
      }
      return i;
    }
    function xh(e, t) {
      switch (e) {
        case $e:
        case qn:
        case la:
          return t + 250;
        case Vi:
        case Ra:
        case hu:
        case mu:
        case Kc:
        case Zc:
        case Jc:
        case ef:
        case tf:
        case vl:
        case nf:
        case yu:
        case gu:
        case rf:
        case As:
        case af:
        case of:
        case lf:
        case uf:
          return t + 5e3;
        case hl:
        case sf:
        case cf:
        case lp:
        case ff:
          return Kt;
        case zs:
        case Eu:
        case Oo:
        case sr:
          return Kt;
        default:
          return m("Should have found matching lanes. This is a bug in React."), Kt;
      }
    }
    function kh(e, t) {
      for (var a = e.pendingLanes, i = e.suspendedLanes, u = e.pingedLanes, d = e.expirationTimes, h = a; h > 0; ) {
        var S = Lo(h), b = 1 << S, k = d[S];
        k === Kt ? ((b & i) === ie || (b & u) !== ie) && (d[S] = xh(b, t)) : k <= t && (e.expiredLanes |= b), h &= ~b;
      }
    }
    function sp(e) {
      return ml(e.pendingLanes);
    }
    function Mo(e) {
      var t = e.pendingLanes & ~sr;
      return t !== ie ? t : t & sr ? sr : ie;
    }
    function cp(e) {
      return (e & $e) !== ie;
    }
    function Us(e) {
      return (e & up) !== ie;
    }
    function _h(e) {
      return (e & Su) === e;
    }
    function Dh(e) {
      var t = $e | la | Ra;
      return (e & t) === ie;
    }
    function Oh(e) {
      return (e & pl) === e;
    }
    function Ps(e, t) {
      var a = qn | la | Vi | Ra;
      return (t & a) !== ie;
    }
    function Mh(e, t) {
      return (t & e.expiredLanes) !== ie;
    }
    function fp(e) {
      return (e & pl) !== ie;
    }
    function Lh() {
      var e = df;
      return df <<= 1, (df & pl) === ie && (df = mu), e;
    }
    function Ir() {
      var e = Br;
      return Br <<= 1, (Br & Su) === ie && (Br = hl), e;
    }
    function wn(e) {
      return e & -e;
    }
    function bu(e) {
      return wn(e);
    }
    function Lo(e) {
      return 31 - vu(e);
    }
    function pf(e) {
      return Lo(e);
    }
    function Yr(e, t) {
      return (e & t) !== ie;
    }
    function gl(e, t) {
      return (e & t) === t;
    }
    function ut(e, t) {
      return e | t;
    }
    function Fs(e, t) {
      return e & ~t;
    }
    function vf(e, t) {
      return e & t;
    }
    function vg(e) {
      return e;
    }
    function Nh(e, t) {
      return e !== Dn && e < t ? e : t;
    }
    function Hs(e) {
      for (var t = [], a = 0; a < Ns; a++)
        t.push(e);
      return t;
    }
    function Sl(e, t, a) {
      e.pendingLanes |= t, t !== Oo && (e.suspendedLanes = ie, e.pingedLanes = ie);
      var i = e.eventTimes, u = pf(t);
      i[u] = a;
    }
    function Ah(e, t) {
      e.suspendedLanes |= t, e.pingedLanes &= ~t;
      for (var a = e.expirationTimes, i = t; i > 0; ) {
        var u = Lo(i), d = 1 << u;
        a[u] = Kt, i &= ~d;
      }
    }
    function hf(e, t, a) {
      e.pingedLanes |= e.suspendedLanes & t;
    }
    function mf(e, t) {
      var a = e.pendingLanes & ~t;
      e.pendingLanes = t, e.suspendedLanes = ie, e.pingedLanes = ie, e.expiredLanes &= t, e.mutableReadLanes &= t, e.entangledLanes &= t;
      for (var i = e.entanglements, u = e.eventTimes, d = e.expirationTimes, h = a; h > 0; ) {
        var S = Lo(h), b = 1 << S;
        i[S] = ie, u[S] = Kt, d[S] = Kt, h &= ~b;
      }
    }
    function dp(e, t) {
      for (var a = e.entangledLanes |= t, i = e.entanglements, u = a; u; ) {
        var d = Lo(u), h = 1 << d;
        // Is this one of the newly entangled lanes?
        h & t | // Is this lane transitively entangled with the newly entangled lanes?
        i[d] & t && (i[d] |= t), u &= ~h;
      }
    }
    function zh(e, t) {
      var a = wn(t), i;
      switch (a) {
        case la:
          i = qn;
          break;
        case Ra:
          i = Vi;
          break;
        case mu:
        case Kc:
        case Zc:
        case Jc:
        case ef:
        case tf:
        case vl:
        case nf:
        case yu:
        case gu:
        case rf:
        case As:
        case af:
        case of:
        case lf:
        case uf:
        case hl:
        case sf:
        case cf:
        case lp:
        case ff:
          i = hu;
          break;
        case Oo:
          i = Eu;
          break;
        default:
          i = Dn;
          break;
      }
      return (i & (e.suspendedLanes | t)) !== Dn ? Dn : i;
    }
    function yf(e, t, a) {
      if (ia)
        for (var i = e.pendingUpdatersLaneMap; a > 0; ) {
          var u = pf(a), d = 1 << u, h = i[u];
          h.add(t), a &= ~d;
        }
    }
    function pp(e, t) {
      if (ia)
        for (var a = e.pendingUpdatersLaneMap, i = e.memoizedUpdaters; t > 0; ) {
          var u = pf(t), d = 1 << u, h = a[u];
          h.size > 0 && (h.forEach(function(S) {
            var b = S.alternate;
            (b === null || !i.has(b)) && i.add(S);
          }), h.clear()), t &= ~d;
        }
    }
    function js(e, t) {
      return null;
    }
    var Tn = $e, $i = la, fi = Ra, Cu = Oo, wu = Dn;
    function xa() {
      return wu;
    }
    function yn(e) {
      wu = e;
    }
    function cr(e, t) {
      var a = wu;
      try {
        return wu = e, t();
      } finally {
        wu = a;
      }
    }
    function hg(e, t) {
      return e !== 0 && e < t ? e : t;
    }
    function mg(e, t) {
      return e > t ? e : t;
    }
    function Tu(e, t) {
      return e !== 0 && e < t;
    }
    function Xn(e) {
      var t = wn(e);
      return Tu(Tn, t) ? Tu($i, t) ? Us(t) ? fi : Cu : $i : Tn;
    }
    function gf(e) {
      var t = e.current.memoizedState;
      return t.isDehydrated;
    }
    var Me;
    function Ru(e) {
      Me = e;
    }
    function vp(e) {
      Me(e);
    }
    var Sf;
    function yg(e) {
      Sf = e;
    }
    var xu;
    function Ef(e) {
      xu = e;
    }
    var bf;
    function Uh(e) {
      bf = e;
    }
    var hp;
    function Ph(e) {
      hp = e;
    }
    var Vs = !1, ku = [], ln = null, $n = null, hr = null, _u = /* @__PURE__ */ new Map(), Du = /* @__PURE__ */ new Map(), Bn = [], Fh = [
      "mousedown",
      "mouseup",
      "touchcancel",
      "touchend",
      "touchstart",
      "auxclick",
      "dblclick",
      "pointercancel",
      "pointerdown",
      "pointerup",
      "dragend",
      "dragstart",
      "drop",
      "compositionend",
      "compositionstart",
      "keydown",
      "keypress",
      "keyup",
      "input",
      "textInput",
      // Intentionally camelCase
      "copy",
      "cut",
      "paste",
      "click",
      "change",
      "contextmenu",
      "reset",
      "submit"
    ];
    function di(e) {
      return Fh.indexOf(e) > -1;
    }
    function gg(e, t, a, i, u) {
      return {
        blockedOn: e,
        domEventName: t,
        eventSystemFlags: a,
        nativeEvent: u,
        targetContainers: [i]
      };
    }
    function mp(e, t) {
      switch (e) {
        case "focusin":
        case "focusout":
          ln = null;
          break;
        case "dragenter":
        case "dragleave":
          $n = null;
          break;
        case "mouseover":
        case "mouseout":
          hr = null;
          break;
        case "pointerover":
        case "pointerout": {
          var a = t.pointerId;
          _u.delete(a);
          break;
        }
        case "gotpointercapture":
        case "lostpointercapture": {
          var i = t.pointerId;
          Du.delete(i);
          break;
        }
      }
    }
    function Ou(e, t, a, i, u, d) {
      if (e === null || e.nativeEvent !== d) {
        var h = gg(t, a, i, u, d);
        if (t !== null) {
          var S = Hu(t);
          S !== null && Sf(S);
        }
        return h;
      }
      e.eventSystemFlags |= i;
      var b = e.targetContainers;
      return u !== null && b.indexOf(u) === -1 && b.push(u), e;
    }
    function Hh(e, t, a, i, u) {
      switch (t) {
        case "focusin": {
          var d = u;
          return ln = Ou(ln, e, t, a, i, d), !0;
        }
        case "dragenter": {
          var h = u;
          return $n = Ou($n, e, t, a, i, h), !0;
        }
        case "mouseover": {
          var S = u;
          return hr = Ou(hr, e, t, a, i, S), !0;
        }
        case "pointerover": {
          var b = u, k = b.pointerId;
          return _u.set(k, Ou(_u.get(k) || null, e, t, a, i, b)), !0;
        }
        case "gotpointercapture": {
          var _ = u, P = _.pointerId;
          return Du.set(P, Ou(Du.get(P) || null, e, t, a, i, _)), !0;
        }
      }
      return !1;
    }
    function yp(e) {
      var t = Js(e.target);
      if (t !== null) {
        var a = ra(t);
        if (a !== null) {
          var i = a.tag;
          if (i === ne) {
            var u = Zd(a);
            if (u !== null) {
              e.blockedOn = u, hp(e.priority, function() {
                xu(a);
              });
              return;
            }
          } else if (i === L) {
            var d = a.stateNode;
            if (gf(d)) {
              e.blockedOn = Hc(a);
              return;
            }
          }
        }
      }
      e.blockedOn = null;
    }
    function Sg(e) {
      for (var t = bf(), a = {
        blockedOn: null,
        target: e,
        priority: t
      }, i = 0; i < Bn.length && Tu(t, Bn[i].priority); i++)
        ;
      Bn.splice(i, 0, a), i === 0 && yp(a);
    }
    function El(e) {
      if (e.blockedOn !== null)
        return !1;
      for (var t = e.targetContainers; t.length > 0; ) {
        var a = t[0], i = fr(e.domEventName, e.eventSystemFlags, a, e.nativeEvent);
        if (i === null) {
          var u = e.nativeEvent, d = new u.constructor(u.type, u);
          bs(d), u.target.dispatchEvent(d), ug();
        } else {
          var h = Hu(i);
          return h !== null && Sf(h), e.blockedOn = i, !1;
        }
        t.shift();
      }
      return !0;
    }
    function Cf(e, t, a) {
      El(e) && a.delete(t);
    }
    function ka() {
      Vs = !1, ln !== null && El(ln) && (ln = null), $n !== null && El($n) && ($n = null), hr !== null && El(hr) && (hr = null), _u.forEach(Cf), Du.forEach(Cf);
    }
    function gt(e, t) {
      e.blockedOn === t && (e.blockedOn = null, Vs || (Vs = !0, s.unstable_scheduleCallback(s.unstable_NormalPriority, ka)));
    }
    function gn(e) {
      if (ku.length > 0) {
        gt(ku[0], e);
        for (var t = 1; t < ku.length; t++) {
          var a = ku[t];
          a.blockedOn === e && (a.blockedOn = null);
        }
      }
      ln !== null && gt(ln, e), $n !== null && gt($n, e), hr !== null && gt(hr, e);
      var i = function(S) {
        return gt(S, e);
      };
      _u.forEach(i), Du.forEach(i);
      for (var u = 0; u < Bn.length; u++) {
        var d = Bn[u];
        d.blockedOn === e && (d.blockedOn = null);
      }
      for (; Bn.length > 0; ) {
        var h = Bn[0];
        if (h.blockedOn !== null)
          break;
        yp(h), h.blockedOn === null && Bn.shift();
      }
    }
    var tn = c.ReactCurrentBatchConfig, Nn = !0;
    function Wr(e) {
      Nn = !!e;
    }
    function Mu() {
      return Nn;
    }
    function An(e, t, a) {
      var i = wf(t), u;
      switch (i) {
        case Tn:
          u = $s;
          break;
        case $i:
          u = bl;
          break;
        case fi:
        default:
          u = Lu;
          break;
      }
      return u.bind(null, t, a, e);
    }
    function $s(e, t, a, i) {
      var u = xa(), d = tn.transition;
      tn.transition = null;
      try {
        yn(Tn), Lu(e, t, a, i);
      } finally {
        yn(u), tn.transition = d;
      }
    }
    function bl(e, t, a, i) {
      var u = xa(), d = tn.transition;
      tn.transition = null;
      try {
        yn($i), Lu(e, t, a, i);
      } finally {
        yn(u), tn.transition = d;
      }
    }
    function Lu(e, t, a, i) {
      Nn && gp(e, t, a, i);
    }
    function gp(e, t, a, i) {
      var u = fr(e, t, a, i);
      if (u === null) {
        Ug(e, t, i, No, a), mp(e, i);
        return;
      }
      if (Hh(u, e, t, a, i)) {
        i.stopPropagation();
        return;
      }
      if (mp(e, i), t & al && di(e)) {
        for (; u !== null; ) {
          var d = Hu(u);
          d !== null && vp(d);
          var h = fr(e, t, a, i);
          if (h === null && Ug(e, t, i, No, a), h === u)
            break;
          u = h;
        }
        u !== null && i.stopPropagation();
        return;
      }
      Ug(e, t, i, null, a);
    }
    var No = null;
    function fr(e, t, a, i) {
      No = null;
      var u = Ac(i), d = Js(u);
      if (d !== null) {
        var h = ra(d);
        if (h === null)
          d = null;
        else {
          var S = h.tag;
          if (S === ne) {
            var b = Zd(h);
            if (b !== null)
              return b;
            d = null;
          } else if (S === L) {
            var k = h.stateNode;
            if (gf(k))
              return Hc(h);
            d = null;
          } else h !== d && (d = null);
        }
      }
      return No = d, null;
    }
    function wf(e) {
      switch (e) {
        case "cancel":
        case "click":
        case "close":
        case "contextmenu":
        case "copy":
        case "cut":
        case "auxclick":
        case "dblclick":
        case "dragend":
        case "dragstart":
        case "drop":
        case "focusin":
        case "focusout":
        case "input":
        case "invalid":
        case "keydown":
        case "keypress":
        case "keyup":
        case "mousedown":
        case "mouseup":
        case "paste":
        case "pause":
        case "play":
        case "pointercancel":
        case "pointerdown":
        case "pointerup":
        case "ratechange":
        case "reset":
        case "resize":
        case "seeked":
        case "submit":
        case "touchcancel":
        case "touchend":
        case "touchstart":
        case "volumechange":
        case "change":
        case "selectionchange":
        case "textInput":
        case "compositionstart":
        case "compositionend":
        case "compositionupdate":
        case "beforeblur":
        case "afterblur":
        case "beforeinput":
        case "blur":
        case "fullscreenchange":
        case "focus":
        case "hashchange":
        case "popstate":
        case "select":
        case "selectstart":
          return Tn;
        case "drag":
        case "dragenter":
        case "dragexit":
        case "dragleave":
        case "dragover":
        case "mousemove":
        case "mouseout":
        case "mouseover":
        case "pointermove":
        case "pointerout":
        case "pointerover":
        case "scroll":
        case "toggle":
        case "touchmove":
        case "wheel":
        case "mouseenter":
        case "mouseleave":
        case "pointerenter":
        case "pointerleave":
          return $i;
        case "message": {
          var t = tp();
          switch (t) {
            case Bc:
              return Tn;
            case cl:
              return $i;
            case ci:
            case gh:
              return fi;
            case Ic:
              return Cu;
            default:
              return fi;
          }
        }
        default:
          return fi;
      }
    }
    function Nu(e, t, a) {
      return e.addEventListener(t, a, !1), a;
    }
    function Bi(e, t, a) {
      return e.addEventListener(t, a, !0), a;
    }
    function Tf(e, t, a, i) {
      return e.addEventListener(t, a, {
        capture: !0,
        passive: i
      }), a;
    }
    function Sp(e, t, a, i) {
      return e.addEventListener(t, a, {
        passive: i
      }), a;
    }
    var _a = null, Au = null, Da = null;
    function Rf(e) {
      return _a = e, Au = Is(), !0;
    }
    function Bs() {
      _a = null, Au = null, Da = null;
    }
    function xf() {
      if (Da)
        return Da;
      var e, t = Au, a = t.length, i, u = Is(), d = u.length;
      for (e = 0; e < a && t[e] === u[e]; e++)
        ;
      var h = a - e;
      for (i = 1; i <= h && t[a - i] === u[d - i]; i++)
        ;
      var S = i > 1 ? 1 - i : void 0;
      return Da = u.slice(e, S), Da;
    }
    function Is() {
      return "value" in _a ? _a.value : _a.textContent;
    }
    function Cl(e) {
      var t, a = e.keyCode;
      return "charCode" in e ? (t = e.charCode, t === 0 && a === 13 && (t = 13)) : t = a, t === 10 && (t = 13), t >= 32 || t === 13 ? t : 0;
    }
    function In() {
      return !0;
    }
    function Ii() {
      return !1;
    }
    function dn(e) {
      function t(a, i, u, d, h) {
        this._reactName = a, this._targetInst = u, this.type = i, this.nativeEvent = d, this.target = h, this.currentTarget = null;
        for (var S in e)
          if (e.hasOwnProperty(S)) {
            var b = e[S];
            b ? this[S] = b(d) : this[S] = d[S];
          }
        var k = d.defaultPrevented != null ? d.defaultPrevented : d.returnValue === !1;
        return k ? this.isDefaultPrevented = In : this.isDefaultPrevented = Ii, this.isPropagationStopped = Ii, this;
      }
      return dt(t.prototype, {
        preventDefault: function() {
          this.defaultPrevented = !0;
          var a = this.nativeEvent;
          a && (a.preventDefault ? a.preventDefault() : typeof a.returnValue != "unknown" && (a.returnValue = !1), this.isDefaultPrevented = In);
        },
        stopPropagation: function() {
          var a = this.nativeEvent;
          a && (a.stopPropagation ? a.stopPropagation() : typeof a.cancelBubble != "unknown" && (a.cancelBubble = !0), this.isPropagationStopped = In);
        },
        /**
         * We release all dispatched `SyntheticEvent`s after each event loop, adding
         * them back into the pool. This allows a way to hold onto a reference that
         * won't be added back into the pool.
         */
        persist: function() {
        },
        /**
         * Checks if this event should be released back into the pool.
         *
         * @return {boolean} True if this should not be released, false otherwise.
         */
        isPersistent: In
      }), t;
    }
    var zn = {
      eventPhase: 0,
      bubbles: 0,
      cancelable: 0,
      timeStamp: function(e) {
        return e.timeStamp || Date.now();
      },
      defaultPrevented: 0,
      isTrusted: 0
    }, kf = dn(zn), wl = dt({}, zn, {
      view: 0,
      detail: 0
    }), Ep = dn(wl), bp, pi, zu;
    function Cp(e) {
      e !== zu && (zu && e.type === "mousemove" ? (bp = e.screenX - zu.screenX, pi = e.screenY - zu.screenY) : (bp = 0, pi = 0), zu = e);
    }
    var vi = dt({}, wl, {
      screenX: 0,
      screenY: 0,
      clientX: 0,
      clientY: 0,
      pageX: 0,
      pageY: 0,
      ctrlKey: 0,
      shiftKey: 0,
      altKey: 0,
      metaKey: 0,
      getModifierState: wp,
      button: 0,
      buttons: 0,
      relatedTarget: function(e) {
        return e.relatedTarget === void 0 ? e.fromElement === e.srcElement ? e.toElement : e.fromElement : e.relatedTarget;
      },
      movementX: function(e) {
        return "movementX" in e ? e.movementX : (Cp(e), bp);
      },
      movementY: function(e) {
        return "movementY" in e ? e.movementY : pi;
      }
    }), _f = dn(vi), Tl = dt({}, vi, {
      dataTransfer: 0
    }), jh = dn(Tl), Vh = dt({}, wl, {
      relatedTarget: 0
    }), Ys = dn(Vh), Df = dt({}, zn, {
      animationName: 0,
      elapsedTime: 0,
      pseudoElement: 0
    }), Eg = dn(Df), bg = dt({}, zn, {
      clipboardData: function(e) {
        return "clipboardData" in e ? e.clipboardData : window.clipboardData;
      }
    }), $h = dn(bg), Bh = dt({}, zn, {
      data: 0
    }), Ao = dn(Bh), Cg = Ao, Uu = {
      Esc: "Escape",
      Spacebar: " ",
      Left: "ArrowLeft",
      Up: "ArrowUp",
      Right: "ArrowRight",
      Down: "ArrowDown",
      Del: "Delete",
      Win: "OS",
      Menu: "ContextMenu",
      Apps: "ContextMenu",
      Scroll: "ScrollLock",
      MozPrintableKey: "Unidentified"
    }, Ih = {
      8: "Backspace",
      9: "Tab",
      12: "Clear",
      13: "Enter",
      16: "Shift",
      17: "Control",
      18: "Alt",
      19: "Pause",
      20: "CapsLock",
      27: "Escape",
      32: " ",
      33: "PageUp",
      34: "PageDown",
      35: "End",
      36: "Home",
      37: "ArrowLeft",
      38: "ArrowUp",
      39: "ArrowRight",
      40: "ArrowDown",
      45: "Insert",
      46: "Delete",
      112: "F1",
      113: "F2",
      114: "F3",
      115: "F4",
      116: "F5",
      117: "F6",
      118: "F7",
      119: "F8",
      120: "F9",
      121: "F10",
      122: "F11",
      123: "F12",
      144: "NumLock",
      145: "ScrollLock",
      224: "Meta"
    };
    function Sn(e) {
      if (e.key) {
        var t = Uu[e.key] || e.key;
        if (t !== "Unidentified")
          return t;
      }
      if (e.type === "keypress") {
        var a = Cl(e);
        return a === 13 ? "Enter" : String.fromCharCode(a);
      }
      return e.type === "keydown" || e.type === "keyup" ? Ih[e.keyCode] || "Unidentified" : "";
    }
    var wg = {
      Alt: "altKey",
      Control: "ctrlKey",
      Meta: "metaKey",
      Shift: "shiftKey"
    };
    function Yh(e) {
      var t = this, a = t.nativeEvent;
      if (a.getModifierState)
        return a.getModifierState(e);
      var i = wg[e];
      return i ? !!a[i] : !1;
    }
    function wp(e) {
      return Yh;
    }
    var Tg = dt({}, wl, {
      key: Sn,
      code: 0,
      location: 0,
      ctrlKey: 0,
      shiftKey: 0,
      altKey: 0,
      metaKey: 0,
      repeat: 0,
      locale: 0,
      getModifierState: wp,
      // Legacy Interface
      charCode: function(e) {
        return e.type === "keypress" ? Cl(e) : 0;
      },
      keyCode: function(e) {
        return e.type === "keydown" || e.type === "keyup" ? e.keyCode : 0;
      },
      which: function(e) {
        return e.type === "keypress" ? Cl(e) : e.type === "keydown" || e.type === "keyup" ? e.keyCode : 0;
      }
    }), Wh = dn(Tg), Qh = dt({}, vi, {
      pointerId: 0,
      width: 0,
      height: 0,
      pressure: 0,
      tangentialPressure: 0,
      tiltX: 0,
      tiltY: 0,
      twist: 0,
      pointerType: 0,
      isPrimary: 0
    }), Gh = dn(Qh), Oa = dt({}, wl, {
      touches: 0,
      targetTouches: 0,
      changedTouches: 0,
      altKey: 0,
      metaKey: 0,
      ctrlKey: 0,
      shiftKey: 0,
      getModifierState: wp
    }), Tp = dn(Oa), Rg = dt({}, zn, {
      propertyName: 0,
      elapsedTime: 0,
      pseudoElement: 0
    }), zo = dn(Rg), Of = dt({}, vi, {
      deltaX: function(e) {
        return "deltaX" in e ? e.deltaX : (
          // Fallback to `wheelDeltaX` for Webkit and normalize (right is positive).
          "wheelDeltaX" in e ? -e.wheelDeltaX : 0
        );
      },
      deltaY: function(e) {
        return "deltaY" in e ? e.deltaY : (
          // Fallback to `wheelDeltaY` for Webkit and normalize (down is positive).
          "wheelDeltaY" in e ? -e.wheelDeltaY : (
            // Fallback to `wheelDelta` for IE<9 and normalize (down is positive).
            "wheelDelta" in e ? -e.wheelDelta : 0
          )
        );
      },
      deltaZ: 0,
      // Browsers without "deltaMode" is reporting in raw wheel delta where one
      // notch on the scroll is always +/- 120, roughly equivalent to pixels.
      // A good approximation of DOM_DELTA_LINE (1) is 5% of viewport size or
      // ~40 pixels, for DOM_DELTA_SCREEN (2) it is 87.5% of viewport size.
      deltaMode: 0
    }), Rl = dn(Of), Mf = [9, 13, 27, 32], Lf = 229, Ws = bn && "CompositionEvent" in window, Qs = null;
    bn && "documentMode" in document && (Qs = document.documentMode);
    var Rp = bn && "TextEvent" in window && !Qs, qh = bn && (!Ws || Qs && Qs > 8 && Qs <= 11), xp = 32, kp = String.fromCharCode(xp);
    function Nf() {
      xr("onBeforeInput", ["compositionend", "keypress", "textInput", "paste"]), xr("onCompositionEnd", ["compositionend", "focusout", "keydown", "keypress", "keyup", "mousedown"]), xr("onCompositionStart", ["compositionstart", "focusout", "keydown", "keypress", "keyup", "mousedown"]), xr("onCompositionUpdate", ["compositionupdate", "focusout", "keydown", "keypress", "keyup", "mousedown"]);
    }
    var Gs = !1;
    function Xh(e) {
      return (e.ctrlKey || e.altKey || e.metaKey) && // ctrlKey && altKey is equivalent to AltGr, and is not a command.
      !(e.ctrlKey && e.altKey);
    }
    function _p(e) {
      switch (e) {
        case "compositionstart":
          return "onCompositionStart";
        case "compositionend":
          return "onCompositionEnd";
        case "compositionupdate":
          return "onCompositionUpdate";
      }
    }
    function xg(e, t) {
      return e === "keydown" && t.keyCode === Lf;
    }
    function Dp(e, t) {
      switch (e) {
        case "keyup":
          return Mf.indexOf(t.keyCode) !== -1;
        case "keydown":
          return t.keyCode !== Lf;
        case "keypress":
        case "mousedown":
        case "focusout":
          return !0;
        default:
          return !1;
      }
    }
    function Af(e) {
      var t = e.detail;
      return typeof t == "object" && "data" in t ? t.data : null;
    }
    function qs(e) {
      return e.locale === "ko";
    }
    var Uo = !1;
    function zf(e, t, a, i, u) {
      var d, h;
      if (Ws ? d = _p(t) : Uo ? Dp(t, i) && (d = "onCompositionEnd") : xg(t, i) && (d = "onCompositionStart"), !d)
        return null;
      qh && !qs(i) && (!Uo && d === "onCompositionStart" ? Uo = Rf(u) : d === "onCompositionEnd" && Uo && (h = xf()));
      var S = nm(a, d);
      if (S.length > 0) {
        var b = new Ao(d, t, null, i, u);
        if (e.push({
          event: b,
          listeners: S
        }), h)
          b.data = h;
        else {
          var k = Af(i);
          k !== null && (b.data = k);
        }
      }
    }
    function Kh(e, t) {
      switch (e) {
        case "compositionend":
          return Af(t);
        case "keypress":
          var a = t.which;
          return a !== xp ? null : (Gs = !0, kp);
        case "textInput":
          var i = t.data;
          return i === kp && Gs ? null : i;
        default:
          return null;
      }
    }
    function kg(e, t) {
      if (Uo) {
        if (e === "compositionend" || !Ws && Dp(e, t)) {
          var a = xf();
          return Bs(), Uo = !1, a;
        }
        return null;
      }
      switch (e) {
        case "paste":
          return null;
        case "keypress":
          if (!Xh(t)) {
            if (t.char && t.char.length > 1)
              return t.char;
            if (t.which)
              return String.fromCharCode(t.which);
          }
          return null;
        case "compositionend":
          return qh && !qs(t) ? null : t.data;
        default:
          return null;
      }
    }
    function Uf(e, t, a, i, u) {
      var d;
      if (Rp ? d = Kh(t, i) : d = kg(t, i), !d)
        return null;
      var h = nm(a, "onBeforeInput");
      if (h.length > 0) {
        var S = new Cg("onBeforeInput", "beforeinput", null, i, u);
        e.push({
          event: S,
          listeners: h
        }), S.data = d;
      }
    }
    function _g(e, t, a, i, u, d, h) {
      zf(e, t, a, i, u), Uf(e, t, a, i, u);
    }
    var Xs = {
      color: !0,
      date: !0,
      datetime: !0,
      "datetime-local": !0,
      email: !0,
      month: !0,
      number: !0,
      password: !0,
      range: !0,
      search: !0,
      tel: !0,
      text: !0,
      time: !0,
      url: !0,
      week: !0
    };
    function Zh(e) {
      var t = e && e.nodeName && e.nodeName.toLowerCase();
      return t === "input" ? !!Xs[e.type] : t === "textarea";
    }
    /**
     * Checks if an event is supported in the current execution environment.
     *
     * NOTE: This will not work correctly for non-generic events such as `change`,
     * `reset`, `load`, `error`, and `select`.
     *
     * Borrows from Modernizr.
     *
     * @param {string} eventNameSuffix Event name, e.g. "click".
     * @return {boolean} True if the event is supported.
     * @internal
     * @license Modernizr 3.0.0pre (Custom Build) | MIT
     */
    function Pf(e) {
      if (!bn)
        return !1;
      var t = "on" + e, a = t in document;
      if (!a) {
        var i = document.createElement("div");
        i.setAttribute(t, "return;"), a = typeof i[t] == "function";
      }
      return a;
    }
    function n() {
      xr("onChange", ["change", "click", "focusin", "focusout", "input", "keydown", "keyup", "selectionchange"]);
    }
    function r(e, t, a, i) {
      zc(i);
      var u = nm(t, "onChange");
      if (u.length > 0) {
        var d = new kf("onChange", "change", null, a, i);
        e.push({
          event: d,
          listeners: u
        });
      }
    }
    var o = null, f = null;
    function v(e) {
      var t = e.nodeName && e.nodeName.toLowerCase();
      return t === "select" || t === "input" && e.type === "file";
    }
    function g(e) {
      var t = [];
      r(t, f, e, Ac(e)), Id(w, t);
    }
    function w(e) {
      _E(e, 0);
    }
    function O(e) {
      var t = Bf(e);
      if (Zl(t))
        return e;
    }
    function N(e, t) {
      if (e === "change")
        return t;
    }
    var K = !1;
    bn && (K = Pf("input") && (!document.documentMode || document.documentMode > 9));
    function ue(e, t) {
      o = e, f = t, o.attachEvent("onpropertychange", le);
    }
    function se() {
      o && (o.detachEvent("onpropertychange", le), o = null, f = null);
    }
    function le(e) {
      e.propertyName === "value" && O(f) && g(e);
    }
    function xe(e, t, a) {
      e === "focusin" ? (se(), ue(t, a)) : e === "focusout" && se();
    }
    function Le(e, t) {
      if (e === "selectionchange" || e === "keyup" || e === "keydown")
        return O(f);
    }
    function ze(e) {
      var t = e.nodeName;
      return t && t.toLowerCase() === "input" && (e.type === "checkbox" || e.type === "radio");
    }
    function Rn(e, t) {
      if (e === "click")
        return O(t);
    }
    function B(e, t) {
      if (e === "input" || e === "change")
        return O(t);
    }
    function U(e) {
      var t = e._wrapperState;
      !t || !t.controlled || e.type !== "number" || Ge(e, "number", e.value);
    }
    function Q(e, t, a, i, u, d, h) {
      var S = a ? Bf(a) : window, b, k;
      if (v(S) ? b = N : Zh(S) ? K ? b = B : (b = Le, k = xe) : ze(S) && (b = Rn), b) {
        var _ = b(t, a);
        if (_) {
          r(e, _, i, u);
          return;
        }
      }
      k && k(t, S, a), t === "focusout" && U(S);
    }
    function pe() {
      Fa("onMouseEnter", ["mouseout", "mouseover"]), Fa("onMouseLeave", ["mouseout", "mouseover"]), Fa("onPointerEnter", ["pointerout", "pointerover"]), Fa("onPointerLeave", ["pointerout", "pointerover"]);
    }
    function He(e, t, a, i, u, d, h) {
      var S = t === "mouseover" || t === "pointerover", b = t === "mouseout" || t === "pointerout";
      if (S && !fh(i)) {
        var k = i.relatedTarget || i.fromElement;
        if (k && (Js(k) || Bp(k)))
          return;
      }
      if (!(!b && !S)) {
        var _;
        if (u.window === u)
          _ = u;
        else {
          var P = u.ownerDocument;
          P ? _ = P.defaultView || P.parentWindow : _ = window;
        }
        var z, q;
        if (b) {
          var Z = i.relatedTarget || i.toElement;
          if (z = a, q = Z ? Js(Z) : null, q !== null) {
            var ee = ra(q);
            (q !== ee || q.tag !== F && q.tag !== $) && (q = null);
          }
        } else
          z = null, q = a;
        if (z !== q) {
          var we = _f, Qe = "onMouseLeave", Ve = "onMouseEnter", _t = "mouse";
          (t === "pointerout" || t === "pointerover") && (we = Gh, Qe = "onPointerLeave", Ve = "onPointerEnter", _t = "pointer");
          var St = z == null ? _ : Bf(z), I = q == null ? _ : Bf(q), te = new we(Qe, _t + "leave", z, i, u);
          te.target = St, te.relatedTarget = I;
          var Y = null, ce = Js(u);
          if (ce === a) {
            var _e = new we(Ve, _t + "enter", q, i, u);
            _e.target = I, _e.relatedTarget = St, Y = _e;
          }
          ux(e, te, Y, z, q);
        }
      }
    }
    function Ke(e, t) {
      return e === t && (e !== 0 || 1 / e === 1 / t) || e !== e && t !== t;
    }
    var Ne = typeof Object.is == "function" ? Object.is : Ke;
    function Ze(e, t) {
      if (Ne(e, t))
        return !0;
      if (typeof e != "object" || e === null || typeof t != "object" || t === null)
        return !1;
      var a = Object.keys(e), i = Object.keys(t);
      if (a.length !== i.length)
        return !1;
      for (var u = 0; u < a.length; u++) {
        var d = a[u];
        if (!ir.call(t, d) || !Ne(e[d], t[d]))
          return !1;
      }
      return !0;
    }
    function Un(e) {
      for (; e && e.firstChild; )
        e = e.firstChild;
      return e;
    }
    function At(e) {
      for (; e; ) {
        if (e.nextSibling)
          return e.nextSibling;
        e = e.parentNode;
      }
    }
    function Yi(e, t) {
      for (var a = Un(e), i = 0, u = 0; a; ) {
        if (a.nodeType === Ni) {
          if (u = i + a.textContent.length, i <= t && u >= t)
            return {
              node: a,
              offset: t - i
            };
          i = u;
        }
        a = Un(At(a));
      }
    }
    function Dg(e) {
      var t = e.ownerDocument, a = t && t.defaultView || window, i = a.getSelection && a.getSelection();
      if (!i || i.rangeCount === 0)
        return null;
      var u = i.anchorNode, d = i.anchorOffset, h = i.focusNode, S = i.focusOffset;
      try {
        u.nodeType, h.nodeType;
      } catch {
        return null;
      }
      return $R(e, u, d, h, S);
    }
    function $R(e, t, a, i, u) {
      var d = 0, h = -1, S = -1, b = 0, k = 0, _ = e, P = null;
      e: for (; ; ) {
        for (var z = null; _ === t && (a === 0 || _.nodeType === Ni) && (h = d + a), _ === i && (u === 0 || _.nodeType === Ni) && (S = d + u), _.nodeType === Ni && (d += _.nodeValue.length), (z = _.firstChild) !== null; )
          P = _, _ = z;
        for (; ; ) {
          if (_ === e)
            break e;
          if (P === t && ++b === a && (h = d), P === i && ++k === u && (S = d), (z = _.nextSibling) !== null)
            break;
          _ = P, P = _.parentNode;
        }
        _ = z;
      }
      return h === -1 || S === -1 ? null : {
        start: h,
        end: S
      };
    }
    function BR(e, t) {
      var a = e.ownerDocument || document, i = a && a.defaultView || window;
      if (i.getSelection) {
        var u = i.getSelection(), d = e.textContent.length, h = Math.min(t.start, d), S = t.end === void 0 ? h : Math.min(t.end, d);
        if (!u.extend && h > S) {
          var b = S;
          S = h, h = b;
        }
        var k = Yi(e, h), _ = Yi(e, S);
        if (k && _) {
          if (u.rangeCount === 1 && u.anchorNode === k.node && u.anchorOffset === k.offset && u.focusNode === _.node && u.focusOffset === _.offset)
            return;
          var P = a.createRange();
          P.setStart(k.node, k.offset), u.removeAllRanges(), h > S ? (u.addRange(P), u.extend(_.node, _.offset)) : (P.setEnd(_.node, _.offset), u.addRange(P));
        }
      }
    }
    function mE(e) {
      return e && e.nodeType === Ni;
    }
    function yE(e, t) {
      return !e || !t ? !1 : e === t ? !0 : mE(e) ? !1 : mE(t) ? yE(e, t.parentNode) : "contains" in e ? e.contains(t) : e.compareDocumentPosition ? !!(e.compareDocumentPosition(t) & 16) : !1;
    }
    function IR(e) {
      return e && e.ownerDocument && yE(e.ownerDocument.documentElement, e);
    }
    function YR(e) {
      try {
        return typeof e.contentWindow.location.href == "string";
      } catch {
        return !1;
      }
    }
    function gE() {
      for (var e = window, t = Eo(); t instanceof e.HTMLIFrameElement; ) {
        if (YR(t))
          e = t.contentWindow;
        else
          return t;
        t = Eo(e.document);
      }
      return t;
    }
    function Og(e) {
      var t = e && e.nodeName && e.nodeName.toLowerCase();
      return t && (t === "input" && (e.type === "text" || e.type === "search" || e.type === "tel" || e.type === "url" || e.type === "password") || t === "textarea" || e.contentEditable === "true");
    }
    function WR() {
      var e = gE();
      return {
        focusedElem: e,
        selectionRange: Og(e) ? GR(e) : null
      };
    }
    function QR(e) {
      var t = gE(), a = e.focusedElem, i = e.selectionRange;
      if (t !== a && IR(a)) {
        i !== null && Og(a) && qR(a, i);
        for (var u = [], d = a; d = d.parentNode; )
          d.nodeType === Ur && u.push({
            element: d,
            left: d.scrollLeft,
            top: d.scrollTop
          });
        typeof a.focus == "function" && a.focus();
        for (var h = 0; h < u.length; h++) {
          var S = u[h];
          S.element.scrollLeft = S.left, S.element.scrollTop = S.top;
        }
      }
    }
    function GR(e) {
      var t;
      return "selectionStart" in e ? t = {
        start: e.selectionStart,
        end: e.selectionEnd
      } : t = Dg(e), t || {
        start: 0,
        end: 0
      };
    }
    function qR(e, t) {
      var a = t.start, i = t.end;
      i === void 0 && (i = a), "selectionStart" in e ? (e.selectionStart = a, e.selectionEnd = Math.min(i, e.value.length)) : BR(e, t);
    }
    var XR = bn && "documentMode" in document && document.documentMode <= 11;
    function KR() {
      xr("onSelect", ["focusout", "contextmenu", "dragend", "focusin", "keydown", "keyup", "mousedown", "mouseup", "selectionchange"]);
    }
    var Ff = null, Mg = null, Op = null, Lg = !1;
    function ZR(e) {
      if ("selectionStart" in e && Og(e))
        return {
          start: e.selectionStart,
          end: e.selectionEnd
        };
      var t = e.ownerDocument && e.ownerDocument.defaultView || window, a = t.getSelection();
      return {
        anchorNode: a.anchorNode,
        anchorOffset: a.anchorOffset,
        focusNode: a.focusNode,
        focusOffset: a.focusOffset
      };
    }
    function JR(e) {
      return e.window === e ? e.document : e.nodeType === Ga ? e : e.ownerDocument;
    }
    function SE(e, t, a) {
      var i = JR(a);
      if (!(Lg || Ff == null || Ff !== Eo(i))) {
        var u = ZR(Ff);
        if (!Op || !Ze(Op, u)) {
          Op = u;
          var d = nm(Mg, "onSelect");
          if (d.length > 0) {
            var h = new kf("onSelect", "select", null, t, a);
            e.push({
              event: h,
              listeners: d
            }), h.target = Ff;
          }
        }
      }
    }
    function ex(e, t, a, i, u, d, h) {
      var S = a ? Bf(a) : window;
      switch (t) {
        case "focusin":
          (Zh(S) || S.contentEditable === "true") && (Ff = S, Mg = a, Op = null);
          break;
        case "focusout":
          Ff = null, Mg = null, Op = null;
          break;
        case "mousedown":
          Lg = !0;
          break;
        case "contextmenu":
        case "mouseup":
        case "dragend":
          Lg = !1, SE(e, i, u);
          break;
        case "selectionchange":
          if (XR)
            break;
        case "keydown":
        case "keyup":
          SE(e, i, u);
      }
    }
    function Jh(e, t) {
      var a = {};
      return a[e.toLowerCase()] = t.toLowerCase(), a["Webkit" + e] = "webkit" + t, a["Moz" + e] = "moz" + t, a;
    }
    var Hf = {
      animationend: Jh("Animation", "AnimationEnd"),
      animationiteration: Jh("Animation", "AnimationIteration"),
      animationstart: Jh("Animation", "AnimationStart"),
      transitionend: Jh("Transition", "TransitionEnd")
    }, Ng = {}, EE = {};
    bn && (EE = document.createElement("div").style, "AnimationEvent" in window || (delete Hf.animationend.animation, delete Hf.animationiteration.animation, delete Hf.animationstart.animation), "TransitionEvent" in window || delete Hf.transitionend.transition);
    function em(e) {
      if (Ng[e])
        return Ng[e];
      if (!Hf[e])
        return e;
      var t = Hf[e];
      for (var a in t)
        if (t.hasOwnProperty(a) && a in EE)
          return Ng[e] = t[a];
      return e;
    }
    var bE = em("animationend"), CE = em("animationiteration"), wE = em("animationstart"), TE = em("transitionend"), RE = /* @__PURE__ */ new Map(), xE = ["abort", "auxClick", "cancel", "canPlay", "canPlayThrough", "click", "close", "contextMenu", "copy", "cut", "drag", "dragEnd", "dragEnter", "dragExit", "dragLeave", "dragOver", "dragStart", "drop", "durationChange", "emptied", "encrypted", "ended", "error", "gotPointerCapture", "input", "invalid", "keyDown", "keyPress", "keyUp", "load", "loadedData", "loadedMetadata", "loadStart", "lostPointerCapture", "mouseDown", "mouseMove", "mouseOut", "mouseOver", "mouseUp", "paste", "pause", "play", "playing", "pointerCancel", "pointerDown", "pointerMove", "pointerOut", "pointerOver", "pointerUp", "progress", "rateChange", "reset", "resize", "seeked", "seeking", "stalled", "submit", "suspend", "timeUpdate", "touchCancel", "touchEnd", "touchStart", "volumeChange", "scroll", "toggle", "touchMove", "waiting", "wheel"];
    function Pu(e, t) {
      RE.set(e, t), xr(t, [e]);
    }
    function tx() {
      for (var e = 0; e < xE.length; e++) {
        var t = xE[e], a = t.toLowerCase(), i = t[0].toUpperCase() + t.slice(1);
        Pu(a, "on" + i);
      }
      Pu(bE, "onAnimationEnd"), Pu(CE, "onAnimationIteration"), Pu(wE, "onAnimationStart"), Pu("dblclick", "onDoubleClick"), Pu("focusin", "onFocus"), Pu("focusout", "onBlur"), Pu(TE, "onTransitionEnd");
    }
    function nx(e, t, a, i, u, d, h) {
      var S = RE.get(t);
      if (S !== void 0) {
        var b = kf, k = t;
        switch (t) {
          case "keypress":
            if (Cl(i) === 0)
              return;
          case "keydown":
          case "keyup":
            b = Wh;
            break;
          case "focusin":
            k = "focus", b = Ys;
            break;
          case "focusout":
            k = "blur", b = Ys;
            break;
          case "beforeblur":
          case "afterblur":
            b = Ys;
            break;
          case "click":
            if (i.button === 2)
              return;
          case "auxclick":
          case "dblclick":
          case "mousedown":
          case "mousemove":
          case "mouseup":
          case "mouseout":
          case "mouseover":
          case "contextmenu":
            b = _f;
            break;
          case "drag":
          case "dragend":
          case "dragenter":
          case "dragexit":
          case "dragleave":
          case "dragover":
          case "dragstart":
          case "drop":
            b = jh;
            break;
          case "touchcancel":
          case "touchend":
          case "touchmove":
          case "touchstart":
            b = Tp;
            break;
          case bE:
          case CE:
          case wE:
            b = Eg;
            break;
          case TE:
            b = zo;
            break;
          case "scroll":
            b = Ep;
            break;
          case "wheel":
            b = Rl;
            break;
          case "copy":
          case "cut":
          case "paste":
            b = $h;
            break;
          case "gotpointercapture":
          case "lostpointercapture":
          case "pointercancel":
          case "pointerdown":
          case "pointermove":
          case "pointerout":
          case "pointerover":
          case "pointerup":
            b = Gh;
            break;
        }
        var _ = (d & al) !== 0;
        {
          var P = !_ && // TODO: ideally, we'd eventually add all events from
          // nonDelegatedEvents list in DOMPluginEventSystem.
          // Then we can remove this special list.
          // This is a breaking change that can wait until React 18.
          t === "scroll", z = ox(a, S, i.type, _, P);
          if (z.length > 0) {
            var q = new b(S, k, null, i, u);
            e.push({
              event: q,
              listeners: z
            });
          }
        }
      }
    }
    tx(), pe(), n(), KR(), Nf();
    function rx(e, t, a, i, u, d, h) {
      nx(e, t, a, i, u, d);
      var S = (d & lg) === 0;
      S && (He(e, t, a, i, u), Q(e, t, a, i, u), ex(e, t, a, i, u), _g(e, t, a, i, u));
    }
    var Mp = ["abort", "canplay", "canplaythrough", "durationchange", "emptied", "encrypted", "ended", "error", "loadeddata", "loadedmetadata", "loadstart", "pause", "play", "playing", "progress", "ratechange", "resize", "seeked", "seeking", "stalled", "suspend", "timeupdate", "volumechange", "waiting"], Ag = new Set(["cancel", "close", "invalid", "load", "scroll", "toggle"].concat(Mp));
    function kE(e, t, a) {
      var i = e.type || "unknown-event";
      e.currentTarget = a, Pi(i, t, void 0, e), e.currentTarget = null;
    }
    function ax(e, t, a) {
      var i;
      if (a)
        for (var u = t.length - 1; u >= 0; u--) {
          var d = t[u], h = d.instance, S = d.currentTarget, b = d.listener;
          if (h !== i && e.isPropagationStopped())
            return;
          kE(e, b, S), i = h;
        }
      else
        for (var k = 0; k < t.length; k++) {
          var _ = t[k], P = _.instance, z = _.currentTarget, q = _.listener;
          if (P !== i && e.isPropagationStopped())
            return;
          kE(e, q, z), i = P;
        }
    }
    function _E(e, t) {
      for (var a = (t & al) !== 0, i = 0; i < e.length; i++) {
        var u = e[i], d = u.event, h = u.listeners;
        ax(d, h, a);
      }
      Gd();
    }
    function ix(e, t, a, i, u) {
      var d = Ac(a), h = [];
      rx(h, e, i, a, d, t), _E(h, t);
    }
    function pn(e, t) {
      Ag.has(e) || m('Did not expect a listenToNonDelegatedEvent() call for "%s". This is a bug in React. Please file an issue.', e);
      var a = !1, i = zk(t), u = sx(e);
      i.has(u) || (DE(t, e, Ss, a), i.add(u));
    }
    function zg(e, t, a) {
      Ag.has(e) && !t && m('Did not expect a listenToNativeEvent() call for "%s" in the bubble phase. This is a bug in React. Please file an issue.', e);
      var i = 0;
      t && (i |= al), DE(a, e, i, t);
    }
    var tm = "_reactListening" + Math.random().toString(36).slice(2);
    function Lp(e) {
      if (!e[tm]) {
        e[tm] = !0, xt.forEach(function(a) {
          a !== "selectionchange" && (Ag.has(a) || zg(a, !1, e), zg(a, !0, e));
        });
        var t = e.nodeType === Ga ? e : e.ownerDocument;
        t !== null && (t[tm] || (t[tm] = !0, zg("selectionchange", !1, t)));
      }
    }
    function DE(e, t, a, i, u) {
      var d = An(e, t, a), h = void 0;
      Ts && (t === "touchstart" || t === "touchmove" || t === "wheel") && (h = !0), e = e, i ? h !== void 0 ? Tf(e, t, d, h) : Bi(e, t, d) : h !== void 0 ? Sp(e, t, d, h) : Nu(e, t, d);
    }
    function OE(e, t) {
      return e === t || e.nodeType === kn && e.parentNode === t;
    }
    function Ug(e, t, a, i, u) {
      var d = i;
      if (!(t & zi) && !(t & Ss)) {
        var h = u;
        if (i !== null) {
          var S = i;
          e: for (; ; ) {
            if (S === null)
              return;
            var b = S.tag;
            if (b === L || b === j) {
              var k = S.stateNode.containerInfo;
              if (OE(k, h))
                break;
              if (b === j)
                for (var _ = S.return; _ !== null; ) {
                  var P = _.tag;
                  if (P === L || P === j) {
                    var z = _.stateNode.containerInfo;
                    if (OE(z, h))
                      return;
                  }
                  _ = _.return;
                }
              for (; k !== null; ) {
                var q = Js(k);
                if (q === null)
                  return;
                var Z = q.tag;
                if (Z === F || Z === $) {
                  S = d = q;
                  continue e;
                }
                k = k.parentNode;
              }
            }
            S = S.return;
          }
        }
      }
      Id(function() {
        return ix(e, t, a, d);
      });
    }
    function Np(e, t, a) {
      return {
        instance: e,
        listener: t,
        currentTarget: a
      };
    }
    function ox(e, t, a, i, u, d) {
      for (var h = t !== null ? t + "Capture" : null, S = i ? h : t, b = [], k = e, _ = null; k !== null; ) {
        var P = k, z = P.stateNode, q = P.tag;
        if (q === F && z !== null && (_ = z, S !== null)) {
          var Z = ol(k, S);
          Z != null && b.push(Np(k, Z, _));
        }
        if (u)
          break;
        k = k.return;
      }
      return b;
    }
    function nm(e, t) {
      for (var a = t + "Capture", i = [], u = e; u !== null; ) {
        var d = u, h = d.stateNode, S = d.tag;
        if (S === F && h !== null) {
          var b = h, k = ol(u, a);
          k != null && i.unshift(Np(u, k, b));
          var _ = ol(u, t);
          _ != null && i.push(Np(u, _, b));
        }
        u = u.return;
      }
      return i;
    }
    function jf(e) {
      if (e === null)
        return null;
      do
        e = e.return;
      while (e && e.tag !== F);
      return e || null;
    }
    function lx(e, t) {
      for (var a = e, i = t, u = 0, d = a; d; d = jf(d))
        u++;
      for (var h = 0, S = i; S; S = jf(S))
        h++;
      for (; u - h > 0; )
        a = jf(a), u--;
      for (; h - u > 0; )
        i = jf(i), h--;
      for (var b = u; b--; ) {
        if (a === i || i !== null && a === i.alternate)
          return a;
        a = jf(a), i = jf(i);
      }
      return null;
    }
    function ME(e, t, a, i, u) {
      for (var d = t._reactName, h = [], S = a; S !== null && S !== i; ) {
        var b = S, k = b.alternate, _ = b.stateNode, P = b.tag;
        if (k !== null && k === i)
          break;
        if (P === F && _ !== null) {
          var z = _;
          if (u) {
            var q = ol(S, d);
            q != null && h.unshift(Np(S, q, z));
          } else if (!u) {
            var Z = ol(S, d);
            Z != null && h.push(Np(S, Z, z));
          }
        }
        S = S.return;
      }
      h.length !== 0 && e.push({
        event: t,
        listeners: h
      });
    }
    function ux(e, t, a, i, u) {
      var d = i && u ? lx(i, u) : null;
      i !== null && ME(e, t, i, d, !1), u !== null && a !== null && ME(e, a, u, d, !0);
    }
    function sx(e, t) {
      return e + "__bubble";
    }
    var Ma = !1, Ap = "dangerouslySetInnerHTML", rm = "suppressContentEditableWarning", Fu = "suppressHydrationWarning", LE = "autoFocus", Ks = "children", Zs = "style", am = "__html", Pg, im, zp, NE, om, AE, zE;
    Pg = {
      // There are working polyfills for <dialog>. Let people use it.
      dialog: !0,
      // Electron ships a custom <webview> tag to display external web content in
      // an isolated frame and process.
      // This tag is not present in non Electron environments such as JSDom which
      // is often used for testing purposes.
      // @see https://electronjs.org/docs/api/webview-tag
      webview: !0
    }, im = function(e, t) {
      Nc(e, t), Hd(e, t), ch(e, t, {
        registrationNameDependencies: bt,
        possibleRegistrationNames: En
      });
    }, AE = bn && !document.documentMode, zp = function(e, t, a) {
      if (!Ma) {
        var i = lm(a), u = lm(t);
        u !== i && (Ma = !0, m("Prop `%s` did not match. Server: %s Client: %s", e, JSON.stringify(u), JSON.stringify(i)));
      }
    }, NE = function(e) {
      if (!Ma) {
        Ma = !0;
        var t = [];
        e.forEach(function(a) {
          t.push(a);
        }), m("Extra attributes from the server: %s", t);
      }
    }, om = function(e, t) {
      t === !1 ? m("Expected `%s` listener to be a function, instead got `false`.\n\nIf you used to conditionally omit it with %s={condition && value}, pass %s={condition ? value : undefined} instead.", e, e, e) : m("Expected `%s` listener to be a function, instead got a value of `%s` type.", e, typeof t);
    }, zE = function(e, t) {
      var a = e.namespaceURI === Li ? e.ownerDocument.createElement(e.tagName) : e.ownerDocument.createElementNS(e.namespaceURI, e.tagName);
      return a.innerHTML = t, a.innerHTML;
    };
    var cx = /\r\n?/g, fx = /\u0000|\uFFFD/g;
    function lm(e) {
      ni(e);
      var t = typeof e == "string" ? e : "" + e;
      return t.replace(cx, `
`).replace(fx, "");
    }
    function um(e, t, a, i) {
      var u = lm(t), d = lm(e);
      if (d !== u && (i && (Ma || (Ma = !0, m('Text content did not match. Server: "%s" Client: "%s"', d, u))), a && Oe))
        throw new Error("Text content does not match server-rendered HTML.");
    }
    function UE(e) {
      return e.nodeType === Ga ? e : e.ownerDocument;
    }
    function dx() {
    }
    function sm(e) {
      e.onclick = dx;
    }
    function px(e, t, a, i, u) {
      for (var d in i)
        if (i.hasOwnProperty(d)) {
          var h = i[d];
          if (d === Zs)
            h && Object.freeze(h), eh(t, h);
          else if (d === Ap) {
            var S = h ? h[am] : void 0;
            S != null && Bv(t, S);
          } else if (d === Ks)
            if (typeof h == "string") {
              var b = e !== "textarea" || h !== "";
              b && Dc(t, h);
            } else typeof h == "number" && Dc(t, "" + h);
          else d === rm || d === Fu || d === LE || (bt.hasOwnProperty(d) ? h != null && (typeof h != "function" && om(d, h), d === "onScroll" && pn("scroll", t)) : h != null && ai(t, d, h, u));
        }
    }
    function vx(e, t, a, i) {
      for (var u = 0; u < t.length; u += 2) {
        var d = t[u], h = t[u + 1];
        d === Zs ? eh(e, h) : d === Ap ? Bv(e, h) : d === Ks ? Dc(e, h) : ai(e, d, h, i);
      }
    }
    function hx(e, t, a, i) {
      var u, d = UE(a), h, S = i;
      if (S === Li && (S = kc(e)), S === Li) {
        if (u = Ai(e, t), !u && e !== e.toLowerCase() && m("<%s /> is using incorrect casing. Use PascalCase for React components, or lowercase for HTML elements.", e), e === "script") {
          var b = d.createElement("div");
          b.innerHTML = "<script><\/script>";
          var k = b.firstChild;
          h = b.removeChild(k);
        } else if (typeof t.is == "string")
          h = d.createElement(e, {
            is: t.is
          });
        else if (h = d.createElement(e), e === "select") {
          var _ = h;
          t.multiple ? _.multiple = !0 : t.size && (_.size = t.size);
        }
      } else
        h = d.createElementNS(S, e);
      return S === Li && !u && Object.prototype.toString.call(h) === "[object HTMLUnknownElement]" && !ir.call(Pg, e) && (Pg[e] = !0, m("The tag <%s> is unrecognized in this browser. If you meant to render a React component, start its name with an uppercase letter.", e)), h;
    }
    function mx(e, t) {
      return UE(t).createTextNode(e);
    }
    function yx(e, t, a, i) {
      var u = Ai(t, a);
      im(t, a);
      var d;
      switch (t) {
        case "dialog":
          pn("cancel", e), pn("close", e), d = a;
          break;
        case "iframe":
        case "object":
        case "embed":
          pn("load", e), d = a;
          break;
        case "video":
        case "audio":
          for (var h = 0; h < Mp.length; h++)
            pn(Mp[h], e);
          d = a;
          break;
        case "source":
          pn("error", e), d = a;
          break;
        case "img":
        case "image":
        case "link":
          pn("error", e), pn("load", e), d = a;
          break;
        case "details":
          pn("toggle", e), d = a;
          break;
        case "input":
          M(e, a), d = C(e, a), pn("invalid", e);
          break;
        case "option":
          $t(e, a), d = a;
          break;
        case "select":
          hs(e, a), d = vs(e, a), pn("invalid", e);
          break;
        case "textarea":
          jv(e, a), d = Od(e, a), pn("invalid", e);
          break;
        default:
          d = a;
      }
      switch (Mc(t, d), px(t, e, i, d, u), t) {
        case "input":
          ma(e), de(e, a, !1);
          break;
        case "textarea":
          ma(e), $v(e);
          break;
        case "option":
          Qt(e, a);
          break;
        case "select":
          _d(e, a);
          break;
        default:
          typeof d.onClick == "function" && sm(e);
          break;
      }
    }
    function gx(e, t, a, i, u) {
      im(t, i);
      var d = null, h, S;
      switch (t) {
        case "input":
          h = C(e, a), S = C(e, i), d = [];
          break;
        case "select":
          h = vs(e, a), S = vs(e, i), d = [];
          break;
        case "textarea":
          h = Od(e, a), S = Od(e, i), d = [];
          break;
        default:
          h = a, S = i, typeof h.onClick != "function" && typeof S.onClick == "function" && sm(e);
          break;
      }
      Mc(t, S);
      var b, k, _ = null;
      for (b in h)
        if (!(S.hasOwnProperty(b) || !h.hasOwnProperty(b) || h[b] == null))
          if (b === Zs) {
            var P = h[b];
            for (k in P)
              P.hasOwnProperty(k) && (_ || (_ = {}), _[k] = "");
          } else b === Ap || b === Ks || b === rm || b === Fu || b === LE || (bt.hasOwnProperty(b) ? d || (d = []) : (d = d || []).push(b, null));
      for (b in S) {
        var z = S[b], q = h != null ? h[b] : void 0;
        if (!(!S.hasOwnProperty(b) || z === q || z == null && q == null))
          if (b === Zs)
            if (z && Object.freeze(z), q) {
              for (k in q)
                q.hasOwnProperty(k) && (!z || !z.hasOwnProperty(k)) && (_ || (_ = {}), _[k] = "");
              for (k in z)
                z.hasOwnProperty(k) && q[k] !== z[k] && (_ || (_ = {}), _[k] = z[k]);
            } else
              _ || (d || (d = []), d.push(b, _)), _ = z;
          else if (b === Ap) {
            var Z = z ? z[am] : void 0, ee = q ? q[am] : void 0;
            Z != null && ee !== Z && (d = d || []).push(b, Z);
          } else b === Ks ? (typeof z == "string" || typeof z == "number") && (d = d || []).push(b, "" + z) : b === rm || b === Fu || (bt.hasOwnProperty(b) ? (z != null && (typeof z != "function" && om(b, z), b === "onScroll" && pn("scroll", e)), !d && q !== z && (d = [])) : (d = d || []).push(b, z));
      }
      return _ && (ys(_, S[Zs]), (d = d || []).push(Zs, _)), d;
    }
    function Sx(e, t, a, i, u) {
      a === "input" && u.type === "radio" && u.name != null && X(e, u);
      var d = Ai(a, i), h = Ai(a, u);
      switch (vx(e, t, d, h), a) {
        case "input":
          J(e, u);
          break;
        case "textarea":
          Vv(e, u);
          break;
        case "select":
          Ky(e, u);
          break;
      }
    }
    function Ex(e) {
      {
        var t = e.toLowerCase();
        return Lc.hasOwnProperty(t) && Lc[t] || null;
      }
    }
    function bx(e, t, a, i, u, d, h) {
      var S, b;
      switch (S = Ai(t, a), im(t, a), t) {
        case "dialog":
          pn("cancel", e), pn("close", e);
          break;
        case "iframe":
        case "object":
        case "embed":
          pn("load", e);
          break;
        case "video":
        case "audio":
          for (var k = 0; k < Mp.length; k++)
            pn(Mp[k], e);
          break;
        case "source":
          pn("error", e);
          break;
        case "img":
        case "image":
        case "link":
          pn("error", e), pn("load", e);
          break;
        case "details":
          pn("toggle", e);
          break;
        case "input":
          M(e, a), pn("invalid", e);
          break;
        case "option":
          $t(e, a);
          break;
        case "select":
          hs(e, a), pn("invalid", e);
          break;
        case "textarea":
          jv(e, a), pn("invalid", e);
          break;
      }
      Mc(t, a);
      {
        b = /* @__PURE__ */ new Set();
        for (var _ = e.attributes, P = 0; P < _.length; P++) {
          var z = _[P].name.toLowerCase();
          switch (z) {
            case "value":
              break;
            case "checked":
              break;
            case "selected":
              break;
            default:
              b.add(_[P].name);
          }
        }
      }
      var q = null;
      for (var Z in a)
        if (a.hasOwnProperty(Z)) {
          var ee = a[Z];
          if (Z === Ks)
            typeof ee == "string" ? e.textContent !== ee && (a[Fu] !== !0 && um(e.textContent, ee, d, h), q = [Ks, ee]) : typeof ee == "number" && e.textContent !== "" + ee && (a[Fu] !== !0 && um(e.textContent, ee, d, h), q = [Ks, "" + ee]);
          else if (bt.hasOwnProperty(Z))
            ee != null && (typeof ee != "function" && om(Z, ee), Z === "onScroll" && pn("scroll", e));
          else if (h && // Convince Flow we've calculated it (it's DEV-only in this method.)
          typeof S == "boolean") {
            var we = void 0, Qe = S && Je ? null : pa(Z);
            if (a[Fu] !== !0) {
              if (!(Z === rm || Z === Fu || // Controlled attributes are not validated
              // TODO: Only ignore them on controlled tags.
              Z === "value" || Z === "checked" || Z === "selected")) {
                if (Z === Ap) {
                  var Ve = e.innerHTML, _t = ee ? ee[am] : void 0;
                  if (_t != null) {
                    var St = zE(e, _t);
                    St !== Ve && zp(Z, Ve, St);
                  }
                } else if (Z === Zs) {
                  if (b.delete(Z), AE) {
                    var I = ig(ee);
                    we = e.getAttribute("style"), I !== we && zp(Z, we, I);
                  }
                } else if (S && !Je)
                  b.delete(Z.toLowerCase()), we = io(e, Z, ee), ee !== we && zp(Z, we, ee);
                else if (!Cn(Z, Qe, S) && !Zt(Z, ee, Qe, S)) {
                  var te = !1;
                  if (Qe !== null)
                    b.delete(Qe.attributeName), we = $a(e, Z, ee, Qe);
                  else {
                    var Y = i;
                    if (Y === Li && (Y = kc(t)), Y === Li)
                      b.delete(Z.toLowerCase());
                    else {
                      var ce = Ex(Z);
                      ce !== null && ce !== Z && (te = !0, b.delete(ce)), b.delete(Z);
                    }
                    we = io(e, Z, ee);
                  }
                  var _e = Je;
                  !_e && ee !== we && !te && zp(Z, we, ee);
                }
              }
            }
          }
        }
      switch (h && // $FlowFixMe - Should be inferred as not undefined.
      b.size > 0 && a[Fu] !== !0 && NE(b), t) {
        case "input":
          ma(e), de(e, a, !0);
          break;
        case "textarea":
          ma(e), $v(e);
          break;
        case "select":
        case "option":
          break;
        default:
          typeof a.onClick == "function" && sm(e);
          break;
      }
      return q;
    }
    function Cx(e, t, a) {
      var i = e.nodeValue !== t;
      return i;
    }
    function Fg(e, t) {
      {
        if (Ma)
          return;
        Ma = !0, m("Did not expect server HTML to contain a <%s> in <%s>.", t.nodeName.toLowerCase(), e.nodeName.toLowerCase());
      }
    }
    function Hg(e, t) {
      {
        if (Ma)
          return;
        Ma = !0, m('Did not expect server HTML to contain the text node "%s" in <%s>.', t.nodeValue, e.nodeName.toLowerCase());
      }
    }
    function jg(e, t, a) {
      {
        if (Ma)
          return;
        Ma = !0, m("Expected server HTML to contain a matching <%s> in <%s>.", t, e.nodeName.toLowerCase());
      }
    }
    function Vg(e, t) {
      {
        if (t === "" || Ma)
          return;
        Ma = !0, m('Expected server HTML to contain a matching text node for "%s" in <%s>.', t, e.nodeName.toLowerCase());
      }
    }
    function wx(e, t, a) {
      switch (t) {
        case "input":
          Xe(e, a);
          return;
        case "textarea":
          Md(e, a);
          return;
        case "select":
          Zy(e, a);
          return;
      }
    }
    var Up = function() {
    }, Pp = function() {
    };
    {
      var Tx = ["address", "applet", "area", "article", "aside", "base", "basefont", "bgsound", "blockquote", "body", "br", "button", "caption", "center", "col", "colgroup", "dd", "details", "dir", "div", "dl", "dt", "embed", "fieldset", "figcaption", "figure", "footer", "form", "frame", "frameset", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hgroup", "hr", "html", "iframe", "img", "input", "isindex", "li", "link", "listing", "main", "marquee", "menu", "menuitem", "meta", "nav", "noembed", "noframes", "noscript", "object", "ol", "p", "param", "plaintext", "pre", "script", "section", "select", "source", "style", "summary", "table", "tbody", "td", "template", "textarea", "tfoot", "th", "thead", "title", "tr", "track", "ul", "wbr", "xmp"], PE = [
        "applet",
        "caption",
        "html",
        "table",
        "td",
        "th",
        "marquee",
        "object",
        "template",
        // https://html.spec.whatwg.org/multipage/syntax.html#html-integration-point
        // TODO: Distinguish by namespace here -- for <title>, including it here
        // errs on the side of fewer warnings
        "foreignObject",
        "desc",
        "title"
      ], Rx = PE.concat(["button"]), xx = ["dd", "dt", "li", "option", "optgroup", "p", "rp", "rt"], FE = {
        current: null,
        formTag: null,
        aTagInScope: null,
        buttonTagInScope: null,
        nobrTagInScope: null,
        pTagInButtonScope: null,
        listItemTagAutoclosing: null,
        dlItemTagAutoclosing: null
      };
      Pp = function(e, t) {
        var a = dt({}, e || FE), i = {
          tag: t
        };
        return PE.indexOf(t) !== -1 && (a.aTagInScope = null, a.buttonTagInScope = null, a.nobrTagInScope = null), Rx.indexOf(t) !== -1 && (a.pTagInButtonScope = null), Tx.indexOf(t) !== -1 && t !== "address" && t !== "div" && t !== "p" && (a.listItemTagAutoclosing = null, a.dlItemTagAutoclosing = null), a.current = i, t === "form" && (a.formTag = i), t === "a" && (a.aTagInScope = i), t === "button" && (a.buttonTagInScope = i), t === "nobr" && (a.nobrTagInScope = i), t === "p" && (a.pTagInButtonScope = i), t === "li" && (a.listItemTagAutoclosing = i), (t === "dd" || t === "dt") && (a.dlItemTagAutoclosing = i), a;
      };
      var kx = function(e, t) {
        switch (t) {
          case "select":
            return e === "option" || e === "optgroup" || e === "#text";
          case "optgroup":
            return e === "option" || e === "#text";
          case "option":
            return e === "#text";
          case "tr":
            return e === "th" || e === "td" || e === "style" || e === "script" || e === "template";
          case "tbody":
          case "thead":
          case "tfoot":
            return e === "tr" || e === "style" || e === "script" || e === "template";
          case "colgroup":
            return e === "col" || e === "template";
          case "table":
            return e === "caption" || e === "colgroup" || e === "tbody" || e === "tfoot" || e === "thead" || e === "style" || e === "script" || e === "template";
          case "head":
            return e === "base" || e === "basefont" || e === "bgsound" || e === "link" || e === "meta" || e === "title" || e === "noscript" || e === "noframes" || e === "style" || e === "script" || e === "template";
          case "html":
            return e === "head" || e === "body" || e === "frameset";
          case "frameset":
            return e === "frame";
          case "#document":
            return e === "html";
        }
        switch (e) {
          case "h1":
          case "h2":
          case "h3":
          case "h4":
          case "h5":
          case "h6":
            return t !== "h1" && t !== "h2" && t !== "h3" && t !== "h4" && t !== "h5" && t !== "h6";
          case "rp":
          case "rt":
            return xx.indexOf(t) === -1;
          case "body":
          case "caption":
          case "col":
          case "colgroup":
          case "frameset":
          case "frame":
          case "head":
          case "html":
          case "tbody":
          case "td":
          case "tfoot":
          case "th":
          case "thead":
          case "tr":
            return t == null;
        }
        return !0;
      }, _x = function(e, t) {
        switch (e) {
          case "address":
          case "article":
          case "aside":
          case "blockquote":
          case "center":
          case "details":
          case "dialog":
          case "dir":
          case "div":
          case "dl":
          case "fieldset":
          case "figcaption":
          case "figure":
          case "footer":
          case "header":
          case "hgroup":
          case "main":
          case "menu":
          case "nav":
          case "ol":
          case "p":
          case "section":
          case "summary":
          case "ul":
          case "pre":
          case "listing":
          case "table":
          case "hr":
          case "xmp":
          case "h1":
          case "h2":
          case "h3":
          case "h4":
          case "h5":
          case "h6":
            return t.pTagInButtonScope;
          case "form":
            return t.formTag || t.pTagInButtonScope;
          case "li":
            return t.listItemTagAutoclosing;
          case "dd":
          case "dt":
            return t.dlItemTagAutoclosing;
          case "button":
            return t.buttonTagInScope;
          case "a":
            return t.aTagInScope;
          case "nobr":
            return t.nobrTagInScope;
        }
        return null;
      }, HE = {};
      Up = function(e, t, a) {
        a = a || FE;
        var i = a.current, u = i && i.tag;
        t != null && (e != null && m("validateDOMNesting: when childText is passed, childTag should be null"), e = "#text");
        var d = kx(e, u) ? null : i, h = d ? null : _x(e, a), S = d || h;
        if (S) {
          var b = S.tag, k = !!d + "|" + e + "|" + b;
          if (!HE[k]) {
            HE[k] = !0;
            var _ = e, P = "";
            if (e === "#text" ? /\S/.test(t) ? _ = "Text nodes" : (_ = "Whitespace text nodes", P = " Make sure you don't have any extra whitespace between tags on each line of your source code.") : _ = "<" + e + ">", d) {
              var z = "";
              b === "table" && e === "tr" && (z += " Add a <tbody>, <thead> or <tfoot> to your code to match the DOM tree generated by the browser."), m("validateDOMNesting(...): %s cannot appear as a child of <%s>.%s%s", _, b, P, z);
            } else
              m("validateDOMNesting(...): %s cannot appear as a descendant of <%s>.", _, b);
          }
        }
      };
    }
    var cm = "suppressHydrationWarning", fm = "$", dm = "/$", Fp = "$?", Hp = "$!", Dx = "style", $g = null, Bg = null;
    function Ox(e) {
      var t, a, i = e.nodeType;
      switch (i) {
        case Ga:
        case nl: {
          t = i === Ga ? "#document" : "#fragment";
          var u = e.documentElement;
          a = u ? u.namespaceURI : Nd(null, "");
          break;
        }
        default: {
          var d = i === kn ? e.parentNode : e, h = d.namespaceURI || null;
          t = d.tagName, a = Nd(h, t);
          break;
        }
      }
      {
        var S = t.toLowerCase(), b = Pp(null, S);
        return {
          namespace: a,
          ancestorInfo: b
        };
      }
    }
    function Mx(e, t, a) {
      {
        var i = e, u = Nd(i.namespace, t), d = Pp(i.ancestorInfo, t);
        return {
          namespace: u,
          ancestorInfo: d
        };
      }
    }
    function jA(e) {
      return e;
    }
    function Lx(e) {
      $g = Mu(), Bg = WR();
      var t = null;
      return Wr(!1), t;
    }
    function Nx(e) {
      QR(Bg), Wr($g), $g = null, Bg = null;
    }
    function Ax(e, t, a, i, u) {
      var d;
      {
        var h = i;
        if (Up(e, null, h.ancestorInfo), typeof t.children == "string" || typeof t.children == "number") {
          var S = "" + t.children, b = Pp(h.ancestorInfo, e);
          Up(null, S, b);
        }
        d = h.namespace;
      }
      var k = hx(e, t, a, d);
      return $p(u, k), Kg(k, t), k;
    }
    function zx(e, t) {
      e.appendChild(t);
    }
    function Ux(e, t, a, i, u) {
      switch (yx(e, t, a, i), t) {
        case "button":
        case "input":
        case "select":
        case "textarea":
          return !!a.autoFocus;
        case "img":
          return !0;
        default:
          return !1;
      }
    }
    function Px(e, t, a, i, u, d) {
      {
        var h = d;
        if (typeof i.children != typeof a.children && (typeof i.children == "string" || typeof i.children == "number")) {
          var S = "" + i.children, b = Pp(h.ancestorInfo, t);
          Up(null, S, b);
        }
      }
      return gx(e, t, a, i);
    }
    function Ig(e, t) {
      return e === "textarea" || e === "noscript" || typeof t.children == "string" || typeof t.children == "number" || typeof t.dangerouslySetInnerHTML == "object" && t.dangerouslySetInnerHTML !== null && t.dangerouslySetInnerHTML.__html != null;
    }
    function Fx(e, t, a, i) {
      {
        var u = a;
        Up(null, e, u.ancestorInfo);
      }
      var d = mx(e, t);
      return $p(i, d), d;
    }
    function Hx() {
      var e = window.event;
      return e === void 0 ? fi : wf(e.type);
    }
    var Yg = typeof setTimeout == "function" ? setTimeout : void 0, jx = typeof clearTimeout == "function" ? clearTimeout : void 0, Wg = -1, jE = typeof Promise == "function" ? Promise : void 0, Vx = typeof queueMicrotask == "function" ? queueMicrotask : typeof jE < "u" ? function(e) {
      return jE.resolve(null).then(e).catch($x);
    } : Yg;
    function $x(e) {
      setTimeout(function() {
        throw e;
      });
    }
    function Bx(e, t, a, i) {
      switch (t) {
        case "button":
        case "input":
        case "select":
        case "textarea":
          a.autoFocus && e.focus();
          return;
        case "img": {
          a.src && (e.src = a.src);
          return;
        }
      }
    }
    function Ix(e, t, a, i, u, d) {
      Sx(e, t, a, i, u), Kg(e, u);
    }
    function VE(e) {
      Dc(e, "");
    }
    function Yx(e, t, a) {
      e.nodeValue = a;
    }
    function Wx(e, t) {
      e.appendChild(t);
    }
    function Qx(e, t) {
      var a;
      e.nodeType === kn ? (a = e.parentNode, a.insertBefore(t, e)) : (a = e, a.appendChild(t));
      var i = e._reactRootContainer;
      i == null && a.onclick === null && sm(a);
    }
    function Gx(e, t, a) {
      e.insertBefore(t, a);
    }
    function qx(e, t, a) {
      e.nodeType === kn ? e.parentNode.insertBefore(t, a) : e.insertBefore(t, a);
    }
    function Xx(e, t) {
      e.removeChild(t);
    }
    function Kx(e, t) {
      e.nodeType === kn ? e.parentNode.removeChild(t) : e.removeChild(t);
    }
    function Qg(e, t) {
      var a = t, i = 0;
      do {
        var u = a.nextSibling;
        if (e.removeChild(a), u && u.nodeType === kn) {
          var d = u.data;
          if (d === dm)
            if (i === 0) {
              e.removeChild(u), gn(t);
              return;
            } else
              i--;
          else (d === fm || d === Fp || d === Hp) && i++;
        }
        a = u;
      } while (a);
      gn(t);
    }
    function Zx(e, t) {
      e.nodeType === kn ? Qg(e.parentNode, t) : e.nodeType === Ur && Qg(e, t), gn(e);
    }
    function Jx(e) {
      e = e;
      var t = e.style;
      typeof t.setProperty == "function" ? t.setProperty("display", "none", "important") : t.display = "none";
    }
    function ek(e) {
      e.nodeValue = "";
    }
    function tk(e, t) {
      e = e;
      var a = t[Dx], i = a != null && a.hasOwnProperty("display") ? a.display : null;
      e.style.display = Oc("display", i);
    }
    function nk(e, t) {
      e.nodeValue = t;
    }
    function rk(e) {
      e.nodeType === Ur ? e.textContent = "" : e.nodeType === Ga && e.documentElement && e.removeChild(e.documentElement);
    }
    function ak(e, t, a) {
      return e.nodeType !== Ur || t.toLowerCase() !== e.nodeName.toLowerCase() ? null : e;
    }
    function ik(e, t) {
      return t === "" || e.nodeType !== Ni ? null : e;
    }
    function ok(e) {
      return e.nodeType !== kn ? null : e;
    }
    function $E(e) {
      return e.data === Fp;
    }
    function Gg(e) {
      return e.data === Hp;
    }
    function lk(e) {
      var t = e.nextSibling && e.nextSibling.dataset, a, i, u;
      return t && (a = t.dgst, i = t.msg, u = t.stck), {
        message: i,
        digest: a,
        stack: u
      };
    }
    function uk(e, t) {
      e._reactRetry = t;
    }
    function pm(e) {
      for (; e != null; e = e.nextSibling) {
        var t = e.nodeType;
        if (t === Ur || t === Ni)
          break;
        if (t === kn) {
          var a = e.data;
          if (a === fm || a === Hp || a === Fp)
            break;
          if (a === dm)
            return null;
        }
      }
      return e;
    }
    function jp(e) {
      return pm(e.nextSibling);
    }
    function sk(e) {
      return pm(e.firstChild);
    }
    function ck(e) {
      return pm(e.firstChild);
    }
    function fk(e) {
      return pm(e.nextSibling);
    }
    function dk(e, t, a, i, u, d, h) {
      $p(d, e), Kg(e, a);
      var S;
      {
        var b = u;
        S = b.namespace;
      }
      var k = (d.mode & je) !== Fe;
      return bx(e, t, a, S, i, k, h);
    }
    function pk(e, t, a, i) {
      return $p(a, e), a.mode & je, Cx(e, t);
    }
    function vk(e, t) {
      $p(t, e);
    }
    function hk(e) {
      for (var t = e.nextSibling, a = 0; t; ) {
        if (t.nodeType === kn) {
          var i = t.data;
          if (i === dm) {
            if (a === 0)
              return jp(t);
            a--;
          } else (i === fm || i === Hp || i === Fp) && a++;
        }
        t = t.nextSibling;
      }
      return null;
    }
    function BE(e) {
      for (var t = e.previousSibling, a = 0; t; ) {
        if (t.nodeType === kn) {
          var i = t.data;
          if (i === fm || i === Hp || i === Fp) {
            if (a === 0)
              return t;
            a--;
          } else i === dm && a++;
        }
        t = t.previousSibling;
      }
      return null;
    }
    function mk(e) {
      gn(e);
    }
    function yk(e) {
      gn(e);
    }
    function gk(e) {
      return e !== "head" && e !== "body";
    }
    function Sk(e, t, a, i) {
      var u = !0;
      um(t.nodeValue, a, i, u);
    }
    function Ek(e, t, a, i, u, d) {
      if (t[cm] !== !0) {
        var h = !0;
        um(i.nodeValue, u, d, h);
      }
    }
    function bk(e, t) {
      t.nodeType === Ur ? Fg(e, t) : t.nodeType === kn || Hg(e, t);
    }
    function Ck(e, t) {
      {
        var a = e.parentNode;
        a !== null && (t.nodeType === Ur ? Fg(a, t) : t.nodeType === kn || Hg(a, t));
      }
    }
    function wk(e, t, a, i, u) {
      (u || t[cm] !== !0) && (i.nodeType === Ur ? Fg(a, i) : i.nodeType === kn || Hg(a, i));
    }
    function Tk(e, t, a) {
      jg(e, t);
    }
    function Rk(e, t) {
      Vg(e, t);
    }
    function xk(e, t, a) {
      {
        var i = e.parentNode;
        i !== null && jg(i, t);
      }
    }
    function kk(e, t) {
      {
        var a = e.parentNode;
        a !== null && Vg(a, t);
      }
    }
    function _k(e, t, a, i, u, d) {
      (d || t[cm] !== !0) && jg(a, i);
    }
    function Dk(e, t, a, i, u) {
      (u || t[cm] !== !0) && Vg(a, i);
    }
    function Ok(e) {
      m("An error occurred during hydration. The server HTML was replaced with client content in <%s>.", e.nodeName.toLowerCase());
    }
    function Mk(e) {
      Lp(e);
    }
    var Vf = Math.random().toString(36).slice(2), $f = "__reactFiber$" + Vf, qg = "__reactProps$" + Vf, Vp = "__reactContainer$" + Vf, Xg = "__reactEvents$" + Vf, Lk = "__reactListeners$" + Vf, Nk = "__reactHandles$" + Vf;
    function Ak(e) {
      delete e[$f], delete e[qg], delete e[Xg], delete e[Lk], delete e[Nk];
    }
    function $p(e, t) {
      t[$f] = e;
    }
    function vm(e, t) {
      t[Vp] = e;
    }
    function IE(e) {
      e[Vp] = null;
    }
    function Bp(e) {
      return !!e[Vp];
    }
    function Js(e) {
      var t = e[$f];
      if (t)
        return t;
      for (var a = e.parentNode; a; ) {
        if (t = a[Vp] || a[$f], t) {
          var i = t.alternate;
          if (t.child !== null || i !== null && i.child !== null)
            for (var u = BE(e); u !== null; ) {
              var d = u[$f];
              if (d)
                return d;
              u = BE(u);
            }
          return t;
        }
        e = a, a = e.parentNode;
      }
      return null;
    }
    function Hu(e) {
      var t = e[$f] || e[Vp];
      return t && (t.tag === F || t.tag === $ || t.tag === ne || t.tag === L) ? t : null;
    }
    function Bf(e) {
      if (e.tag === F || e.tag === $)
        return e.stateNode;
      throw new Error("getNodeFromInstance: Invalid argument.");
    }
    function hm(e) {
      return e[qg] || null;
    }
    function Kg(e, t) {
      e[qg] = t;
    }
    function zk(e) {
      var t = e[Xg];
      return t === void 0 && (t = e[Xg] = /* @__PURE__ */ new Set()), t;
    }
    var YE = {}, WE = c.ReactDebugCurrentFrame;
    function mm(e) {
      if (e) {
        var t = e._owner, a = oi(e.type, e._source, t ? t.type : null);
        WE.setExtraStackFrame(a);
      } else
        WE.setExtraStackFrame(null);
    }
    function Wi(e, t, a, i, u) {
      {
        var d = Function.call.bind(ir);
        for (var h in e)
          if (d(e, h)) {
            var S = void 0;
            try {
              if (typeof e[h] != "function") {
                var b = Error((i || "React class") + ": " + a + " type `" + h + "` is invalid; it must be a function, usually from the `prop-types` package, but received `" + typeof e[h] + "`.This often happens because of typos such as `PropTypes.function` instead of `PropTypes.func`.");
                throw b.name = "Invariant Violation", b;
              }
              S = e[h](t, h, i, a, null, "SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED");
            } catch (k) {
              S = k;
            }
            S && !(S instanceof Error) && (mm(u), m("%s: type specification of %s `%s` is invalid; the type checker function must return `null` or an `Error` but returned a %s. You may have forgotten to pass an argument to the type checker creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and shape all require an argument).", i || "React class", a, h, typeof S), mm(null)), S instanceof Error && !(S.message in YE) && (YE[S.message] = !0, mm(u), m("Failed %s type: %s", a, S.message), mm(null));
          }
      }
    }
    var Zg = [], ym;
    ym = [];
    var xl = -1;
    function ju(e) {
      return {
        current: e
      };
    }
    function Qr(e, t) {
      if (xl < 0) {
        m("Unexpected pop.");
        return;
      }
      t !== ym[xl] && m("Unexpected Fiber popped."), e.current = Zg[xl], Zg[xl] = null, ym[xl] = null, xl--;
    }
    function Gr(e, t, a) {
      xl++, Zg[xl] = e.current, ym[xl] = a, e.current = t;
    }
    var Jg;
    Jg = {};
    var Za = {};
    Object.freeze(Za);
    var kl = ju(Za), Po = ju(!1), e0 = Za;
    function If(e, t, a) {
      return a && Fo(t) ? e0 : kl.current;
    }
    function QE(e, t, a) {
      {
        var i = e.stateNode;
        i.__reactInternalMemoizedUnmaskedChildContext = t, i.__reactInternalMemoizedMaskedChildContext = a;
      }
    }
    function Yf(e, t) {
      {
        var a = e.type, i = a.contextTypes;
        if (!i)
          return Za;
        var u = e.stateNode;
        if (u && u.__reactInternalMemoizedUnmaskedChildContext === t)
          return u.__reactInternalMemoizedMaskedChildContext;
        var d = {};
        for (var h in i)
          d[h] = t[h];
        {
          var S = it(e) || "Unknown";
          Wi(i, d, "context", S);
        }
        return u && QE(e, t, d), d;
      }
    }
    function gm() {
      return Po.current;
    }
    function Fo(e) {
      {
        var t = e.childContextTypes;
        return t != null;
      }
    }
    function Sm(e) {
      Qr(Po, e), Qr(kl, e);
    }
    function t0(e) {
      Qr(Po, e), Qr(kl, e);
    }
    function GE(e, t, a) {
      {
        if (kl.current !== Za)
          throw new Error("Unexpected context found on stack. This error is likely caused by a bug in React. Please file an issue.");
        Gr(kl, t, e), Gr(Po, a, e);
      }
    }
    function qE(e, t, a) {
      {
        var i = e.stateNode, u = t.childContextTypes;
        if (typeof i.getChildContext != "function") {
          {
            var d = it(e) || "Unknown";
            Jg[d] || (Jg[d] = !0, m("%s.childContextTypes is specified but there is no getChildContext() method on the instance. You can either define getChildContext() on %s or remove childContextTypes from it.", d, d));
          }
          return a;
        }
        var h = i.getChildContext();
        for (var S in h)
          if (!(S in u))
            throw new Error((it(e) || "Unknown") + '.getChildContext(): key "' + S + '" is not defined in childContextTypes.');
        {
          var b = it(e) || "Unknown";
          Wi(u, h, "child context", b);
        }
        return dt({}, a, h);
      }
    }
    function Em(e) {
      {
        var t = e.stateNode, a = t && t.__reactInternalMemoizedMergedChildContext || Za;
        return e0 = kl.current, Gr(kl, a, e), Gr(Po, Po.current, e), !0;
      }
    }
    function XE(e, t, a) {
      {
        var i = e.stateNode;
        if (!i)
          throw new Error("Expected to have an instance by this point. This error is likely caused by a bug in React. Please file an issue.");
        if (a) {
          var u = qE(e, t, e0);
          i.__reactInternalMemoizedMergedChildContext = u, Qr(Po, e), Qr(kl, e), Gr(kl, u, e), Gr(Po, a, e);
        } else
          Qr(Po, e), Gr(Po, a, e);
      }
    }
    function Uk(e) {
      {
        if (!Jd(e) || e.tag !== D)
          throw new Error("Expected subtree parent to be a mounted class component. This error is likely caused by a bug in React. Please file an issue.");
        var t = e;
        do {
          switch (t.tag) {
            case L:
              return t.stateNode.context;
            case D: {
              var a = t.type;
              if (Fo(a))
                return t.stateNode.__reactInternalMemoizedMergedChildContext;
              break;
            }
          }
          t = t.return;
        } while (t !== null);
        throw new Error("Found unexpected detached subtree parent. This error is likely caused by a bug in React. Please file an issue.");
      }
    }
    var Vu = 0, bm = 1, _l = null, n0 = !1, r0 = !1;
    function KE(e) {
      _l === null ? _l = [e] : _l.push(e);
    }
    function Pk(e) {
      n0 = !0, KE(e);
    }
    function ZE() {
      n0 && $u();
    }
    function $u() {
      if (!r0 && _l !== null) {
        r0 = !0;
        var e = 0, t = xa();
        try {
          var a = !0, i = _l;
          for (yn(Tn); e < i.length; e++) {
            var u = i[e];
            do
              u = u(a);
            while (u !== null);
          }
          _l = null, n0 = !1;
        } catch (d) {
          throw _l !== null && (_l = _l.slice(e + 1)), Vc(Bc, $u), d;
        } finally {
          yn(t), r0 = !1;
        }
      }
      return null;
    }
    var Wf = [], Qf = 0, Cm = null, wm = 0, hi = [], mi = 0, ec = null, Dl = 1, Ol = "";
    function Fk(e) {
      return nc(), (e.flags & Xd) !== Ye;
    }
    function Hk(e) {
      return nc(), wm;
    }
    function jk() {
      var e = Ol, t = Dl, a = t & ~Vk(t);
      return a.toString(32) + e;
    }
    function tc(e, t) {
      nc(), Wf[Qf++] = wm, Wf[Qf++] = Cm, Cm = e, wm = t;
    }
    function JE(e, t, a) {
      nc(), hi[mi++] = Dl, hi[mi++] = Ol, hi[mi++] = ec, ec = e;
      var i = Dl, u = Ol, d = Tm(i) - 1, h = i & ~(1 << d), S = a + 1, b = Tm(t) + d;
      if (b > 30) {
        var k = d - d % 5, _ = (1 << k) - 1, P = (h & _).toString(32), z = h >> k, q = d - k, Z = Tm(t) + q, ee = S << q, we = ee | z, Qe = P + u;
        Dl = 1 << Z | we, Ol = Qe;
      } else {
        var Ve = S << d, _t = Ve | h, St = u;
        Dl = 1 << b | _t, Ol = St;
      }
    }
    function a0(e) {
      nc();
      var t = e.return;
      if (t !== null) {
        var a = 1, i = 0;
        tc(e, a), JE(e, a, i);
      }
    }
    function Tm(e) {
      return 32 - vu(e);
    }
    function Vk(e) {
      return 1 << Tm(e) - 1;
    }
    function i0(e) {
      for (; e === Cm; )
        Cm = Wf[--Qf], Wf[Qf] = null, wm = Wf[--Qf], Wf[Qf] = null;
      for (; e === ec; )
        ec = hi[--mi], hi[mi] = null, Ol = hi[--mi], hi[mi] = null, Dl = hi[--mi], hi[mi] = null;
    }
    function $k() {
      return nc(), ec !== null ? {
        id: Dl,
        overflow: Ol
      } : null;
    }
    function Bk(e, t) {
      nc(), hi[mi++] = Dl, hi[mi++] = Ol, hi[mi++] = ec, Dl = t.id, Ol = t.overflow, ec = e;
    }
    function nc() {
      yr() || m("Expected to be hydrating. This is a bug in React. Please file an issue.");
    }
    var mr = null, yi = null, Qi = !1, rc = !1, Bu = null;
    function Ik() {
      Qi && m("We should not be hydrating here. This is a bug in React. Please file a bug.");
    }
    function eb() {
      rc = !0;
    }
    function Yk() {
      return rc;
    }
    function Wk(e) {
      var t = e.stateNode.containerInfo;
      return yi = ck(t), mr = e, Qi = !0, Bu = null, rc = !1, !0;
    }
    function Qk(e, t, a) {
      return yi = fk(t), mr = e, Qi = !0, Bu = null, rc = !1, a !== null && Bk(e, a), !0;
    }
    function tb(e, t) {
      switch (e.tag) {
        case L: {
          bk(e.stateNode.containerInfo, t);
          break;
        }
        case F: {
          var a = (e.mode & je) !== Fe;
          wk(
            e.type,
            e.memoizedProps,
            e.stateNode,
            t,
            // TODO: Delete this argument when we remove the legacy root API.
            a
          );
          break;
        }
        case ne: {
          var i = e.memoizedState;
          i.dehydrated !== null && Ck(i.dehydrated, t);
          break;
        }
      }
    }
    function nb(e, t) {
      tb(e, t);
      var a = KD();
      a.stateNode = t, a.return = e;
      var i = e.deletions;
      i === null ? (e.deletions = [a], e.flags |= Pt) : i.push(a);
    }
    function o0(e, t) {
      {
        if (rc)
          return;
        switch (e.tag) {
          case L: {
            var a = e.stateNode.containerInfo;
            switch (t.tag) {
              case F:
                var i = t.type;
                t.pendingProps, Tk(a, i);
                break;
              case $:
                var u = t.pendingProps;
                Rk(a, u);
                break;
            }
            break;
          }
          case F: {
            var d = e.type, h = e.memoizedProps, S = e.stateNode;
            switch (t.tag) {
              case F: {
                var b = t.type, k = t.pendingProps, _ = (e.mode & je) !== Fe;
                _k(
                  d,
                  h,
                  S,
                  b,
                  k,
                  // TODO: Delete this argument when we remove the legacy root API.
                  _
                );
                break;
              }
              case $: {
                var P = t.pendingProps, z = (e.mode & je) !== Fe;
                Dk(
                  d,
                  h,
                  S,
                  P,
                  // TODO: Delete this argument when we remove the legacy root API.
                  z
                );
                break;
              }
            }
            break;
          }
          case ne: {
            var q = e.memoizedState, Z = q.dehydrated;
            if (Z !== null) switch (t.tag) {
              case F:
                var ee = t.type;
                t.pendingProps, xk(Z, ee);
                break;
              case $:
                var we = t.pendingProps;
                kk(Z, we);
                break;
            }
            break;
          }
          default:
            return;
        }
      }
    }
    function rb(e, t) {
      t.flags = t.flags & ~Ea | Jt, o0(e, t);
    }
    function ab(e, t) {
      switch (e.tag) {
        case F: {
          var a = e.type;
          e.pendingProps;
          var i = ak(t, a);
          return i !== null ? (e.stateNode = i, mr = e, yi = sk(i), !0) : !1;
        }
        case $: {
          var u = e.pendingProps, d = ik(t, u);
          return d !== null ? (e.stateNode = d, mr = e, yi = null, !0) : !1;
        }
        case ne: {
          var h = ok(t);
          if (h !== null) {
            var S = {
              dehydrated: h,
              treeContext: $k(),
              retryLane: sr
            };
            e.memoizedState = S;
            var b = ZD(h);
            return b.return = e, e.child = b, mr = e, yi = null, !0;
          }
          return !1;
        }
        default:
          return !1;
      }
    }
    function l0(e) {
      return (e.mode & je) !== Fe && (e.flags & tt) === Ye;
    }
    function u0(e) {
      throw new Error("Hydration failed because the initial UI does not match what was rendered on the server.");
    }
    function s0(e) {
      if (Qi) {
        var t = yi;
        if (!t) {
          l0(e) && (o0(mr, e), u0()), rb(mr, e), Qi = !1, mr = e;
          return;
        }
        var a = t;
        if (!ab(e, t)) {
          l0(e) && (o0(mr, e), u0()), t = jp(a);
          var i = mr;
          if (!t || !ab(e, t)) {
            rb(mr, e), Qi = !1, mr = e;
            return;
          }
          nb(i, a);
        }
      }
    }
    function Gk(e, t, a) {
      var i = e.stateNode, u = !rc, d = dk(i, e.type, e.memoizedProps, t, a, e, u);
      return e.updateQueue = d, d !== null;
    }
    function qk(e) {
      var t = e.stateNode, a = e.memoizedProps, i = pk(t, a, e);
      if (i) {
        var u = mr;
        if (u !== null)
          switch (u.tag) {
            case L: {
              var d = u.stateNode.containerInfo, h = (u.mode & je) !== Fe;
              Sk(
                d,
                t,
                a,
                // TODO: Delete this argument when we remove the legacy root API.
                h
              );
              break;
            }
            case F: {
              var S = u.type, b = u.memoizedProps, k = u.stateNode, _ = (u.mode & je) !== Fe;
              Ek(
                S,
                b,
                k,
                t,
                a,
                // TODO: Delete this argument when we remove the legacy root API.
                _
              );
              break;
            }
          }
      }
      return i;
    }
    function Xk(e) {
      var t = e.memoizedState, a = t !== null ? t.dehydrated : null;
      if (!a)
        throw new Error("Expected to have a hydrated suspense instance. This error is likely caused by a bug in React. Please file an issue.");
      vk(a, e);
    }
    function Kk(e) {
      var t = e.memoizedState, a = t !== null ? t.dehydrated : null;
      if (!a)
        throw new Error("Expected to have a hydrated suspense instance. This error is likely caused by a bug in React. Please file an issue.");
      return hk(a);
    }
    function ib(e) {
      for (var t = e.return; t !== null && t.tag !== F && t.tag !== L && t.tag !== ne; )
        t = t.return;
      mr = t;
    }
    function Rm(e) {
      if (e !== mr)
        return !1;
      if (!Qi)
        return ib(e), Qi = !0, !1;
      if (e.tag !== L && (e.tag !== F || gk(e.type) && !Ig(e.type, e.memoizedProps))) {
        var t = yi;
        if (t)
          if (l0(e))
            ob(e), u0();
          else
            for (; t; )
              nb(e, t), t = jp(t);
      }
      return ib(e), e.tag === ne ? yi = Kk(e) : yi = mr ? jp(e.stateNode) : null, !0;
    }
    function Zk() {
      return Qi && yi !== null;
    }
    function ob(e) {
      for (var t = yi; t; )
        tb(e, t), t = jp(t);
    }
    function Gf() {
      mr = null, yi = null, Qi = !1, rc = !1;
    }
    function lb() {
      Bu !== null && (ew(Bu), Bu = null);
    }
    function yr() {
      return Qi;
    }
    function c0(e) {
      Bu === null ? Bu = [e] : Bu.push(e);
    }
    var Jk = c.ReactCurrentBatchConfig, e5 = null;
    function t5() {
      return Jk.transition;
    }
    var Gi = {
      recordUnsafeLifecycleWarnings: function(e, t) {
      },
      flushPendingUnsafeLifecycleWarnings: function() {
      },
      recordLegacyContextWarning: function(e, t) {
      },
      flushLegacyContextWarning: function() {
      },
      discardPendingWarnings: function() {
      }
    };
    {
      var n5 = function(e) {
        for (var t = null, a = e; a !== null; )
          a.mode & vt && (t = a), a = a.return;
        return t;
      }, ac = function(e) {
        var t = [];
        return e.forEach(function(a) {
          t.push(a);
        }), t.sort().join(", ");
      }, Ip = [], Yp = [], Wp = [], Qp = [], Gp = [], qp = [], ic = /* @__PURE__ */ new Set();
      Gi.recordUnsafeLifecycleWarnings = function(e, t) {
        ic.has(e.type) || (typeof t.componentWillMount == "function" && // Don't warn about react-lifecycles-compat polyfilled components.
        t.componentWillMount.__suppressDeprecationWarning !== !0 && Ip.push(e), e.mode & vt && typeof t.UNSAFE_componentWillMount == "function" && Yp.push(e), typeof t.componentWillReceiveProps == "function" && t.componentWillReceiveProps.__suppressDeprecationWarning !== !0 && Wp.push(e), e.mode & vt && typeof t.UNSAFE_componentWillReceiveProps == "function" && Qp.push(e), typeof t.componentWillUpdate == "function" && t.componentWillUpdate.__suppressDeprecationWarning !== !0 && Gp.push(e), e.mode & vt && typeof t.UNSAFE_componentWillUpdate == "function" && qp.push(e));
      }, Gi.flushPendingUnsafeLifecycleWarnings = function() {
        var e = /* @__PURE__ */ new Set();
        Ip.length > 0 && (Ip.forEach(function(z) {
          e.add(it(z) || "Component"), ic.add(z.type);
        }), Ip = []);
        var t = /* @__PURE__ */ new Set();
        Yp.length > 0 && (Yp.forEach(function(z) {
          t.add(it(z) || "Component"), ic.add(z.type);
        }), Yp = []);
        var a = /* @__PURE__ */ new Set();
        Wp.length > 0 && (Wp.forEach(function(z) {
          a.add(it(z) || "Component"), ic.add(z.type);
        }), Wp = []);
        var i = /* @__PURE__ */ new Set();
        Qp.length > 0 && (Qp.forEach(function(z) {
          i.add(it(z) || "Component"), ic.add(z.type);
        }), Qp = []);
        var u = /* @__PURE__ */ new Set();
        Gp.length > 0 && (Gp.forEach(function(z) {
          u.add(it(z) || "Component"), ic.add(z.type);
        }), Gp = []);
        var d = /* @__PURE__ */ new Set();
        if (qp.length > 0 && (qp.forEach(function(z) {
          d.add(it(z) || "Component"), ic.add(z.type);
        }), qp = []), t.size > 0) {
          var h = ac(t);
          m(`Using UNSAFE_componentWillMount in strict mode is not recommended and may indicate bugs in your code. See https://reactjs.org/link/unsafe-component-lifecycles for details.

* Move code with side effects to componentDidMount, and set initial state in the constructor.

Please update the following components: %s`, h);
        }
        if (i.size > 0) {
          var S = ac(i);
          m(`Using UNSAFE_componentWillReceiveProps in strict mode is not recommended and may indicate bugs in your code. See https://reactjs.org/link/unsafe-component-lifecycles for details.

* Move data fetching code or side effects to componentDidUpdate.
* If you're updating state whenever props change, refactor your code to use memoization techniques or move it to static getDerivedStateFromProps. Learn more at: https://reactjs.org/link/derived-state

Please update the following components: %s`, S);
        }
        if (d.size > 0) {
          var b = ac(d);
          m(`Using UNSAFE_componentWillUpdate in strict mode is not recommended and may indicate bugs in your code. See https://reactjs.org/link/unsafe-component-lifecycles for details.

* Move data fetching code or side effects to componentDidUpdate.

Please update the following components: %s`, b);
        }
        if (e.size > 0) {
          var k = ac(e);
          E(`componentWillMount has been renamed, and is not recommended for use. See https://reactjs.org/link/unsafe-component-lifecycles for details.

* Move code with side effects to componentDidMount, and set initial state in the constructor.
* Rename componentWillMount to UNSAFE_componentWillMount to suppress this warning in non-strict mode. In React 18.x, only the UNSAFE_ name will work. To rename all deprecated lifecycles to their new names, you can run \`npx react-codemod rename-unsafe-lifecycles\` in your project source folder.

Please update the following components: %s`, k);
        }
        if (a.size > 0) {
          var _ = ac(a);
          E(`componentWillReceiveProps has been renamed, and is not recommended for use. See https://reactjs.org/link/unsafe-component-lifecycles for details.

* Move data fetching code or side effects to componentDidUpdate.
* If you're updating state whenever props change, refactor your code to use memoization techniques or move it to static getDerivedStateFromProps. Learn more at: https://reactjs.org/link/derived-state
* Rename componentWillReceiveProps to UNSAFE_componentWillReceiveProps to suppress this warning in non-strict mode. In React 18.x, only the UNSAFE_ name will work. To rename all deprecated lifecycles to their new names, you can run \`npx react-codemod rename-unsafe-lifecycles\` in your project source folder.

Please update the following components: %s`, _);
        }
        if (u.size > 0) {
          var P = ac(u);
          E(`componentWillUpdate has been renamed, and is not recommended for use. See https://reactjs.org/link/unsafe-component-lifecycles for details.

* Move data fetching code or side effects to componentDidUpdate.
* Rename componentWillUpdate to UNSAFE_componentWillUpdate to suppress this warning in non-strict mode. In React 18.x, only the UNSAFE_ name will work. To rename all deprecated lifecycles to their new names, you can run \`npx react-codemod rename-unsafe-lifecycles\` in your project source folder.

Please update the following components: %s`, P);
        }
      };
      var xm = /* @__PURE__ */ new Map(), ub = /* @__PURE__ */ new Set();
      Gi.recordLegacyContextWarning = function(e, t) {
        var a = n5(e);
        if (a === null) {
          m("Expected to find a StrictMode component in a strict mode tree. This error is likely caused by a bug in React. Please file an issue.");
          return;
        }
        if (!ub.has(e.type)) {
          var i = xm.get(a);
          (e.type.contextTypes != null || e.type.childContextTypes != null || t !== null && typeof t.getChildContext == "function") && (i === void 0 && (i = [], xm.set(a, i)), i.push(e));
        }
      }, Gi.flushLegacyContextWarning = function() {
        xm.forEach(function(e, t) {
          if (e.length !== 0) {
            var a = e[0], i = /* @__PURE__ */ new Set();
            e.forEach(function(d) {
              i.add(it(d) || "Component"), ub.add(d.type);
            });
            var u = ac(i);
            try {
              Ft(a), m(`Legacy context API has been detected within a strict-mode tree.

The old API will be supported in all 16.x releases, but applications using it should migrate to the new version.

Please update the following components: %s

Learn more about this warning here: https://reactjs.org/link/legacy-context`, u);
            } finally {
              hn();
            }
          }
        });
      }, Gi.discardPendingWarnings = function() {
        Ip = [], Yp = [], Wp = [], Qp = [], Gp = [], qp = [], xm = /* @__PURE__ */ new Map();
      };
    }
    var f0, d0, p0, v0, h0, sb = function(e, t) {
    };
    f0 = !1, d0 = !1, p0 = {}, v0 = {}, h0 = {}, sb = function(e, t) {
      if (!(e === null || typeof e != "object") && !(!e._store || e._store.validated || e.key != null)) {
        if (typeof e._store != "object")
          throw new Error("React Component in warnForMissingKey should have a _store. This error is likely caused by a bug in React. Please file an issue.");
        e._store.validated = !0;
        var a = it(t) || "Component";
        v0[a] || (v0[a] = !0, m('Each child in a list should have a unique "key" prop. See https://reactjs.org/link/warning-keys for more information.'));
      }
    };
    function r5(e) {
      return e.prototype && e.prototype.isReactComponent;
    }
    function Xp(e, t, a) {
      var i = a.ref;
      if (i !== null && typeof i != "function" && typeof i != "object") {
        if ((e.mode & vt || at) && // We warn in ReactElement.js if owner and self are equal for string refs
        // because these cannot be automatically converted to an arrow function
        // using a codemod. Therefore, we don't have to warn about string refs again.
        !(a._owner && a._self && a._owner.stateNode !== a._self) && // Will already throw with "Function components cannot have string refs"
        !(a._owner && a._owner.tag !== D) && // Will already warn with "Function components cannot be given refs"
        !(typeof a.type == "function" && !r5(a.type)) && // Will already throw with "Element ref was specified as a string (someStringRef) but no owner was set"
        a._owner) {
          var u = it(e) || "Component";
          p0[u] || (m('Component "%s" contains the string ref "%s". Support for string refs will be removed in a future major release. We recommend using useRef() or createRef() instead. Learn more about using refs safely here: https://reactjs.org/link/strict-mode-string-ref', u, i), p0[u] = !0);
        }
        if (a._owner) {
          var d = a._owner, h;
          if (d) {
            var S = d;
            if (S.tag !== D)
              throw new Error("Function components cannot have string refs. We recommend using useRef() instead. Learn more about using refs safely here: https://reactjs.org/link/strict-mode-string-ref");
            h = S.stateNode;
          }
          if (!h)
            throw new Error("Missing owner for string ref " + i + ". This error is likely caused by a bug in React. Please file an issue.");
          var b = h;
          or(i, "ref");
          var k = "" + i;
          if (t !== null && t.ref !== null && typeof t.ref == "function" && t.ref._stringRef === k)
            return t.ref;
          var _ = function(P) {
            var z = b.refs;
            P === null ? delete z[k] : z[k] = P;
          };
          return _._stringRef = k, _;
        } else {
          if (typeof i != "string")
            throw new Error("Expected ref to be a function, a string, an object returned by React.createRef(), or null.");
          if (!a._owner)
            throw new Error("Element ref was specified as a string (" + i + `) but no owner was set. This could happen for one of the following reasons:
1. You may be adding a ref to a function component
2. You may be adding a ref to a component that was not created inside a component's render method
3. You have multiple copies of React loaded
See https://reactjs.org/link/refs-must-have-owner for more information.`);
        }
      }
      return i;
    }
    function km(e, t) {
      var a = Object.prototype.toString.call(t);
      throw new Error("Objects are not valid as a React child (found: " + (a === "[object Object]" ? "object with keys {" + Object.keys(t).join(", ") + "}" : a) + "). If you meant to render a collection of children, use an array instead.");
    }
    function _m(e) {
      {
        var t = it(e) || "Component";
        if (h0[t])
          return;
        h0[t] = !0, m("Functions are not valid as a React child. This may happen if you return a Component instead of <Component /> from render. Or maybe you meant to call this function rather than return it.");
      }
    }
    function cb(e) {
      var t = e._payload, a = e._init;
      return a(t);
    }
    function fb(e) {
      function t(I, te) {
        if (e) {
          var Y = I.deletions;
          Y === null ? (I.deletions = [te], I.flags |= Pt) : Y.push(te);
        }
      }
      function a(I, te) {
        if (!e)
          return null;
        for (var Y = te; Y !== null; )
          t(I, Y), Y = Y.sibling;
        return null;
      }
      function i(I, te) {
        for (var Y = /* @__PURE__ */ new Map(), ce = te; ce !== null; )
          ce.key !== null ? Y.set(ce.key, ce) : Y.set(ce.index, ce), ce = ce.sibling;
        return Y;
      }
      function u(I, te) {
        var Y = vc(I, te);
        return Y.index = 0, Y.sibling = null, Y;
      }
      function d(I, te, Y) {
        if (I.index = Y, !e)
          return I.flags |= Xd, te;
        var ce = I.alternate;
        if (ce !== null) {
          var _e = ce.index;
          return _e < te ? (I.flags |= Jt, te) : _e;
        } else
          return I.flags |= Jt, te;
      }
      function h(I) {
        return e && I.alternate === null && (I.flags |= Jt), I;
      }
      function S(I, te, Y, ce) {
        if (te === null || te.tag !== $) {
          var _e = c1(Y, I.mode, ce);
          return _e.return = I, _e;
        } else {
          var Re = u(te, Y);
          return Re.return = I, Re;
        }
      }
      function b(I, te, Y, ce) {
        var _e = Y.type;
        if (_e === Ba)
          return _(I, te, Y.props.children, ce, Y.key);
        if (te !== null && (te.elementType === _e || // Keep this check inline so it only runs on the false path:
        mw(te, Y) || // Lazy types should reconcile their resolved type.
        // We need to do this after the Hot Reloading check above,
        // because hot reloading has different semantics than prod because
        // it doesn't resuspend. So we can't let the call below suspend.
        typeof _e == "object" && _e !== null && _e.$$typeof === Qn && cb(_e) === te.type)) {
          var Re = u(te, Y.props);
          return Re.ref = Xp(I, te, Y), Re.return = I, Re._debugSource = Y._source, Re._debugOwner = Y._owner, Re;
        }
        var et = s1(Y, I.mode, ce);
        return et.ref = Xp(I, te, Y), et.return = I, et;
      }
      function k(I, te, Y, ce) {
        if (te === null || te.tag !== j || te.stateNode.containerInfo !== Y.containerInfo || te.stateNode.implementation !== Y.implementation) {
          var _e = f1(Y, I.mode, ce);
          return _e.return = I, _e;
        } else {
          var Re = u(te, Y.children || []);
          return Re.return = I, Re;
        }
      }
      function _(I, te, Y, ce, _e) {
        if (te === null || te.tag !== H) {
          var Re = es(Y, I.mode, ce, _e);
          return Re.return = I, Re;
        } else {
          var et = u(te, Y);
          return et.return = I, et;
        }
      }
      function P(I, te, Y) {
        if (typeof te == "string" && te !== "" || typeof te == "number") {
          var ce = c1("" + te, I.mode, Y);
          return ce.return = I, ce;
        }
        if (typeof te == "object" && te !== null) {
          switch (te.$$typeof) {
            case oo: {
              var _e = s1(te, I.mode, Y);
              return _e.ref = Xp(I, null, te), _e.return = I, _e;
            }
            case ta: {
              var Re = f1(te, I.mode, Y);
              return Re.return = I, Re;
            }
            case Qn: {
              var et = te._payload, lt = te._init;
              return P(I, lt(et), Y);
            }
          }
          if (wt(te) || Ia(te)) {
            var Yt = es(te, I.mode, Y, null);
            return Yt.return = I, Yt;
          }
          km(I, te);
        }
        return typeof te == "function" && _m(I), null;
      }
      function z(I, te, Y, ce) {
        var _e = te !== null ? te.key : null;
        if (typeof Y == "string" && Y !== "" || typeof Y == "number")
          return _e !== null ? null : S(I, te, "" + Y, ce);
        if (typeof Y == "object" && Y !== null) {
          switch (Y.$$typeof) {
            case oo:
              return Y.key === _e ? b(I, te, Y, ce) : null;
            case ta:
              return Y.key === _e ? k(I, te, Y, ce) : null;
            case Qn: {
              var Re = Y._payload, et = Y._init;
              return z(I, te, et(Re), ce);
            }
          }
          if (wt(Y) || Ia(Y))
            return _e !== null ? null : _(I, te, Y, ce, null);
          km(I, Y);
        }
        return typeof Y == "function" && _m(I), null;
      }
      function q(I, te, Y, ce, _e) {
        if (typeof ce == "string" && ce !== "" || typeof ce == "number") {
          var Re = I.get(Y) || null;
          return S(te, Re, "" + ce, _e);
        }
        if (typeof ce == "object" && ce !== null) {
          switch (ce.$$typeof) {
            case oo: {
              var et = I.get(ce.key === null ? Y : ce.key) || null;
              return b(te, et, ce, _e);
            }
            case ta: {
              var lt = I.get(ce.key === null ? Y : ce.key) || null;
              return k(te, lt, ce, _e);
            }
            case Qn:
              var Yt = ce._payload, zt = ce._init;
              return q(I, te, Y, zt(Yt), _e);
          }
          if (wt(ce) || Ia(ce)) {
            var Pn = I.get(Y) || null;
            return _(te, Pn, ce, _e, null);
          }
          km(te, ce);
        }
        return typeof ce == "function" && _m(te), null;
      }
      function Z(I, te, Y) {
        {
          if (typeof I != "object" || I === null)
            return te;
          switch (I.$$typeof) {
            case oo:
            case ta:
              sb(I, Y);
              var ce = I.key;
              if (typeof ce != "string")
                break;
              if (te === null) {
                te = /* @__PURE__ */ new Set(), te.add(ce);
                break;
              }
              if (!te.has(ce)) {
                te.add(ce);
                break;
              }
              m("Encountered two children with the same key, `%s`. Keys should be unique so that components maintain their identity across updates. Non-unique keys may cause children to be duplicated and/or omitted — the behavior is unsupported and could change in a future version.", ce);
              break;
            case Qn:
              var _e = I._payload, Re = I._init;
              Z(Re(_e), te, Y);
              break;
          }
        }
        return te;
      }
      function ee(I, te, Y, ce) {
        for (var _e = null, Re = 0; Re < Y.length; Re++) {
          var et = Y[Re];
          _e = Z(et, _e, I);
        }
        for (var lt = null, Yt = null, zt = te, Pn = 0, Ut = 0, On = null; zt !== null && Ut < Y.length; Ut++) {
          zt.index > Ut ? (On = zt, zt = null) : On = zt.sibling;
          var Xr = z(I, zt, Y[Ut], ce);
          if (Xr === null) {
            zt === null && (zt = On);
            break;
          }
          e && zt && Xr.alternate === null && t(I, zt), Pn = d(Xr, Pn, Ut), Yt === null ? lt = Xr : Yt.sibling = Xr, Yt = Xr, zt = On;
        }
        if (Ut === Y.length) {
          if (a(I, zt), yr()) {
            var Tr = Ut;
            tc(I, Tr);
          }
          return lt;
        }
        if (zt === null) {
          for (; Ut < Y.length; Ut++) {
            var ei = P(I, Y[Ut], ce);
            ei !== null && (Pn = d(ei, Pn, Ut), Yt === null ? lt = ei : Yt.sibling = ei, Yt = ei);
          }
          if (yr()) {
            var fa = Ut;
            tc(I, fa);
          }
          return lt;
        }
        for (var da = i(I, zt); Ut < Y.length; Ut++) {
          var Kr = q(da, I, Ut, Y[Ut], ce);
          Kr !== null && (e && Kr.alternate !== null && da.delete(Kr.key === null ? Ut : Kr.key), Pn = d(Kr, Pn, Ut), Yt === null ? lt = Kr : Yt.sibling = Kr, Yt = Kr);
        }
        if (e && da.forEach(function(pd) {
          return t(I, pd);
        }), yr()) {
          var Pl = Ut;
          tc(I, Pl);
        }
        return lt;
      }
      function we(I, te, Y, ce) {
        var _e = Ia(Y);
        if (typeof _e != "function")
          throw new Error("An object is not an iterable. This error is likely caused by a bug in React. Please file an issue.");
        {
          typeof Symbol == "function" && // $FlowFixMe Flow doesn't know about toStringTag
          Y[Symbol.toStringTag] === "Generator" && (d0 || m("Using Generators as children is unsupported and will likely yield unexpected results because enumerating a generator mutates it. You may convert it to an array with `Array.from()` or the `[...spread]` operator before rendering. Keep in mind you might need to polyfill these features for older browsers."), d0 = !0), Y.entries === _e && (f0 || m("Using Maps as children is not supported. Use an array of keyed ReactElements instead."), f0 = !0);
          var Re = _e.call(Y);
          if (Re)
            for (var et = null, lt = Re.next(); !lt.done; lt = Re.next()) {
              var Yt = lt.value;
              et = Z(Yt, et, I);
            }
        }
        var zt = _e.call(Y);
        if (zt == null)
          throw new Error("An iterable object provided no iterator.");
        for (var Pn = null, Ut = null, On = te, Xr = 0, Tr = 0, ei = null, fa = zt.next(); On !== null && !fa.done; Tr++, fa = zt.next()) {
          On.index > Tr ? (ei = On, On = null) : ei = On.sibling;
          var da = z(I, On, fa.value, ce);
          if (da === null) {
            On === null && (On = ei);
            break;
          }
          e && On && da.alternate === null && t(I, On), Xr = d(da, Xr, Tr), Ut === null ? Pn = da : Ut.sibling = da, Ut = da, On = ei;
        }
        if (fa.done) {
          if (a(I, On), yr()) {
            var Kr = Tr;
            tc(I, Kr);
          }
          return Pn;
        }
        if (On === null) {
          for (; !fa.done; Tr++, fa = zt.next()) {
            var Pl = P(I, fa.value, ce);
            Pl !== null && (Xr = d(Pl, Xr, Tr), Ut === null ? Pn = Pl : Ut.sibling = Pl, Ut = Pl);
          }
          if (yr()) {
            var pd = Tr;
            tc(I, pd);
          }
          return Pn;
        }
        for (var _v = i(I, On); !fa.done; Tr++, fa = zt.next()) {
          var Wo = q(_v, I, Tr, fa.value, ce);
          Wo !== null && (e && Wo.alternate !== null && _v.delete(Wo.key === null ? Tr : Wo.key), Xr = d(Wo, Xr, Tr), Ut === null ? Pn = Wo : Ut.sibling = Wo, Ut = Wo);
        }
        if (e && _v.forEach(function(D2) {
          return t(I, D2);
        }), yr()) {
          var _2 = Tr;
          tc(I, _2);
        }
        return Pn;
      }
      function Qe(I, te, Y, ce) {
        if (te !== null && te.tag === $) {
          a(I, te.sibling);
          var _e = u(te, Y);
          return _e.return = I, _e;
        }
        a(I, te);
        var Re = c1(Y, I.mode, ce);
        return Re.return = I, Re;
      }
      function Ve(I, te, Y, ce) {
        for (var _e = Y.key, Re = te; Re !== null; ) {
          if (Re.key === _e) {
            var et = Y.type;
            if (et === Ba) {
              if (Re.tag === H) {
                a(I, Re.sibling);
                var lt = u(Re, Y.props.children);
                return lt.return = I, lt._debugSource = Y._source, lt._debugOwner = Y._owner, lt;
              }
            } else if (Re.elementType === et || // Keep this check inline so it only runs on the false path:
            mw(Re, Y) || // Lazy types should reconcile their resolved type.
            // We need to do this after the Hot Reloading check above,
            // because hot reloading has different semantics than prod because
            // it doesn't resuspend. So we can't let the call below suspend.
            typeof et == "object" && et !== null && et.$$typeof === Qn && cb(et) === Re.type) {
              a(I, Re.sibling);
              var Yt = u(Re, Y.props);
              return Yt.ref = Xp(I, Re, Y), Yt.return = I, Yt._debugSource = Y._source, Yt._debugOwner = Y._owner, Yt;
            }
            a(I, Re);
            break;
          } else
            t(I, Re);
          Re = Re.sibling;
        }
        if (Y.type === Ba) {
          var zt = es(Y.props.children, I.mode, ce, Y.key);
          return zt.return = I, zt;
        } else {
          var Pn = s1(Y, I.mode, ce);
          return Pn.ref = Xp(I, te, Y), Pn.return = I, Pn;
        }
      }
      function _t(I, te, Y, ce) {
        for (var _e = Y.key, Re = te; Re !== null; ) {
          if (Re.key === _e)
            if (Re.tag === j && Re.stateNode.containerInfo === Y.containerInfo && Re.stateNode.implementation === Y.implementation) {
              a(I, Re.sibling);
              var et = u(Re, Y.children || []);
              return et.return = I, et;
            } else {
              a(I, Re);
              break;
            }
          else
            t(I, Re);
          Re = Re.sibling;
        }
        var lt = f1(Y, I.mode, ce);
        return lt.return = I, lt;
      }
      function St(I, te, Y, ce) {
        var _e = typeof Y == "object" && Y !== null && Y.type === Ba && Y.key === null;
        if (_e && (Y = Y.props.children), typeof Y == "object" && Y !== null) {
          switch (Y.$$typeof) {
            case oo:
              return h(Ve(I, te, Y, ce));
            case ta:
              return h(_t(I, te, Y, ce));
            case Qn:
              var Re = Y._payload, et = Y._init;
              return St(I, te, et(Re), ce);
          }
          if (wt(Y))
            return ee(I, te, Y, ce);
          if (Ia(Y))
            return we(I, te, Y, ce);
          km(I, Y);
        }
        return typeof Y == "string" && Y !== "" || typeof Y == "number" ? h(Qe(I, te, "" + Y, ce)) : (typeof Y == "function" && _m(I), a(I, te));
      }
      return St;
    }
    var qf = fb(!0), db = fb(!1);
    function a5(e, t) {
      if (e !== null && t.child !== e.child)
        throw new Error("Resuming work not yet implemented.");
      if (t.child !== null) {
        var a = t.child, i = vc(a, a.pendingProps);
        for (t.child = i, i.return = t; a.sibling !== null; )
          a = a.sibling, i = i.sibling = vc(a, a.pendingProps), i.return = t;
        i.sibling = null;
      }
    }
    function i5(e, t) {
      for (var a = e.child; a !== null; )
        WD(a, t), a = a.sibling;
    }
    var m0 = ju(null), y0;
    y0 = {};
    var Dm = null, Xf = null, g0 = null, Om = !1;
    function Mm() {
      Dm = null, Xf = null, g0 = null, Om = !1;
    }
    function pb() {
      Om = !0;
    }
    function vb() {
      Om = !1;
    }
    function hb(e, t, a) {
      Gr(m0, t._currentValue, e), t._currentValue = a, t._currentRenderer !== void 0 && t._currentRenderer !== null && t._currentRenderer !== y0 && m("Detected multiple renderers concurrently rendering the same context provider. This is currently unsupported."), t._currentRenderer = y0;
    }
    function S0(e, t) {
      var a = m0.current;
      Qr(m0, t), e._currentValue = a;
    }
    function E0(e, t, a) {
      for (var i = e; i !== null; ) {
        var u = i.alternate;
        if (gl(i.childLanes, t) ? u !== null && !gl(u.childLanes, t) && (u.childLanes = ut(u.childLanes, t)) : (i.childLanes = ut(i.childLanes, t), u !== null && (u.childLanes = ut(u.childLanes, t))), i === a)
          break;
        i = i.return;
      }
      i !== a && m("Expected to find the propagation root when scheduling context work. This error is likely caused by a bug in React. Please file an issue.");
    }
    function o5(e, t, a) {
      l5(e, t, a);
    }
    function l5(e, t, a) {
      var i = e.child;
      for (i !== null && (i.return = e); i !== null; ) {
        var u = void 0, d = i.dependencies;
        if (d !== null) {
          u = i.child;
          for (var h = d.firstContext; h !== null; ) {
            if (h.context === t) {
              if (i.tag === D) {
                var S = bu(a), b = Ml(Kt, S);
                b.tag = Nm;
                var k = i.updateQueue;
                if (k !== null) {
                  var _ = k.shared, P = _.pending;
                  P === null ? b.next = b : (b.next = P.next, P.next = b), _.pending = b;
                }
              }
              i.lanes = ut(i.lanes, a);
              var z = i.alternate;
              z !== null && (z.lanes = ut(z.lanes, a)), E0(i.return, a, e), d.lanes = ut(d.lanes, a);
              break;
            }
            h = h.next;
          }
        } else if (i.tag === ve)
          u = i.type === e.type ? null : i.child;
        else if (i.tag === Rt) {
          var q = i.return;
          if (q === null)
            throw new Error("We just came from a parent so we must have had a parent. This is a bug in React.");
          q.lanes = ut(q.lanes, a);
          var Z = q.alternate;
          Z !== null && (Z.lanes = ut(Z.lanes, a)), E0(q, a, e), u = i.sibling;
        } else
          u = i.child;
        if (u !== null)
          u.return = i;
        else
          for (u = i; u !== null; ) {
            if (u === e) {
              u = null;
              break;
            }
            var ee = u.sibling;
            if (ee !== null) {
              ee.return = u.return, u = ee;
              break;
            }
            u = u.return;
          }
        i = u;
      }
    }
    function Kf(e, t) {
      Dm = e, Xf = null, g0 = null;
      var a = e.dependencies;
      if (a !== null) {
        var i = a.firstContext;
        i !== null && (Yr(a.lanes, t) && fv(), a.firstContext = null);
      }
    }
    function Yn(e) {
      Om && m("Context can only be read while React is rendering. In classes, you can read it in the render method or getDerivedStateFromProps. In function components, you can read it directly in the function body, but not inside Hooks like useReducer() or useMemo().");
      var t = e._currentValue;
      if (g0 !== e) {
        var a = {
          context: e,
          memoizedValue: t,
          next: null
        };
        if (Xf === null) {
          if (Dm === null)
            throw new Error("Context can only be read while React is rendering. In classes, you can read it in the render method or getDerivedStateFromProps. In function components, you can read it directly in the function body, but not inside Hooks like useReducer() or useMemo().");
          Xf = a, Dm.dependencies = {
            lanes: ie,
            firstContext: a
          };
        } else
          Xf = Xf.next = a;
      }
      return t;
    }
    var oc = null;
    function b0(e) {
      oc === null ? oc = [e] : oc.push(e);
    }
    function u5() {
      if (oc !== null) {
        for (var e = 0; e < oc.length; e++) {
          var t = oc[e], a = t.interleaved;
          if (a !== null) {
            t.interleaved = null;
            var i = a.next, u = t.pending;
            if (u !== null) {
              var d = u.next;
              u.next = i, a.next = d;
            }
            t.pending = a;
          }
        }
        oc = null;
      }
    }
    function mb(e, t, a, i) {
      var u = t.interleaved;
      return u === null ? (a.next = a, b0(t)) : (a.next = u.next, u.next = a), t.interleaved = a, Lm(e, i);
    }
    function s5(e, t, a, i) {
      var u = t.interleaved;
      u === null ? (a.next = a, b0(t)) : (a.next = u.next, u.next = a), t.interleaved = a;
    }
    function c5(e, t, a, i) {
      var u = t.interleaved;
      return u === null ? (a.next = a, b0(t)) : (a.next = u.next, u.next = a), t.interleaved = a, Lm(e, i);
    }
    function La(e, t) {
      return Lm(e, t);
    }
    var f5 = Lm;
    function Lm(e, t) {
      e.lanes = ut(e.lanes, t);
      var a = e.alternate;
      a !== null && (a.lanes = ut(a.lanes, t)), a === null && (e.flags & (Jt | Ea)) !== Ye && dw(e);
      for (var i = e, u = e.return; u !== null; )
        u.childLanes = ut(u.childLanes, t), a = u.alternate, a !== null ? a.childLanes = ut(a.childLanes, t) : (u.flags & (Jt | Ea)) !== Ye && dw(e), i = u, u = u.return;
      if (i.tag === L) {
        var d = i.stateNode;
        return d;
      } else
        return null;
    }
    var yb = 0, gb = 1, Nm = 2, C0 = 3, Am = !1, w0, zm;
    w0 = !1, zm = null;
    function T0(e) {
      var t = {
        baseState: e.memoizedState,
        firstBaseUpdate: null,
        lastBaseUpdate: null,
        shared: {
          pending: null,
          interleaved: null,
          lanes: ie
        },
        effects: null
      };
      e.updateQueue = t;
    }
    function Sb(e, t) {
      var a = t.updateQueue, i = e.updateQueue;
      if (a === i) {
        var u = {
          baseState: i.baseState,
          firstBaseUpdate: i.firstBaseUpdate,
          lastBaseUpdate: i.lastBaseUpdate,
          shared: i.shared,
          effects: i.effects
        };
        t.updateQueue = u;
      }
    }
    function Ml(e, t) {
      var a = {
        eventTime: e,
        lane: t,
        tag: yb,
        payload: null,
        callback: null,
        next: null
      };
      return a;
    }
    function Iu(e, t, a) {
      var i = e.updateQueue;
      if (i === null)
        return null;
      var u = i.shared;
      if (zm === u && !w0 && (m("An update (setState, replaceState, or forceUpdate) was scheduled from inside an update function. Update functions should be pure, with zero side-effects. Consider using componentDidUpdate or a callback."), w0 = !0), sD()) {
        var d = u.pending;
        return d === null ? t.next = t : (t.next = d.next, d.next = t), u.pending = t, f5(e, a);
      } else
        return c5(e, u, t, a);
    }
    function Um(e, t, a) {
      var i = t.updateQueue;
      if (i !== null) {
        var u = i.shared;
        if (fp(a)) {
          var d = u.lanes;
          d = vf(d, e.pendingLanes);
          var h = ut(d, a);
          u.lanes = h, dp(e, h);
        }
      }
    }
    function R0(e, t) {
      var a = e.updateQueue, i = e.alternate;
      if (i !== null) {
        var u = i.updateQueue;
        if (a === u) {
          var d = null, h = null, S = a.firstBaseUpdate;
          if (S !== null) {
            var b = S;
            do {
              var k = {
                eventTime: b.eventTime,
                lane: b.lane,
                tag: b.tag,
                payload: b.payload,
                callback: b.callback,
                next: null
              };
              h === null ? d = h = k : (h.next = k, h = k), b = b.next;
            } while (b !== null);
            h === null ? d = h = t : (h.next = t, h = t);
          } else
            d = h = t;
          a = {
            baseState: u.baseState,
            firstBaseUpdate: d,
            lastBaseUpdate: h,
            shared: u.shared,
            effects: u.effects
          }, e.updateQueue = a;
          return;
        }
      }
      var _ = a.lastBaseUpdate;
      _ === null ? a.firstBaseUpdate = t : _.next = t, a.lastBaseUpdate = t;
    }
    function d5(e, t, a, i, u, d) {
      switch (a.tag) {
        case gb: {
          var h = a.payload;
          if (typeof h == "function") {
            pb();
            var S = h.call(d, i, u);
            {
              if (e.mode & vt) {
                _n(!0);
                try {
                  h.call(d, i, u);
                } finally {
                  _n(!1);
                }
              }
              vb();
            }
            return S;
          }
          return h;
        }
        case C0:
          e.flags = e.flags & ~Vn | tt;
        case yb: {
          var b = a.payload, k;
          if (typeof b == "function") {
            pb(), k = b.call(d, i, u);
            {
              if (e.mode & vt) {
                _n(!0);
                try {
                  b.call(d, i, u);
                } finally {
                  _n(!1);
                }
              }
              vb();
            }
          } else
            k = b;
          return k == null ? i : dt({}, i, k);
        }
        case Nm:
          return Am = !0, i;
      }
      return i;
    }
    function Pm(e, t, a, i) {
      var u = e.updateQueue;
      Am = !1, zm = u.shared;
      var d = u.firstBaseUpdate, h = u.lastBaseUpdate, S = u.shared.pending;
      if (S !== null) {
        u.shared.pending = null;
        var b = S, k = b.next;
        b.next = null, h === null ? d = k : h.next = k, h = b;
        var _ = e.alternate;
        if (_ !== null) {
          var P = _.updateQueue, z = P.lastBaseUpdate;
          z !== h && (z === null ? P.firstBaseUpdate = k : z.next = k, P.lastBaseUpdate = b);
        }
      }
      if (d !== null) {
        var q = u.baseState, Z = ie, ee = null, we = null, Qe = null, Ve = d;
        do {
          var _t = Ve.lane, St = Ve.eventTime;
          if (gl(i, _t)) {
            if (Qe !== null) {
              var te = {
                eventTime: St,
                // This update is going to be committed so we never want uncommit
                // it. Using NoLane works because 0 is a subset of all bitmasks, so
                // this will never be skipped by the check above.
                lane: Dn,
                tag: Ve.tag,
                payload: Ve.payload,
                callback: Ve.callback,
                next: null
              };
              Qe = Qe.next = te;
            }
            q = d5(e, u, Ve, q, t, a);
            var Y = Ve.callback;
            if (Y !== null && // If the update was already committed, we should not queue its
            // callback again.
            Ve.lane !== Dn) {
              e.flags |= si;
              var ce = u.effects;
              ce === null ? u.effects = [Ve] : ce.push(Ve);
            }
          } else {
            var I = {
              eventTime: St,
              lane: _t,
              tag: Ve.tag,
              payload: Ve.payload,
              callback: Ve.callback,
              next: null
            };
            Qe === null ? (we = Qe = I, ee = q) : Qe = Qe.next = I, Z = ut(Z, _t);
          }
          if (Ve = Ve.next, Ve === null) {
            if (S = u.shared.pending, S === null)
              break;
            var _e = S, Re = _e.next;
            _e.next = null, Ve = Re, u.lastBaseUpdate = _e, u.shared.pending = null;
          }
        } while (!0);
        Qe === null && (ee = q), u.baseState = ee, u.firstBaseUpdate = we, u.lastBaseUpdate = Qe;
        var et = u.shared.interleaved;
        if (et !== null) {
          var lt = et;
          do
            Z = ut(Z, lt.lane), lt = lt.next;
          while (lt !== et);
        } else d === null && (u.shared.lanes = ie);
        wv(Z), e.lanes = Z, e.memoizedState = q;
      }
      zm = null;
    }
    function p5(e, t) {
      if (typeof e != "function")
        throw new Error("Invalid argument passed as callback. Expected a function. Instead " + ("received: " + e));
      e.call(t);
    }
    function Eb() {
      Am = !1;
    }
    function Fm() {
      return Am;
    }
    function bb(e, t, a) {
      var i = t.effects;
      if (t.effects = null, i !== null)
        for (var u = 0; u < i.length; u++) {
          var d = i[u], h = d.callback;
          h !== null && (d.callback = null, p5(h, a));
        }
    }
    var Kp = {}, Yu = ju(Kp), Zp = ju(Kp), Hm = ju(Kp);
    function jm(e) {
      if (e === Kp)
        throw new Error("Expected host context to exist. This error is likely caused by a bug in React. Please file an issue.");
      return e;
    }
    function Cb() {
      var e = jm(Hm.current);
      return e;
    }
    function x0(e, t) {
      Gr(Hm, t, e), Gr(Zp, e, e), Gr(Yu, Kp, e);
      var a = Ox(t);
      Qr(Yu, e), Gr(Yu, a, e);
    }
    function Zf(e) {
      Qr(Yu, e), Qr(Zp, e), Qr(Hm, e);
    }
    function k0() {
      var e = jm(Yu.current);
      return e;
    }
    function wb(e) {
      jm(Hm.current);
      var t = jm(Yu.current), a = Mx(t, e.type);
      t !== a && (Gr(Zp, e, e), Gr(Yu, a, e));
    }
    function _0(e) {
      Zp.current === e && (Qr(Yu, e), Qr(Zp, e));
    }
    var v5 = 0, Tb = 1, Rb = 1, Jp = 2, qi = ju(v5);
    function D0(e, t) {
      return (e & t) !== 0;
    }
    function Jf(e) {
      return e & Tb;
    }
    function O0(e, t) {
      return e & Tb | t;
    }
    function h5(e, t) {
      return e | t;
    }
    function Wu(e, t) {
      Gr(qi, t, e);
    }
    function ed(e) {
      Qr(qi, e);
    }
    function m5(e, t) {
      var a = e.memoizedState;
      return a !== null ? a.dehydrated !== null : (e.memoizedProps, !0);
    }
    function Vm(e) {
      for (var t = e; t !== null; ) {
        if (t.tag === ne) {
          var a = t.memoizedState;
          if (a !== null) {
            var i = a.dehydrated;
            if (i === null || $E(i) || Gg(i))
              return t;
          }
        } else if (t.tag === ye && // revealOrder undefined can't be trusted because it don't
        // keep track of whether it suspended or not.
        t.memoizedProps.revealOrder !== void 0) {
          var u = (t.flags & tt) !== Ye;
          if (u)
            return t;
        } else if (t.child !== null) {
          t.child.return = t, t = t.child;
          continue;
        }
        if (t === e)
          return null;
        for (; t.sibling === null; ) {
          if (t.return === null || t.return === e)
            return null;
          t = t.return;
        }
        t.sibling.return = t.return, t = t.sibling;
      }
      return null;
    }
    var Na = (
      /*   */
      0
    ), Kn = (
      /* */
      1
    ), Ho = (
      /*  */
      2
    ), Zn = (
      /*    */
      4
    ), gr = (
      /*   */
      8
    ), M0 = [];
    function L0() {
      for (var e = 0; e < M0.length; e++) {
        var t = M0[e];
        t._workInProgressVersionPrimary = null;
      }
      M0.length = 0;
    }
    function y5(e, t) {
      var a = t._getVersion, i = a(t._source);
      e.mutableSourceEagerHydrationData == null ? e.mutableSourceEagerHydrationData = [t, i] : e.mutableSourceEagerHydrationData.push(t, i);
    }
    var ke = c.ReactCurrentDispatcher, ev = c.ReactCurrentBatchConfig, N0, td;
    N0 = /* @__PURE__ */ new Set();
    var lc = ie, It = null, Jn = null, er = null, $m = !1, tv = !1, nv = 0, g5 = 0, S5 = 25, re = null, gi = null, Qu = -1, A0 = !1;
    function jt() {
      {
        var e = re;
        gi === null ? gi = [e] : gi.push(e);
      }
    }
    function Se() {
      {
        var e = re;
        gi !== null && (Qu++, gi[Qu] !== e && E5(e));
      }
    }
    function nd(e) {
      e != null && !wt(e) && m("%s received a final argument that is not an array (instead, received `%s`). When specified, the final argument must be an array.", re, typeof e);
    }
    function E5(e) {
      {
        var t = it(It);
        if (!N0.has(t) && (N0.add(t), gi !== null)) {
          for (var a = "", i = 30, u = 0; u <= Qu; u++) {
            for (var d = gi[u], h = u === Qu ? e : d, S = u + 1 + ". " + d; S.length < i; )
              S += " ";
            S += h + `
`, a += S;
          }
          m(`React has detected a change in the order of Hooks called by %s. This will lead to bugs and errors if not fixed. For more information, read the Rules of Hooks: https://reactjs.org/link/rules-of-hooks

   Previous render            Next render
   ------------------------------------------------------
%s   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`, t, a);
        }
      }
    }
    function qr() {
      throw new Error(`Invalid hook call. Hooks can only be called inside of the body of a function component. This could happen for one of the following reasons:
1. You might have mismatching versions of React and the renderer (such as React DOM)
2. You might be breaking the Rules of Hooks
3. You might have more than one copy of React in the same app
See https://reactjs.org/link/invalid-hook-call for tips about how to debug and fix this problem.`);
    }
    function z0(e, t) {
      if (A0)
        return !1;
      if (t === null)
        return m("%s received a final argument during this render, but not during the previous render. Even though the final argument is optional, its type cannot change between renders.", re), !1;
      e.length !== t.length && m(`The final argument passed to %s changed size between renders. The order and size of this array must remain constant.

Previous: %s
Incoming: %s`, re, "[" + t.join(", ") + "]", "[" + e.join(", ") + "]");
      for (var a = 0; a < t.length && a < e.length; a++)
        if (!Ne(e[a], t[a]))
          return !1;
      return !0;
    }
    function rd(e, t, a, i, u, d) {
      lc = d, It = t, gi = e !== null ? e._debugHookTypes : null, Qu = -1, A0 = e !== null && e.type !== t.type, t.memoizedState = null, t.updateQueue = null, t.lanes = ie, e !== null && e.memoizedState !== null ? ke.current = Qb : gi !== null ? ke.current = Wb : ke.current = Yb;
      var h = a(i, u);
      if (tv) {
        var S = 0;
        do {
          if (tv = !1, nv = 0, S >= S5)
            throw new Error("Too many re-renders. React limits the number of renders to prevent an infinite loop.");
          S += 1, A0 = !1, Jn = null, er = null, t.updateQueue = null, Qu = -1, ke.current = Gb, h = a(i, u);
        } while (tv);
      }
      ke.current = ty, t._debugHookTypes = gi;
      var b = Jn !== null && Jn.next !== null;
      if (lc = ie, It = null, Jn = null, er = null, re = null, gi = null, Qu = -1, e !== null && (e.flags & Gn) !== (t.flags & Gn) && // Disable this warning in legacy mode, because legacy Suspense is weird
      // and creates false positives. To make this work in legacy mode, we'd
      // need to mark fibers that commit in an incomplete state, somehow. For
      // now I'll disable the warning that most of the bugs that would trigger
      // it are either exclusive to concurrent mode or exist in both.
      (e.mode & je) !== Fe && m("Internal React error: Expected static flag was missing. Please notify the React team."), $m = !1, b)
        throw new Error("Rendered fewer hooks than expected. This may be caused by an accidental early return statement.");
      return h;
    }
    function ad() {
      var e = nv !== 0;
      return nv = 0, e;
    }
    function xb(e, t, a) {
      t.updateQueue = e.updateQueue, (t.mode & oa) !== Fe ? t.flags &= ~(sl | Hr | an | st) : t.flags &= ~(an | st), e.lanes = Fs(e.lanes, a);
    }
    function kb() {
      if (ke.current = ty, $m) {
        for (var e = It.memoizedState; e !== null; ) {
          var t = e.queue;
          t !== null && (t.pending = null), e = e.next;
        }
        $m = !1;
      }
      lc = ie, It = null, Jn = null, er = null, gi = null, Qu = -1, re = null, jb = !1, tv = !1, nv = 0;
    }
    function jo() {
      var e = {
        memoizedState: null,
        baseState: null,
        baseQueue: null,
        queue: null,
        next: null
      };
      return er === null ? It.memoizedState = er = e : er = er.next = e, er;
    }
    function Si() {
      var e;
      if (Jn === null) {
        var t = It.alternate;
        t !== null ? e = t.memoizedState : e = null;
      } else
        e = Jn.next;
      var a;
      if (er === null ? a = It.memoizedState : a = er.next, a !== null)
        er = a, a = er.next, Jn = e;
      else {
        if (e === null)
          throw new Error("Rendered more hooks than during the previous render.");
        Jn = e;
        var i = {
          memoizedState: Jn.memoizedState,
          baseState: Jn.baseState,
          baseQueue: Jn.baseQueue,
          queue: Jn.queue,
          next: null
        };
        er === null ? It.memoizedState = er = i : er = er.next = i;
      }
      return er;
    }
    function _b() {
      return {
        lastEffect: null,
        stores: null
      };
    }
    function U0(e, t) {
      return typeof t == "function" ? t(e) : t;
    }
    function P0(e, t, a) {
      var i = jo(), u;
      a !== void 0 ? u = a(t) : u = t, i.memoizedState = i.baseState = u;
      var d = {
        pending: null,
        interleaved: null,
        lanes: ie,
        dispatch: null,
        lastRenderedReducer: e,
        lastRenderedState: u
      };
      i.queue = d;
      var h = d.dispatch = T5.bind(null, It, d);
      return [i.memoizedState, h];
    }
    function F0(e, t, a) {
      var i = Si(), u = i.queue;
      if (u === null)
        throw new Error("Should have a queue. This is likely a bug in React. Please file an issue.");
      u.lastRenderedReducer = e;
      var d = Jn, h = d.baseQueue, S = u.pending;
      if (S !== null) {
        if (h !== null) {
          var b = h.next, k = S.next;
          h.next = k, S.next = b;
        }
        d.baseQueue !== h && m("Internal error: Expected work-in-progress queue to be a clone. This is a bug in React."), d.baseQueue = h = S, u.pending = null;
      }
      if (h !== null) {
        var _ = h.next, P = d.baseState, z = null, q = null, Z = null, ee = _;
        do {
          var we = ee.lane;
          if (gl(lc, we)) {
            if (Z !== null) {
              var Ve = {
                // This update is going to be committed so we never want uncommit
                // it. Using NoLane works because 0 is a subset of all bitmasks, so
                // this will never be skipped by the check above.
                lane: Dn,
                action: ee.action,
                hasEagerState: ee.hasEagerState,
                eagerState: ee.eagerState,
                next: null
              };
              Z = Z.next = Ve;
            }
            if (ee.hasEagerState)
              P = ee.eagerState;
            else {
              var _t = ee.action;
              P = e(P, _t);
            }
          } else {
            var Qe = {
              lane: we,
              action: ee.action,
              hasEagerState: ee.hasEagerState,
              eagerState: ee.eagerState,
              next: null
            };
            Z === null ? (q = Z = Qe, z = P) : Z = Z.next = Qe, It.lanes = ut(It.lanes, we), wv(we);
          }
          ee = ee.next;
        } while (ee !== null && ee !== _);
        Z === null ? z = P : Z.next = q, Ne(P, i.memoizedState) || fv(), i.memoizedState = P, i.baseState = z, i.baseQueue = Z, u.lastRenderedState = P;
      }
      var St = u.interleaved;
      if (St !== null) {
        var I = St;
        do {
          var te = I.lane;
          It.lanes = ut(It.lanes, te), wv(te), I = I.next;
        } while (I !== St);
      } else h === null && (u.lanes = ie);
      var Y = u.dispatch;
      return [i.memoizedState, Y];
    }
    function H0(e, t, a) {
      var i = Si(), u = i.queue;
      if (u === null)
        throw new Error("Should have a queue. This is likely a bug in React. Please file an issue.");
      u.lastRenderedReducer = e;
      var d = u.dispatch, h = u.pending, S = i.memoizedState;
      if (h !== null) {
        u.pending = null;
        var b = h.next, k = b;
        do {
          var _ = k.action;
          S = e(S, _), k = k.next;
        } while (k !== b);
        Ne(S, i.memoizedState) || fv(), i.memoizedState = S, i.baseQueue === null && (i.baseState = S), u.lastRenderedState = S;
      }
      return [S, d];
    }
    function VA(e, t, a) {
    }
    function $A(e, t, a) {
    }
    function j0(e, t, a) {
      var i = It, u = jo(), d, h = yr();
      if (h) {
        if (a === void 0)
          throw new Error("Missing getServerSnapshot, which is required for server-rendered content. Will revert to client rendering.");
        d = a(), td || d !== a() && (m("The result of getServerSnapshot should be cached to avoid an infinite loop"), td = !0);
      } else {
        if (d = t(), !td) {
          var S = t();
          Ne(d, S) || (m("The result of getSnapshot should be cached to avoid an infinite loop"), td = !0);
        }
        var b = Ey();
        if (b === null)
          throw new Error("Expected a work-in-progress root. This is a bug in React. Please file an issue.");
        Ps(b, lc) || Db(i, t, d);
      }
      u.memoizedState = d;
      var k = {
        value: d,
        getSnapshot: t
      };
      return u.queue = k, Qm(Mb.bind(null, i, k, e), [e]), i.flags |= an, rv(Kn | gr, Ob.bind(null, i, k, d, t), void 0, null), d;
    }
    function Bm(e, t, a) {
      var i = It, u = Si(), d = t();
      if (!td) {
        var h = t();
        Ne(d, h) || (m("The result of getSnapshot should be cached to avoid an infinite loop"), td = !0);
      }
      var S = u.memoizedState, b = !Ne(S, d);
      b && (u.memoizedState = d, fv());
      var k = u.queue;
      if (iv(Mb.bind(null, i, k, e), [e]), k.getSnapshot !== t || b || // Check if the susbcribe function changed. We can save some memory by
      // checking whether we scheduled a subscription effect above.
      er !== null && er.memoizedState.tag & Kn) {
        i.flags |= an, rv(Kn | gr, Ob.bind(null, i, k, d, t), void 0, null);
        var _ = Ey();
        if (_ === null)
          throw new Error("Expected a work-in-progress root. This is a bug in React. Please file an issue.");
        Ps(_, lc) || Db(i, t, d);
      }
      return d;
    }
    function Db(e, t, a) {
      e.flags |= ks;
      var i = {
        getSnapshot: t,
        value: a
      }, u = It.updateQueue;
      if (u === null)
        u = _b(), It.updateQueue = u, u.stores = [i];
      else {
        var d = u.stores;
        d === null ? u.stores = [i] : d.push(i);
      }
    }
    function Ob(e, t, a, i) {
      t.value = a, t.getSnapshot = i, Lb(t) && Nb(e);
    }
    function Mb(e, t, a) {
      var i = function() {
        Lb(t) && Nb(e);
      };
      return a(i);
    }
    function Lb(e) {
      var t = e.getSnapshot, a = e.value;
      try {
        var i = t();
        return !Ne(a, i);
      } catch {
        return !0;
      }
    }
    function Nb(e) {
      var t = La(e, $e);
      t !== null && ar(t, e, $e, Kt);
    }
    function Im(e) {
      var t = jo();
      typeof e == "function" && (e = e()), t.memoizedState = t.baseState = e;
      var a = {
        pending: null,
        interleaved: null,
        lanes: ie,
        dispatch: null,
        lastRenderedReducer: U0,
        lastRenderedState: e
      };
      t.queue = a;
      var i = a.dispatch = R5.bind(null, It, a);
      return [t.memoizedState, i];
    }
    function V0(e) {
      return F0(U0);
    }
    function $0(e) {
      return H0(U0);
    }
    function rv(e, t, a, i) {
      var u = {
        tag: e,
        create: t,
        destroy: a,
        deps: i,
        // Circular
        next: null
      }, d = It.updateQueue;
      if (d === null)
        d = _b(), It.updateQueue = d, d.lastEffect = u.next = u;
      else {
        var h = d.lastEffect;
        if (h === null)
          d.lastEffect = u.next = u;
        else {
          var S = h.next;
          h.next = u, u.next = S, d.lastEffect = u;
        }
      }
      return u;
    }
    function B0(e) {
      var t = jo();
      {
        var a = {
          current: e
        };
        return t.memoizedState = a, a;
      }
    }
    function Ym(e) {
      var t = Si();
      return t.memoizedState;
    }
    function av(e, t, a, i) {
      var u = jo(), d = i === void 0 ? null : i;
      It.flags |= e, u.memoizedState = rv(Kn | t, a, void 0, d);
    }
    function Wm(e, t, a, i) {
      var u = Si(), d = i === void 0 ? null : i, h = void 0;
      if (Jn !== null) {
        var S = Jn.memoizedState;
        if (h = S.destroy, d !== null) {
          var b = S.deps;
          if (z0(d, b)) {
            u.memoizedState = rv(t, a, h, d);
            return;
          }
        }
      }
      It.flags |= e, u.memoizedState = rv(Kn | t, a, h, d);
    }
    function Qm(e, t) {
      return (It.mode & oa) !== Fe ? av(sl | an | ko, gr, e, t) : av(an | ko, gr, e, t);
    }
    function iv(e, t) {
      return Wm(an, gr, e, t);
    }
    function I0(e, t) {
      return av(st, Ho, e, t);
    }
    function Gm(e, t) {
      return Wm(st, Ho, e, t);
    }
    function Y0(e, t) {
      var a = st;
      return a |= Fr, (It.mode & oa) !== Fe && (a |= Hr), av(a, Zn, e, t);
    }
    function qm(e, t) {
      return Wm(st, Zn, e, t);
    }
    function Ab(e, t) {
      if (typeof t == "function") {
        var a = t, i = e();
        return a(i), function() {
          a(null);
        };
      } else if (t != null) {
        var u = t;
        u.hasOwnProperty("current") || m("Expected useImperativeHandle() first argument to either be a ref callback or React.createRef() object. Instead received: %s.", "an object with keys {" + Object.keys(u).join(", ") + "}");
        var d = e();
        return u.current = d, function() {
          u.current = null;
        };
      }
    }
    function W0(e, t, a) {
      typeof t != "function" && m("Expected useImperativeHandle() second argument to be a function that creates a handle. Instead received: %s.", t !== null ? typeof t : "null");
      var i = a != null ? a.concat([e]) : null, u = st;
      return u |= Fr, (It.mode & oa) !== Fe && (u |= Hr), av(u, Zn, Ab.bind(null, t, e), i);
    }
    function Xm(e, t, a) {
      typeof t != "function" && m("Expected useImperativeHandle() second argument to be a function that creates a handle. Instead received: %s.", t !== null ? typeof t : "null");
      var i = a != null ? a.concat([e]) : null;
      return Wm(st, Zn, Ab.bind(null, t, e), i);
    }
    function b5(e, t) {
    }
    var Km = b5;
    function Q0(e, t) {
      var a = jo(), i = t === void 0 ? null : t;
      return a.memoizedState = [e, i], e;
    }
    function Zm(e, t) {
      var a = Si(), i = t === void 0 ? null : t, u = a.memoizedState;
      if (u !== null && i !== null) {
        var d = u[1];
        if (z0(i, d))
          return u[0];
      }
      return a.memoizedState = [e, i], e;
    }
    function G0(e, t) {
      var a = jo(), i = t === void 0 ? null : t, u = e();
      return a.memoizedState = [u, i], u;
    }
    function Jm(e, t) {
      var a = Si(), i = t === void 0 ? null : t, u = a.memoizedState;
      if (u !== null && i !== null) {
        var d = u[1];
        if (z0(i, d))
          return u[0];
      }
      var h = e();
      return a.memoizedState = [h, i], h;
    }
    function q0(e) {
      var t = jo();
      return t.memoizedState = e, e;
    }
    function zb(e) {
      var t = Si(), a = Jn, i = a.memoizedState;
      return Pb(t, i, e);
    }
    function Ub(e) {
      var t = Si();
      if (Jn === null)
        return t.memoizedState = e, e;
      var a = Jn.memoizedState;
      return Pb(t, a, e);
    }
    function Pb(e, t, a) {
      var i = !Dh(lc);
      if (i) {
        if (!Ne(a, t)) {
          var u = Lh();
          It.lanes = ut(It.lanes, u), wv(u), e.baseState = !0;
        }
        return t;
      } else
        return e.baseState && (e.baseState = !1, fv()), e.memoizedState = a, a;
    }
    function C5(e, t, a) {
      var i = xa();
      yn(hg(i, $i)), e(!0);
      var u = ev.transition;
      ev.transition = {};
      var d = ev.transition;
      ev.transition._updatedFibers = /* @__PURE__ */ new Set();
      try {
        e(!1), t();
      } finally {
        if (yn(i), ev.transition = u, u === null && d._updatedFibers) {
          var h = d._updatedFibers.size;
          h > 10 && E("Detected a large number of updates inside startTransition. If this is due to a subscription please re-write it to use React provided hooks. Otherwise concurrent mode guarantees are off the table."), d._updatedFibers.clear();
        }
      }
    }
    function X0() {
      var e = Im(!1), t = e[0], a = e[1], i = C5.bind(null, a), u = jo();
      return u.memoizedState = i, [t, i];
    }
    function Fb() {
      var e = V0(), t = e[0], a = Si(), i = a.memoizedState;
      return [t, i];
    }
    function Hb() {
      var e = $0(), t = e[0], a = Si(), i = a.memoizedState;
      return [t, i];
    }
    var jb = !1;
    function w5() {
      return jb;
    }
    function K0() {
      var e = jo(), t = Ey(), a = t.identifierPrefix, i;
      if (yr()) {
        var u = jk();
        i = ":" + a + "R" + u;
        var d = nv++;
        d > 0 && (i += "H" + d.toString(32)), i += ":";
      } else {
        var h = g5++;
        i = ":" + a + "r" + h.toString(32) + ":";
      }
      return e.memoizedState = i, i;
    }
    function ey() {
      var e = Si(), t = e.memoizedState;
      return t;
    }
    function T5(e, t, a) {
      typeof arguments[3] == "function" && m("State updates from the useState() and useReducer() Hooks don't support the second callback argument. To execute a side effect after rendering, declare it in the component body with useEffect().");
      var i = Zu(e), u = {
        lane: i,
        action: a,
        hasEagerState: !1,
        eagerState: null,
        next: null
      };
      if (Vb(e))
        $b(t, u);
      else {
        var d = mb(e, t, u, i);
        if (d !== null) {
          var h = ca();
          ar(d, e, i, h), Bb(d, t, i);
        }
      }
      Ib(e, i);
    }
    function R5(e, t, a) {
      typeof arguments[3] == "function" && m("State updates from the useState() and useReducer() Hooks don't support the second callback argument. To execute a side effect after rendering, declare it in the component body with useEffect().");
      var i = Zu(e), u = {
        lane: i,
        action: a,
        hasEagerState: !1,
        eagerState: null,
        next: null
      };
      if (Vb(e))
        $b(t, u);
      else {
        var d = e.alternate;
        if (e.lanes === ie && (d === null || d.lanes === ie)) {
          var h = t.lastRenderedReducer;
          if (h !== null) {
            var S;
            S = ke.current, ke.current = Xi;
            try {
              var b = t.lastRenderedState, k = h(b, a);
              if (u.hasEagerState = !0, u.eagerState = k, Ne(k, b)) {
                s5(e, t, u, i);
                return;
              }
            } catch {
            } finally {
              ke.current = S;
            }
          }
        }
        var _ = mb(e, t, u, i);
        if (_ !== null) {
          var P = ca();
          ar(_, e, i, P), Bb(_, t, i);
        }
      }
      Ib(e, i);
    }
    function Vb(e) {
      var t = e.alternate;
      return e === It || t !== null && t === It;
    }
    function $b(e, t) {
      tv = $m = !0;
      var a = e.pending;
      a === null ? t.next = t : (t.next = a.next, a.next = t), e.pending = t;
    }
    function Bb(e, t, a) {
      if (fp(a)) {
        var i = t.lanes;
        i = vf(i, e.pendingLanes);
        var u = ut(i, a);
        t.lanes = u, dp(e, u);
      }
    }
    function Ib(e, t, a) {
      Ms(e, t);
    }
    var ty = {
      readContext: Yn,
      useCallback: qr,
      useContext: qr,
      useEffect: qr,
      useImperativeHandle: qr,
      useInsertionEffect: qr,
      useLayoutEffect: qr,
      useMemo: qr,
      useReducer: qr,
      useRef: qr,
      useState: qr,
      useDebugValue: qr,
      useDeferredValue: qr,
      useTransition: qr,
      useMutableSource: qr,
      useSyncExternalStore: qr,
      useId: qr,
      unstable_isNewReconciler: he
    }, Yb = null, Wb = null, Qb = null, Gb = null, Vo = null, Xi = null, ny = null;
    {
      var Z0 = function() {
        m("Context can only be read while React is rendering. In classes, you can read it in the render method or getDerivedStateFromProps. In function components, you can read it directly in the function body, but not inside Hooks like useReducer() or useMemo().");
      }, ot = function() {
        m("Do not call Hooks inside useEffect(...), useMemo(...), or other built-in Hooks. You can only call Hooks at the top level of your React function. For more information, see https://reactjs.org/link/rules-of-hooks");
      };
      Yb = {
        readContext: function(e) {
          return Yn(e);
        },
        useCallback: function(e, t) {
          return re = "useCallback", jt(), nd(t), Q0(e, t);
        },
        useContext: function(e) {
          return re = "useContext", jt(), Yn(e);
        },
        useEffect: function(e, t) {
          return re = "useEffect", jt(), nd(t), Qm(e, t);
        },
        useImperativeHandle: function(e, t, a) {
          return re = "useImperativeHandle", jt(), nd(a), W0(e, t, a);
        },
        useInsertionEffect: function(e, t) {
          return re = "useInsertionEffect", jt(), nd(t), I0(e, t);
        },
        useLayoutEffect: function(e, t) {
          return re = "useLayoutEffect", jt(), nd(t), Y0(e, t);
        },
        useMemo: function(e, t) {
          re = "useMemo", jt(), nd(t);
          var a = ke.current;
          ke.current = Vo;
          try {
            return G0(e, t);
          } finally {
            ke.current = a;
          }
        },
        useReducer: function(e, t, a) {
          re = "useReducer", jt();
          var i = ke.current;
          ke.current = Vo;
          try {
            return P0(e, t, a);
          } finally {
            ke.current = i;
          }
        },
        useRef: function(e) {
          return re = "useRef", jt(), B0(e);
        },
        useState: function(e) {
          re = "useState", jt();
          var t = ke.current;
          ke.current = Vo;
          try {
            return Im(e);
          } finally {
            ke.current = t;
          }
        },
        useDebugValue: function(e, t) {
          return re = "useDebugValue", jt(), void 0;
        },
        useDeferredValue: function(e) {
          return re = "useDeferredValue", jt(), q0(e);
        },
        useTransition: function() {
          return re = "useTransition", jt(), X0();
        },
        useMutableSource: function(e, t, a) {
          return re = "useMutableSource", jt(), void 0;
        },
        useSyncExternalStore: function(e, t, a) {
          return re = "useSyncExternalStore", jt(), j0(e, t, a);
        },
        useId: function() {
          return re = "useId", jt(), K0();
        },
        unstable_isNewReconciler: he
      }, Wb = {
        readContext: function(e) {
          return Yn(e);
        },
        useCallback: function(e, t) {
          return re = "useCallback", Se(), Q0(e, t);
        },
        useContext: function(e) {
          return re = "useContext", Se(), Yn(e);
        },
        useEffect: function(e, t) {
          return re = "useEffect", Se(), Qm(e, t);
        },
        useImperativeHandle: function(e, t, a) {
          return re = "useImperativeHandle", Se(), W0(e, t, a);
        },
        useInsertionEffect: function(e, t) {
          return re = "useInsertionEffect", Se(), I0(e, t);
        },
        useLayoutEffect: function(e, t) {
          return re = "useLayoutEffect", Se(), Y0(e, t);
        },
        useMemo: function(e, t) {
          re = "useMemo", Se();
          var a = ke.current;
          ke.current = Vo;
          try {
            return G0(e, t);
          } finally {
            ke.current = a;
          }
        },
        useReducer: function(e, t, a) {
          re = "useReducer", Se();
          var i = ke.current;
          ke.current = Vo;
          try {
            return P0(e, t, a);
          } finally {
            ke.current = i;
          }
        },
        useRef: function(e) {
          return re = "useRef", Se(), B0(e);
        },
        useState: function(e) {
          re = "useState", Se();
          var t = ke.current;
          ke.current = Vo;
          try {
            return Im(e);
          } finally {
            ke.current = t;
          }
        },
        useDebugValue: function(e, t) {
          return re = "useDebugValue", Se(), void 0;
        },
        useDeferredValue: function(e) {
          return re = "useDeferredValue", Se(), q0(e);
        },
        useTransition: function() {
          return re = "useTransition", Se(), X0();
        },
        useMutableSource: function(e, t, a) {
          return re = "useMutableSource", Se(), void 0;
        },
        useSyncExternalStore: function(e, t, a) {
          return re = "useSyncExternalStore", Se(), j0(e, t, a);
        },
        useId: function() {
          return re = "useId", Se(), K0();
        },
        unstable_isNewReconciler: he
      }, Qb = {
        readContext: function(e) {
          return Yn(e);
        },
        useCallback: function(e, t) {
          return re = "useCallback", Se(), Zm(e, t);
        },
        useContext: function(e) {
          return re = "useContext", Se(), Yn(e);
        },
        useEffect: function(e, t) {
          return re = "useEffect", Se(), iv(e, t);
        },
        useImperativeHandle: function(e, t, a) {
          return re = "useImperativeHandle", Se(), Xm(e, t, a);
        },
        useInsertionEffect: function(e, t) {
          return re = "useInsertionEffect", Se(), Gm(e, t);
        },
        useLayoutEffect: function(e, t) {
          return re = "useLayoutEffect", Se(), qm(e, t);
        },
        useMemo: function(e, t) {
          re = "useMemo", Se();
          var a = ke.current;
          ke.current = Xi;
          try {
            return Jm(e, t);
          } finally {
            ke.current = a;
          }
        },
        useReducer: function(e, t, a) {
          re = "useReducer", Se();
          var i = ke.current;
          ke.current = Xi;
          try {
            return F0(e, t, a);
          } finally {
            ke.current = i;
          }
        },
        useRef: function(e) {
          return re = "useRef", Se(), Ym();
        },
        useState: function(e) {
          re = "useState", Se();
          var t = ke.current;
          ke.current = Xi;
          try {
            return V0(e);
          } finally {
            ke.current = t;
          }
        },
        useDebugValue: function(e, t) {
          return re = "useDebugValue", Se(), Km();
        },
        useDeferredValue: function(e) {
          return re = "useDeferredValue", Se(), zb(e);
        },
        useTransition: function() {
          return re = "useTransition", Se(), Fb();
        },
        useMutableSource: function(e, t, a) {
          return re = "useMutableSource", Se(), void 0;
        },
        useSyncExternalStore: function(e, t, a) {
          return re = "useSyncExternalStore", Se(), Bm(e, t);
        },
        useId: function() {
          return re = "useId", Se(), ey();
        },
        unstable_isNewReconciler: he
      }, Gb = {
        readContext: function(e) {
          return Yn(e);
        },
        useCallback: function(e, t) {
          return re = "useCallback", Se(), Zm(e, t);
        },
        useContext: function(e) {
          return re = "useContext", Se(), Yn(e);
        },
        useEffect: function(e, t) {
          return re = "useEffect", Se(), iv(e, t);
        },
        useImperativeHandle: function(e, t, a) {
          return re = "useImperativeHandle", Se(), Xm(e, t, a);
        },
        useInsertionEffect: function(e, t) {
          return re = "useInsertionEffect", Se(), Gm(e, t);
        },
        useLayoutEffect: function(e, t) {
          return re = "useLayoutEffect", Se(), qm(e, t);
        },
        useMemo: function(e, t) {
          re = "useMemo", Se();
          var a = ke.current;
          ke.current = ny;
          try {
            return Jm(e, t);
          } finally {
            ke.current = a;
          }
        },
        useReducer: function(e, t, a) {
          re = "useReducer", Se();
          var i = ke.current;
          ke.current = ny;
          try {
            return H0(e, t, a);
          } finally {
            ke.current = i;
          }
        },
        useRef: function(e) {
          return re = "useRef", Se(), Ym();
        },
        useState: function(e) {
          re = "useState", Se();
          var t = ke.current;
          ke.current = ny;
          try {
            return $0(e);
          } finally {
            ke.current = t;
          }
        },
        useDebugValue: function(e, t) {
          return re = "useDebugValue", Se(), Km();
        },
        useDeferredValue: function(e) {
          return re = "useDeferredValue", Se(), Ub(e);
        },
        useTransition: function() {
          return re = "useTransition", Se(), Hb();
        },
        useMutableSource: function(e, t, a) {
          return re = "useMutableSource", Se(), void 0;
        },
        useSyncExternalStore: function(e, t, a) {
          return re = "useSyncExternalStore", Se(), Bm(e, t);
        },
        useId: function() {
          return re = "useId", Se(), ey();
        },
        unstable_isNewReconciler: he
      }, Vo = {
        readContext: function(e) {
          return Z0(), Yn(e);
        },
        useCallback: function(e, t) {
          return re = "useCallback", ot(), jt(), Q0(e, t);
        },
        useContext: function(e) {
          return re = "useContext", ot(), jt(), Yn(e);
        },
        useEffect: function(e, t) {
          return re = "useEffect", ot(), jt(), Qm(e, t);
        },
        useImperativeHandle: function(e, t, a) {
          return re = "useImperativeHandle", ot(), jt(), W0(e, t, a);
        },
        useInsertionEffect: function(e, t) {
          return re = "useInsertionEffect", ot(), jt(), I0(e, t);
        },
        useLayoutEffect: function(e, t) {
          return re = "useLayoutEffect", ot(), jt(), Y0(e, t);
        },
        useMemo: function(e, t) {
          re = "useMemo", ot(), jt();
          var a = ke.current;
          ke.current = Vo;
          try {
            return G0(e, t);
          } finally {
            ke.current = a;
          }
        },
        useReducer: function(e, t, a) {
          re = "useReducer", ot(), jt();
          var i = ke.current;
          ke.current = Vo;
          try {
            return P0(e, t, a);
          } finally {
            ke.current = i;
          }
        },
        useRef: function(e) {
          return re = "useRef", ot(), jt(), B0(e);
        },
        useState: function(e) {
          re = "useState", ot(), jt();
          var t = ke.current;
          ke.current = Vo;
          try {
            return Im(e);
          } finally {
            ke.current = t;
          }
        },
        useDebugValue: function(e, t) {
          return re = "useDebugValue", ot(), jt(), void 0;
        },
        useDeferredValue: function(e) {
          return re = "useDeferredValue", ot(), jt(), q0(e);
        },
        useTransition: function() {
          return re = "useTransition", ot(), jt(), X0();
        },
        useMutableSource: function(e, t, a) {
          return re = "useMutableSource", ot(), jt(), void 0;
        },
        useSyncExternalStore: function(e, t, a) {
          return re = "useSyncExternalStore", ot(), jt(), j0(e, t, a);
        },
        useId: function() {
          return re = "useId", ot(), jt(), K0();
        },
        unstable_isNewReconciler: he
      }, Xi = {
        readContext: function(e) {
          return Z0(), Yn(e);
        },
        useCallback: function(e, t) {
          return re = "useCallback", ot(), Se(), Zm(e, t);
        },
        useContext: function(e) {
          return re = "useContext", ot(), Se(), Yn(e);
        },
        useEffect: function(e, t) {
          return re = "useEffect", ot(), Se(), iv(e, t);
        },
        useImperativeHandle: function(e, t, a) {
          return re = "useImperativeHandle", ot(), Se(), Xm(e, t, a);
        },
        useInsertionEffect: function(e, t) {
          return re = "useInsertionEffect", ot(), Se(), Gm(e, t);
        },
        useLayoutEffect: function(e, t) {
          return re = "useLayoutEffect", ot(), Se(), qm(e, t);
        },
        useMemo: function(e, t) {
          re = "useMemo", ot(), Se();
          var a = ke.current;
          ke.current = Xi;
          try {
            return Jm(e, t);
          } finally {
            ke.current = a;
          }
        },
        useReducer: function(e, t, a) {
          re = "useReducer", ot(), Se();
          var i = ke.current;
          ke.current = Xi;
          try {
            return F0(e, t, a);
          } finally {
            ke.current = i;
          }
        },
        useRef: function(e) {
          return re = "useRef", ot(), Se(), Ym();
        },
        useState: function(e) {
          re = "useState", ot(), Se();
          var t = ke.current;
          ke.current = Xi;
          try {
            return V0(e);
          } finally {
            ke.current = t;
          }
        },
        useDebugValue: function(e, t) {
          return re = "useDebugValue", ot(), Se(), Km();
        },
        useDeferredValue: function(e) {
          return re = "useDeferredValue", ot(), Se(), zb(e);
        },
        useTransition: function() {
          return re = "useTransition", ot(), Se(), Fb();
        },
        useMutableSource: function(e, t, a) {
          return re = "useMutableSource", ot(), Se(), void 0;
        },
        useSyncExternalStore: function(e, t, a) {
          return re = "useSyncExternalStore", ot(), Se(), Bm(e, t);
        },
        useId: function() {
          return re = "useId", ot(), Se(), ey();
        },
        unstable_isNewReconciler: he
      }, ny = {
        readContext: function(e) {
          return Z0(), Yn(e);
        },
        useCallback: function(e, t) {
          return re = "useCallback", ot(), Se(), Zm(e, t);
        },
        useContext: function(e) {
          return re = "useContext", ot(), Se(), Yn(e);
        },
        useEffect: function(e, t) {
          return re = "useEffect", ot(), Se(), iv(e, t);
        },
        useImperativeHandle: function(e, t, a) {
          return re = "useImperativeHandle", ot(), Se(), Xm(e, t, a);
        },
        useInsertionEffect: function(e, t) {
          return re = "useInsertionEffect", ot(), Se(), Gm(e, t);
        },
        useLayoutEffect: function(e, t) {
          return re = "useLayoutEffect", ot(), Se(), qm(e, t);
        },
        useMemo: function(e, t) {
          re = "useMemo", ot(), Se();
          var a = ke.current;
          ke.current = Xi;
          try {
            return Jm(e, t);
          } finally {
            ke.current = a;
          }
        },
        useReducer: function(e, t, a) {
          re = "useReducer", ot(), Se();
          var i = ke.current;
          ke.current = Xi;
          try {
            return H0(e, t, a);
          } finally {
            ke.current = i;
          }
        },
        useRef: function(e) {
          return re = "useRef", ot(), Se(), Ym();
        },
        useState: function(e) {
          re = "useState", ot(), Se();
          var t = ke.current;
          ke.current = Xi;
          try {
            return $0(e);
          } finally {
            ke.current = t;
          }
        },
        useDebugValue: function(e, t) {
          return re = "useDebugValue", ot(), Se(), Km();
        },
        useDeferredValue: function(e) {
          return re = "useDeferredValue", ot(), Se(), Ub(e);
        },
        useTransition: function() {
          return re = "useTransition", ot(), Se(), Hb();
        },
        useMutableSource: function(e, t, a) {
          return re = "useMutableSource", ot(), Se(), void 0;
        },
        useSyncExternalStore: function(e, t, a) {
          return re = "useSyncExternalStore", ot(), Se(), Bm(e, t);
        },
        useId: function() {
          return re = "useId", ot(), Se(), ey();
        },
        unstable_isNewReconciler: he
      };
    }
    var Gu = s.unstable_now, qb = 0, ry = -1, ov = -1, ay = -1, J0 = !1, iy = !1;
    function Xb() {
      return J0;
    }
    function x5() {
      iy = !0;
    }
    function k5() {
      J0 = !1, iy = !1;
    }
    function _5() {
      J0 = iy, iy = !1;
    }
    function Kb() {
      return qb;
    }
    function Zb() {
      qb = Gu();
    }
    function eS(e) {
      ov = Gu(), e.actualStartTime < 0 && (e.actualStartTime = Gu());
    }
    function Jb(e) {
      ov = -1;
    }
    function oy(e, t) {
      if (ov >= 0) {
        var a = Gu() - ov;
        e.actualDuration += a, t && (e.selfBaseDuration = a), ov = -1;
      }
    }
    function $o(e) {
      if (ry >= 0) {
        var t = Gu() - ry;
        ry = -1;
        for (var a = e.return; a !== null; ) {
          switch (a.tag) {
            case L:
              var i = a.stateNode;
              i.effectDuration += t;
              return;
            case oe:
              var u = a.stateNode;
              u.effectDuration += t;
              return;
          }
          a = a.return;
        }
      }
    }
    function tS(e) {
      if (ay >= 0) {
        var t = Gu() - ay;
        ay = -1;
        for (var a = e.return; a !== null; ) {
          switch (a.tag) {
            case L:
              var i = a.stateNode;
              i !== null && (i.passiveEffectDuration += t);
              return;
            case oe:
              var u = a.stateNode;
              u !== null && (u.passiveEffectDuration += t);
              return;
          }
          a = a.return;
        }
      }
    }
    function Bo() {
      ry = Gu();
    }
    function nS() {
      ay = Gu();
    }
    function rS(e) {
      for (var t = e.child; t; )
        e.actualDuration += t.actualDuration, t = t.sibling;
    }
    function Ki(e, t) {
      if (e && e.defaultProps) {
        var a = dt({}, t), i = e.defaultProps;
        for (var u in i)
          a[u] === void 0 && (a[u] = i[u]);
        return a;
      }
      return t;
    }
    var aS = {}, iS, oS, lS, uS, sS, eC, ly, cS, fS, dS, lv;
    {
      iS = /* @__PURE__ */ new Set(), oS = /* @__PURE__ */ new Set(), lS = /* @__PURE__ */ new Set(), uS = /* @__PURE__ */ new Set(), cS = /* @__PURE__ */ new Set(), sS = /* @__PURE__ */ new Set(), fS = /* @__PURE__ */ new Set(), dS = /* @__PURE__ */ new Set(), lv = /* @__PURE__ */ new Set();
      var tC = /* @__PURE__ */ new Set();
      ly = function(e, t) {
        if (!(e === null || typeof e == "function")) {
          var a = t + "_" + e;
          tC.has(a) || (tC.add(a), m("%s(...): Expected the last optional `callback` argument to be a function. Instead received: %s.", t, e));
        }
      }, eC = function(e, t) {
        if (t === void 0) {
          var a = Ct(e) || "Component";
          sS.has(a) || (sS.add(a), m("%s.getDerivedStateFromProps(): A valid state object (or null) must be returned. You have returned undefined.", a));
        }
      }, Object.defineProperty(aS, "_processChildContext", {
        enumerable: !1,
        value: function() {
          throw new Error("_processChildContext is not available in React 16+. This likely means you have multiple copies of React and are attempting to nest a React 15 tree inside a React 16 tree using unstable_renderSubtreeIntoContainer, which isn't supported. Try to make sure you have only one copy of React (and ideally, switch to ReactDOM.createPortal).");
        }
      }), Object.freeze(aS);
    }
    function pS(e, t, a, i) {
      var u = e.memoizedState, d = a(i, u);
      {
        if (e.mode & vt) {
          _n(!0);
          try {
            d = a(i, u);
          } finally {
            _n(!1);
          }
        }
        eC(t, d);
      }
      var h = d == null ? u : dt({}, u, d);
      if (e.memoizedState = h, e.lanes === ie) {
        var S = e.updateQueue;
        S.baseState = h;
      }
    }
    var vS = {
      isMounted: aa,
      enqueueSetState: function(e, t, a) {
        var i = ga(e), u = ca(), d = Zu(i), h = Ml(u, d);
        h.payload = t, a != null && (ly(a, "setState"), h.callback = a);
        var S = Iu(i, h, d);
        S !== null && (ar(S, i, d, u), Um(S, i, d)), Ms(i, d);
      },
      enqueueReplaceState: function(e, t, a) {
        var i = ga(e), u = ca(), d = Zu(i), h = Ml(u, d);
        h.tag = gb, h.payload = t, a != null && (ly(a, "replaceState"), h.callback = a);
        var S = Iu(i, h, d);
        S !== null && (ar(S, i, d, u), Um(S, i, d)), Ms(i, d);
      },
      enqueueForceUpdate: function(e, t) {
        var a = ga(e), i = ca(), u = Zu(a), d = Ml(i, u);
        d.tag = Nm, t != null && (ly(t, "forceUpdate"), d.callback = t);
        var h = Iu(a, d, u);
        h !== null && (ar(h, a, u, i), Um(h, a, u)), Xc(a, u);
      }
    };
    function nC(e, t, a, i, u, d, h) {
      var S = e.stateNode;
      if (typeof S.shouldComponentUpdate == "function") {
        var b = S.shouldComponentUpdate(i, d, h);
        {
          if (e.mode & vt) {
            _n(!0);
            try {
              b = S.shouldComponentUpdate(i, d, h);
            } finally {
              _n(!1);
            }
          }
          b === void 0 && m("%s.shouldComponentUpdate(): Returned undefined instead of a boolean value. Make sure to return true or false.", Ct(t) || "Component");
        }
        return b;
      }
      return t.prototype && t.prototype.isPureReactComponent ? !Ze(a, i) || !Ze(u, d) : !0;
    }
    function D5(e, t, a) {
      var i = e.stateNode;
      {
        var u = Ct(t) || "Component", d = i.render;
        d || (t.prototype && typeof t.prototype.render == "function" ? m("%s(...): No `render` method found on the returned component instance: did you accidentally return an object from the constructor?", u) : m("%s(...): No `render` method found on the returned component instance: you may have forgotten to define `render`.", u)), i.getInitialState && !i.getInitialState.isReactClassApproved && !i.state && m("getInitialState was defined on %s, a plain JavaScript class. This is only supported for classes created using React.createClass. Did you mean to define a state property instead?", u), i.getDefaultProps && !i.getDefaultProps.isReactClassApproved && m("getDefaultProps was defined on %s, a plain JavaScript class. This is only supported for classes created using React.createClass. Use a static property to define defaultProps instead.", u), i.propTypes && m("propTypes was defined as an instance property on %s. Use a static property to define propTypes instead.", u), i.contextType && m("contextType was defined as an instance property on %s. Use a static property to define contextType instead.", u), t.childContextTypes && !lv.has(t) && // Strict Mode has its own warning for legacy context, so we can skip
        // this one.
        (e.mode & vt) === Fe && (lv.add(t), m(`%s uses the legacy childContextTypes API which is no longer supported and will be removed in the next major release. Use React.createContext() instead

.Learn more about this warning here: https://reactjs.org/link/legacy-context`, u)), t.contextTypes && !lv.has(t) && // Strict Mode has its own warning for legacy context, so we can skip
        // this one.
        (e.mode & vt) === Fe && (lv.add(t), m(`%s uses the legacy contextTypes API which is no longer supported and will be removed in the next major release. Use React.createContext() with static contextType instead.

Learn more about this warning here: https://reactjs.org/link/legacy-context`, u)), i.contextTypes && m("contextTypes was defined as an instance property on %s. Use a static property to define contextTypes instead.", u), t.contextType && t.contextTypes && !fS.has(t) && (fS.add(t), m("%s declares both contextTypes and contextType static properties. The legacy contextTypes property will be ignored.", u)), typeof i.componentShouldUpdate == "function" && m("%s has a method called componentShouldUpdate(). Did you mean shouldComponentUpdate()? The name is phrased as a question because the function is expected to return a value.", u), t.prototype && t.prototype.isPureReactComponent && typeof i.shouldComponentUpdate < "u" && m("%s has a method called shouldComponentUpdate(). shouldComponentUpdate should not be used when extending React.PureComponent. Please extend React.Component if shouldComponentUpdate is used.", Ct(t) || "A pure component"), typeof i.componentDidUnmount == "function" && m("%s has a method called componentDidUnmount(). But there is no such lifecycle method. Did you mean componentWillUnmount()?", u), typeof i.componentDidReceiveProps == "function" && m("%s has a method called componentDidReceiveProps(). But there is no such lifecycle method. If you meant to update the state in response to changing props, use componentWillReceiveProps(). If you meant to fetch data or run side-effects or mutations after React has updated the UI, use componentDidUpdate().", u), typeof i.componentWillRecieveProps == "function" && m("%s has a method called componentWillRecieveProps(). Did you mean componentWillReceiveProps()?", u), typeof i.UNSAFE_componentWillRecieveProps == "function" && m("%s has a method called UNSAFE_componentWillRecieveProps(). Did you mean UNSAFE_componentWillReceiveProps()?", u);
        var h = i.props !== a;
        i.props !== void 0 && h && m("%s(...): When calling super() in `%s`, make sure to pass up the same props that your component's constructor was passed.", u, u), i.defaultProps && m("Setting defaultProps as an instance property on %s is not supported and will be ignored. Instead, define defaultProps as a static property on %s.", u, u), typeof i.getSnapshotBeforeUpdate == "function" && typeof i.componentDidUpdate != "function" && !lS.has(t) && (lS.add(t), m("%s: getSnapshotBeforeUpdate() should be used with componentDidUpdate(). This component defines getSnapshotBeforeUpdate() only.", Ct(t))), typeof i.getDerivedStateFromProps == "function" && m("%s: getDerivedStateFromProps() is defined as an instance method and will be ignored. Instead, declare it as a static method.", u), typeof i.getDerivedStateFromError == "function" && m("%s: getDerivedStateFromError() is defined as an instance method and will be ignored. Instead, declare it as a static method.", u), typeof t.getSnapshotBeforeUpdate == "function" && m("%s: getSnapshotBeforeUpdate() is defined as a static method and will be ignored. Instead, declare it as an instance method.", u);
        var S = i.state;
        S && (typeof S != "object" || wt(S)) && m("%s.state: must be set to an object or null", u), typeof i.getChildContext == "function" && typeof t.childContextTypes != "object" && m("%s.getChildContext(): childContextTypes must be defined in order to use getChildContext().", u);
      }
    }
    function rC(e, t) {
      t.updater = vS, e.stateNode = t, lu(t, e), t._reactInternalInstance = aS;
    }
    function aC(e, t, a) {
      var i = !1, u = Za, d = Za, h = t.contextType;
      if ("contextType" in t) {
        var S = (
          // Allow null for conditional declaration
          h === null || h !== void 0 && h.$$typeof === ls && h._context === void 0
        );
        if (!S && !dS.has(t)) {
          dS.add(t);
          var b = "";
          h === void 0 ? b = " However, it is set to undefined. This can be caused by a typo or by mixing up named and default imports. This can also happen due to a circular dependency, so try moving the createContext() call to a separate file." : typeof h != "object" ? b = " However, it is set to a " + typeof h + "." : h.$$typeof === Bl ? b = " Did you accidentally pass the Context.Provider instead?" : h._context !== void 0 ? b = " Did you accidentally pass the Context.Consumer instead?" : b = " However, it is set to an object with keys {" + Object.keys(h).join(", ") + "}.", m("%s defines an invalid contextType. contextType should point to the Context object returned by React.createContext().%s", Ct(t) || "Component", b);
        }
      }
      if (typeof h == "object" && h !== null)
        d = Yn(h);
      else {
        u = If(e, t, !0);
        var k = t.contextTypes;
        i = k != null, d = i ? Yf(e, u) : Za;
      }
      var _ = new t(a, d);
      if (e.mode & vt) {
        _n(!0);
        try {
          _ = new t(a, d);
        } finally {
          _n(!1);
        }
      }
      var P = e.memoizedState = _.state !== null && _.state !== void 0 ? _.state : null;
      rC(e, _);
      {
        if (typeof t.getDerivedStateFromProps == "function" && P === null) {
          var z = Ct(t) || "Component";
          oS.has(z) || (oS.add(z), m("`%s` uses `getDerivedStateFromProps` but its initial state is %s. This is not recommended. Instead, define the initial state by assigning an object to `this.state` in the constructor of `%s`. This ensures that `getDerivedStateFromProps` arguments have a consistent shape.", z, _.state === null ? "null" : "undefined", z));
        }
        if (typeof t.getDerivedStateFromProps == "function" || typeof _.getSnapshotBeforeUpdate == "function") {
          var q = null, Z = null, ee = null;
          if (typeof _.componentWillMount == "function" && _.componentWillMount.__suppressDeprecationWarning !== !0 ? q = "componentWillMount" : typeof _.UNSAFE_componentWillMount == "function" && (q = "UNSAFE_componentWillMount"), typeof _.componentWillReceiveProps == "function" && _.componentWillReceiveProps.__suppressDeprecationWarning !== !0 ? Z = "componentWillReceiveProps" : typeof _.UNSAFE_componentWillReceiveProps == "function" && (Z = "UNSAFE_componentWillReceiveProps"), typeof _.componentWillUpdate == "function" && _.componentWillUpdate.__suppressDeprecationWarning !== !0 ? ee = "componentWillUpdate" : typeof _.UNSAFE_componentWillUpdate == "function" && (ee = "UNSAFE_componentWillUpdate"), q !== null || Z !== null || ee !== null) {
            var we = Ct(t) || "Component", Qe = typeof t.getDerivedStateFromProps == "function" ? "getDerivedStateFromProps()" : "getSnapshotBeforeUpdate()";
            uS.has(we) || (uS.add(we), m(`Unsafe legacy lifecycles will not be called for components using new component APIs.

%s uses %s but also contains the following legacy lifecycles:%s%s%s

The above lifecycles should be removed. Learn more about this warning here:
https://reactjs.org/link/unsafe-component-lifecycles`, we, Qe, q !== null ? `
  ` + q : "", Z !== null ? `
  ` + Z : "", ee !== null ? `
  ` + ee : ""));
          }
        }
      }
      return i && QE(e, u, d), _;
    }
    function O5(e, t) {
      var a = t.state;
      typeof t.componentWillMount == "function" && t.componentWillMount(), typeof t.UNSAFE_componentWillMount == "function" && t.UNSAFE_componentWillMount(), a !== t.state && (m("%s.componentWillMount(): Assigning directly to this.state is deprecated (except inside a component's constructor). Use setState instead.", it(e) || "Component"), vS.enqueueReplaceState(t, t.state, null));
    }
    function iC(e, t, a, i) {
      var u = t.state;
      if (typeof t.componentWillReceiveProps == "function" && t.componentWillReceiveProps(a, i), typeof t.UNSAFE_componentWillReceiveProps == "function" && t.UNSAFE_componentWillReceiveProps(a, i), t.state !== u) {
        {
          var d = it(e) || "Component";
          iS.has(d) || (iS.add(d), m("%s.componentWillReceiveProps(): Assigning directly to this.state is deprecated (except inside a component's constructor). Use setState instead.", d));
        }
        vS.enqueueReplaceState(t, t.state, null);
      }
    }
    function hS(e, t, a, i) {
      D5(e, t, a);
      var u = e.stateNode;
      u.props = a, u.state = e.memoizedState, u.refs = {}, T0(e);
      var d = t.contextType;
      if (typeof d == "object" && d !== null)
        u.context = Yn(d);
      else {
        var h = If(e, t, !0);
        u.context = Yf(e, h);
      }
      {
        if (u.state === a) {
          var S = Ct(t) || "Component";
          cS.has(S) || (cS.add(S), m("%s: It is not recommended to assign props directly to state because updates to props won't be reflected in state. In most cases, it is better to use props directly.", S));
        }
        e.mode & vt && Gi.recordLegacyContextWarning(e, u), Gi.recordUnsafeLifecycleWarnings(e, u);
      }
      u.state = e.memoizedState;
      var b = t.getDerivedStateFromProps;
      if (typeof b == "function" && (pS(e, t, b, a), u.state = e.memoizedState), typeof t.getDerivedStateFromProps != "function" && typeof u.getSnapshotBeforeUpdate != "function" && (typeof u.UNSAFE_componentWillMount == "function" || typeof u.componentWillMount == "function") && (O5(e, u), Pm(e, a, u, i), u.state = e.memoizedState), typeof u.componentDidMount == "function") {
        var k = st;
        k |= Fr, (e.mode & oa) !== Fe && (k |= Hr), e.flags |= k;
      }
    }
    function M5(e, t, a, i) {
      var u = e.stateNode, d = e.memoizedProps;
      u.props = d;
      var h = u.context, S = t.contextType, b = Za;
      if (typeof S == "object" && S !== null)
        b = Yn(S);
      else {
        var k = If(e, t, !0);
        b = Yf(e, k);
      }
      var _ = t.getDerivedStateFromProps, P = typeof _ == "function" || typeof u.getSnapshotBeforeUpdate == "function";
      !P && (typeof u.UNSAFE_componentWillReceiveProps == "function" || typeof u.componentWillReceiveProps == "function") && (d !== a || h !== b) && iC(e, u, a, b), Eb();
      var z = e.memoizedState, q = u.state = z;
      if (Pm(e, a, u, i), q = e.memoizedState, d === a && z === q && !gm() && !Fm()) {
        if (typeof u.componentDidMount == "function") {
          var Z = st;
          Z |= Fr, (e.mode & oa) !== Fe && (Z |= Hr), e.flags |= Z;
        }
        return !1;
      }
      typeof _ == "function" && (pS(e, t, _, a), q = e.memoizedState);
      var ee = Fm() || nC(e, t, d, a, z, q, b);
      if (ee) {
        if (!P && (typeof u.UNSAFE_componentWillMount == "function" || typeof u.componentWillMount == "function") && (typeof u.componentWillMount == "function" && u.componentWillMount(), typeof u.UNSAFE_componentWillMount == "function" && u.UNSAFE_componentWillMount()), typeof u.componentDidMount == "function") {
          var we = st;
          we |= Fr, (e.mode & oa) !== Fe && (we |= Hr), e.flags |= we;
        }
      } else {
        if (typeof u.componentDidMount == "function") {
          var Qe = st;
          Qe |= Fr, (e.mode & oa) !== Fe && (Qe |= Hr), e.flags |= Qe;
        }
        e.memoizedProps = a, e.memoizedState = q;
      }
      return u.props = a, u.state = q, u.context = b, ee;
    }
    function L5(e, t, a, i, u) {
      var d = t.stateNode;
      Sb(e, t);
      var h = t.memoizedProps, S = t.type === t.elementType ? h : Ki(t.type, h);
      d.props = S;
      var b = t.pendingProps, k = d.context, _ = a.contextType, P = Za;
      if (typeof _ == "object" && _ !== null)
        P = Yn(_);
      else {
        var z = If(t, a, !0);
        P = Yf(t, z);
      }
      var q = a.getDerivedStateFromProps, Z = typeof q == "function" || typeof d.getSnapshotBeforeUpdate == "function";
      !Z && (typeof d.UNSAFE_componentWillReceiveProps == "function" || typeof d.componentWillReceiveProps == "function") && (h !== b || k !== P) && iC(t, d, i, P), Eb();
      var ee = t.memoizedState, we = d.state = ee;
      if (Pm(t, i, d, u), we = t.memoizedState, h === b && ee === we && !gm() && !Fm() && !Be)
        return typeof d.componentDidUpdate == "function" && (h !== e.memoizedProps || ee !== e.memoizedState) && (t.flags |= st), typeof d.getSnapshotBeforeUpdate == "function" && (h !== e.memoizedProps || ee !== e.memoizedState) && (t.flags |= Sa), !1;
      typeof q == "function" && (pS(t, a, q, i), we = t.memoizedState);
      var Qe = Fm() || nC(t, a, S, i, ee, we, P) || // TODO: In some cases, we'll end up checking if context has changed twice,
      // both before and after `shouldComponentUpdate` has been called. Not ideal,
      // but I'm loath to refactor this function. This only happens for memoized
      // components so it's not that common.
      Be;
      return Qe ? (!Z && (typeof d.UNSAFE_componentWillUpdate == "function" || typeof d.componentWillUpdate == "function") && (typeof d.componentWillUpdate == "function" && d.componentWillUpdate(i, we, P), typeof d.UNSAFE_componentWillUpdate == "function" && d.UNSAFE_componentWillUpdate(i, we, P)), typeof d.componentDidUpdate == "function" && (t.flags |= st), typeof d.getSnapshotBeforeUpdate == "function" && (t.flags |= Sa)) : (typeof d.componentDidUpdate == "function" && (h !== e.memoizedProps || ee !== e.memoizedState) && (t.flags |= st), typeof d.getSnapshotBeforeUpdate == "function" && (h !== e.memoizedProps || ee !== e.memoizedState) && (t.flags |= Sa), t.memoizedProps = i, t.memoizedState = we), d.props = i, d.state = we, d.context = P, Qe;
    }
    function uc(e, t) {
      return {
        value: e,
        source: t,
        stack: Gl(t),
        digest: null
      };
    }
    function mS(e, t, a) {
      return {
        value: e,
        source: null,
        stack: a ?? null,
        digest: t ?? null
      };
    }
    function N5(e, t) {
      return !0;
    }
    function yS(e, t) {
      try {
        var a = N5(e, t);
        if (a === !1)
          return;
        var i = t.value, u = t.source, d = t.stack, h = d !== null ? d : "";
        if (i != null && i._suppressLogging) {
          if (e.tag === D)
            return;
          console.error(i);
        }
        var S = u ? it(u) : null, b = S ? "The above error occurred in the <" + S + "> component:" : "The above error occurred in one of your React components:", k;
        if (e.tag === L)
          k = `Consider adding an error boundary to your tree to customize error handling behavior.
Visit https://reactjs.org/link/error-boundaries to learn more about error boundaries.`;
        else {
          var _ = it(e) || "Anonymous";
          k = "React will try to recreate this component tree from scratch " + ("using the error boundary you provided, " + _ + ".");
        }
        var P = b + `
` + h + `

` + ("" + k);
        console.error(P);
      } catch (z) {
        setTimeout(function() {
          throw z;
        });
      }
    }
    var A5 = typeof WeakMap == "function" ? WeakMap : Map;
    function oC(e, t, a) {
      var i = Ml(Kt, a);
      i.tag = C0, i.payload = {
        element: null
      };
      var u = t.value;
      return i.callback = function() {
        xD(u), yS(e, t);
      }, i;
    }
    function gS(e, t, a) {
      var i = Ml(Kt, a);
      i.tag = C0;
      var u = e.type.getDerivedStateFromError;
      if (typeof u == "function") {
        var d = t.value;
        i.payload = function() {
          return u(d);
        }, i.callback = function() {
          yw(e), yS(e, t);
        };
      }
      var h = e.stateNode;
      return h !== null && typeof h.componentDidCatch == "function" && (i.callback = function() {
        yw(e), yS(e, t), typeof u != "function" && TD(this);
        var b = t.value, k = t.stack;
        this.componentDidCatch(b, {
          componentStack: k !== null ? k : ""
        }), typeof u != "function" && (Yr(e.lanes, $e) || m("%s: Error boundaries should implement getDerivedStateFromError(). In that method, return a state update to display an error message or fallback UI.", it(e) || "Unknown"));
      }), i;
    }
    function lC(e, t, a) {
      var i = e.pingCache, u;
      if (i === null ? (i = e.pingCache = new A5(), u = /* @__PURE__ */ new Set(), i.set(t, u)) : (u = i.get(t), u === void 0 && (u = /* @__PURE__ */ new Set(), i.set(t, u))), !u.has(a)) {
        u.add(a);
        var d = kD.bind(null, e, t, a);
        ia && Tv(e, a), t.then(d, d);
      }
    }
    function z5(e, t, a, i) {
      var u = e.updateQueue;
      if (u === null) {
        var d = /* @__PURE__ */ new Set();
        d.add(a), e.updateQueue = d;
      } else
        u.add(a);
    }
    function U5(e, t) {
      var a = e.tag;
      if ((e.mode & je) === Fe && (a === T || a === fe || a === me)) {
        var i = e.alternate;
        i ? (e.updateQueue = i.updateQueue, e.memoizedState = i.memoizedState, e.lanes = i.lanes) : (e.updateQueue = null, e.memoizedState = null);
      }
    }
    function uC(e) {
      var t = e;
      do {
        if (t.tag === ne && m5(t))
          return t;
        t = t.return;
      } while (t !== null);
      return null;
    }
    function sC(e, t, a, i, u) {
      if ((e.mode & je) === Fe) {
        if (e === t)
          e.flags |= Vn;
        else {
          if (e.flags |= tt, a.flags |= _s, a.flags &= ~(Fc | na), a.tag === D) {
            var d = a.alternate;
            if (d === null)
              a.tag = Et;
            else {
              var h = Ml(Kt, $e);
              h.tag = Nm, Iu(a, h, $e);
            }
          }
          a.lanes = ut(a.lanes, $e);
        }
        return e;
      }
      return e.flags |= Vn, e.lanes = u, e;
    }
    function P5(e, t, a, i, u) {
      if (a.flags |= na, ia && Tv(e, u), i !== null && typeof i == "object" && typeof i.then == "function") {
        var d = i;
        U5(a), yr() && a.mode & je && eb();
        var h = uC(t);
        if (h !== null) {
          h.flags &= ~mn, sC(h, t, a, e, u), h.mode & je && lC(e, d, u), z5(h, e, d);
          return;
        } else {
          if (!cp(u)) {
            lC(e, d, u), KS();
            return;
          }
          var S = new Error("A component suspended while responding to synchronous input. This will cause the UI to be replaced with a loading indicator. To fix, updates that suspend should be wrapped with startTransition.");
          i = S;
        }
      } else if (yr() && a.mode & je) {
        eb();
        var b = uC(t);
        if (b !== null) {
          (b.flags & Vn) === Ye && (b.flags |= mn), sC(b, t, a, e, u), c0(uc(i, a));
          return;
        }
      }
      i = uc(i, a), mD(i);
      var k = t;
      do {
        switch (k.tag) {
          case L: {
            var _ = i;
            k.flags |= Vn;
            var P = bu(u);
            k.lanes = ut(k.lanes, P);
            var z = oC(k, _, P);
            R0(k, z);
            return;
          }
          case D:
            var q = i, Z = k.type, ee = k.stateNode;
            if ((k.flags & tt) === Ye && (typeof Z.getDerivedStateFromError == "function" || ee !== null && typeof ee.componentDidCatch == "function" && !uw(ee))) {
              k.flags |= Vn;
              var we = bu(u);
              k.lanes = ut(k.lanes, we);
              var Qe = gS(k, q, we);
              R0(k, Qe);
              return;
            }
            break;
        }
        k = k.return;
      } while (k !== null);
    }
    function F5() {
      return null;
    }
    var uv = c.ReactCurrentOwner, Zi = !1, SS, sv, ES, bS, CS, sc, wS, uy, cv;
    SS = {}, sv = {}, ES = {}, bS = {}, CS = {}, sc = !1, wS = {}, uy = {}, cv = {};
    function ua(e, t, a, i) {
      e === null ? t.child = db(t, null, a, i) : t.child = qf(t, e.child, a, i);
    }
    function H5(e, t, a, i) {
      t.child = qf(t, e.child, null, i), t.child = qf(t, null, a, i);
    }
    function cC(e, t, a, i, u) {
      if (t.type !== t.elementType) {
        var d = a.propTypes;
        d && Wi(
          d,
          i,
          // Resolved props
          "prop",
          Ct(a)
        );
      }
      var h = a.render, S = t.ref, b, k;
      Kf(t, u), fu(t);
      {
        if (uv.current = t, zr(!0), b = rd(e, t, h, i, S, u), k = ad(), t.mode & vt) {
          _n(!0);
          try {
            b = rd(e, t, h, i, S, u), k = ad();
          } finally {
            _n(!1);
          }
        }
        zr(!1);
      }
      return $r(), e !== null && !Zi ? (xb(e, t, u), Ll(e, t, u)) : (yr() && k && a0(t), t.flags |= Ro, ua(e, t, b, u), t.child);
    }
    function fC(e, t, a, i, u) {
      if (e === null) {
        var d = a.type;
        if (ID(d) && a.compare === null && // SimpleMemoComponent codepath doesn't resolve outer props either.
        a.defaultProps === void 0) {
          var h = d;
          return h = dd(d), t.tag = me, t.type = h, xS(t, d), dC(e, t, h, i, u);
        }
        {
          var S = d.propTypes;
          if (S && Wi(
            S,
            i,
            // Resolved props
            "prop",
            Ct(d)
          ), a.defaultProps !== void 0) {
            var b = Ct(d) || "Unknown";
            cv[b] || (m("%s: Support for defaultProps will be removed from memo components in a future major release. Use JavaScript default parameters instead.", b), cv[b] = !0);
          }
        }
        var k = u1(a.type, null, i, t, t.mode, u);
        return k.ref = t.ref, k.return = t, t.child = k, k;
      }
      {
        var _ = a.type, P = _.propTypes;
        P && Wi(
          P,
          i,
          // Resolved props
          "prop",
          Ct(_)
        );
      }
      var z = e.child, q = LS(e, u);
      if (!q) {
        var Z = z.memoizedProps, ee = a.compare;
        if (ee = ee !== null ? ee : Ze, ee(Z, i) && e.ref === t.ref)
          return Ll(e, t, u);
      }
      t.flags |= Ro;
      var we = vc(z, i);
      return we.ref = t.ref, we.return = t, t.child = we, we;
    }
    function dC(e, t, a, i, u) {
      if (t.type !== t.elementType) {
        var d = t.elementType;
        if (d.$$typeof === Qn) {
          var h = d, S = h._payload, b = h._init;
          try {
            d = b(S);
          } catch {
            d = null;
          }
          var k = d && d.propTypes;
          k && Wi(
            k,
            i,
            // Resolved (SimpleMemoComponent has no defaultProps)
            "prop",
            Ct(d)
          );
        }
      }
      if (e !== null) {
        var _ = e.memoizedProps;
        if (Ze(_, i) && e.ref === t.ref && // Prevent bailout if the implementation changed due to hot reload.
        t.type === e.type)
          if (Zi = !1, t.pendingProps = i = _, LS(e, u))
            (e.flags & _s) !== Ye && (Zi = !0);
          else return t.lanes = e.lanes, Ll(e, t, u);
      }
      return TS(e, t, a, i, u);
    }
    function pC(e, t, a) {
      var i = t.pendingProps, u = i.children, d = e !== null ? e.memoizedState : null;
      if (i.mode === "hidden" || Te)
        if ((t.mode & je) === Fe) {
          var h = {
            baseLanes: ie,
            cachePool: null,
            transitions: null
          };
          t.memoizedState = h, by(t, a);
        } else if (Yr(a, sr)) {
          var P = {
            baseLanes: ie,
            cachePool: null,
            transitions: null
          };
          t.memoizedState = P;
          var z = d !== null ? d.baseLanes : a;
          by(t, z);
        } else {
          var S = null, b;
          if (d !== null) {
            var k = d.baseLanes;
            b = ut(k, a);
          } else
            b = a;
          t.lanes = t.childLanes = sr;
          var _ = {
            baseLanes: b,
            cachePool: S,
            transitions: null
          };
          return t.memoizedState = _, t.updateQueue = null, by(t, b), null;
        }
      else {
        var q;
        d !== null ? (q = ut(d.baseLanes, a), t.memoizedState = null) : q = a, by(t, q);
      }
      return ua(e, t, u, a), t.child;
    }
    function j5(e, t, a) {
      var i = t.pendingProps;
      return ua(e, t, i, a), t.child;
    }
    function V5(e, t, a) {
      var i = t.pendingProps.children;
      return ua(e, t, i, a), t.child;
    }
    function $5(e, t, a) {
      {
        t.flags |= st;
        {
          var i = t.stateNode;
          i.effectDuration = 0, i.passiveEffectDuration = 0;
        }
      }
      var u = t.pendingProps, d = u.children;
      return ua(e, t, d, a), t.child;
    }
    function vC(e, t) {
      var a = t.ref;
      (e === null && a !== null || e !== null && e.ref !== a) && (t.flags |= Pr, t.flags |= Kd);
    }
    function TS(e, t, a, i, u) {
      if (t.type !== t.elementType) {
        var d = a.propTypes;
        d && Wi(
          d,
          i,
          // Resolved props
          "prop",
          Ct(a)
        );
      }
      var h;
      {
        var S = If(t, a, !0);
        h = Yf(t, S);
      }
      var b, k;
      Kf(t, u), fu(t);
      {
        if (uv.current = t, zr(!0), b = rd(e, t, a, i, h, u), k = ad(), t.mode & vt) {
          _n(!0);
          try {
            b = rd(e, t, a, i, h, u), k = ad();
          } finally {
            _n(!1);
          }
        }
        zr(!1);
      }
      return $r(), e !== null && !Zi ? (xb(e, t, u), Ll(e, t, u)) : (yr() && k && a0(t), t.flags |= Ro, ua(e, t, b, u), t.child);
    }
    function hC(e, t, a, i, u) {
      {
        switch (i2(t)) {
          case !1: {
            var d = t.stateNode, h = t.type, S = new h(t.memoizedProps, d.context), b = S.state;
            d.updater.enqueueSetState(d, b, null);
            break;
          }
          case !0: {
            t.flags |= tt, t.flags |= Vn;
            var k = new Error("Simulated error coming from DevTools"), _ = bu(u);
            t.lanes = ut(t.lanes, _);
            var P = gS(t, uc(k, t), _);
            R0(t, P);
            break;
          }
        }
        if (t.type !== t.elementType) {
          var z = a.propTypes;
          z && Wi(
            z,
            i,
            // Resolved props
            "prop",
            Ct(a)
          );
        }
      }
      var q;
      Fo(a) ? (q = !0, Em(t)) : q = !1, Kf(t, u);
      var Z = t.stateNode, ee;
      Z === null ? (cy(e, t), aC(t, a, i), hS(t, a, i, u), ee = !0) : e === null ? ee = M5(t, a, i, u) : ee = L5(e, t, a, i, u);
      var we = RS(e, t, a, ee, q, u);
      {
        var Qe = t.stateNode;
        ee && Qe.props !== i && (sc || m("It looks like %s is reassigning its own `this.props` while rendering. This is not supported and can lead to confusing bugs.", it(t) || "a component"), sc = !0);
      }
      return we;
    }
    function RS(e, t, a, i, u, d) {
      vC(e, t);
      var h = (t.flags & tt) !== Ye;
      if (!i && !h)
        return u && XE(t, a, !1), Ll(e, t, d);
      var S = t.stateNode;
      uv.current = t;
      var b;
      if (h && typeof a.getDerivedStateFromError != "function")
        b = null, Jb();
      else {
        fu(t);
        {
          if (zr(!0), b = S.render(), t.mode & vt) {
            _n(!0);
            try {
              S.render();
            } finally {
              _n(!1);
            }
          }
          zr(!1);
        }
        $r();
      }
      return t.flags |= Ro, e !== null && h ? H5(e, t, b, d) : ua(e, t, b, d), t.memoizedState = S.state, u && XE(t, a, !0), t.child;
    }
    function mC(e) {
      var t = e.stateNode;
      t.pendingContext ? GE(e, t.pendingContext, t.pendingContext !== t.context) : t.context && GE(e, t.context, !1), x0(e, t.containerInfo);
    }
    function B5(e, t, a) {
      if (mC(t), e === null)
        throw new Error("Should have a current fiber. This is a bug in React.");
      var i = t.pendingProps, u = t.memoizedState, d = u.element;
      Sb(e, t), Pm(t, i, null, a);
      var h = t.memoizedState;
      t.stateNode;
      var S = h.element;
      if (u.isDehydrated) {
        var b = {
          element: S,
          isDehydrated: !1,
          cache: h.cache,
          pendingSuspenseBoundaries: h.pendingSuspenseBoundaries,
          transitions: h.transitions
        }, k = t.updateQueue;
        if (k.baseState = b, t.memoizedState = b, t.flags & mn) {
          var _ = uc(new Error("There was an error while hydrating. Because the error happened outside of a Suspense boundary, the entire root will switch to client rendering."), t);
          return yC(e, t, S, a, _);
        } else if (S !== d) {
          var P = uc(new Error("This root received an early update, before anything was able hydrate. Switched the entire root to client rendering."), t);
          return yC(e, t, S, a, P);
        } else {
          Wk(t);
          var z = db(t, null, S, a);
          t.child = z;
          for (var q = z; q; )
            q.flags = q.flags & ~Jt | Ea, q = q.sibling;
        }
      } else {
        if (Gf(), S === d)
          return Ll(e, t, a);
        ua(e, t, S, a);
      }
      return t.child;
    }
    function yC(e, t, a, i, u) {
      return Gf(), c0(u), t.flags |= mn, ua(e, t, a, i), t.child;
    }
    function I5(e, t, a) {
      wb(t), e === null && s0(t);
      var i = t.type, u = t.pendingProps, d = e !== null ? e.memoizedProps : null, h = u.children, S = Ig(i, u);
      return S ? h = null : d !== null && Ig(i, d) && (t.flags |= Ht), vC(e, t), ua(e, t, h, a), t.child;
    }
    function Y5(e, t) {
      return e === null && s0(t), null;
    }
    function W5(e, t, a, i) {
      cy(e, t);
      var u = t.pendingProps, d = a, h = d._payload, S = d._init, b = S(h);
      t.type = b;
      var k = t.tag = YD(b), _ = Ki(b, u), P;
      switch (k) {
        case T:
          return xS(t, b), t.type = b = dd(b), P = TS(null, t, b, _, i), P;
        case D:
          return t.type = b = n1(b), P = hC(null, t, b, _, i), P;
        case fe:
          return t.type = b = r1(b), P = cC(null, t, b, _, i), P;
        case Ce: {
          if (t.type !== t.elementType) {
            var z = b.propTypes;
            z && Wi(
              z,
              _,
              // Resolved for outer only
              "prop",
              Ct(b)
            );
          }
          return P = fC(
            null,
            t,
            b,
            Ki(b.type, _),
            // The inner type can have defaults too
            i
          ), P;
        }
      }
      var q = "";
      throw b !== null && typeof b == "object" && b.$$typeof === Qn && (q = " Did you wrap a component in React.lazy() more than once?"), new Error("Element type is invalid. Received a promise that resolves to: " + b + ". " + ("Lazy element type must resolve to a class or function." + q));
    }
    function Q5(e, t, a, i, u) {
      cy(e, t), t.tag = D;
      var d;
      return Fo(a) ? (d = !0, Em(t)) : d = !1, Kf(t, u), aC(t, a, i), hS(t, a, i, u), RS(null, t, a, !0, d, u);
    }
    function G5(e, t, a, i) {
      cy(e, t);
      var u = t.pendingProps, d;
      {
        var h = If(t, a, !1);
        d = Yf(t, h);
      }
      Kf(t, i);
      var S, b;
      fu(t);
      {
        if (a.prototype && typeof a.prototype.render == "function") {
          var k = Ct(a) || "Unknown";
          SS[k] || (m("The <%s /> component appears to have a render method, but doesn't extend React.Component. This is likely to cause errors. Change %s to extend React.Component instead.", k, k), SS[k] = !0);
        }
        t.mode & vt && Gi.recordLegacyContextWarning(t, null), zr(!0), uv.current = t, S = rd(null, t, a, u, d, i), b = ad(), zr(!1);
      }
      if ($r(), t.flags |= Ro, typeof S == "object" && S !== null && typeof S.render == "function" && S.$$typeof === void 0) {
        var _ = Ct(a) || "Unknown";
        sv[_] || (m("The <%s /> component appears to be a function component that returns a class instance. Change %s to a class that extends React.Component instead. If you can't use a class try assigning the prototype on the function as a workaround. `%s.prototype = React.Component.prototype`. Don't use an arrow function since it cannot be called with `new` by React.", _, _, _), sv[_] = !0);
      }
      if (
        // Run these checks in production only if the flag is off.
        // Eventually we'll delete this branch altogether.
        typeof S == "object" && S !== null && typeof S.render == "function" && S.$$typeof === void 0
      ) {
        {
          var P = Ct(a) || "Unknown";
          sv[P] || (m("The <%s /> component appears to be a function component that returns a class instance. Change %s to a class that extends React.Component instead. If you can't use a class try assigning the prototype on the function as a workaround. `%s.prototype = React.Component.prototype`. Don't use an arrow function since it cannot be called with `new` by React.", P, P, P), sv[P] = !0);
        }
        t.tag = D, t.memoizedState = null, t.updateQueue = null;
        var z = !1;
        return Fo(a) ? (z = !0, Em(t)) : z = !1, t.memoizedState = S.state !== null && S.state !== void 0 ? S.state : null, T0(t), rC(t, S), hS(t, a, u, i), RS(null, t, a, !0, z, i);
      } else {
        if (t.tag = T, t.mode & vt) {
          _n(!0);
          try {
            S = rd(null, t, a, u, d, i), b = ad();
          } finally {
            _n(!1);
          }
        }
        return yr() && b && a0(t), ua(null, t, S, i), xS(t, a), t.child;
      }
    }
    function xS(e, t) {
      {
        if (t && t.childContextTypes && m("%s(...): childContextTypes cannot be defined on a function component.", t.displayName || t.name || "Component"), e.ref !== null) {
          var a = "", i = vr();
          i && (a += `

Check the render method of \`` + i + "`.");
          var u = i || "", d = e._debugSource;
          d && (u = d.fileName + ":" + d.lineNumber), CS[u] || (CS[u] = !0, m("Function components cannot be given refs. Attempts to access this ref will fail. Did you mean to use React.forwardRef()?%s", a));
        }
        if (t.defaultProps !== void 0) {
          var h = Ct(t) || "Unknown";
          cv[h] || (m("%s: Support for defaultProps will be removed from function components in a future major release. Use JavaScript default parameters instead.", h), cv[h] = !0);
        }
        if (typeof t.getDerivedStateFromProps == "function") {
          var S = Ct(t) || "Unknown";
          bS[S] || (m("%s: Function components do not support getDerivedStateFromProps.", S), bS[S] = !0);
        }
        if (typeof t.contextType == "object" && t.contextType !== null) {
          var b = Ct(t) || "Unknown";
          ES[b] || (m("%s: Function components do not support contextType.", b), ES[b] = !0);
        }
      }
    }
    var kS = {
      dehydrated: null,
      treeContext: null,
      retryLane: Dn
    };
    function _S(e) {
      return {
        baseLanes: e,
        cachePool: F5(),
        transitions: null
      };
    }
    function q5(e, t) {
      var a = null;
      return {
        baseLanes: ut(e.baseLanes, t),
        cachePool: a,
        transitions: e.transitions
      };
    }
    function X5(e, t, a, i) {
      if (t !== null) {
        var u = t.memoizedState;
        if (u === null)
          return !1;
      }
      return D0(e, Jp);
    }
    function K5(e, t) {
      return Fs(e.childLanes, t);
    }
    function gC(e, t, a) {
      var i = t.pendingProps;
      o2(t) && (t.flags |= tt);
      var u = qi.current, d = !1, h = (t.flags & tt) !== Ye;
      if (h || X5(u, e) ? (d = !0, t.flags &= ~tt) : (e === null || e.memoizedState !== null) && (u = h5(u, Rb)), u = Jf(u), Wu(t, u), e === null) {
        s0(t);
        var S = t.memoizedState;
        if (S !== null) {
          var b = S.dehydrated;
          if (b !== null)
            return n_(t, b);
        }
        var k = i.children, _ = i.fallback;
        if (d) {
          var P = Z5(t, k, _, a), z = t.child;
          return z.memoizedState = _S(a), t.memoizedState = kS, P;
        } else
          return DS(t, k);
      } else {
        var q = e.memoizedState;
        if (q !== null) {
          var Z = q.dehydrated;
          if (Z !== null)
            return r_(e, t, h, i, Z, q, a);
        }
        if (d) {
          var ee = i.fallback, we = i.children, Qe = e_(e, t, we, ee, a), Ve = t.child, _t = e.child.memoizedState;
          return Ve.memoizedState = _t === null ? _S(a) : q5(_t, a), Ve.childLanes = K5(e, a), t.memoizedState = kS, Qe;
        } else {
          var St = i.children, I = J5(e, t, St, a);
          return t.memoizedState = null, I;
        }
      }
    }
    function DS(e, t, a) {
      var i = e.mode, u = {
        mode: "visible",
        children: t
      }, d = OS(u, i);
      return d.return = e, e.child = d, d;
    }
    function Z5(e, t, a, i) {
      var u = e.mode, d = e.child, h = {
        mode: "hidden",
        children: t
      }, S, b;
      return (u & je) === Fe && d !== null ? (S = d, S.childLanes = ie, S.pendingProps = h, e.mode & nt && (S.actualDuration = 0, S.actualStartTime = -1, S.selfBaseDuration = 0, S.treeBaseDuration = 0), b = es(a, u, i, null)) : (S = OS(h, u), b = es(a, u, i, null)), S.return = e, b.return = e, S.sibling = b, e.child = S, b;
    }
    function OS(e, t, a) {
      return Sw(e, t, ie, null);
    }
    function SC(e, t) {
      return vc(e, t);
    }
    function J5(e, t, a, i) {
      var u = e.child, d = u.sibling, h = SC(u, {
        mode: "visible",
        children: a
      });
      if ((t.mode & je) === Fe && (h.lanes = i), h.return = t, h.sibling = null, d !== null) {
        var S = t.deletions;
        S === null ? (t.deletions = [d], t.flags |= Pt) : S.push(d);
      }
      return t.child = h, h;
    }
    function e_(e, t, a, i, u) {
      var d = t.mode, h = e.child, S = h.sibling, b = {
        mode: "hidden",
        children: a
      }, k;
      if (
        // In legacy mode, we commit the primary tree as if it successfully
        // completed, even though it's in an inconsistent state.
        (d & je) === Fe && // Make sure we're on the second pass, i.e. the primary child fragment was
        // already cloned. In legacy mode, the only case where this isn't true is
        // when DevTools forces us to display a fallback; we skip the first render
        // pass entirely and go straight to rendering the fallback. (In Concurrent
        // Mode, SuspenseList can also trigger this scenario, but this is a legacy-
        // only codepath.)
        t.child !== h
      ) {
        var _ = t.child;
        k = _, k.childLanes = ie, k.pendingProps = b, t.mode & nt && (k.actualDuration = 0, k.actualStartTime = -1, k.selfBaseDuration = h.selfBaseDuration, k.treeBaseDuration = h.treeBaseDuration), t.deletions = null;
      } else
        k = SC(h, b), k.subtreeFlags = h.subtreeFlags & Gn;
      var P;
      return S !== null ? P = vc(S, i) : (P = es(i, d, u, null), P.flags |= Jt), P.return = t, k.return = t, k.sibling = P, t.child = k, P;
    }
    function sy(e, t, a, i) {
      i !== null && c0(i), qf(t, e.child, null, a);
      var u = t.pendingProps, d = u.children, h = DS(t, d);
      return h.flags |= Jt, t.memoizedState = null, h;
    }
    function t_(e, t, a, i, u) {
      var d = t.mode, h = {
        mode: "visible",
        children: a
      }, S = OS(h, d), b = es(i, d, u, null);
      return b.flags |= Jt, S.return = t, b.return = t, S.sibling = b, t.child = S, (t.mode & je) !== Fe && qf(t, e.child, null, u), b;
    }
    function n_(e, t, a) {
      return (e.mode & je) === Fe ? (m("Cannot hydrate Suspense in legacy mode. Switch from ReactDOM.hydrate(element, container) to ReactDOMClient.hydrateRoot(container, <App />).render(element) or remove the Suspense components from the server rendered components."), e.lanes = $e) : Gg(t) ? e.lanes = Vi : e.lanes = sr, null;
    }
    function r_(e, t, a, i, u, d, h) {
      if (a)
        if (t.flags & mn) {
          t.flags &= ~mn;
          var I = mS(new Error("There was an error while hydrating this Suspense boundary. Switched to client rendering."));
          return sy(e, t, h, I);
        } else {
          if (t.memoizedState !== null)
            return t.child = e.child, t.flags |= tt, null;
          var te = i.children, Y = i.fallback, ce = t_(e, t, te, Y, h), _e = t.child;
          return _e.memoizedState = _S(h), t.memoizedState = kS, ce;
        }
      else {
        if (Ik(), (t.mode & je) === Fe)
          return sy(
            e,
            t,
            h,
            // TODO: When we delete legacy mode, we should make this error argument
            // required — every concurrent mode path that causes hydration to
            // de-opt to client rendering should have an error message.
            null
          );
        if (Gg(u)) {
          var S, b, k;
          {
            var _ = lk(u);
            S = _.digest, b = _.message, k = _.stack;
          }
          var P;
          b ? P = new Error(b) : P = new Error("The server could not finish this Suspense boundary, likely due to an error during server rendering. Switched to client rendering.");
          var z = mS(P, S, k);
          return sy(e, t, h, z);
        }
        var q = Yr(h, e.childLanes);
        if (Zi || q) {
          var Z = Ey();
          if (Z !== null) {
            var ee = zh(Z, h);
            if (ee !== Dn && ee !== d.retryLane) {
              d.retryLane = ee;
              var we = Kt;
              La(e, ee), ar(Z, e, ee, we);
            }
          }
          KS();
          var Qe = mS(new Error("This Suspense boundary received an update before it finished hydrating. This caused the boundary to switch to client rendering. The usual way to fix this is to wrap the original update in startTransition."));
          return sy(e, t, h, Qe);
        } else if ($E(u)) {
          t.flags |= tt, t.child = e.child;
          var Ve = _D.bind(null, e);
          return uk(u, Ve), null;
        } else {
          Qk(t, u, d.treeContext);
          var _t = i.children, St = DS(t, _t);
          return St.flags |= Ea, St;
        }
      }
    }
    function EC(e, t, a) {
      e.lanes = ut(e.lanes, t);
      var i = e.alternate;
      i !== null && (i.lanes = ut(i.lanes, t)), E0(e.return, t, a);
    }
    function a_(e, t, a) {
      for (var i = t; i !== null; ) {
        if (i.tag === ne) {
          var u = i.memoizedState;
          u !== null && EC(i, a, e);
        } else if (i.tag === ye)
          EC(i, a, e);
        else if (i.child !== null) {
          i.child.return = i, i = i.child;
          continue;
        }
        if (i === e)
          return;
        for (; i.sibling === null; ) {
          if (i.return === null || i.return === e)
            return;
          i = i.return;
        }
        i.sibling.return = i.return, i = i.sibling;
      }
    }
    function i_(e) {
      for (var t = e, a = null; t !== null; ) {
        var i = t.alternate;
        i !== null && Vm(i) === null && (a = t), t = t.sibling;
      }
      return a;
    }
    function o_(e) {
      if (e !== void 0 && e !== "forwards" && e !== "backwards" && e !== "together" && !wS[e])
        if (wS[e] = !0, typeof e == "string")
          switch (e.toLowerCase()) {
            case "together":
            case "forwards":
            case "backwards": {
              m('"%s" is not a valid value for revealOrder on <SuspenseList />. Use lowercase "%s" instead.', e, e.toLowerCase());
              break;
            }
            case "forward":
            case "backward": {
              m('"%s" is not a valid value for revealOrder on <SuspenseList />. React uses the -s suffix in the spelling. Use "%ss" instead.', e, e.toLowerCase());
              break;
            }
            default:
              m('"%s" is not a supported revealOrder on <SuspenseList />. Did you mean "together", "forwards" or "backwards"?', e);
              break;
          }
        else
          m('%s is not a supported value for revealOrder on <SuspenseList />. Did you mean "together", "forwards" or "backwards"?', e);
    }
    function l_(e, t) {
      e !== void 0 && !uy[e] && (e !== "collapsed" && e !== "hidden" ? (uy[e] = !0, m('"%s" is not a supported value for tail on <SuspenseList />. Did you mean "collapsed" or "hidden"?', e)) : t !== "forwards" && t !== "backwards" && (uy[e] = !0, m('<SuspenseList tail="%s" /> is only valid if revealOrder is "forwards" or "backwards". Did you mean to specify revealOrder="forwards"?', e)));
    }
    function bC(e, t) {
      {
        var a = wt(e), i = !a && typeof Ia(e) == "function";
        if (a || i) {
          var u = a ? "array" : "iterable";
          return m("A nested %s was passed to row #%s in <SuspenseList />. Wrap it in an additional SuspenseList to configure its revealOrder: <SuspenseList revealOrder=...> ... <SuspenseList revealOrder=...>{%s}</SuspenseList> ... </SuspenseList>", u, t, u), !1;
        }
      }
      return !0;
    }
    function u_(e, t) {
      if ((t === "forwards" || t === "backwards") && e !== void 0 && e !== null && e !== !1)
        if (wt(e)) {
          for (var a = 0; a < e.length; a++)
            if (!bC(e[a], a))
              return;
        } else {
          var i = Ia(e);
          if (typeof i == "function") {
            var u = i.call(e);
            if (u)
              for (var d = u.next(), h = 0; !d.done; d = u.next()) {
                if (!bC(d.value, h))
                  return;
                h++;
              }
          } else
            m('A single row was passed to a <SuspenseList revealOrder="%s" />. This is not useful since it needs multiple rows. Did you mean to pass multiple children or an array?', t);
        }
    }
    function MS(e, t, a, i, u) {
      var d = e.memoizedState;
      d === null ? e.memoizedState = {
        isBackwards: t,
        rendering: null,
        renderingStartTime: 0,
        last: i,
        tail: a,
        tailMode: u
      } : (d.isBackwards = t, d.rendering = null, d.renderingStartTime = 0, d.last = i, d.tail = a, d.tailMode = u);
    }
    function CC(e, t, a) {
      var i = t.pendingProps, u = i.revealOrder, d = i.tail, h = i.children;
      o_(u), l_(d, u), u_(h, u), ua(e, t, h, a);
      var S = qi.current, b = D0(S, Jp);
      if (b)
        S = O0(S, Jp), t.flags |= tt;
      else {
        var k = e !== null && (e.flags & tt) !== Ye;
        k && a_(t, t.child, a), S = Jf(S);
      }
      if (Wu(t, S), (t.mode & je) === Fe)
        t.memoizedState = null;
      else
        switch (u) {
          case "forwards": {
            var _ = i_(t.child), P;
            _ === null ? (P = t.child, t.child = null) : (P = _.sibling, _.sibling = null), MS(
              t,
              !1,
              // isBackwards
              P,
              _,
              d
            );
            break;
          }
          case "backwards": {
            var z = null, q = t.child;
            for (t.child = null; q !== null; ) {
              var Z = q.alternate;
              if (Z !== null && Vm(Z) === null) {
                t.child = q;
                break;
              }
              var ee = q.sibling;
              q.sibling = z, z = q, q = ee;
            }
            MS(
              t,
              !0,
              // isBackwards
              z,
              null,
              // last
              d
            );
            break;
          }
          case "together": {
            MS(
              t,
              !1,
              // isBackwards
              null,
              // tail
              null,
              // last
              void 0
            );
            break;
          }
          default:
            t.memoizedState = null;
        }
      return t.child;
    }
    function s_(e, t, a) {
      x0(t, t.stateNode.containerInfo);
      var i = t.pendingProps;
      return e === null ? t.child = qf(t, null, i, a) : ua(e, t, i, a), t.child;
    }
    var wC = !1;
    function c_(e, t, a) {
      var i = t.type, u = i._context, d = t.pendingProps, h = t.memoizedProps, S = d.value;
      {
        "value" in d || wC || (wC = !0, m("The `value` prop is required for the `<Context.Provider>`. Did you misspell it or forget to pass it?"));
        var b = t.type.propTypes;
        b && Wi(b, d, "prop", "Context.Provider");
      }
      if (hb(t, u, S), h !== null) {
        var k = h.value;
        if (Ne(k, S)) {
          if (h.children === d.children && !gm())
            return Ll(e, t, a);
        } else
          o5(t, u, a);
      }
      var _ = d.children;
      return ua(e, t, _, a), t.child;
    }
    var TC = !1;
    function f_(e, t, a) {
      var i = t.type;
      i._context === void 0 ? i !== i.Consumer && (TC || (TC = !0, m("Rendering <Context> directly is not supported and will be removed in a future major release. Did you mean to render <Context.Consumer> instead?"))) : i = i._context;
      var u = t.pendingProps, d = u.children;
      typeof d != "function" && m("A context consumer was rendered with multiple children, or a child that isn't a function. A context consumer expects a single child that is a function. If you did pass a function, make sure there is no trailing or leading whitespace around it."), Kf(t, a);
      var h = Yn(i);
      fu(t);
      var S;
      return uv.current = t, zr(!0), S = d(h), zr(!1), $r(), t.flags |= Ro, ua(e, t, S, a), t.child;
    }
    function fv() {
      Zi = !0;
    }
    function cy(e, t) {
      (t.mode & je) === Fe && e !== null && (e.alternate = null, t.alternate = null, t.flags |= Jt);
    }
    function Ll(e, t, a) {
      return e !== null && (t.dependencies = e.dependencies), Jb(), wv(t.lanes), Yr(a, t.childLanes) ? (a5(e, t), t.child) : null;
    }
    function d_(e, t, a) {
      {
        var i = t.return;
        if (i === null)
          throw new Error("Cannot swap the root fiber.");
        if (e.alternate = null, t.alternate = null, a.index = t.index, a.sibling = t.sibling, a.return = t.return, a.ref = t.ref, t === i.child)
          i.child = a;
        else {
          var u = i.child;
          if (u === null)
            throw new Error("Expected parent to have a child.");
          for (; u.sibling !== t; )
            if (u = u.sibling, u === null)
              throw new Error("Expected to find the previous sibling.");
          u.sibling = a;
        }
        var d = i.deletions;
        return d === null ? (i.deletions = [e], i.flags |= Pt) : d.push(e), a.flags |= Jt, a;
      }
    }
    function LS(e, t) {
      var a = e.lanes;
      return !!Yr(a, t);
    }
    function p_(e, t, a) {
      switch (t.tag) {
        case L:
          mC(t), t.stateNode, Gf();
          break;
        case F:
          wb(t);
          break;
        case D: {
          var i = t.type;
          Fo(i) && Em(t);
          break;
        }
        case j:
          x0(t, t.stateNode.containerInfo);
          break;
        case ve: {
          var u = t.memoizedProps.value, d = t.type._context;
          hb(t, d, u);
          break;
        }
        case oe:
          {
            var h = Yr(a, t.childLanes);
            h && (t.flags |= st);
            {
              var S = t.stateNode;
              S.effectDuration = 0, S.passiveEffectDuration = 0;
            }
          }
          break;
        case ne: {
          var b = t.memoizedState;
          if (b !== null) {
            if (b.dehydrated !== null)
              return Wu(t, Jf(qi.current)), t.flags |= tt, null;
            var k = t.child, _ = k.childLanes;
            if (Yr(a, _))
              return gC(e, t, a);
            Wu(t, Jf(qi.current));
            var P = Ll(e, t, a);
            return P !== null ? P.sibling : null;
          } else
            Wu(t, Jf(qi.current));
          break;
        }
        case ye: {
          var z = (e.flags & tt) !== Ye, q = Yr(a, t.childLanes);
          if (z) {
            if (q)
              return CC(e, t, a);
            t.flags |= tt;
          }
          var Z = t.memoizedState;
          if (Z !== null && (Z.rendering = null, Z.tail = null, Z.lastEffect = null), Wu(t, qi.current), q)
            break;
          return null;
        }
        case qe:
        case rt:
          return t.lanes = ie, pC(e, t, a);
      }
      return Ll(e, t, a);
    }
    function RC(e, t, a) {
      if (t._debugNeedsRemount && e !== null)
        return d_(e, t, u1(t.type, t.key, t.pendingProps, t._debugOwner || null, t.mode, t.lanes));
      if (e !== null) {
        var i = e.memoizedProps, u = t.pendingProps;
        if (i !== u || gm() || // Force a re-render if the implementation changed due to hot reload:
        t.type !== e.type)
          Zi = !0;
        else {
          var d = LS(e, a);
          if (!d && // If this is the second pass of an error or suspense boundary, there
          // may not be work scheduled on `current`, so we check for this flag.
          (t.flags & tt) === Ye)
            return Zi = !1, p_(e, t, a);
          (e.flags & _s) !== Ye ? Zi = !0 : Zi = !1;
        }
      } else if (Zi = !1, yr() && Fk(t)) {
        var h = t.index, S = Hk();
        JE(t, S, h);
      }
      switch (t.lanes = ie, t.tag) {
        case A:
          return G5(e, t, t.type, a);
        case We: {
          var b = t.elementType;
          return W5(e, t, b, a);
        }
        case T: {
          var k = t.type, _ = t.pendingProps, P = t.elementType === k ? _ : Ki(k, _);
          return TS(e, t, k, P, a);
        }
        case D: {
          var z = t.type, q = t.pendingProps, Z = t.elementType === z ? q : Ki(z, q);
          return hC(e, t, z, Z, a);
        }
        case L:
          return B5(e, t, a);
        case F:
          return I5(e, t, a);
        case $:
          return Y5(e, t);
        case ne:
          return gC(e, t, a);
        case j:
          return s_(e, t, a);
        case fe: {
          var ee = t.type, we = t.pendingProps, Qe = t.elementType === ee ? we : Ki(ee, we);
          return cC(e, t, ee, Qe, a);
        }
        case H:
          return j5(e, t, a);
        case G:
          return V5(e, t, a);
        case oe:
          return $5(e, t, a);
        case ve:
          return c_(e, t, a);
        case W:
          return f_(e, t, a);
        case Ce: {
          var Ve = t.type, _t = t.pendingProps, St = Ki(Ve, _t);
          if (t.type !== t.elementType) {
            var I = Ve.propTypes;
            I && Wi(
              I,
              St,
              // Resolved for outer only
              "prop",
              Ct(Ve)
            );
          }
          return St = Ki(Ve.type, St), fC(e, t, Ve, St, a);
        }
        case me:
          return dC(e, t, t.type, t.pendingProps, a);
        case Et: {
          var te = t.type, Y = t.pendingProps, ce = t.elementType === te ? Y : Ki(te, Y);
          return Q5(e, t, te, ce, a);
        }
        case ye:
          return CC(e, t, a);
        case Ue:
          break;
        case qe:
          return pC(e, t, a);
      }
      throw new Error("Unknown unit of work tag (" + t.tag + "). This error is likely caused by a bug in React. Please file an issue.");
    }
    function id(e) {
      e.flags |= st;
    }
    function xC(e) {
      e.flags |= Pr, e.flags |= Kd;
    }
    var kC, NS, _C, DC;
    kC = function(e, t, a, i) {
      for (var u = t.child; u !== null; ) {
        if (u.tag === F || u.tag === $)
          zx(e, u.stateNode);
        else if (u.tag !== j) {
          if (u.child !== null) {
            u.child.return = u, u = u.child;
            continue;
          }
        }
        if (u === t)
          return;
        for (; u.sibling === null; ) {
          if (u.return === null || u.return === t)
            return;
          u = u.return;
        }
        u.sibling.return = u.return, u = u.sibling;
      }
    }, NS = function(e, t) {
    }, _C = function(e, t, a, i, u) {
      var d = e.memoizedProps;
      if (d !== i) {
        var h = t.stateNode, S = k0(), b = Px(h, a, d, i, u, S);
        t.updateQueue = b, b && id(t);
      }
    }, DC = function(e, t, a, i) {
      a !== i && id(t);
    };
    function dv(e, t) {
      if (!yr())
        switch (e.tailMode) {
          case "hidden": {
            for (var a = e.tail, i = null; a !== null; )
              a.alternate !== null && (i = a), a = a.sibling;
            i === null ? e.tail = null : i.sibling = null;
            break;
          }
          case "collapsed": {
            for (var u = e.tail, d = null; u !== null; )
              u.alternate !== null && (d = u), u = u.sibling;
            d === null ? !t && e.tail !== null ? e.tail.sibling = null : e.tail = null : d.sibling = null;
            break;
          }
        }
    }
    function Sr(e) {
      var t = e.alternate !== null && e.alternate.child === e.child, a = ie, i = Ye;
      if (t) {
        if ((e.mode & nt) !== Fe) {
          for (var b = e.selfBaseDuration, k = e.child; k !== null; )
            a = ut(a, ut(k.lanes, k.childLanes)), i |= k.subtreeFlags & Gn, i |= k.flags & Gn, b += k.treeBaseDuration, k = k.sibling;
          e.treeBaseDuration = b;
        } else
          for (var _ = e.child; _ !== null; )
            a = ut(a, ut(_.lanes, _.childLanes)), i |= _.subtreeFlags & Gn, i |= _.flags & Gn, _.return = e, _ = _.sibling;
        e.subtreeFlags |= i;
      } else {
        if ((e.mode & nt) !== Fe) {
          for (var u = e.actualDuration, d = e.selfBaseDuration, h = e.child; h !== null; )
            a = ut(a, ut(h.lanes, h.childLanes)), i |= h.subtreeFlags, i |= h.flags, u += h.actualDuration, d += h.treeBaseDuration, h = h.sibling;
          e.actualDuration = u, e.treeBaseDuration = d;
        } else
          for (var S = e.child; S !== null; )
            a = ut(a, ut(S.lanes, S.childLanes)), i |= S.subtreeFlags, i |= S.flags, S.return = e, S = S.sibling;
        e.subtreeFlags |= i;
      }
      return e.childLanes = a, t;
    }
    function v_(e, t, a) {
      if (Zk() && (t.mode & je) !== Fe && (t.flags & tt) === Ye)
        return ob(t), Gf(), t.flags |= mn | na | Vn, !1;
      var i = Rm(t);
      if (a !== null && a.dehydrated !== null)
        if (e === null) {
          if (!i)
            throw new Error("A dehydrated suspense component was completed without a hydrated node. This is probably a bug in React.");
          if (Xk(t), Sr(t), (t.mode & nt) !== Fe) {
            var u = a !== null;
            if (u) {
              var d = t.child;
              d !== null && (t.treeBaseDuration -= d.treeBaseDuration);
            }
          }
          return !1;
        } else {
          if (Gf(), (t.flags & tt) === Ye && (t.memoizedState = null), t.flags |= st, Sr(t), (t.mode & nt) !== Fe) {
            var h = a !== null;
            if (h) {
              var S = t.child;
              S !== null && (t.treeBaseDuration -= S.treeBaseDuration);
            }
          }
          return !1;
        }
      else
        return lb(), !0;
    }
    function OC(e, t, a) {
      var i = t.pendingProps;
      switch (i0(t), t.tag) {
        case A:
        case We:
        case me:
        case T:
        case fe:
        case H:
        case G:
        case oe:
        case W:
        case Ce:
          return Sr(t), null;
        case D: {
          var u = t.type;
          return Fo(u) && Sm(t), Sr(t), null;
        }
        case L: {
          var d = t.stateNode;
          if (Zf(t), t0(t), L0(), d.pendingContext && (d.context = d.pendingContext, d.pendingContext = null), e === null || e.child === null) {
            var h = Rm(t);
            if (h)
              id(t);
            else if (e !== null) {
              var S = e.memoizedState;
              // Check if this is a client root
              (!S.isDehydrated || // Check if we reverted to client rendering (e.g. due to an error)
              (t.flags & mn) !== Ye) && (t.flags |= Sa, lb());
            }
          }
          return NS(e, t), Sr(t), null;
        }
        case F: {
          _0(t);
          var b = Cb(), k = t.type;
          if (e !== null && t.stateNode != null)
            _C(e, t, k, i, b), e.ref !== t.ref && xC(t);
          else {
            if (!i) {
              if (t.stateNode === null)
                throw new Error("We must have new props for new mounts. This error is likely caused by a bug in React. Please file an issue.");
              return Sr(t), null;
            }
            var _ = k0(), P = Rm(t);
            if (P)
              Gk(t, b, _) && id(t);
            else {
              var z = Ax(k, i, b, _, t);
              kC(z, t, !1, !1), t.stateNode = z, Ux(z, k, i, b) && id(t);
            }
            t.ref !== null && xC(t);
          }
          return Sr(t), null;
        }
        case $: {
          var q = i;
          if (e && t.stateNode != null) {
            var Z = e.memoizedProps;
            DC(e, t, Z, q);
          } else {
            if (typeof q != "string" && t.stateNode === null)
              throw new Error("We must have new props for new mounts. This error is likely caused by a bug in React. Please file an issue.");
            var ee = Cb(), we = k0(), Qe = Rm(t);
            Qe ? qk(t) && id(t) : t.stateNode = Fx(q, ee, we, t);
          }
          return Sr(t), null;
        }
        case ne: {
          ed(t);
          var Ve = t.memoizedState;
          if (e === null || e.memoizedState !== null && e.memoizedState.dehydrated !== null) {
            var _t = v_(e, t, Ve);
            if (!_t)
              return t.flags & Vn ? t : null;
          }
          if ((t.flags & tt) !== Ye)
            return t.lanes = a, (t.mode & nt) !== Fe && rS(t), t;
          var St = Ve !== null, I = e !== null && e.memoizedState !== null;
          if (St !== I && St) {
            var te = t.child;
            if (te.flags |= xo, (t.mode & je) !== Fe) {
              var Y = e === null && (t.memoizedProps.unstable_avoidThisFallback !== !0 || !V);
              Y || D0(qi.current, Rb) ? hD() : KS();
            }
          }
          var ce = t.updateQueue;
          if (ce !== null && (t.flags |= st), Sr(t), (t.mode & nt) !== Fe && St) {
            var _e = t.child;
            _e !== null && (t.treeBaseDuration -= _e.treeBaseDuration);
          }
          return null;
        }
        case j:
          return Zf(t), NS(e, t), e === null && Mk(t.stateNode.containerInfo), Sr(t), null;
        case ve:
          var Re = t.type._context;
          return S0(Re, t), Sr(t), null;
        case Et: {
          var et = t.type;
          return Fo(et) && Sm(t), Sr(t), null;
        }
        case ye: {
          ed(t);
          var lt = t.memoizedState;
          if (lt === null)
            return Sr(t), null;
          var Yt = (t.flags & tt) !== Ye, zt = lt.rendering;
          if (zt === null)
            if (Yt)
              dv(lt, !1);
            else {
              var Pn = yD() && (e === null || (e.flags & tt) === Ye);
              if (!Pn)
                for (var Ut = t.child; Ut !== null; ) {
                  var On = Vm(Ut);
                  if (On !== null) {
                    Yt = !0, t.flags |= tt, dv(lt, !1);
                    var Xr = On.updateQueue;
                    return Xr !== null && (t.updateQueue = Xr, t.flags |= st), t.subtreeFlags = Ye, i5(t, a), Wu(t, O0(qi.current, Jp)), t.child;
                  }
                  Ut = Ut.sibling;
                }
              lt.tail !== null && fn() > KC() && (t.flags |= tt, Yt = !0, dv(lt, !1), t.lanes = Th);
            }
          else {
            if (!Yt) {
              var Tr = Vm(zt);
              if (Tr !== null) {
                t.flags |= tt, Yt = !0;
                var ei = Tr.updateQueue;
                if (ei !== null && (t.updateQueue = ei, t.flags |= st), dv(lt, !0), lt.tail === null && lt.tailMode === "hidden" && !zt.alternate && !yr())
                  return Sr(t), null;
              } else // The time it took to render last row is greater than the remaining
              // time we have to render. So rendering one more row would likely
              // exceed it.
              fn() * 2 - lt.renderingStartTime > KC() && a !== sr && (t.flags |= tt, Yt = !0, dv(lt, !1), t.lanes = Th);
            }
            if (lt.isBackwards)
              zt.sibling = t.child, t.child = zt;
            else {
              var fa = lt.last;
              fa !== null ? fa.sibling = zt : t.child = zt, lt.last = zt;
            }
          }
          if (lt.tail !== null) {
            var da = lt.tail;
            lt.rendering = da, lt.tail = da.sibling, lt.renderingStartTime = fn(), da.sibling = null;
            var Kr = qi.current;
            return Yt ? Kr = O0(Kr, Jp) : Kr = Jf(Kr), Wu(t, Kr), da;
          }
          return Sr(t), null;
        }
        case Ue:
          break;
        case qe:
        case rt: {
          XS(t);
          var Pl = t.memoizedState, pd = Pl !== null;
          if (e !== null) {
            var _v = e.memoizedState, Wo = _v !== null;
            Wo !== pd && // LegacyHidden doesn't do any hiding — it only pre-renders.
            !Te && (t.flags |= xo);
          }
          return !pd || (t.mode & je) === Fe ? Sr(t) : Yr(Yo, sr) && (Sr(t), t.subtreeFlags & (Jt | st) && (t.flags |= xo)), null;
        }
        case Ot:
          return null;
        case mt:
          return null;
      }
      throw new Error("Unknown unit of work tag (" + t.tag + "). This error is likely caused by a bug in React. Please file an issue.");
    }
    function h_(e, t, a) {
      switch (i0(t), t.tag) {
        case D: {
          var i = t.type;
          Fo(i) && Sm(t);
          var u = t.flags;
          return u & Vn ? (t.flags = u & ~Vn | tt, (t.mode & nt) !== Fe && rS(t), t) : null;
        }
        case L: {
          t.stateNode, Zf(t), t0(t), L0();
          var d = t.flags;
          return (d & Vn) !== Ye && (d & tt) === Ye ? (t.flags = d & ~Vn | tt, t) : null;
        }
        case F:
          return _0(t), null;
        case ne: {
          ed(t);
          var h = t.memoizedState;
          if (h !== null && h.dehydrated !== null) {
            if (t.alternate === null)
              throw new Error("Threw in newly mounted dehydrated component. This is likely a bug in React. Please file an issue.");
            Gf();
          }
          var S = t.flags;
          return S & Vn ? (t.flags = S & ~Vn | tt, (t.mode & nt) !== Fe && rS(t), t) : null;
        }
        case ye:
          return ed(t), null;
        case j:
          return Zf(t), null;
        case ve:
          var b = t.type._context;
          return S0(b, t), null;
        case qe:
        case rt:
          return XS(t), null;
        case Ot:
          return null;
        default:
          return null;
      }
    }
    function MC(e, t, a) {
      switch (i0(t), t.tag) {
        case D: {
          var i = t.type.childContextTypes;
          i != null && Sm(t);
          break;
        }
        case L: {
          t.stateNode, Zf(t), t0(t), L0();
          break;
        }
        case F: {
          _0(t);
          break;
        }
        case j:
          Zf(t);
          break;
        case ne:
          ed(t);
          break;
        case ye:
          ed(t);
          break;
        case ve:
          var u = t.type._context;
          S0(u, t);
          break;
        case qe:
        case rt:
          XS(t);
          break;
      }
    }
    var LC = null;
    LC = /* @__PURE__ */ new Set();
    var fy = !1, Er = !1, m_ = typeof WeakSet == "function" ? WeakSet : Set, Ae = null, od = null, ld = null;
    function y_(e) {
      ul(null, function() {
        throw e;
      }), qd();
    }
    var g_ = function(e, t) {
      if (t.props = e.memoizedProps, t.state = e.memoizedState, e.mode & nt)
        try {
          Bo(), t.componentWillUnmount();
        } finally {
          $o(e);
        }
      else
        t.componentWillUnmount();
    };
    function NC(e, t) {
      try {
        qu(Zn, e);
      } catch (a) {
        nn(e, t, a);
      }
    }
    function AS(e, t, a) {
      try {
        g_(e, a);
      } catch (i) {
        nn(e, t, i);
      }
    }
    function S_(e, t, a) {
      try {
        a.componentDidMount();
      } catch (i) {
        nn(e, t, i);
      }
    }
    function AC(e, t) {
      try {
        UC(e);
      } catch (a) {
        nn(e, t, a);
      }
    }
    function ud(e, t) {
      var a = e.ref;
      if (a !== null)
        if (typeof a == "function") {
          var i;
          try {
            if (yt && Lt && e.mode & nt)
              try {
                Bo(), i = a(null);
              } finally {
                $o(e);
              }
            else
              i = a(null);
          } catch (u) {
            nn(e, t, u);
          }
          typeof i == "function" && m("Unexpected return value from a callback ref in %s. A callback ref should not return a function.", it(e));
        } else
          a.current = null;
    }
    function dy(e, t, a) {
      try {
        a();
      } catch (i) {
        nn(e, t, i);
      }
    }
    var zC = !1;
    function E_(e, t) {
      Lx(e.containerInfo), Ae = t, b_();
      var a = zC;
      return zC = !1, a;
    }
    function b_() {
      for (; Ae !== null; ) {
        var e = Ae, t = e.child;
        (e.subtreeFlags & uu) !== Ye && t !== null ? (t.return = e, Ae = t) : C_();
      }
    }
    function C_() {
      for (; Ae !== null; ) {
        var e = Ae;
        Ft(e);
        try {
          w_(e);
        } catch (a) {
          nn(e, e.return, a);
        }
        hn();
        var t = e.sibling;
        if (t !== null) {
          t.return = e.return, Ae = t;
          return;
        }
        Ae = e.return;
      }
    }
    function w_(e) {
      var t = e.alternate, a = e.flags;
      if ((a & Sa) !== Ye) {
        switch (Ft(e), e.tag) {
          case T:
          case fe:
          case me:
            break;
          case D: {
            if (t !== null) {
              var i = t.memoizedProps, u = t.memoizedState, d = e.stateNode;
              e.type === e.elementType && !sc && (d.props !== e.memoizedProps && m("Expected %s props to match memoized props before getSnapshotBeforeUpdate. This might either be because of a bug in React, or because a component reassigns its own `this.props`. Please file an issue.", it(e) || "instance"), d.state !== e.memoizedState && m("Expected %s state to match memoized state before getSnapshotBeforeUpdate. This might either be because of a bug in React, or because a component reassigns its own `this.state`. Please file an issue.", it(e) || "instance"));
              var h = d.getSnapshotBeforeUpdate(e.elementType === e.type ? i : Ki(e.type, i), u);
              {
                var S = LC;
                h === void 0 && !S.has(e.type) && (S.add(e.type), m("%s.getSnapshotBeforeUpdate(): A snapshot value (or null) must be returned. You have returned undefined.", it(e)));
              }
              d.__reactInternalSnapshotBeforeUpdate = h;
            }
            break;
          }
          case L: {
            {
              var b = e.stateNode;
              rk(b.containerInfo);
            }
            break;
          }
          case F:
          case $:
          case j:
          case Et:
            break;
          default:
            throw new Error("This unit of work tag should not have side-effects. This error is likely caused by a bug in React. Please file an issue.");
        }
        hn();
      }
    }
    function Ji(e, t, a) {
      var i = t.updateQueue, u = i !== null ? i.lastEffect : null;
      if (u !== null) {
        var d = u.next, h = d;
        do {
          if ((h.tag & e) === e) {
            var S = h.destroy;
            h.destroy = void 0, S !== void 0 && ((e & gr) !== Na ? bh(t) : (e & Zn) !== Na && Ka(t), (e & Ho) !== Na && Rv(!0), dy(t, a, S), (e & Ho) !== Na && Rv(!1), (e & gr) !== Na ? Qc() : (e & Zn) !== Na && du());
          }
          h = h.next;
        } while (h !== d);
      }
    }
    function qu(e, t) {
      var a = t.updateQueue, i = a !== null ? a.lastEffect : null;
      if (i !== null) {
        var u = i.next, d = u;
        do {
          if ((d.tag & e) === e) {
            (e & gr) !== Na ? Do(t) : (e & Zn) !== Na && Ch(t);
            var h = d.create;
            (e & Ho) !== Na && Rv(!0), d.destroy = h(), (e & Ho) !== Na && Rv(!1), (e & gr) !== Na ? Wc() : (e & Zn) !== Na && Ds();
            {
              var S = d.destroy;
              if (S !== void 0 && typeof S != "function") {
                var b = void 0;
                (d.tag & Zn) !== Ye ? b = "useLayoutEffect" : (d.tag & Ho) !== Ye ? b = "useInsertionEffect" : b = "useEffect";
                var k = void 0;
                S === null ? k = " You returned null. If your effect does not require clean up, return undefined (or nothing)." : typeof S.then == "function" ? k = `

It looks like you wrote ` + b + `(async () => ...) or returned a Promise. Instead, write the async function inside your effect and call it immediately:

` + b + `(() => {
  async function fetchData() {
    // You can await here
    const response = await MyAPI.getData(someId);
    // ...
  }
  fetchData();
}, [someId]); // Or [] if effect doesn't need props or state

Learn more about data fetching with Hooks: https://reactjs.org/link/hooks-data-fetching` : k = " You returned: " + S, m("%s must not return anything besides a function, which is used for clean-up.%s", b, k);
              }
            }
          }
          d = d.next;
        } while (d !== u);
      }
    }
    function T_(e, t) {
      if ((t.flags & st) !== Ye)
        switch (t.tag) {
          case oe: {
            var a = t.stateNode.passiveEffectDuration, i = t.memoizedProps, u = i.id, d = i.onPostCommit, h = Kb(), S = t.alternate === null ? "mount" : "update";
            Xb() && (S = "nested-update"), typeof d == "function" && d(u, S, a, h);
            var b = t.return;
            e: for (; b !== null; ) {
              switch (b.tag) {
                case L:
                  var k = b.stateNode;
                  k.passiveEffectDuration += a;
                  break e;
                case oe:
                  var _ = b.stateNode;
                  _.passiveEffectDuration += a;
                  break e;
              }
              b = b.return;
            }
            break;
          }
        }
    }
    function R_(e, t, a, i) {
      if ((a.flags & lr) !== Ye)
        switch (a.tag) {
          case T:
          case fe:
          case me: {
            if (!Er)
              if (a.mode & nt)
                try {
                  Bo(), qu(Zn | Kn, a);
                } finally {
                  $o(a);
                }
              else
                qu(Zn | Kn, a);
            break;
          }
          case D: {
            var u = a.stateNode;
            if (a.flags & st && !Er)
              if (t === null)
                if (a.type === a.elementType && !sc && (u.props !== a.memoizedProps && m("Expected %s props to match memoized props before componentDidMount. This might either be because of a bug in React, or because a component reassigns its own `this.props`. Please file an issue.", it(a) || "instance"), u.state !== a.memoizedState && m("Expected %s state to match memoized state before componentDidMount. This might either be because of a bug in React, or because a component reassigns its own `this.state`. Please file an issue.", it(a) || "instance")), a.mode & nt)
                  try {
                    Bo(), u.componentDidMount();
                  } finally {
                    $o(a);
                  }
                else
                  u.componentDidMount();
              else {
                var d = a.elementType === a.type ? t.memoizedProps : Ki(a.type, t.memoizedProps), h = t.memoizedState;
                if (a.type === a.elementType && !sc && (u.props !== a.memoizedProps && m("Expected %s props to match memoized props before componentDidUpdate. This might either be because of a bug in React, or because a component reassigns its own `this.props`. Please file an issue.", it(a) || "instance"), u.state !== a.memoizedState && m("Expected %s state to match memoized state before componentDidUpdate. This might either be because of a bug in React, or because a component reassigns its own `this.state`. Please file an issue.", it(a) || "instance")), a.mode & nt)
                  try {
                    Bo(), u.componentDidUpdate(d, h, u.__reactInternalSnapshotBeforeUpdate);
                  } finally {
                    $o(a);
                  }
                else
                  u.componentDidUpdate(d, h, u.__reactInternalSnapshotBeforeUpdate);
              }
            var S = a.updateQueue;
            S !== null && (a.type === a.elementType && !sc && (u.props !== a.memoizedProps && m("Expected %s props to match memoized props before processing the update queue. This might either be because of a bug in React, or because a component reassigns its own `this.props`. Please file an issue.", it(a) || "instance"), u.state !== a.memoizedState && m("Expected %s state to match memoized state before processing the update queue. This might either be because of a bug in React, or because a component reassigns its own `this.state`. Please file an issue.", it(a) || "instance")), bb(a, S, u));
            break;
          }
          case L: {
            var b = a.updateQueue;
            if (b !== null) {
              var k = null;
              if (a.child !== null)
                switch (a.child.tag) {
                  case F:
                    k = a.child.stateNode;
                    break;
                  case D:
                    k = a.child.stateNode;
                    break;
                }
              bb(a, b, k);
            }
            break;
          }
          case F: {
            var _ = a.stateNode;
            if (t === null && a.flags & st) {
              var P = a.type, z = a.memoizedProps;
              Bx(_, P, z);
            }
            break;
          }
          case $:
            break;
          case j:
            break;
          case oe: {
            {
              var q = a.memoizedProps, Z = q.onCommit, ee = q.onRender, we = a.stateNode.effectDuration, Qe = Kb(), Ve = t === null ? "mount" : "update";
              Xb() && (Ve = "nested-update"), typeof ee == "function" && ee(a.memoizedProps.id, Ve, a.actualDuration, a.treeBaseDuration, a.actualStartTime, Qe);
              {
                typeof Z == "function" && Z(a.memoizedProps.id, Ve, we, Qe), CD(a);
                var _t = a.return;
                e: for (; _t !== null; ) {
                  switch (_t.tag) {
                    case L:
                      var St = _t.stateNode;
                      St.effectDuration += we;
                      break e;
                    case oe:
                      var I = _t.stateNode;
                      I.effectDuration += we;
                      break e;
                  }
                  _t = _t.return;
                }
              }
            }
            break;
          }
          case ne: {
            N_(e, a);
            break;
          }
          case ye:
          case Et:
          case Ue:
          case qe:
          case rt:
          case mt:
            break;
          default:
            throw new Error("This unit of work tag should not have side-effects. This error is likely caused by a bug in React. Please file an issue.");
        }
      Er || a.flags & Pr && UC(a);
    }
    function x_(e) {
      switch (e.tag) {
        case T:
        case fe:
        case me: {
          if (e.mode & nt)
            try {
              Bo(), NC(e, e.return);
            } finally {
              $o(e);
            }
          else
            NC(e, e.return);
          break;
        }
        case D: {
          var t = e.stateNode;
          typeof t.componentDidMount == "function" && S_(e, e.return, t), AC(e, e.return);
          break;
        }
        case F: {
          AC(e, e.return);
          break;
        }
      }
    }
    function k_(e, t) {
      for (var a = null, i = e; ; ) {
        if (i.tag === F) {
          if (a === null) {
            a = i;
            try {
              var u = i.stateNode;
              t ? Jx(u) : tk(i.stateNode, i.memoizedProps);
            } catch (h) {
              nn(e, e.return, h);
            }
          }
        } else if (i.tag === $) {
          if (a === null)
            try {
              var d = i.stateNode;
              t ? ek(d) : nk(d, i.memoizedProps);
            } catch (h) {
              nn(e, e.return, h);
            }
        } else if (!((i.tag === qe || i.tag === rt) && i.memoizedState !== null && i !== e)) {
          if (i.child !== null) {
            i.child.return = i, i = i.child;
            continue;
          }
        }
        if (i === e)
          return;
        for (; i.sibling === null; ) {
          if (i.return === null || i.return === e)
            return;
          a === i && (a = null), i = i.return;
        }
        a === i && (a = null), i.sibling.return = i.return, i = i.sibling;
      }
    }
    function UC(e) {
      var t = e.ref;
      if (t !== null) {
        var a = e.stateNode, i;
        switch (e.tag) {
          case F:
            i = a;
            break;
          default:
            i = a;
        }
        if (typeof t == "function") {
          var u;
          if (e.mode & nt)
            try {
              Bo(), u = t(i);
            } finally {
              $o(e);
            }
          else
            u = t(i);
          typeof u == "function" && m("Unexpected return value from a callback ref in %s. A callback ref should not return a function.", it(e));
        } else
          t.hasOwnProperty("current") || m("Unexpected ref object provided for %s. Use either a ref-setter function or React.createRef().", it(e)), t.current = i;
      }
    }
    function __(e) {
      var t = e.alternate;
      t !== null && (t.return = null), e.return = null;
    }
    function PC(e) {
      var t = e.alternate;
      t !== null && (e.alternate = null, PC(t));
      {
        if (e.child = null, e.deletions = null, e.sibling = null, e.tag === F) {
          var a = e.stateNode;
          a !== null && Ak(a);
        }
        e.stateNode = null, e._debugOwner = null, e.return = null, e.dependencies = null, e.memoizedProps = null, e.memoizedState = null, e.pendingProps = null, e.stateNode = null, e.updateQueue = null;
      }
    }
    function D_(e) {
      for (var t = e.return; t !== null; ) {
        if (FC(t))
          return t;
        t = t.return;
      }
      throw new Error("Expected to find a host parent. This error is likely caused by a bug in React. Please file an issue.");
    }
    function FC(e) {
      return e.tag === F || e.tag === L || e.tag === j;
    }
    function HC(e) {
      var t = e;
      e: for (; ; ) {
        for (; t.sibling === null; ) {
          if (t.return === null || FC(t.return))
            return null;
          t = t.return;
        }
        for (t.sibling.return = t.return, t = t.sibling; t.tag !== F && t.tag !== $ && t.tag !== Rt; ) {
          if (t.flags & Jt || t.child === null || t.tag === j)
            continue e;
          t.child.return = t, t = t.child;
        }
        if (!(t.flags & Jt))
          return t.stateNode;
      }
    }
    function O_(e) {
      var t = D_(e);
      switch (t.tag) {
        case F: {
          var a = t.stateNode;
          t.flags & Ht && (VE(a), t.flags &= ~Ht);
          var i = HC(e);
          US(e, i, a);
          break;
        }
        case L:
        case j: {
          var u = t.stateNode.containerInfo, d = HC(e);
          zS(e, d, u);
          break;
        }
        default:
          throw new Error("Invalid host parent fiber. This error is likely caused by a bug in React. Please file an issue.");
      }
    }
    function zS(e, t, a) {
      var i = e.tag, u = i === F || i === $;
      if (u) {
        var d = e.stateNode;
        t ? qx(a, d, t) : Qx(a, d);
      } else if (i !== j) {
        var h = e.child;
        if (h !== null) {
          zS(h, t, a);
          for (var S = h.sibling; S !== null; )
            zS(S, t, a), S = S.sibling;
        }
      }
    }
    function US(e, t, a) {
      var i = e.tag, u = i === F || i === $;
      if (u) {
        var d = e.stateNode;
        t ? Gx(a, d, t) : Wx(a, d);
      } else if (i !== j) {
        var h = e.child;
        if (h !== null) {
          US(h, t, a);
          for (var S = h.sibling; S !== null; )
            US(S, t, a), S = S.sibling;
        }
      }
    }
    var br = null, eo = !1;
    function M_(e, t, a) {
      {
        var i = t;
        e: for (; i !== null; ) {
          switch (i.tag) {
            case F: {
              br = i.stateNode, eo = !1;
              break e;
            }
            case L: {
              br = i.stateNode.containerInfo, eo = !0;
              break e;
            }
            case j: {
              br = i.stateNode.containerInfo, eo = !0;
              break e;
            }
          }
          i = i.return;
        }
        if (br === null)
          throw new Error("Expected to find a host parent. This error is likely caused by a bug in React. Please file an issue.");
        jC(e, t, a), br = null, eo = !1;
      }
      __(a);
    }
    function Xu(e, t, a) {
      for (var i = a.child; i !== null; )
        jC(e, t, i), i = i.sibling;
    }
    function jC(e, t, a) {
      switch (fl(a), a.tag) {
        case F:
          Er || ud(a, t);
        case $: {
          {
            var i = br, u = eo;
            br = null, Xu(e, t, a), br = i, eo = u, br !== null && (eo ? Kx(br, a.stateNode) : Xx(br, a.stateNode));
          }
          return;
        }
        case Rt: {
          br !== null && (eo ? Zx(br, a.stateNode) : Qg(br, a.stateNode));
          return;
        }
        case j: {
          {
            var d = br, h = eo;
            br = a.stateNode.containerInfo, eo = !0, Xu(e, t, a), br = d, eo = h;
          }
          return;
        }
        case T:
        case fe:
        case Ce:
        case me: {
          if (!Er) {
            var S = a.updateQueue;
            if (S !== null) {
              var b = S.lastEffect;
              if (b !== null) {
                var k = b.next, _ = k;
                do {
                  var P = _, z = P.destroy, q = P.tag;
                  z !== void 0 && ((q & Ho) !== Na ? dy(a, t, z) : (q & Zn) !== Na && (Ka(a), a.mode & nt ? (Bo(), dy(a, t, z), $o(a)) : dy(a, t, z), du())), _ = _.next;
                } while (_ !== k);
              }
            }
          }
          Xu(e, t, a);
          return;
        }
        case D: {
          if (!Er) {
            ud(a, t);
            var Z = a.stateNode;
            typeof Z.componentWillUnmount == "function" && AS(a, t, Z);
          }
          Xu(e, t, a);
          return;
        }
        case Ue: {
          Xu(e, t, a);
          return;
        }
        case qe: {
          if (
            // TODO: Remove this dead flag
            a.mode & je
          ) {
            var ee = Er;
            Er = ee || a.memoizedState !== null, Xu(e, t, a), Er = ee;
          } else
            Xu(e, t, a);
          break;
        }
        default: {
          Xu(e, t, a);
          return;
        }
      }
    }
    function L_(e) {
      e.memoizedState;
    }
    function N_(e, t) {
      var a = t.memoizedState;
      if (a === null) {
        var i = t.alternate;
        if (i !== null) {
          var u = i.memoizedState;
          if (u !== null) {
            var d = u.dehydrated;
            d !== null && yk(d);
          }
        }
      }
    }
    function VC(e) {
      var t = e.updateQueue;
      if (t !== null) {
        e.updateQueue = null;
        var a = e.stateNode;
        a === null && (a = e.stateNode = new m_()), t.forEach(function(i) {
          var u = DD.bind(null, e, i);
          if (!a.has(i)) {
            if (a.add(i), ia)
              if (od !== null && ld !== null)
                Tv(ld, od);
              else
                throw Error("Expected finished root and lanes to be set. This is a bug in React.");
            i.then(u, u);
          }
        });
      }
    }
    function A_(e, t, a) {
      od = a, ld = e, Ft(t), $C(t, e), Ft(t), od = null, ld = null;
    }
    function to(e, t, a) {
      var i = t.deletions;
      if (i !== null)
        for (var u = 0; u < i.length; u++) {
          var d = i[u];
          try {
            M_(e, t, d);
          } catch (b) {
            nn(d, t, b);
          }
        }
      var h = Tc();
      if (t.subtreeFlags & jr)
        for (var S = t.child; S !== null; )
          Ft(S), $C(S, e), S = S.sibling;
      Ft(h);
    }
    function $C(e, t, a) {
      var i = e.alternate, u = e.flags;
      switch (e.tag) {
        case T:
        case fe:
        case Ce:
        case me: {
          if (to(t, e), Io(e), u & st) {
            try {
              Ji(Ho | Kn, e, e.return), qu(Ho | Kn, e);
            } catch (et) {
              nn(e, e.return, et);
            }
            if (e.mode & nt) {
              try {
                Bo(), Ji(Zn | Kn, e, e.return);
              } catch (et) {
                nn(e, e.return, et);
              }
              $o(e);
            } else
              try {
                Ji(Zn | Kn, e, e.return);
              } catch (et) {
                nn(e, e.return, et);
              }
          }
          return;
        }
        case D: {
          to(t, e), Io(e), u & Pr && i !== null && ud(i, i.return);
          return;
        }
        case F: {
          to(t, e), Io(e), u & Pr && i !== null && ud(i, i.return);
          {
            if (e.flags & Ht) {
              var d = e.stateNode;
              try {
                VE(d);
              } catch (et) {
                nn(e, e.return, et);
              }
            }
            if (u & st) {
              var h = e.stateNode;
              if (h != null) {
                var S = e.memoizedProps, b = i !== null ? i.memoizedProps : S, k = e.type, _ = e.updateQueue;
                if (e.updateQueue = null, _ !== null)
                  try {
                    Ix(h, _, k, b, S, e);
                  } catch (et) {
                    nn(e, e.return, et);
                  }
              }
            }
          }
          return;
        }
        case $: {
          if (to(t, e), Io(e), u & st) {
            if (e.stateNode === null)
              throw new Error("This should have a text node initialized. This error is likely caused by a bug in React. Please file an issue.");
            var P = e.stateNode, z = e.memoizedProps, q = i !== null ? i.memoizedProps : z;
            try {
              Yx(P, q, z);
            } catch (et) {
              nn(e, e.return, et);
            }
          }
          return;
        }
        case L: {
          if (to(t, e), Io(e), u & st && i !== null) {
            var Z = i.memoizedState;
            if (Z.isDehydrated)
              try {
                mk(t.containerInfo);
              } catch (et) {
                nn(e, e.return, et);
              }
          }
          return;
        }
        case j: {
          to(t, e), Io(e);
          return;
        }
        case ne: {
          to(t, e), Io(e);
          var ee = e.child;
          if (ee.flags & xo) {
            var we = ee.stateNode, Qe = ee.memoizedState, Ve = Qe !== null;
            if (we.isHidden = Ve, Ve) {
              var _t = ee.alternate !== null && ee.alternate.memoizedState !== null;
              _t || vD();
            }
          }
          if (u & st) {
            try {
              L_(e);
            } catch (et) {
              nn(e, e.return, et);
            }
            VC(e);
          }
          return;
        }
        case qe: {
          var St = i !== null && i.memoizedState !== null;
          if (
            // TODO: Remove this dead flag
            e.mode & je
          ) {
            var I = Er;
            Er = I || St, to(t, e), Er = I;
          } else
            to(t, e);
          if (Io(e), u & xo) {
            var te = e.stateNode, Y = e.memoizedState, ce = Y !== null, _e = e;
            if (te.isHidden = ce, ce && !St && (_e.mode & je) !== Fe) {
              Ae = _e;
              for (var Re = _e.child; Re !== null; )
                Ae = Re, U_(Re), Re = Re.sibling;
            }
            k_(_e, ce);
          }
          return;
        }
        case ye: {
          to(t, e), Io(e), u & st && VC(e);
          return;
        }
        case Ue:
          return;
        default: {
          to(t, e), Io(e);
          return;
        }
      }
    }
    function Io(e) {
      var t = e.flags;
      if (t & Jt) {
        try {
          O_(e);
        } catch (a) {
          nn(e, e.return, a);
        }
        e.flags &= ~Jt;
      }
      t & Ea && (e.flags &= ~Ea);
    }
    function z_(e, t, a) {
      od = a, ld = t, Ae = e, BC(e, t, a), od = null, ld = null;
    }
    function BC(e, t, a) {
      for (var i = (e.mode & je) !== Fe; Ae !== null; ) {
        var u = Ae, d = u.child;
        if (u.tag === qe && i) {
          var h = u.memoizedState !== null, S = h || fy;
          if (S) {
            PS(e, t, a);
            continue;
          } else {
            var b = u.alternate, k = b !== null && b.memoizedState !== null, _ = k || Er, P = fy, z = Er;
            fy = S, Er = _, Er && !z && (Ae = u, P_(u));
            for (var q = d; q !== null; )
              Ae = q, BC(
                q,
                // New root; bubble back up to here and stop.
                t,
                a
              ), q = q.sibling;
            Ae = u, fy = P, Er = z, PS(e, t, a);
            continue;
          }
        }
        (u.subtreeFlags & lr) !== Ye && d !== null ? (d.return = u, Ae = d) : PS(e, t, a);
      }
    }
    function PS(e, t, a) {
      for (; Ae !== null; ) {
        var i = Ae;
        if ((i.flags & lr) !== Ye) {
          var u = i.alternate;
          Ft(i);
          try {
            R_(t, u, i, a);
          } catch (h) {
            nn(i, i.return, h);
          }
          hn();
        }
        if (i === e) {
          Ae = null;
          return;
        }
        var d = i.sibling;
        if (d !== null) {
          d.return = i.return, Ae = d;
          return;
        }
        Ae = i.return;
      }
    }
    function U_(e) {
      for (; Ae !== null; ) {
        var t = Ae, a = t.child;
        switch (t.tag) {
          case T:
          case fe:
          case Ce:
          case me: {
            if (t.mode & nt)
              try {
                Bo(), Ji(Zn, t, t.return);
              } finally {
                $o(t);
              }
            else
              Ji(Zn, t, t.return);
            break;
          }
          case D: {
            ud(t, t.return);
            var i = t.stateNode;
            typeof i.componentWillUnmount == "function" && AS(t, t.return, i);
            break;
          }
          case F: {
            ud(t, t.return);
            break;
          }
          case qe: {
            var u = t.memoizedState !== null;
            if (u) {
              IC(e);
              continue;
            }
            break;
          }
        }
        a !== null ? (a.return = t, Ae = a) : IC(e);
      }
    }
    function IC(e) {
      for (; Ae !== null; ) {
        var t = Ae;
        if (t === e) {
          Ae = null;
          return;
        }
        var a = t.sibling;
        if (a !== null) {
          a.return = t.return, Ae = a;
          return;
        }
        Ae = t.return;
      }
    }
    function P_(e) {
      for (; Ae !== null; ) {
        var t = Ae, a = t.child;
        if (t.tag === qe) {
          var i = t.memoizedState !== null;
          if (i) {
            YC(e);
            continue;
          }
        }
        a !== null ? (a.return = t, Ae = a) : YC(e);
      }
    }
    function YC(e) {
      for (; Ae !== null; ) {
        var t = Ae;
        Ft(t);
        try {
          x_(t);
        } catch (i) {
          nn(t, t.return, i);
        }
        if (hn(), t === e) {
          Ae = null;
          return;
        }
        var a = t.sibling;
        if (a !== null) {
          a.return = t.return, Ae = a;
          return;
        }
        Ae = t.return;
      }
    }
    function F_(e, t, a, i) {
      Ae = t, H_(t, e, a, i);
    }
    function H_(e, t, a, i) {
      for (; Ae !== null; ) {
        var u = Ae, d = u.child;
        (u.subtreeFlags & ba) !== Ye && d !== null ? (d.return = u, Ae = d) : j_(e, t, a, i);
      }
    }
    function j_(e, t, a, i) {
      for (; Ae !== null; ) {
        var u = Ae;
        if ((u.flags & an) !== Ye) {
          Ft(u);
          try {
            V_(t, u, a, i);
          } catch (h) {
            nn(u, u.return, h);
          }
          hn();
        }
        if (u === e) {
          Ae = null;
          return;
        }
        var d = u.sibling;
        if (d !== null) {
          d.return = u.return, Ae = d;
          return;
        }
        Ae = u.return;
      }
    }
    function V_(e, t, a, i) {
      switch (t.tag) {
        case T:
        case fe:
        case me: {
          if (t.mode & nt) {
            nS();
            try {
              qu(gr | Kn, t);
            } finally {
              tS(t);
            }
          } else
            qu(gr | Kn, t);
          break;
        }
      }
    }
    function $_(e) {
      Ae = e, B_();
    }
    function B_() {
      for (; Ae !== null; ) {
        var e = Ae, t = e.child;
        if ((Ae.flags & Pt) !== Ye) {
          var a = e.deletions;
          if (a !== null) {
            for (var i = 0; i < a.length; i++) {
              var u = a[i];
              Ae = u, W_(u, e);
            }
            {
              var d = e.alternate;
              if (d !== null) {
                var h = d.child;
                if (h !== null) {
                  d.child = null;
                  do {
                    var S = h.sibling;
                    h.sibling = null, h = S;
                  } while (h !== null);
                }
              }
            }
            Ae = e;
          }
        }
        (e.subtreeFlags & ba) !== Ye && t !== null ? (t.return = e, Ae = t) : I_();
      }
    }
    function I_() {
      for (; Ae !== null; ) {
        var e = Ae;
        (e.flags & an) !== Ye && (Ft(e), Y_(e), hn());
        var t = e.sibling;
        if (t !== null) {
          t.return = e.return, Ae = t;
          return;
        }
        Ae = e.return;
      }
    }
    function Y_(e) {
      switch (e.tag) {
        case T:
        case fe:
        case me: {
          e.mode & nt ? (nS(), Ji(gr | Kn, e, e.return), tS(e)) : Ji(gr | Kn, e, e.return);
          break;
        }
      }
    }
    function W_(e, t) {
      for (; Ae !== null; ) {
        var a = Ae;
        Ft(a), G_(a, t), hn();
        var i = a.child;
        i !== null ? (i.return = a, Ae = i) : Q_(e);
      }
    }
    function Q_(e) {
      for (; Ae !== null; ) {
        var t = Ae, a = t.sibling, i = t.return;
        if (PC(t), t === e) {
          Ae = null;
          return;
        }
        if (a !== null) {
          a.return = i, Ae = a;
          return;
        }
        Ae = i;
      }
    }
    function G_(e, t) {
      switch (e.tag) {
        case T:
        case fe:
        case me: {
          e.mode & nt ? (nS(), Ji(gr, e, t), tS(e)) : Ji(gr, e, t);
          break;
        }
      }
    }
    function q_(e) {
      switch (e.tag) {
        case T:
        case fe:
        case me: {
          try {
            qu(Zn | Kn, e);
          } catch (a) {
            nn(e, e.return, a);
          }
          break;
        }
        case D: {
          var t = e.stateNode;
          try {
            t.componentDidMount();
          } catch (a) {
            nn(e, e.return, a);
          }
          break;
        }
      }
    }
    function X_(e) {
      switch (e.tag) {
        case T:
        case fe:
        case me: {
          try {
            qu(gr | Kn, e);
          } catch (t) {
            nn(e, e.return, t);
          }
          break;
        }
      }
    }
    function K_(e) {
      switch (e.tag) {
        case T:
        case fe:
        case me: {
          try {
            Ji(Zn | Kn, e, e.return);
          } catch (a) {
            nn(e, e.return, a);
          }
          break;
        }
        case D: {
          var t = e.stateNode;
          typeof t.componentWillUnmount == "function" && AS(e, e.return, t);
          break;
        }
      }
    }
    function Z_(e) {
      switch (e.tag) {
        case T:
        case fe:
        case me:
          try {
            Ji(gr | Kn, e, e.return);
          } catch (t) {
            nn(e, e.return, t);
          }
      }
    }
    if (typeof Symbol == "function" && Symbol.for) {
      var pv = Symbol.for;
      pv("selector.component"), pv("selector.has_pseudo_class"), pv("selector.role"), pv("selector.test_id"), pv("selector.text");
    }
    var J_ = [];
    function eD() {
      J_.forEach(function(e) {
        return e();
      });
    }
    var tD = c.ReactCurrentActQueue;
    function nD(e) {
      {
        var t = (
          // $FlowExpectedError – Flow doesn't know about IS_REACT_ACT_ENVIRONMENT global
          typeof IS_REACT_ACT_ENVIRONMENT < "u" ? IS_REACT_ACT_ENVIRONMENT : void 0
        ), a = typeof jest < "u";
        return a && t !== !1;
      }
    }
    function WC() {
      {
        var e = (
          // $FlowExpectedError – Flow doesn't know about IS_REACT_ACT_ENVIRONMENT global
          typeof IS_REACT_ACT_ENVIRONMENT < "u" ? IS_REACT_ACT_ENVIRONMENT : void 0
        );
        return !e && tD.current !== null && m("The current testing environment is not configured to support act(...)"), e;
      }
    }
    var rD = Math.ceil, FS = c.ReactCurrentDispatcher, HS = c.ReactCurrentOwner, Cr = c.ReactCurrentBatchConfig, no = c.ReactCurrentActQueue, tr = (
      /*             */
      0
    ), QC = (
      /*               */
      1
    ), wr = (
      /*                */
      2
    ), Ei = (
      /*                */
      4
    ), Nl = 0, vv = 1, cc = 2, py = 3, hv = 4, GC = 5, jS = 6, kt = tr, sa = null, xn = null, nr = ie, Yo = ie, VS = ju(ie), rr = Nl, mv = null, vy = ie, yv = ie, hy = ie, gv = null, Aa = null, $S = 0, qC = 500, XC = 1 / 0, aD = 500, Al = null;
    function Sv() {
      XC = fn() + aD;
    }
    function KC() {
      return XC;
    }
    var my = !1, BS = null, sd = null, fc = !1, Ku = null, Ev = ie, IS = [], YS = null, iD = 50, bv = 0, WS = null, QS = !1, yy = !1, oD = 50, cd = 0, gy = null, Cv = Kt, Sy = ie, ZC = !1;
    function Ey() {
      return sa;
    }
    function ca() {
      return (kt & (wr | Ei)) !== tr ? fn() : (Cv !== Kt || (Cv = fn()), Cv);
    }
    function Zu(e) {
      var t = e.mode;
      if ((t & je) === Fe)
        return $e;
      if ((kt & wr) !== tr && nr !== ie)
        return bu(nr);
      var a = t5() !== e5;
      if (a) {
        if (Cr.transition !== null) {
          var i = Cr.transition;
          i._updatedFibers || (i._updatedFibers = /* @__PURE__ */ new Set()), i._updatedFibers.add(e);
        }
        return Sy === Dn && (Sy = Lh()), Sy;
      }
      var u = xa();
      if (u !== Dn)
        return u;
      var d = Hx();
      return d;
    }
    function lD(e) {
      var t = e.mode;
      return (t & je) === Fe ? $e : Ir();
    }
    function ar(e, t, a, i) {
      MD(), ZC && m("useInsertionEffect must not schedule updates."), QS && (yy = !0), Sl(e, a, i), (kt & wr) !== ie && e === sa ? AD(t) : (ia && yf(e, t, a), zD(t), e === sa && ((kt & wr) === tr && (yv = ut(yv, a)), rr === hv && Ju(e, nr)), za(e, i), a === $e && kt === tr && (t.mode & je) === Fe && // Treat `act` as if it's inside `batchedUpdates`, even in legacy mode.
      !no.isBatchingLegacy && (Sv(), ZE()));
    }
    function uD(e, t, a) {
      var i = e.current;
      i.lanes = t, Sl(e, t, a), za(e, a);
    }
    function sD(e) {
      return (
        // TODO: Remove outdated deferRenderPhaseUpdateToNextBatch experiment. We
        // decided not to enable it.
        (kt & wr) !== tr
      );
    }
    function za(e, t) {
      var a = e.callbackNode;
      kh(e, t);
      var i = yl(e, e === sa ? nr : ie);
      if (i === ie) {
        a !== null && vw(a), e.callbackNode = null, e.callbackPriority = Dn;
        return;
      }
      var u = wn(i), d = e.callbackPriority;
      if (d === u && // Special case related to `act`. If the currently scheduled task is a
      // Scheduler task, rather than an `act` task, cancel it and re-scheduled
      // on the `act` queue.
      !(no.current !== null && a !== e1)) {
        a == null && d !== $e && m("Expected scheduled callback to exist. This error is likely caused by a bug in React. Please file an issue.");
        return;
      }
      a != null && vw(a);
      var h;
      if (u === $e)
        e.tag === Vu ? (no.isBatchingLegacy !== null && (no.didScheduleLegacyUpdate = !0), Pk(tw.bind(null, e))) : KE(tw.bind(null, e)), no.current !== null ? no.current.push($u) : Vx(function() {
          (kt & (wr | Ei)) === tr && $u();
        }), h = null;
      else {
        var S;
        switch (Xn(i)) {
          case Tn:
            S = Bc;
            break;
          case $i:
            S = cl;
            break;
          case fi:
            S = ci;
            break;
          case Cu:
            S = Ic;
            break;
          default:
            S = ci;
            break;
        }
        h = t1(S, JC.bind(null, e));
      }
      e.callbackPriority = u, e.callbackNode = h;
    }
    function JC(e, t) {
      if (k5(), Cv = Kt, Sy = ie, (kt & (wr | Ei)) !== tr)
        throw new Error("Should not already be working.");
      var a = e.callbackNode, i = Ul();
      if (i && e.callbackNode !== a)
        return null;
      var u = yl(e, e === sa ? nr : ie);
      if (u === ie)
        return null;
      var d = !Ps(e, u) && !Mh(e, u) && !t, h = d ? SD(e, u) : Cy(e, u);
      if (h !== Nl) {
        if (h === cc) {
          var S = Mo(e);
          S !== ie && (u = S, h = GS(e, S));
        }
        if (h === vv) {
          var b = mv;
          throw dc(e, ie), Ju(e, u), za(e, fn()), b;
        }
        if (h === jS)
          Ju(e, u);
        else {
          var k = !Ps(e, u), _ = e.current.alternate;
          if (k && !fD(_)) {
            if (h = Cy(e, u), h === cc) {
              var P = Mo(e);
              P !== ie && (u = P, h = GS(e, P));
            }
            if (h === vv) {
              var z = mv;
              throw dc(e, ie), Ju(e, u), za(e, fn()), z;
            }
          }
          e.finishedWork = _, e.finishedLanes = u, cD(e, h, u);
        }
      }
      return za(e, fn()), e.callbackNode === a ? JC.bind(null, e) : null;
    }
    function GS(e, t) {
      var a = gv;
      if (gf(e)) {
        var i = dc(e, t);
        i.flags |= mn, Ok(e.containerInfo);
      }
      var u = Cy(e, t);
      if (u !== cc) {
        var d = Aa;
        Aa = a, d !== null && ew(d);
      }
      return u;
    }
    function ew(e) {
      Aa === null ? Aa = e : Aa.push.apply(Aa, e);
    }
    function cD(e, t, a) {
      switch (t) {
        case Nl:
        case vv:
          throw new Error("Root did not complete. This is a bug in React.");
        case cc: {
          pc(e, Aa, Al);
          break;
        }
        case py: {
          if (Ju(e, a), _h(a) && // do not delay if we're inside an act() scope
          !hw()) {
            var i = $S + qC - fn();
            if (i > 10) {
              var u = yl(e, ie);
              if (u !== ie)
                break;
              var d = e.suspendedLanes;
              if (!gl(d, a)) {
                ca(), hf(e, d);
                break;
              }
              e.timeoutHandle = Yg(pc.bind(null, e, Aa, Al), i);
              break;
            }
          }
          pc(e, Aa, Al);
          break;
        }
        case hv: {
          if (Ju(e, a), Oh(a))
            break;
          if (!hw()) {
            var h = Rh(e, a), S = h, b = fn() - S, k = OD(b) - b;
            if (k > 10) {
              e.timeoutHandle = Yg(pc.bind(null, e, Aa, Al), k);
              break;
            }
          }
          pc(e, Aa, Al);
          break;
        }
        case GC: {
          pc(e, Aa, Al);
          break;
        }
        default:
          throw new Error("Unknown root exit status.");
      }
    }
    function fD(e) {
      for (var t = e; ; ) {
        if (t.flags & ks) {
          var a = t.updateQueue;
          if (a !== null) {
            var i = a.stores;
            if (i !== null)
              for (var u = 0; u < i.length; u++) {
                var d = i[u], h = d.getSnapshot, S = d.value;
                try {
                  if (!Ne(h(), S))
                    return !1;
                } catch {
                  return !1;
                }
              }
          }
        }
        var b = t.child;
        if (t.subtreeFlags & ks && b !== null) {
          b.return = t, t = b;
          continue;
        }
        if (t === e)
          return !0;
        for (; t.sibling === null; ) {
          if (t.return === null || t.return === e)
            return !0;
          t = t.return;
        }
        t.sibling.return = t.return, t = t.sibling;
      }
      return !0;
    }
    function Ju(e, t) {
      t = Fs(t, hy), t = Fs(t, yv), Ah(e, t);
    }
    function tw(e) {
      if (_5(), (kt & (wr | Ei)) !== tr)
        throw new Error("Should not already be working.");
      Ul();
      var t = yl(e, ie);
      if (!Yr(t, $e))
        return za(e, fn()), null;
      var a = Cy(e, t);
      if (e.tag !== Vu && a === cc) {
        var i = Mo(e);
        i !== ie && (t = i, a = GS(e, i));
      }
      if (a === vv) {
        var u = mv;
        throw dc(e, ie), Ju(e, t), za(e, fn()), u;
      }
      if (a === jS)
        throw new Error("Root did not complete. This is a bug in React.");
      var d = e.current.alternate;
      return e.finishedWork = d, e.finishedLanes = t, pc(e, Aa, Al), za(e, fn()), null;
    }
    function dD(e, t) {
      t !== ie && (dp(e, ut(t, $e)), za(e, fn()), (kt & (wr | Ei)) === tr && (Sv(), $u()));
    }
    function qS(e, t) {
      var a = kt;
      kt |= QC;
      try {
        return e(t);
      } finally {
        kt = a, kt === tr && // Treat `act` as if it's inside `batchedUpdates`, even in legacy mode.
        !no.isBatchingLegacy && (Sv(), ZE());
      }
    }
    function pD(e, t, a, i, u) {
      var d = xa(), h = Cr.transition;
      try {
        return Cr.transition = null, yn(Tn), e(t, a, i, u);
      } finally {
        yn(d), Cr.transition = h, kt === tr && Sv();
      }
    }
    function zl(e) {
      Ku !== null && Ku.tag === Vu && (kt & (wr | Ei)) === tr && Ul();
      var t = kt;
      kt |= QC;
      var a = Cr.transition, i = xa();
      try {
        return Cr.transition = null, yn(Tn), e ? e() : void 0;
      } finally {
        yn(i), Cr.transition = a, kt = t, (kt & (wr | Ei)) === tr && $u();
      }
    }
    function nw() {
      return (kt & (wr | Ei)) !== tr;
    }
    function by(e, t) {
      Gr(VS, Yo, e), Yo = ut(Yo, t);
    }
    function XS(e) {
      Yo = VS.current, Qr(VS, e);
    }
    function dc(e, t) {
      e.finishedWork = null, e.finishedLanes = ie;
      var a = e.timeoutHandle;
      if (a !== Wg && (e.timeoutHandle = Wg, jx(a)), xn !== null)
        for (var i = xn.return; i !== null; ) {
          var u = i.alternate;
          MC(u, i), i = i.return;
        }
      sa = e;
      var d = vc(e.current, null);
      return xn = d, nr = Yo = t, rr = Nl, mv = null, vy = ie, yv = ie, hy = ie, gv = null, Aa = null, u5(), Gi.discardPendingWarnings(), d;
    }
    function rw(e, t) {
      do {
        var a = xn;
        try {
          if (Mm(), kb(), hn(), HS.current = null, a === null || a.return === null) {
            rr = vv, mv = t, xn = null;
            return;
          }
          if (yt && a.mode & nt && oy(a, !0), ct)
            if ($r(), t !== null && typeof t == "object" && typeof t.then == "function") {
              var i = t;
              dl(a, i, nr);
            } else
              Os(a, t, nr);
          P5(e, a.return, a, t, nr), lw(a);
        } catch (u) {
          t = u, xn === a && a !== null ? (a = a.return, xn = a) : a = xn;
          continue;
        }
        return;
      } while (!0);
    }
    function aw() {
      var e = FS.current;
      return FS.current = ty, e === null ? ty : e;
    }
    function iw(e) {
      FS.current = e;
    }
    function vD() {
      $S = fn();
    }
    function wv(e) {
      vy = ut(e, vy);
    }
    function hD() {
      rr === Nl && (rr = py);
    }
    function KS() {
      (rr === Nl || rr === py || rr === cc) && (rr = hv), sa !== null && (Us(vy) || Us(yv)) && Ju(sa, nr);
    }
    function mD(e) {
      rr !== hv && (rr = cc), gv === null ? gv = [e] : gv.push(e);
    }
    function yD() {
      return rr === Nl;
    }
    function Cy(e, t) {
      var a = kt;
      kt |= wr;
      var i = aw();
      if (sa !== e || nr !== t) {
        if (ia) {
          var u = e.memoizedUpdaters;
          u.size > 0 && (Tv(e, nr), u.clear()), pp(e, t);
        }
        Al = js(), dc(e, t);
      }
      on(t);
      do
        try {
          gD();
          break;
        } catch (d) {
          rw(e, d);
        }
      while (!0);
      if (Mm(), kt = a, iw(i), xn !== null)
        throw new Error("Cannot commit an incomplete root. This error is likely caused by a bug in React. Please file an issue.");
      return qc(), sa = null, nr = ie, rr;
    }
    function gD() {
      for (; xn !== null; )
        ow(xn);
    }
    function SD(e, t) {
      var a = kt;
      kt |= wr;
      var i = aw();
      if (sa !== e || nr !== t) {
        if (ia) {
          var u = e.memoizedUpdaters;
          u.size > 0 && (Tv(e, nr), u.clear()), pp(e, t);
        }
        Al = js(), Sv(), dc(e, t);
      }
      on(t);
      do
        try {
          ED();
          break;
        } catch (d) {
          rw(e, d);
        }
      while (!0);
      return Mm(), iw(i), kt = a, xn !== null ? (Gc(), Nl) : (qc(), sa = null, nr = ie, rr);
    }
    function ED() {
      for (; xn !== null && !$c(); )
        ow(xn);
    }
    function ow(e) {
      var t = e.alternate;
      Ft(e);
      var a;
      (e.mode & nt) !== Fe ? (eS(e), a = ZS(t, e, Yo), oy(e, !0)) : a = ZS(t, e, Yo), hn(), e.memoizedProps = e.pendingProps, a === null ? lw(e) : xn = a, HS.current = null;
    }
    function lw(e) {
      var t = e;
      do {
        var a = t.alternate, i = t.return;
        if ((t.flags & na) === Ye) {
          Ft(t);
          var u = void 0;
          if ((t.mode & nt) === Fe ? u = OC(a, t, Yo) : (eS(t), u = OC(a, t, Yo), oy(t, !1)), hn(), u !== null) {
            xn = u;
            return;
          }
        } else {
          var d = h_(a, t);
          if (d !== null) {
            d.flags &= vh, xn = d;
            return;
          }
          if ((t.mode & nt) !== Fe) {
            oy(t, !1);
            for (var h = t.actualDuration, S = t.child; S !== null; )
              h += S.actualDuration, S = S.sibling;
            t.actualDuration = h;
          }
          if (i !== null)
            i.flags |= na, i.subtreeFlags = Ye, i.deletions = null;
          else {
            rr = jS, xn = null;
            return;
          }
        }
        var b = t.sibling;
        if (b !== null) {
          xn = b;
          return;
        }
        t = i, xn = t;
      } while (t !== null);
      rr === Nl && (rr = GC);
    }
    function pc(e, t, a) {
      var i = xa(), u = Cr.transition;
      try {
        Cr.transition = null, yn(Tn), bD(e, t, a, i);
      } finally {
        Cr.transition = u, yn(i);
      }
      return null;
    }
    function bD(e, t, a, i) {
      do
        Ul();
      while (Ku !== null);
      if (LD(), (kt & (wr | Ei)) !== tr)
        throw new Error("Should not already be working.");
      var u = e.finishedWork, d = e.finishedLanes;
      if (_o(d), u === null)
        return Yc(), null;
      if (d === ie && m("root.finishedLanes should not be empty during a commit. This is a bug in React."), e.finishedWork = null, e.finishedLanes = ie, u === e.current)
        throw new Error("Cannot commit the same tree as before. This error is likely caused by a bug in React. Please file an issue.");
      e.callbackNode = null, e.callbackPriority = Dn;
      var h = ut(u.lanes, u.childLanes);
      mf(e, h), e === sa && (sa = null, xn = null, nr = ie), ((u.subtreeFlags & ba) !== Ye || (u.flags & ba) !== Ye) && (fc || (fc = !0, YS = a, t1(ci, function() {
        return Ul(), null;
      })));
      var S = (u.subtreeFlags & (uu | jr | lr | ba)) !== Ye, b = (u.flags & (uu | jr | lr | ba)) !== Ye;
      if (S || b) {
        var k = Cr.transition;
        Cr.transition = null;
        var _ = xa();
        yn(Tn);
        var P = kt;
        kt |= Ei, HS.current = null, E_(e, u), Zb(), A_(e, u, d), Nx(e.containerInfo), e.current = u, ap(d), z_(u, e, d), pu(), yh(), kt = P, yn(_), Cr.transition = k;
      } else
        e.current = u, Zb();
      var z = fc;
      if (fc ? (fc = !1, Ku = e, Ev = d) : (cd = 0, gy = null), h = e.pendingLanes, h === ie && (sd = null), z || fw(e.current, !1), cu(u.stateNode, i), ia && e.memoizedUpdaters.clear(), eD(), za(e, fn()), t !== null)
        for (var q = e.onRecoverableError, Z = 0; Z < t.length; Z++) {
          var ee = t[Z], we = ee.stack, Qe = ee.digest;
          q(ee.value, {
            componentStack: we,
            digest: Qe
          });
        }
      if (my) {
        my = !1;
        var Ve = BS;
        throw BS = null, Ve;
      }
      return Yr(Ev, $e) && e.tag !== Vu && Ul(), h = e.pendingLanes, Yr(h, $e) ? (x5(), e === WS ? bv++ : (bv = 0, WS = e)) : bv = 0, $u(), Yc(), null;
    }
    function Ul() {
      if (Ku !== null) {
        var e = Xn(Ev), t = mg(fi, e), a = Cr.transition, i = xa();
        try {
          return Cr.transition = null, yn(t), wD();
        } finally {
          yn(i), Cr.transition = a;
        }
      }
      return !1;
    }
    function CD(e) {
      IS.push(e), fc || (fc = !0, t1(ci, function() {
        return Ul(), null;
      }));
    }
    function wD() {
      if (Ku === null)
        return !1;
      var e = YS;
      YS = null;
      var t = Ku, a = Ev;
      if (Ku = null, Ev = ie, (kt & (wr | Ei)) !== tr)
        throw new Error("Cannot flush passive effects while already rendering.");
      QS = !0, yy = !1, wh(a);
      var i = kt;
      kt |= Ei, $_(t.current), F_(t, t.current, a, e);
      {
        var u = IS;
        IS = [];
        for (var d = 0; d < u.length; d++) {
          var h = u[d];
          T_(t, h);
        }
      }
      ip(), fw(t.current, !0), kt = i, $u(), yy ? t === gy ? cd++ : (cd = 0, gy = t) : cd = 0, QS = !1, yy = !1, Ta(t);
      {
        var S = t.current.stateNode;
        S.effectDuration = 0, S.passiveEffectDuration = 0;
      }
      return !0;
    }
    function uw(e) {
      return sd !== null && sd.has(e);
    }
    function TD(e) {
      sd === null ? sd = /* @__PURE__ */ new Set([e]) : sd.add(e);
    }
    function RD(e) {
      my || (my = !0, BS = e);
    }
    var xD = RD;
    function sw(e, t, a) {
      var i = uc(a, t), u = oC(e, i, $e), d = Iu(e, u, $e), h = ca();
      d !== null && (Sl(d, $e, h), za(d, h));
    }
    function nn(e, t, a) {
      if (y_(a), Rv(!1), e.tag === L) {
        sw(e, e, a);
        return;
      }
      var i = null;
      for (i = t; i !== null; ) {
        if (i.tag === L) {
          sw(i, e, a);
          return;
        } else if (i.tag === D) {
          var u = i.type, d = i.stateNode;
          if (typeof u.getDerivedStateFromError == "function" || typeof d.componentDidCatch == "function" && !uw(d)) {
            var h = uc(a, e), S = gS(i, h, $e), b = Iu(i, S, $e), k = ca();
            b !== null && (Sl(b, $e, k), za(b, k));
            return;
          }
        }
        i = i.return;
      }
      m(`Internal React error: Attempted to capture a commit phase error inside a detached tree. This indicates a bug in React. Likely causes include deleting the same fiber more than once, committing an already-finished tree, or an inconsistent return pointer.

Error message:

%s`, a);
    }
    function kD(e, t, a) {
      var i = e.pingCache;
      i !== null && i.delete(t);
      var u = ca();
      hf(e, a), UD(e), sa === e && gl(nr, a) && (rr === hv || rr === py && _h(nr) && fn() - $S < qC ? dc(e, ie) : hy = ut(hy, a)), za(e, u);
    }
    function cw(e, t) {
      t === Dn && (t = lD(e));
      var a = ca(), i = La(e, t);
      i !== null && (Sl(i, t, a), za(i, a));
    }
    function _D(e) {
      var t = e.memoizedState, a = Dn;
      t !== null && (a = t.retryLane), cw(e, a);
    }
    function DD(e, t) {
      var a = Dn, i;
      switch (e.tag) {
        case ne:
          i = e.stateNode;
          var u = e.memoizedState;
          u !== null && (a = u.retryLane);
          break;
        case ye:
          i = e.stateNode;
          break;
        default:
          throw new Error("Pinged unknown suspense boundary type. This is probably a bug in React.");
      }
      i !== null && i.delete(t), cw(e, a);
    }
    function OD(e) {
      return e < 120 ? 120 : e < 480 ? 480 : e < 1080 ? 1080 : e < 1920 ? 1920 : e < 3e3 ? 3e3 : e < 4320 ? 4320 : rD(e / 1960) * 1960;
    }
    function MD() {
      if (bv > iD)
        throw bv = 0, WS = null, new Error("Maximum update depth exceeded. This can happen when a component repeatedly calls setState inside componentWillUpdate or componentDidUpdate. React limits the number of nested updates to prevent infinite loops.");
      cd > oD && (cd = 0, gy = null, m("Maximum update depth exceeded. This can happen when a component calls setState inside useEffect, but useEffect either doesn't have a dependency array, or one of the dependencies changes on every render."));
    }
    function LD() {
      Gi.flushLegacyContextWarning(), Gi.flushPendingUnsafeLifecycleWarnings();
    }
    function fw(e, t) {
      Ft(e), wy(e, Hr, K_), t && wy(e, sl, Z_), wy(e, Hr, q_), t && wy(e, sl, X_), hn();
    }
    function wy(e, t, a) {
      for (var i = e, u = null; i !== null; ) {
        var d = i.subtreeFlags & t;
        i !== u && i.child !== null && d !== Ye ? i = i.child : ((i.flags & t) !== Ye && a(i), i.sibling !== null ? i = i.sibling : i = u = i.return);
      }
    }
    var Ty = null;
    function dw(e) {
      {
        if ((kt & wr) !== tr || !(e.mode & je))
          return;
        var t = e.tag;
        if (t !== A && t !== L && t !== D && t !== T && t !== fe && t !== Ce && t !== me)
          return;
        var a = it(e) || "ReactComponent";
        if (Ty !== null) {
          if (Ty.has(a))
            return;
          Ty.add(a);
        } else
          Ty = /* @__PURE__ */ new Set([a]);
        var i = cn;
        try {
          Ft(e), m("Can't perform a React state update on a component that hasn't mounted yet. This indicates that you have a side-effect in your render function that asynchronously later calls tries to update the component. Move this work to useEffect instead.");
        } finally {
          i ? Ft(e) : hn();
        }
      }
    }
    var ZS;
    {
      var ND = null;
      ZS = function(e, t, a) {
        var i = Ew(ND, t);
        try {
          return RC(e, t, a);
        } catch (d) {
          if (Yk() || d !== null && typeof d == "object" && typeof d.then == "function")
            throw d;
          if (Mm(), kb(), MC(e, t), Ew(t, i), t.mode & nt && eS(t), ul(null, RC, null, e, t, a), fg()) {
            var u = qd();
            typeof u == "object" && u !== null && u._suppressLogging && typeof d == "object" && d !== null && !d._suppressLogging && (d._suppressLogging = !0);
          }
          throw d;
        }
      };
    }
    var pw = !1, JS;
    JS = /* @__PURE__ */ new Set();
    function AD(e) {
      if (Ar && !w5())
        switch (e.tag) {
          case T:
          case fe:
          case me: {
            var t = xn && it(xn) || "Unknown", a = t;
            if (!JS.has(a)) {
              JS.add(a);
              var i = it(e) || "Unknown";
              m("Cannot update a component (`%s`) while rendering a different component (`%s`). To locate the bad setState() call inside `%s`, follow the stack trace as described in https://reactjs.org/link/setstate-in-render", i, t, t);
            }
            break;
          }
          case D: {
            pw || (m("Cannot update during an existing state transition (such as within `render`). Render methods should be a pure function of props and state."), pw = !0);
            break;
          }
        }
    }
    function Tv(e, t) {
      if (ia) {
        var a = e.memoizedUpdaters;
        a.forEach(function(i) {
          yf(e, i, t);
        });
      }
    }
    var e1 = {};
    function t1(e, t) {
      {
        var a = no.current;
        return a !== null ? (a.push(t), e1) : Vc(e, t);
      }
    }
    function vw(e) {
      if (e !== e1)
        return mh(e);
    }
    function hw() {
      return no.current !== null;
    }
    function zD(e) {
      {
        if (e.mode & je) {
          if (!WC())
            return;
        } else if (!nD() || kt !== tr || e.tag !== T && e.tag !== fe && e.tag !== me)
          return;
        if (no.current === null) {
          var t = cn;
          try {
            Ft(e), m(`An update to %s inside a test was not wrapped in act(...).

When testing, code that causes React state updates should be wrapped into act(...):

act(() => {
  /* fire events that update state */
});
/* assert on the output */

This ensures that you're testing the behavior the user would see in the browser. Learn more at https://reactjs.org/link/wrap-tests-with-act`, it(e));
          } finally {
            t ? Ft(e) : hn();
          }
        }
      }
    }
    function UD(e) {
      e.tag !== Vu && WC() && no.current === null && m(`A suspended resource finished loading inside a test, but the event was not wrapped in act(...).

When testing, code that resolves suspended data should be wrapped into act(...):

act(() => {
  /* finish loading suspended data */
});
/* assert on the output */

This ensures that you're testing the behavior the user would see in the browser. Learn more at https://reactjs.org/link/wrap-tests-with-act`);
    }
    function Rv(e) {
      ZC = e;
    }
    var bi = null, fd = null, PD = function(e) {
      bi = e;
    };
    function dd(e) {
      {
        if (bi === null)
          return e;
        var t = bi(e);
        return t === void 0 ? e : t.current;
      }
    }
    function n1(e) {
      return dd(e);
    }
    function r1(e) {
      {
        if (bi === null)
          return e;
        var t = bi(e);
        if (t === void 0) {
          if (e != null && typeof e.render == "function") {
            var a = dd(e.render);
            if (e.render !== a) {
              var i = {
                $$typeof: vn,
                render: a
              };
              return e.displayName !== void 0 && (i.displayName = e.displayName), i;
            }
          }
          return e;
        }
        return t.current;
      }
    }
    function mw(e, t) {
      {
        if (bi === null)
          return !1;
        var a = e.elementType, i = t.type, u = !1, d = typeof i == "object" && i !== null ? i.$$typeof : null;
        switch (e.tag) {
          case D: {
            typeof i == "function" && (u = !0);
            break;
          }
          case T: {
            (typeof i == "function" || d === Qn) && (u = !0);
            break;
          }
          case fe: {
            (d === vn || d === Qn) && (u = !0);
            break;
          }
          case Ce:
          case me: {
            (d === so || d === Qn) && (u = !0);
            break;
          }
          default:
            return !1;
        }
        if (u) {
          var h = bi(a);
          if (h !== void 0 && h === bi(i))
            return !0;
        }
        return !1;
      }
    }
    function yw(e) {
      {
        if (bi === null || typeof WeakSet != "function")
          return;
        fd === null && (fd = /* @__PURE__ */ new WeakSet()), fd.add(e);
      }
    }
    var FD = function(e, t) {
      {
        if (bi === null)
          return;
        var a = t.staleFamilies, i = t.updatedFamilies;
        Ul(), zl(function() {
          a1(e.current, i, a);
        });
      }
    }, HD = function(e, t) {
      {
        if (e.context !== Za)
          return;
        Ul(), zl(function() {
          xv(t, e, null, null);
        });
      }
    };
    function a1(e, t, a) {
      {
        var i = e.alternate, u = e.child, d = e.sibling, h = e.tag, S = e.type, b = null;
        switch (h) {
          case T:
          case me:
          case D:
            b = S;
            break;
          case fe:
            b = S.render;
            break;
        }
        if (bi === null)
          throw new Error("Expected resolveFamily to be set during hot reload.");
        var k = !1, _ = !1;
        if (b !== null) {
          var P = bi(b);
          P !== void 0 && (a.has(P) ? _ = !0 : t.has(P) && (h === D ? _ = !0 : k = !0));
        }
        if (fd !== null && (fd.has(e) || i !== null && fd.has(i)) && (_ = !0), _ && (e._debugNeedsRemount = !0), _ || k) {
          var z = La(e, $e);
          z !== null && ar(z, e, $e, Kt);
        }
        u !== null && !_ && a1(u, t, a), d !== null && a1(d, t, a);
      }
    }
    var jD = function(e, t) {
      {
        var a = /* @__PURE__ */ new Set(), i = new Set(t.map(function(u) {
          return u.current;
        }));
        return i1(e.current, i, a), a;
      }
    };
    function i1(e, t, a) {
      {
        var i = e.child, u = e.sibling, d = e.tag, h = e.type, S = null;
        switch (d) {
          case T:
          case me:
          case D:
            S = h;
            break;
          case fe:
            S = h.render;
            break;
        }
        var b = !1;
        S !== null && t.has(S) && (b = !0), b ? VD(e, a) : i !== null && i1(i, t, a), u !== null && i1(u, t, a);
      }
    }
    function VD(e, t) {
      {
        var a = $D(e, t);
        if (a)
          return;
        for (var i = e; ; ) {
          switch (i.tag) {
            case F:
              t.add(i.stateNode);
              return;
            case j:
              t.add(i.stateNode.containerInfo);
              return;
            case L:
              t.add(i.stateNode.containerInfo);
              return;
          }
          if (i.return === null)
            throw new Error("Expected to reach root first.");
          i = i.return;
        }
      }
    }
    function $D(e, t) {
      for (var a = e, i = !1; ; ) {
        if (a.tag === F)
          i = !0, t.add(a.stateNode);
        else if (a.child !== null) {
          a.child.return = a, a = a.child;
          continue;
        }
        if (a === e)
          return i;
        for (; a.sibling === null; ) {
          if (a.return === null || a.return === e)
            return i;
          a = a.return;
        }
        a.sibling.return = a.return, a = a.sibling;
      }
      return !1;
    }
    var o1;
    {
      o1 = !1;
      try {
        var gw = Object.preventExtensions({});
      } catch {
        o1 = !0;
      }
    }
    function BD(e, t, a, i) {
      this.tag = e, this.key = a, this.elementType = null, this.type = null, this.stateNode = null, this.return = null, this.child = null, this.sibling = null, this.index = 0, this.ref = null, this.pendingProps = t, this.memoizedProps = null, this.updateQueue = null, this.memoizedState = null, this.dependencies = null, this.mode = i, this.flags = Ye, this.subtreeFlags = Ye, this.deletions = null, this.lanes = ie, this.childLanes = ie, this.alternate = null, this.actualDuration = Number.NaN, this.actualStartTime = Number.NaN, this.selfBaseDuration = Number.NaN, this.treeBaseDuration = Number.NaN, this.actualDuration = 0, this.actualStartTime = -1, this.selfBaseDuration = 0, this.treeBaseDuration = 0, this._debugSource = null, this._debugOwner = null, this._debugNeedsRemount = !1, this._debugHookTypes = null, !o1 && typeof Object.preventExtensions == "function" && Object.preventExtensions(this);
    }
    var Ja = function(e, t, a, i) {
      return new BD(e, t, a, i);
    };
    function l1(e) {
      var t = e.prototype;
      return !!(t && t.isReactComponent);
    }
    function ID(e) {
      return typeof e == "function" && !l1(e) && e.defaultProps === void 0;
    }
    function YD(e) {
      if (typeof e == "function")
        return l1(e) ? D : T;
      if (e != null) {
        var t = e.$$typeof;
        if (t === vn)
          return fe;
        if (t === so)
          return Ce;
      }
      return A;
    }
    function vc(e, t) {
      var a = e.alternate;
      a === null ? (a = Ja(e.tag, t, e.key, e.mode), a.elementType = e.elementType, a.type = e.type, a.stateNode = e.stateNode, a._debugSource = e._debugSource, a._debugOwner = e._debugOwner, a._debugHookTypes = e._debugHookTypes, a.alternate = e, e.alternate = a) : (a.pendingProps = t, a.type = e.type, a.flags = Ye, a.subtreeFlags = Ye, a.deletions = null, a.actualDuration = 0, a.actualStartTime = -1), a.flags = e.flags & Gn, a.childLanes = e.childLanes, a.lanes = e.lanes, a.child = e.child, a.memoizedProps = e.memoizedProps, a.memoizedState = e.memoizedState, a.updateQueue = e.updateQueue;
      var i = e.dependencies;
      switch (a.dependencies = i === null ? null : {
        lanes: i.lanes,
        firstContext: i.firstContext
      }, a.sibling = e.sibling, a.index = e.index, a.ref = e.ref, a.selfBaseDuration = e.selfBaseDuration, a.treeBaseDuration = e.treeBaseDuration, a._debugNeedsRemount = e._debugNeedsRemount, a.tag) {
        case A:
        case T:
        case me:
          a.type = dd(e.type);
          break;
        case D:
          a.type = n1(e.type);
          break;
        case fe:
          a.type = r1(e.type);
          break;
      }
      return a;
    }
    function WD(e, t) {
      e.flags &= Gn | Jt;
      var a = e.alternate;
      if (a === null)
        e.childLanes = ie, e.lanes = t, e.child = null, e.subtreeFlags = Ye, e.memoizedProps = null, e.memoizedState = null, e.updateQueue = null, e.dependencies = null, e.stateNode = null, e.selfBaseDuration = 0, e.treeBaseDuration = 0;
      else {
        e.childLanes = a.childLanes, e.lanes = a.lanes, e.child = a.child, e.subtreeFlags = Ye, e.deletions = null, e.memoizedProps = a.memoizedProps, e.memoizedState = a.memoizedState, e.updateQueue = a.updateQueue, e.type = a.type;
        var i = a.dependencies;
        e.dependencies = i === null ? null : {
          lanes: i.lanes,
          firstContext: i.firstContext
        }, e.selfBaseDuration = a.selfBaseDuration, e.treeBaseDuration = a.treeBaseDuration;
      }
      return e;
    }
    function QD(e, t, a) {
      var i;
      return e === bm ? (i = je, t === !0 && (i |= vt, i |= oa)) : i = Fe, ia && (i |= nt), Ja(L, null, null, i);
    }
    function u1(e, t, a, i, u, d) {
      var h = A, S = e;
      if (typeof e == "function")
        l1(e) ? (h = D, S = n1(S)) : S = dd(S);
      else if (typeof e == "string")
        h = F;
      else
        e: switch (e) {
          case Ba:
            return es(a.children, u, d, t);
          case lo:
            h = G, u |= vt, (u & je) !== Fe && (u |= oa);
            break;
          case uo:
            return GD(a, u, d, t);
          case va:
            return qD(a, u, d, t);
          case Zo:
            return XD(a, u, d, t);
          case us:
            return Sw(a, u, d, t);
          case Cc:
          case Ec:
          case wd:
          case Td:
          case bc:
          default: {
            if (typeof e == "object" && e !== null)
              switch (e.$$typeof) {
                case Bl:
                  h = ve;
                  break e;
                case ls:
                  h = W;
                  break e;
                case vn:
                  h = fe, S = r1(S);
                  break e;
                case so:
                  h = Ce;
                  break e;
                case Qn:
                  h = We, S = null;
                  break e;
              }
            var b = "";
            {
              (e === void 0 || typeof e == "object" && e !== null && Object.keys(e).length === 0) && (b += " You likely forgot to export your component from the file it's defined in, or you might have mixed up default and named imports.");
              var k = i ? it(i) : null;
              k && (b += `

Check the render method of \`` + k + "`.");
            }
            throw new Error("Element type is invalid: expected a string (for built-in components) or a class/function (for composite components) " + ("but got: " + (e == null ? e : typeof e) + "." + b));
          }
        }
      var _ = Ja(h, a, t, u);
      return _.elementType = e, _.type = S, _.lanes = d, _._debugOwner = i, _;
    }
    function s1(e, t, a) {
      var i = null;
      i = e._owner;
      var u = e.type, d = e.key, h = e.props, S = u1(u, d, h, i, t, a);
      return S._debugSource = e._source, S._debugOwner = e._owner, S;
    }
    function es(e, t, a, i) {
      var u = Ja(H, e, i, t);
      return u.lanes = a, u;
    }
    function GD(e, t, a, i) {
      typeof e.id != "string" && m('Profiler must specify an "id" of type `string` as a prop. Received the type `%s` instead.', typeof e.id);
      var u = Ja(oe, e, i, t | nt);
      return u.elementType = uo, u.lanes = a, u.stateNode = {
        effectDuration: 0,
        passiveEffectDuration: 0
      }, u;
    }
    function qD(e, t, a, i) {
      var u = Ja(ne, e, i, t);
      return u.elementType = va, u.lanes = a, u;
    }
    function XD(e, t, a, i) {
      var u = Ja(ye, e, i, t);
      return u.elementType = Zo, u.lanes = a, u;
    }
    function Sw(e, t, a, i) {
      var u = Ja(qe, e, i, t);
      u.elementType = us, u.lanes = a;
      var d = {
        isHidden: !1
      };
      return u.stateNode = d, u;
    }
    function c1(e, t, a) {
      var i = Ja($, e, null, t);
      return i.lanes = a, i;
    }
    function KD() {
      var e = Ja(F, null, null, Fe);
      return e.elementType = "DELETED", e;
    }
    function ZD(e) {
      var t = Ja(Rt, null, null, Fe);
      return t.stateNode = e, t;
    }
    function f1(e, t, a) {
      var i = e.children !== null ? e.children : [], u = Ja(j, i, e.key, t);
      return u.lanes = a, u.stateNode = {
        containerInfo: e.containerInfo,
        pendingChildren: null,
        // Used by persistent updates
        implementation: e.implementation
      }, u;
    }
    function Ew(e, t) {
      return e === null && (e = Ja(A, null, null, Fe)), e.tag = t.tag, e.key = t.key, e.elementType = t.elementType, e.type = t.type, e.stateNode = t.stateNode, e.return = t.return, e.child = t.child, e.sibling = t.sibling, e.index = t.index, e.ref = t.ref, e.pendingProps = t.pendingProps, e.memoizedProps = t.memoizedProps, e.updateQueue = t.updateQueue, e.memoizedState = t.memoizedState, e.dependencies = t.dependencies, e.mode = t.mode, e.flags = t.flags, e.subtreeFlags = t.subtreeFlags, e.deletions = t.deletions, e.lanes = t.lanes, e.childLanes = t.childLanes, e.alternate = t.alternate, e.actualDuration = t.actualDuration, e.actualStartTime = t.actualStartTime, e.selfBaseDuration = t.selfBaseDuration, e.treeBaseDuration = t.treeBaseDuration, e._debugSource = t._debugSource, e._debugOwner = t._debugOwner, e._debugNeedsRemount = t._debugNeedsRemount, e._debugHookTypes = t._debugHookTypes, e;
    }
    function JD(e, t, a, i, u) {
      this.tag = t, this.containerInfo = e, this.pendingChildren = null, this.current = null, this.pingCache = null, this.finishedWork = null, this.timeoutHandle = Wg, this.context = null, this.pendingContext = null, this.callbackNode = null, this.callbackPriority = Dn, this.eventTimes = Hs(ie), this.expirationTimes = Hs(Kt), this.pendingLanes = ie, this.suspendedLanes = ie, this.pingedLanes = ie, this.expiredLanes = ie, this.mutableReadLanes = ie, this.finishedLanes = ie, this.entangledLanes = ie, this.entanglements = Hs(ie), this.identifierPrefix = i, this.onRecoverableError = u, this.mutableSourceEagerHydrationData = null, this.effectDuration = 0, this.passiveEffectDuration = 0;
      {
        this.memoizedUpdaters = /* @__PURE__ */ new Set();
        for (var d = this.pendingUpdatersLaneMap = [], h = 0; h < Ns; h++)
          d.push(/* @__PURE__ */ new Set());
      }
      switch (t) {
        case bm:
          this._debugRootType = a ? "hydrateRoot()" : "createRoot()";
          break;
        case Vu:
          this._debugRootType = a ? "hydrate()" : "render()";
          break;
      }
    }
    function bw(e, t, a, i, u, d, h, S, b, k) {
      var _ = new JD(e, t, a, S, b), P = QD(t, d);
      _.current = P, P.stateNode = _;
      {
        var z = {
          element: i,
          isDehydrated: a,
          cache: null,
          // not enabled yet
          transitions: null,
          pendingSuspenseBoundaries: null
        };
        P.memoizedState = z;
      }
      return T0(P), _;
    }
    var d1 = "18.3.1";
    function e2(e, t, a) {
      var i = arguments.length > 3 && arguments[3] !== void 0 ? arguments[3] : null;
      return xi(i), {
        // This tag allow us to uniquely identify this as a React Portal
        $$typeof: ta,
        key: i == null ? null : "" + i,
        children: e,
        containerInfo: t,
        implementation: a
      };
    }
    var p1, v1;
    p1 = !1, v1 = {};
    function Cw(e) {
      if (!e)
        return Za;
      var t = ga(e), a = Uk(t);
      if (t.tag === D) {
        var i = t.type;
        if (Fo(i))
          return qE(t, i, a);
      }
      return a;
    }
    function t2(e, t) {
      {
        var a = ga(e);
        if (a === void 0) {
          if (typeof e.render == "function")
            throw new Error("Unable to find node on an unmounted component.");
          var i = Object.keys(e).join(",");
          throw new Error("Argument appears to not be a ReactComponent. Keys: " + i);
        }
        var u = Ca(a);
        if (u === null)
          return null;
        if (u.mode & vt) {
          var d = it(a) || "Component";
          if (!v1[d]) {
            v1[d] = !0;
            var h = cn;
            try {
              Ft(u), a.mode & vt ? m("%s is deprecated in StrictMode. %s was passed an instance of %s which is inside StrictMode. Instead, add a ref directly to the element you want to reference. Learn more about using refs safely here: https://reactjs.org/link/strict-mode-find-node", t, t, d) : m("%s is deprecated in StrictMode. %s was passed an instance of %s which renders StrictMode children. Instead, add a ref directly to the element you want to reference. Learn more about using refs safely here: https://reactjs.org/link/strict-mode-find-node", t, t, d);
            } finally {
              h ? Ft(h) : hn();
            }
          }
        }
        return u.stateNode;
      }
    }
    function ww(e, t, a, i, u, d, h, S) {
      var b = !1, k = null;
      return bw(e, t, b, k, a, i, u, d, h);
    }
    function Tw(e, t, a, i, u, d, h, S, b, k) {
      var _ = !0, P = bw(a, i, _, e, u, d, h, S, b);
      P.context = Cw(null);
      var z = P.current, q = ca(), Z = Zu(z), ee = Ml(q, Z);
      return ee.callback = t ?? null, Iu(z, ee, Z), uD(P, Z, q), P;
    }
    function xv(e, t, a, i) {
      rp(t, e);
      var u = t.current, d = ca(), h = Zu(u);
      op(h);
      var S = Cw(a);
      t.context === null ? t.context = S : t.pendingContext = S, Ar && cn !== null && !p1 && (p1 = !0, m(`Render methods should be a pure function of props and state; triggering nested component updates from render is not allowed. If necessary, trigger nested updates in componentDidUpdate.

Check the render method of %s.`, it(cn) || "Unknown"));
      var b = Ml(d, h);
      b.payload = {
        element: e
      }, i = i === void 0 ? null : i, i !== null && (typeof i != "function" && m("render(...): Expected the last optional `callback` argument to be a function. Instead received: %s.", i), b.callback = i);
      var k = Iu(u, b, h);
      return k !== null && (ar(k, u, h, d), Um(k, u, h)), h;
    }
    function Ry(e) {
      var t = e.current;
      if (!t.child)
        return null;
      switch (t.child.tag) {
        case F:
          return t.child.stateNode;
        default:
          return t.child.stateNode;
      }
    }
    function n2(e) {
      switch (e.tag) {
        case L: {
          var t = e.stateNode;
          if (gf(t)) {
            var a = sp(t);
            dD(t, a);
          }
          break;
        }
        case ne: {
          zl(function() {
            var u = La(e, $e);
            if (u !== null) {
              var d = ca();
              ar(u, e, $e, d);
            }
          });
          var i = $e;
          h1(e, i);
          break;
        }
      }
    }
    function Rw(e, t) {
      var a = e.memoizedState;
      a !== null && a.dehydrated !== null && (a.retryLane = Nh(a.retryLane, t));
    }
    function h1(e, t) {
      Rw(e, t);
      var a = e.alternate;
      a && Rw(a, t);
    }
    function r2(e) {
      if (e.tag === ne) {
        var t = zs, a = La(e, t);
        if (a !== null) {
          var i = ca();
          ar(a, e, t, i);
        }
        h1(e, t);
      }
    }
    function a2(e) {
      if (e.tag === ne) {
        var t = Zu(e), a = La(e, t);
        if (a !== null) {
          var i = ca();
          ar(a, e, t, i);
        }
        h1(e, t);
      }
    }
    function xw(e) {
      var t = hh(e);
      return t === null ? null : t.stateNode;
    }
    var kw = function(e) {
      return null;
    };
    function i2(e) {
      return kw(e);
    }
    var _w = function(e) {
      return !1;
    };
    function o2(e) {
      return _w(e);
    }
    var Dw = null, Ow = null, Mw = null, Lw = null, Nw = null, Aw = null, zw = null, Uw = null, Pw = null;
    {
      var Fw = function(e, t, a) {
        var i = t[a], u = wt(e) ? e.slice() : dt({}, e);
        return a + 1 === t.length ? (wt(u) ? u.splice(i, 1) : delete u[i], u) : (u[i] = Fw(e[i], t, a + 1), u);
      }, Hw = function(e, t) {
        return Fw(e, t, 0);
      }, jw = function(e, t, a, i) {
        var u = t[i], d = wt(e) ? e.slice() : dt({}, e);
        if (i + 1 === t.length) {
          var h = a[i];
          d[h] = d[u], wt(d) ? d.splice(u, 1) : delete d[u];
        } else
          d[u] = jw(
            // $FlowFixMe number or string is fine here
            e[u],
            t,
            a,
            i + 1
          );
        return d;
      }, Vw = function(e, t, a) {
        if (t.length !== a.length) {
          E("copyWithRename() expects paths of the same length");
          return;
        } else
          for (var i = 0; i < a.length - 1; i++)
            if (t[i] !== a[i]) {
              E("copyWithRename() expects paths to be the same except for the deepest key");
              return;
            }
        return jw(e, t, a, 0);
      }, $w = function(e, t, a, i) {
        if (a >= t.length)
          return i;
        var u = t[a], d = wt(e) ? e.slice() : dt({}, e);
        return d[u] = $w(e[u], t, a + 1, i), d;
      }, Bw = function(e, t, a) {
        return $w(e, t, 0, a);
      }, m1 = function(e, t) {
        for (var a = e.memoizedState; a !== null && t > 0; )
          a = a.next, t--;
        return a;
      };
      Dw = function(e, t, a, i) {
        var u = m1(e, t);
        if (u !== null) {
          var d = Bw(u.memoizedState, a, i);
          u.memoizedState = d, u.baseState = d, e.memoizedProps = dt({}, e.memoizedProps);
          var h = La(e, $e);
          h !== null && ar(h, e, $e, Kt);
        }
      }, Ow = function(e, t, a) {
        var i = m1(e, t);
        if (i !== null) {
          var u = Hw(i.memoizedState, a);
          i.memoizedState = u, i.baseState = u, e.memoizedProps = dt({}, e.memoizedProps);
          var d = La(e, $e);
          d !== null && ar(d, e, $e, Kt);
        }
      }, Mw = function(e, t, a, i) {
        var u = m1(e, t);
        if (u !== null) {
          var d = Vw(u.memoizedState, a, i);
          u.memoizedState = d, u.baseState = d, e.memoizedProps = dt({}, e.memoizedProps);
          var h = La(e, $e);
          h !== null && ar(h, e, $e, Kt);
        }
      }, Lw = function(e, t, a) {
        e.pendingProps = Bw(e.memoizedProps, t, a), e.alternate && (e.alternate.pendingProps = e.pendingProps);
        var i = La(e, $e);
        i !== null && ar(i, e, $e, Kt);
      }, Nw = function(e, t) {
        e.pendingProps = Hw(e.memoizedProps, t), e.alternate && (e.alternate.pendingProps = e.pendingProps);
        var a = La(e, $e);
        a !== null && ar(a, e, $e, Kt);
      }, Aw = function(e, t, a) {
        e.pendingProps = Vw(e.memoizedProps, t, a), e.alternate && (e.alternate.pendingProps = e.pendingProps);
        var i = La(e, $e);
        i !== null && ar(i, e, $e, Kt);
      }, zw = function(e) {
        var t = La(e, $e);
        t !== null && ar(t, e, $e, Kt);
      }, Uw = function(e) {
        kw = e;
      }, Pw = function(e) {
        _w = e;
      };
    }
    function l2(e) {
      var t = Ca(e);
      return t === null ? null : t.stateNode;
    }
    function u2(e) {
      return null;
    }
    function s2() {
      return cn;
    }
    function c2(e) {
      var t = e.findFiberByHostInstance, a = c.ReactCurrentDispatcher;
      return np({
        bundleType: e.bundleType,
        version: e.version,
        rendererPackageName: e.rendererPackageName,
        rendererConfig: e.rendererConfig,
        overrideHookState: Dw,
        overrideHookStateDeletePath: Ow,
        overrideHookStateRenamePath: Mw,
        overrideProps: Lw,
        overridePropsDeletePath: Nw,
        overridePropsRenamePath: Aw,
        setErrorHandler: Uw,
        setSuspenseHandler: Pw,
        scheduleUpdate: zw,
        currentDispatcherRef: a,
        findHostInstanceByFiber: l2,
        findFiberByHostInstance: t || u2,
        // React Refresh
        findHostInstancesForRefresh: jD,
        scheduleRefresh: FD,
        scheduleRoot: HD,
        setRefreshHandler: PD,
        // Enables DevTools to append owner stacks to error messages in DEV mode.
        getCurrentFiber: s2,
        // Enables DevTools to detect reconciler version rather than renderer version
        // which may not match for third party renderers.
        reconcilerVersion: d1
      });
    }
    var Iw = typeof reportError == "function" ? (
      // In modern browsers, reportError will dispatch an error event,
      // emulating an uncaught JavaScript error.
      reportError
    ) : function(e) {
      console.error(e);
    };
    function y1(e) {
      this._internalRoot = e;
    }
    xy.prototype.render = y1.prototype.render = function(e) {
      var t = this._internalRoot;
      if (t === null)
        throw new Error("Cannot update an unmounted root.");
      {
        typeof arguments[1] == "function" ? m("render(...): does not support the second callback argument. To execute a side effect after rendering, declare it in a component body with useEffect().") : ky(arguments[1]) ? m("You passed a container to the second argument of root.render(...). You don't need to pass it again since you already passed it to create the root.") : typeof arguments[1] < "u" && m("You passed a second argument to root.render(...) but it only accepts one argument.");
        var a = t.containerInfo;
        if (a.nodeType !== kn) {
          var i = xw(t.current);
          i && i.parentNode !== a && m("render(...): It looks like the React-rendered content of the root container was removed without using React. This is not supported and will cause errors. Instead, call root.unmount() to empty a root's container.");
        }
      }
      xv(e, t, null, null);
    }, xy.prototype.unmount = y1.prototype.unmount = function() {
      typeof arguments[0] == "function" && m("unmount(...): does not support a callback argument. To execute a side effect after rendering, declare it in a component body with useEffect().");
      var e = this._internalRoot;
      if (e !== null) {
        this._internalRoot = null;
        var t = e.containerInfo;
        nw() && m("Attempted to synchronously unmount a root while React was already rendering. React cannot finish unmounting the root until the current render has completed, which may lead to a race condition."), zl(function() {
          xv(null, e, null, null);
        }), IE(t);
      }
    };
    function f2(e, t) {
      if (!ky(e))
        throw new Error("createRoot(...): Target container is not a DOM element.");
      Yw(e);
      var a = !1, i = !1, u = "", d = Iw;
      t != null && (t.hydrate ? E("hydrate through createRoot is deprecated. Use ReactDOMClient.hydrateRoot(container, <App />) instead.") : typeof t == "object" && t !== null && t.$$typeof === oo && m(`You passed a JSX element to createRoot. You probably meant to call root.render instead. Example usage:

  let root = createRoot(domContainer);
  root.render(<App />);`), t.unstable_strictMode === !0 && (a = !0), t.identifierPrefix !== void 0 && (u = t.identifierPrefix), t.onRecoverableError !== void 0 && (d = t.onRecoverableError), t.transitionCallbacks !== void 0 && t.transitionCallbacks);
      var h = ww(e, bm, null, a, i, u, d);
      vm(h.current, e);
      var S = e.nodeType === kn ? e.parentNode : e;
      return Lp(S), new y1(h);
    }
    function xy(e) {
      this._internalRoot = e;
    }
    function d2(e) {
      e && Sg(e);
    }
    xy.prototype.unstable_scheduleHydration = d2;
    function p2(e, t, a) {
      if (!ky(e))
        throw new Error("hydrateRoot(...): Target container is not a DOM element.");
      Yw(e), t === void 0 && m("Must provide initial children as second argument to hydrateRoot. Example usage: hydrateRoot(domContainer, <App />)");
      var i = a ?? null, u = a != null && a.hydratedSources || null, d = !1, h = !1, S = "", b = Iw;
      a != null && (a.unstable_strictMode === !0 && (d = !0), a.identifierPrefix !== void 0 && (S = a.identifierPrefix), a.onRecoverableError !== void 0 && (b = a.onRecoverableError));
      var k = Tw(t, null, e, bm, i, d, h, S, b);
      if (vm(k.current, e), Lp(e), u)
        for (var _ = 0; _ < u.length; _++) {
          var P = u[_];
          y5(k, P);
        }
      return new xy(k);
    }
    function ky(e) {
      return !!(e && (e.nodeType === Ur || e.nodeType === Ga || e.nodeType === nl || !ae));
    }
    function kv(e) {
      return !!(e && (e.nodeType === Ur || e.nodeType === Ga || e.nodeType === nl || e.nodeType === kn && e.nodeValue === " react-mount-point-unstable "));
    }
    function Yw(e) {
      e.nodeType === Ur && e.tagName && e.tagName.toUpperCase() === "BODY" && m("createRoot(): Creating roots directly with document.body is discouraged, since its children are often manipulated by third-party scripts and browser extensions. This may lead to subtle reconciliation issues. Try using a container element created for your app."), Bp(e) && (e._reactRootContainer ? m("You are calling ReactDOMClient.createRoot() on a container that was previously passed to ReactDOM.render(). This is not supported.") : m("You are calling ReactDOMClient.createRoot() on a container that has already been passed to createRoot() before. Instead, call root.render() on the existing root instead if you want to update it."));
    }
    var v2 = c.ReactCurrentOwner, Ww;
    Ww = function(e) {
      if (e._reactRootContainer && e.nodeType !== kn) {
        var t = xw(e._reactRootContainer.current);
        t && t.parentNode !== e && m("render(...): It looks like the React-rendered content of this container was removed without using React. This is not supported and will cause errors. Instead, call ReactDOM.unmountComponentAtNode to empty a container.");
      }
      var a = !!e._reactRootContainer, i = g1(e), u = !!(i && Hu(i));
      u && !a && m("render(...): Replacing React-rendered children with a new root component. If you intended to update the children of this node, you should instead have the existing children update their state and render the new components instead of calling ReactDOM.render."), e.nodeType === Ur && e.tagName && e.tagName.toUpperCase() === "BODY" && m("render(): Rendering components directly into document.body is discouraged, since its children are often manipulated by third-party scripts and browser extensions. This may lead to subtle reconciliation issues. Try rendering into a container element created for your app.");
    };
    function g1(e) {
      return e ? e.nodeType === Ga ? e.documentElement : e.firstChild : null;
    }
    function Qw() {
    }
    function h2(e, t, a, i, u) {
      if (u) {
        if (typeof i == "function") {
          var d = i;
          i = function() {
            var z = Ry(h);
            d.call(z);
          };
        }
        var h = Tw(
          t,
          i,
          e,
          Vu,
          null,
          // hydrationCallbacks
          !1,
          // isStrictMode
          !1,
          // concurrentUpdatesByDefaultOverride,
          "",
          // identifierPrefix
          Qw
        );
        e._reactRootContainer = h, vm(h.current, e);
        var S = e.nodeType === kn ? e.parentNode : e;
        return Lp(S), zl(), h;
      } else {
        for (var b; b = e.lastChild; )
          e.removeChild(b);
        if (typeof i == "function") {
          var k = i;
          i = function() {
            var z = Ry(_);
            k.call(z);
          };
        }
        var _ = ww(
          e,
          Vu,
          null,
          // hydrationCallbacks
          !1,
          // isStrictMode
          !1,
          // concurrentUpdatesByDefaultOverride,
          "",
          // identifierPrefix
          Qw
        );
        e._reactRootContainer = _, vm(_.current, e);
        var P = e.nodeType === kn ? e.parentNode : e;
        return Lp(P), zl(function() {
          xv(t, _, a, i);
        }), _;
      }
    }
    function m2(e, t) {
      e !== null && typeof e != "function" && m("%s(...): Expected the last optional `callback` argument to be a function. Instead received: %s.", t, e);
    }
    function _y(e, t, a, i, u) {
      Ww(a), m2(u === void 0 ? null : u, "render");
      var d = a._reactRootContainer, h;
      if (!d)
        h = h2(a, t, e, u, i);
      else {
        if (h = d, typeof u == "function") {
          var S = u;
          u = function() {
            var b = Ry(h);
            S.call(b);
          };
        }
        xv(t, h, e, u);
      }
      return Ry(h);
    }
    var Gw = !1;
    function y2(e) {
      {
        Gw || (Gw = !0, m("findDOMNode is deprecated and will be removed in the next major release. Instead, add a ref directly to the element you want to reference. Learn more about using refs safely here: https://reactjs.org/link/strict-mode-find-node"));
        var t = v2.current;
        if (t !== null && t.stateNode !== null) {
          var a = t.stateNode._warnedAboutRefsInRender;
          a || m("%s is accessing findDOMNode inside its render(). render() should be a pure function of props and state. It should never access something that requires stale data from the previous render, such as refs. Move this logic to componentDidMount and componentDidUpdate instead.", Ct(t.type) || "A component"), t.stateNode._warnedAboutRefsInRender = !0;
        }
      }
      return e == null ? null : e.nodeType === Ur ? e : t2(e, "findDOMNode");
    }
    function g2(e, t, a) {
      if (m("ReactDOM.hydrate is no longer supported in React 18. Use hydrateRoot instead. Until you switch to the new API, your app will behave as if it's running React 17. Learn more: https://reactjs.org/link/switch-to-createroot"), !kv(t))
        throw new Error("Target container is not a DOM element.");
      {
        var i = Bp(t) && t._reactRootContainer === void 0;
        i && m("You are calling ReactDOM.hydrate() on a container that was previously passed to ReactDOMClient.createRoot(). This is not supported. Did you mean to call hydrateRoot(container, element)?");
      }
      return _y(null, e, t, !0, a);
    }
    function S2(e, t, a) {
      if (m("ReactDOM.render is no longer supported in React 18. Use createRoot instead. Until you switch to the new API, your app will behave as if it's running React 17. Learn more: https://reactjs.org/link/switch-to-createroot"), !kv(t))
        throw new Error("Target container is not a DOM element.");
      {
        var i = Bp(t) && t._reactRootContainer === void 0;
        i && m("You are calling ReactDOM.render() on a container that was previously passed to ReactDOMClient.createRoot(). This is not supported. Did you mean to call root.render(element)?");
      }
      return _y(null, e, t, !1, a);
    }
    function E2(e, t, a, i) {
      if (m("ReactDOM.unstable_renderSubtreeIntoContainer() is no longer supported in React 18. Consider using a portal instead. Until you switch to the createRoot API, your app will behave as if it's running React 17. Learn more: https://reactjs.org/link/switch-to-createroot"), !kv(a))
        throw new Error("Target container is not a DOM element.");
      if (e == null || !xs(e))
        throw new Error("parentComponent must be a valid React Component");
      return _y(e, t, a, !1, i);
    }
    var qw = !1;
    function b2(e) {
      if (qw || (qw = !0, m("unmountComponentAtNode is deprecated and will be removed in the next major release. Switch to the createRoot API. Learn more: https://reactjs.org/link/switch-to-createroot")), !kv(e))
        throw new Error("unmountComponentAtNode(...): Target container is not a DOM element.");
      {
        var t = Bp(e) && e._reactRootContainer === void 0;
        t && m("You are calling ReactDOM.unmountComponentAtNode() on a container that was previously passed to ReactDOMClient.createRoot(). This is not supported. Did you mean to call root.unmount()?");
      }
      if (e._reactRootContainer) {
        {
          var a = g1(e), i = a && !Hu(a);
          i && m("unmountComponentAtNode(): The node you're attempting to unmount was rendered by another copy of React.");
        }
        return zl(function() {
          _y(null, null, e, !1, function() {
            e._reactRootContainer = null, IE(e);
          });
        }), !0;
      } else {
        {
          var u = g1(e), d = !!(u && Hu(u)), h = e.nodeType === Ur && kv(e.parentNode) && !!e.parentNode._reactRootContainer;
          d && m("unmountComponentAtNode(): The node you're attempting to unmount was rendered by React and is not a top-level container. %s", h ? "You may have accidentally passed in a React root node instead of its container." : "Instead, have the parent component update its state and rerender in order to remove this component.");
        }
        return !1;
      }
    }
    Ru(n2), yg(r2), Ef(a2), Uh(xa), Ph(cr), (typeof Map != "function" || // $FlowIssue Flow incorrectly thinks Map has no prototype
    Map.prototype == null || typeof Map.prototype.forEach != "function" || typeof Set != "function" || // $FlowIssue Flow incorrectly thinks Set has no prototype
    Set.prototype == null || typeof Set.prototype.clear != "function" || typeof Set.prototype.forEach != "function") && m("React depends on Map and Set built-in types. Make sure that you load a polyfill in older browsers. https://reactjs.org/link/react-polyfills"), dh(wx), Uc(qS, pD, zl);
    function C2(e, t) {
      var a = arguments.length > 2 && arguments[2] !== void 0 ? arguments[2] : null;
      if (!ky(t))
        throw new Error("Target container is not a DOM element.");
      return e2(e, t, null, a);
    }
    function w2(e, t, a, i) {
      return E2(e, t, a, i);
    }
    var S1 = {
      usingClientEntryPoint: !1,
      // Keep in sync with ReactTestUtils.js.
      // This is an array for better minification.
      Events: [Hu, Bf, hm, zc, ws, qS]
    };
    function T2(e, t) {
      return S1.usingClientEntryPoint || m('You are importing createRoot from "react-dom" which is not supported. You should instead import it from "react-dom/client".'), f2(e, t);
    }
    function R2(e, t, a) {
      return S1.usingClientEntryPoint || m('You are importing hydrateRoot from "react-dom" which is not supported. You should instead import it from "react-dom/client".'), p2(e, t, a);
    }
    function x2(e) {
      return nw() && m("flushSync was called from inside a lifecycle method. React cannot flush when React is already rendering. Consider moving this call to a scheduler task or micro task."), zl(e);
    }
    var k2 = c2({
      findFiberByHostInstance: Js,
      bundleType: 1,
      version: d1,
      rendererPackageName: "react-dom"
    });
    if (!k2 && bn && window.top === window.self && (navigator.userAgent.indexOf("Chrome") > -1 && navigator.userAgent.indexOf("Edge") === -1 || navigator.userAgent.indexOf("Firefox") > -1)) {
      var Xw = window.location.protocol;
      /^(https?|file):$/.test(Xw) && console.info("%cDownload the React DevTools for a better development experience: https://reactjs.org/link/react-devtools" + (Xw === "file:" ? `
You might need to use a local HTTP server (instead of file://): https://reactjs.org/link/react-devtools-faq` : ""), "font-weight:bold");
    }
    Pa.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED = S1, Pa.createPortal = C2, Pa.createRoot = T2, Pa.findDOMNode = y2, Pa.flushSync = x2, Pa.hydrate = g2, Pa.hydrateRoot = R2, Pa.render = S2, Pa.unmountComponentAtNode = b2, Pa.unstable_batchedUpdates = qS, Pa.unstable_renderSubtreeIntoContainer = w2, Pa.version = d1, typeof __REACT_DEVTOOLS_GLOBAL_HOOK__ < "u" && typeof __REACT_DEVTOOLS_GLOBAL_HOOK__.registerInternalModuleStop == "function" && __REACT_DEVTOOLS_GLOBAL_HOOK__.registerInternalModuleStop(new Error());
  }()), Pa;
}
function AT() {
  if (!(typeof __REACT_DEVTOOLS_GLOBAL_HOOK__ > "u" || typeof __REACT_DEVTOOLS_GLOBAL_HOOK__.checkDCE != "function")) {
    if (process.env.NODE_ENV !== "production")
      throw new Error("^_^");
    try {
      __REACT_DEVTOOLS_GLOBAL_HOOK__.checkDCE(AT);
    } catch (l) {
      console.error(l);
    }
  }
}
process.env.NODE_ENV === "production" ? (AT(), z1.exports = $2()) : z1.exports = B2();
var I2 = z1.exports, U1, Oy = I2;
if (process.env.NODE_ENV === "production")
  U1 = Oy.createRoot, Oy.hydrateRoot;
else {
  var iT = Oy.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED;
  U1 = function(l, s) {
    iT.usingClientEntryPoint = !0;
    try {
      return Oy.createRoot(l, s);
    } finally {
      iT.usingClientEntryPoint = !1;
    }
  };
}
const P1 = Math.min, vd = Math.max, Uy = Math.round, ns = (l) => ({
  x: l,
  y: l
}), Y2 = {
  left: "right",
  right: "left",
  bottom: "top",
  top: "bottom"
}, W2 = {
  start: "end",
  end: "start"
};
function oT(l, s, c) {
  return vd(l, P1(s, c));
}
function $y(l, s) {
  return typeof l == "function" ? l(s) : l;
}
function hc(l) {
  return l.split("-")[0];
}
function By(l) {
  return l.split("-")[1];
}
function zT(l) {
  return l === "x" ? "y" : "x";
}
function UT(l) {
  return l === "y" ? "height" : "width";
}
function Iy(l) {
  return ["top", "bottom"].includes(hc(l)) ? "y" : "x";
}
function PT(l) {
  return zT(Iy(l));
}
function Q2(l, s, c) {
  c === void 0 && (c = !1);
  const p = By(l), y = PT(l), E = UT(y);
  let m = y === "x" ? p === (c ? "end" : "start") ? "right" : "left" : p === "start" ? "bottom" : "top";
  return s.reference[E] > s.floating[E] && (m = Py(m)), [m, Py(m)];
}
function G2(l) {
  const s = Py(l);
  return [F1(l), s, F1(s)];
}
function F1(l) {
  return l.replace(/start|end/g, (s) => W2[s]);
}
function q2(l, s, c) {
  const p = ["left", "right"], y = ["right", "left"], E = ["top", "bottom"], m = ["bottom", "top"];
  switch (l) {
    case "top":
    case "bottom":
      return c ? s ? y : p : s ? p : y;
    case "left":
    case "right":
      return s ? E : m;
    default:
      return [];
  }
}
function X2(l, s, c, p) {
  const y = By(l);
  let E = q2(hc(l), c === "start", p);
  return y && (E = E.map((m) => m + "-" + y), s && (E = E.concat(E.map(F1)))), E;
}
function Py(l) {
  return l.replace(/left|right|bottom|top/g, (s) => Y2[s]);
}
function K2(l) {
  return {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    ...l
  };
}
function Z2(l) {
  return typeof l != "number" ? K2(l) : {
    top: l,
    right: l,
    bottom: l,
    left: l
  };
}
function Fy(l) {
  const {
    x: s,
    y: c,
    width: p,
    height: y
  } = l;
  return {
    width: p,
    height: y,
    top: c,
    left: s,
    right: s + p,
    bottom: c + y,
    x: s,
    y: c
  };
}
function lT(l, s, c) {
  let {
    reference: p,
    floating: y
  } = l;
  const E = Iy(s), m = PT(s), x = UT(m), T = hc(s), D = E === "y", A = p.x + p.width / 2 - y.width / 2, L = p.y + p.height / 2 - y.height / 2, j = p[x] / 2 - y[x] / 2;
  let F;
  switch (T) {
    case "top":
      F = {
        x: A,
        y: p.y - y.height
      };
      break;
    case "bottom":
      F = {
        x: A,
        y: p.y + p.height
      };
      break;
    case "right":
      F = {
        x: p.x + p.width,
        y: L
      };
      break;
    case "left":
      F = {
        x: p.x - y.width,
        y: L
      };
      break;
    default:
      F = {
        x: p.x,
        y: p.y
      };
  }
  switch (By(s)) {
    case "start":
      F[m] -= j * (c && D ? -1 : 1);
      break;
    case "end":
      F[m] += j * (c && D ? -1 : 1);
      break;
  }
  return F;
}
const J2 = async (l, s, c) => {
  const {
    placement: p = "bottom",
    strategy: y = "absolute",
    middleware: E = [],
    platform: m
  } = c, x = E.filter(Boolean), T = await (m.isRTL == null ? void 0 : m.isRTL(s));
  let D = await m.getElementRects({
    reference: l,
    floating: s,
    strategy: y
  }), {
    x: A,
    y: L
  } = lT(D, p, T), j = p, F = {}, $ = 0;
  for (let H = 0; H < x.length; H++) {
    const {
      name: G,
      fn: W
    } = x[H], {
      x: ve,
      y: fe,
      data: oe,
      reset: ne
    } = await W({
      x: A,
      y: L,
      initialPlacement: p,
      placement: j,
      strategy: y,
      middlewareData: F,
      rects: D,
      platform: m,
      elements: {
        reference: l,
        floating: s
      }
    });
    A = ve ?? A, L = fe ?? L, F = {
      ...F,
      [G]: {
        ...F[G],
        ...oe
      }
    }, ne && $ <= 50 && ($++, typeof ne == "object" && (ne.placement && (j = ne.placement), ne.rects && (D = ne.rects === !0 ? await m.getElementRects({
      reference: l,
      floating: s,
      strategy: y
    }) : ne.rects), {
      x: A,
      y: L
    } = lT(D, j, T)), H = -1);
  }
  return {
    x: A,
    y: L,
    placement: j,
    strategy: y,
    middlewareData: F
  };
};
async function FT(l, s) {
  var c;
  s === void 0 && (s = {});
  const {
    x: p,
    y,
    platform: E,
    rects: m,
    elements: x,
    strategy: T
  } = l, {
    boundary: D = "clippingAncestors",
    rootBoundary: A = "viewport",
    elementContext: L = "floating",
    altBoundary: j = !1,
    padding: F = 0
  } = $y(s, l), $ = Z2(F), H = x[j ? L === "floating" ? "reference" : "floating" : L], G = Fy(await E.getClippingRect({
    element: (c = await (E.isElement == null ? void 0 : E.isElement(H))) == null || c ? H : H.contextElement || await (E.getDocumentElement == null ? void 0 : E.getDocumentElement(x.floating)),
    boundary: D,
    rootBoundary: A,
    strategy: T
  })), W = L === "floating" ? {
    ...m.floating,
    x: p,
    y
  } : m.reference, ve = await (E.getOffsetParent == null ? void 0 : E.getOffsetParent(x.floating)), fe = await (E.isElement == null ? void 0 : E.isElement(ve)) ? await (E.getScale == null ? void 0 : E.getScale(ve)) || {
    x: 1,
    y: 1
  } : {
    x: 1,
    y: 1
  }, oe = Fy(E.convertOffsetParentRelativeRectToViewportRelativeRect ? await E.convertOffsetParentRelativeRectToViewportRelativeRect({
    elements: x,
    rect: W,
    offsetParent: ve,
    strategy: T
  }) : W);
  return {
    top: (G.top - oe.top + $.top) / fe.y,
    bottom: (oe.bottom - G.bottom + $.bottom) / fe.y,
    left: (G.left - oe.left + $.left) / fe.x,
    right: (oe.right - G.right + $.right) / fe.x
  };
}
const eO = function(l) {
  return l === void 0 && (l = {}), {
    name: "flip",
    options: l,
    async fn(s) {
      var c, p;
      const {
        placement: y,
        middlewareData: E,
        rects: m,
        initialPlacement: x,
        platform: T,
        elements: D
      } = s, {
        mainAxis: A = !0,
        crossAxis: L = !0,
        fallbackPlacements: j,
        fallbackStrategy: F = "bestFit",
        fallbackAxisSideDirection: $ = "none",
        flipAlignment: H = !0,
        ...G
      } = $y(l, s);
      if ((c = E.arrow) != null && c.alignmentOffset)
        return {};
      const W = hc(y), ve = hc(x) === x, fe = await (T.isRTL == null ? void 0 : T.isRTL(D.floating)), oe = j || (ve || !H ? [Py(x)] : G2(x));
      !j && $ !== "none" && oe.push(...X2(x, H, $, fe));
      const ne = [x, ...oe], Ce = await FT(s, G), me = [];
      let We = ((p = E.flip) == null ? void 0 : p.overflows) || [];
      if (A && me.push(Ce[W]), L) {
        const Ue = Q2(y, m, fe);
        me.push(Ce[Ue[0]], Ce[Ue[1]]);
      }
      if (We = [...We, {
        placement: y,
        overflows: me
      }], !me.every((Ue) => Ue <= 0)) {
        var Et, Rt;
        const Ue = (((Et = E.flip) == null ? void 0 : Et.index) || 0) + 1, qe = ne[Ue];
        if (qe)
          return {
            data: {
              index: Ue,
              overflows: We
            },
            reset: {
              placement: qe
            }
          };
        let rt = (Rt = We.filter((Ot) => Ot.overflows[0] <= 0).sort((Ot, mt) => Ot.overflows[1] - mt.overflows[1])[0]) == null ? void 0 : Rt.placement;
        if (!rt)
          switch (F) {
            case "bestFit": {
              var ye;
              const Ot = (ye = We.map((mt) => [mt.placement, mt.overflows.filter((Oe) => Oe > 0).reduce((Oe, he) => Oe + he, 0)]).sort((mt, Oe) => mt[1] - Oe[1])[0]) == null ? void 0 : ye[0];
              Ot && (rt = Ot);
              break;
            }
            case "initialPlacement":
              rt = x;
              break;
          }
        if (y !== rt)
          return {
            reset: {
              placement: rt
            }
          };
      }
      return {};
    }
  };
};
async function tO(l, s) {
  const {
    placement: c,
    platform: p,
    elements: y
  } = l, E = await (p.isRTL == null ? void 0 : p.isRTL(y.floating)), m = hc(c), x = By(c), T = Iy(c) === "y", D = ["left", "top"].includes(m) ? -1 : 1, A = E && T ? -1 : 1, L = $y(s, l);
  let {
    mainAxis: j,
    crossAxis: F,
    alignmentAxis: $
  } = typeof L == "number" ? {
    mainAxis: L,
    crossAxis: 0,
    alignmentAxis: null
  } : {
    mainAxis: 0,
    crossAxis: 0,
    alignmentAxis: null,
    ...L
  };
  return x && typeof $ == "number" && (F = x === "end" ? $ * -1 : $), T ? {
    x: F * A,
    y: j * D
  } : {
    x: j * D,
    y: F * A
  };
}
const nO = function(l) {
  return l === void 0 && (l = 0), {
    name: "offset",
    options: l,
    async fn(s) {
      var c, p;
      const {
        x: y,
        y: E,
        placement: m,
        middlewareData: x
      } = s, T = await tO(s, l);
      return m === ((c = x.offset) == null ? void 0 : c.placement) && (p = x.arrow) != null && p.alignmentOffset ? {} : {
        x: y + T.x,
        y: E + T.y,
        data: {
          ...T,
          placement: m
        }
      };
    }
  };
}, rO = function(l) {
  return l === void 0 && (l = {}), {
    name: "shift",
    options: l,
    async fn(s) {
      const {
        x: c,
        y: p,
        placement: y
      } = s, {
        mainAxis: E = !0,
        crossAxis: m = !1,
        limiter: x = {
          fn: (G) => {
            let {
              x: W,
              y: ve
            } = G;
            return {
              x: W,
              y: ve
            };
          }
        },
        ...T
      } = $y(l, s), D = {
        x: c,
        y: p
      }, A = await FT(s, T), L = Iy(hc(y)), j = zT(L);
      let F = D[j], $ = D[L];
      if (E) {
        const G = j === "y" ? "top" : "left", W = j === "y" ? "bottom" : "right", ve = F + A[G], fe = F - A[W];
        F = oT(ve, F, fe);
      }
      if (m) {
        const G = L === "y" ? "top" : "left", W = L === "y" ? "bottom" : "right", ve = $ + A[G], fe = $ - A[W];
        $ = oT(ve, $, fe);
      }
      const H = x.fn({
        ...s,
        [j]: F,
        [L]: $
      });
      return {
        ...H,
        data: {
          x: H.x - c,
          y: H.y - p
        }
      };
    }
  };
};
function Yy() {
  return typeof window < "u";
}
function bd(l) {
  return HT(l) ? (l.nodeName || "").toLowerCase() : "#document";
}
function ti(l) {
  var s;
  return (l == null || (s = l.ownerDocument) == null ? void 0 : s.defaultView) || window;
}
function Hl(l) {
  var s;
  return (s = (HT(l) ? l.ownerDocument : l.document) || window.document) == null ? void 0 : s.documentElement;
}
function HT(l) {
  return Yy() ? l instanceof Node || l instanceof ti(l).Node : !1;
}
function ro(l) {
  return Yy() ? l instanceof Element || l instanceof ti(l).Element : !1;
}
function Go(l) {
  return Yy() ? l instanceof HTMLElement || l instanceof ti(l).HTMLElement : !1;
}
function uT(l) {
  return !Yy() || typeof ShadowRoot > "u" ? !1 : l instanceof ShadowRoot || l instanceof ti(l).ShadowRoot;
}
function Uv(l) {
  const {
    overflow: s,
    overflowX: c,
    overflowY: p,
    display: y
  } = ao(l);
  return /auto|scroll|overlay|hidden|clip/.test(s + p + c) && !["inline", "contents"].includes(y);
}
function aO(l) {
  return ["table", "td", "th"].includes(bd(l));
}
function Wy(l) {
  return [":popover-open", ":modal"].some((s) => {
    try {
      return l.matches(s);
    } catch {
      return !1;
    }
  });
}
function J1(l) {
  const s = eE(), c = ro(l) ? ao(l) : l;
  return c.transform !== "none" || c.perspective !== "none" || (c.containerType ? c.containerType !== "normal" : !1) || !s && (c.backdropFilter ? c.backdropFilter !== "none" : !1) || !s && (c.filter ? c.filter !== "none" : !1) || ["transform", "perspective", "filter"].some((p) => (c.willChange || "").includes(p)) || ["paint", "layout", "strict", "content"].some((p) => (c.contain || "").includes(p));
}
function iO(l) {
  let s = rs(l);
  for (; Go(s) && !yd(s); ) {
    if (J1(s))
      return s;
    if (Wy(s))
      return null;
    s = rs(s);
  }
  return null;
}
function eE() {
  return typeof CSS > "u" || !CSS.supports ? !1 : CSS.supports("-webkit-backdrop-filter", "none");
}
function yd(l) {
  return ["html", "body", "#document"].includes(bd(l));
}
function ao(l) {
  return ti(l).getComputedStyle(l);
}
function Qy(l) {
  return ro(l) ? {
    scrollLeft: l.scrollLeft,
    scrollTop: l.scrollTop
  } : {
    scrollLeft: l.scrollX,
    scrollTop: l.scrollY
  };
}
function rs(l) {
  if (bd(l) === "html")
    return l;
  const s = (
    // Step into the shadow DOM of the parent of a slotted node.
    l.assignedSlot || // DOM Element detected.
    l.parentNode || // ShadowRoot detected.
    uT(l) && l.host || // Fallback.
    Hl(l)
  );
  return uT(s) ? s.host : s;
}
function jT(l) {
  const s = rs(l);
  return yd(s) ? l.ownerDocument ? l.ownerDocument.body : l.body : Go(s) && Uv(s) ? s : jT(s);
}
function H1(l, s, c) {
  var p;
  s === void 0 && (s = []), c === void 0 && (c = !0);
  const y = jT(l), E = y === ((p = l.ownerDocument) == null ? void 0 : p.body), m = ti(y);
  if (E) {
    const x = j1(m);
    return s.concat(m, m.visualViewport || [], Uv(y) ? y : [], x && c ? H1(x) : []);
  }
  return s.concat(y, H1(y, [], c));
}
function j1(l) {
  return l.parent && Object.getPrototypeOf(l.parent) ? l.frameElement : null;
}
function VT(l) {
  const s = ao(l);
  let c = parseFloat(s.width) || 0, p = parseFloat(s.height) || 0;
  const y = Go(l), E = y ? l.offsetWidth : c, m = y ? l.offsetHeight : p, x = Uy(c) !== E || Uy(p) !== m;
  return x && (c = E, p = m), {
    width: c,
    height: p,
    $: x
  };
}
function $T(l) {
  return ro(l) ? l : l.contextElement;
}
function hd(l) {
  const s = $T(l);
  if (!Go(s))
    return ns(1);
  const c = s.getBoundingClientRect(), {
    width: p,
    height: y,
    $: E
  } = VT(s);
  let m = (E ? Uy(c.width) : c.width) / p, x = (E ? Uy(c.height) : c.height) / y;
  return (!m || !Number.isFinite(m)) && (m = 1), (!x || !Number.isFinite(x)) && (x = 1), {
    x: m,
    y: x
  };
}
const oO = /* @__PURE__ */ ns(0);
function BT(l) {
  const s = ti(l);
  return !eE() || !s.visualViewport ? oO : {
    x: s.visualViewport.offsetLeft,
    y: s.visualViewport.offsetTop
  };
}
function lO(l, s, c) {
  return s === void 0 && (s = !1), !c || s && c !== ti(l) ? !1 : s;
}
function Nv(l, s, c, p) {
  s === void 0 && (s = !1), c === void 0 && (c = !1);
  const y = l.getBoundingClientRect(), E = $T(l);
  let m = ns(1);
  s && (p ? ro(p) && (m = hd(p)) : m = hd(l));
  const x = lO(E, c, p) ? BT(E) : ns(0);
  let T = (y.left + x.x) / m.x, D = (y.top + x.y) / m.y, A = y.width / m.x, L = y.height / m.y;
  if (E) {
    const j = ti(E), F = p && ro(p) ? ti(p) : p;
    let $ = j, H = j1($);
    for (; H && p && F !== $; ) {
      const G = hd(H), W = H.getBoundingClientRect(), ve = ao(H), fe = W.left + (H.clientLeft + parseFloat(ve.paddingLeft)) * G.x, oe = W.top + (H.clientTop + parseFloat(ve.paddingTop)) * G.y;
      T *= G.x, D *= G.y, A *= G.x, L *= G.y, T += fe, D += oe, $ = ti(H), H = j1($);
    }
  }
  return Fy({
    width: A,
    height: L,
    x: T,
    y: D
  });
}
function uO(l) {
  let {
    elements: s,
    rect: c,
    offsetParent: p,
    strategy: y
  } = l;
  const E = y === "fixed", m = Hl(p), x = s ? Wy(s.floating) : !1;
  if (p === m || x && E)
    return c;
  let T = {
    scrollLeft: 0,
    scrollTop: 0
  }, D = ns(1);
  const A = ns(0), L = Go(p);
  if ((L || !L && !E) && ((bd(p) !== "body" || Uv(m)) && (T = Qy(p)), Go(p))) {
    const j = Nv(p);
    D = hd(p), A.x = j.x + p.clientLeft, A.y = j.y + p.clientTop;
  }
  return {
    width: c.width * D.x,
    height: c.height * D.y,
    x: c.x * D.x - T.scrollLeft * D.x + A.x,
    y: c.y * D.y - T.scrollTop * D.y + A.y
  };
}
function sO(l) {
  return Array.from(l.getClientRects());
}
function V1(l, s) {
  const c = Qy(l).scrollLeft;
  return s ? s.left + c : Nv(Hl(l)).left + c;
}
function cO(l) {
  const s = Hl(l), c = Qy(l), p = l.ownerDocument.body, y = vd(s.scrollWidth, s.clientWidth, p.scrollWidth, p.clientWidth), E = vd(s.scrollHeight, s.clientHeight, p.scrollHeight, p.clientHeight);
  let m = -c.scrollLeft + V1(l);
  const x = -c.scrollTop;
  return ao(p).direction === "rtl" && (m += vd(s.clientWidth, p.clientWidth) - y), {
    width: y,
    height: E,
    x: m,
    y: x
  };
}
function fO(l, s) {
  const c = ti(l), p = Hl(l), y = c.visualViewport;
  let E = p.clientWidth, m = p.clientHeight, x = 0, T = 0;
  if (y) {
    E = y.width, m = y.height;
    const D = eE();
    (!D || D && s === "fixed") && (x = y.offsetLeft, T = y.offsetTop);
  }
  return {
    width: E,
    height: m,
    x,
    y: T
  };
}
function dO(l, s) {
  const c = Nv(l, !0, s === "fixed"), p = c.top + l.clientTop, y = c.left + l.clientLeft, E = Go(l) ? hd(l) : ns(1), m = l.clientWidth * E.x, x = l.clientHeight * E.y, T = y * E.x, D = p * E.y;
  return {
    width: m,
    height: x,
    x: T,
    y: D
  };
}
function sT(l, s, c) {
  let p;
  if (s === "viewport")
    p = fO(l, c);
  else if (s === "document")
    p = cO(Hl(l));
  else if (ro(s))
    p = dO(s, c);
  else {
    const y = BT(l);
    p = {
      ...s,
      x: s.x - y.x,
      y: s.y - y.y
    };
  }
  return Fy(p);
}
function IT(l, s) {
  const c = rs(l);
  return c === s || !ro(c) || yd(c) ? !1 : ao(c).position === "fixed" || IT(c, s);
}
function pO(l, s) {
  const c = s.get(l);
  if (c)
    return c;
  let p = H1(l, [], !1).filter((x) => ro(x) && bd(x) !== "body"), y = null;
  const E = ao(l).position === "fixed";
  let m = E ? rs(l) : l;
  for (; ro(m) && !yd(m); ) {
    const x = ao(m), T = J1(m);
    !T && x.position === "fixed" && (y = null), (E ? !T && !y : !T && x.position === "static" && y && ["absolute", "fixed"].includes(y.position) || Uv(m) && !T && IT(l, m)) ? p = p.filter((D) => D !== m) : y = x, m = rs(m);
  }
  return s.set(l, p), p;
}
function vO(l) {
  let {
    element: s,
    boundary: c,
    rootBoundary: p,
    strategy: y
  } = l;
  const E = [...c === "clippingAncestors" ? Wy(s) ? [] : pO(s, this._c) : [].concat(c), p], m = E[0], x = E.reduce((T, D) => {
    const A = sT(s, D, y);
    return T.top = vd(A.top, T.top), T.right = P1(A.right, T.right), T.bottom = P1(A.bottom, T.bottom), T.left = vd(A.left, T.left), T;
  }, sT(s, m, y));
  return {
    width: x.right - x.left,
    height: x.bottom - x.top,
    x: x.left,
    y: x.top
  };
}
function hO(l) {
  const {
    width: s,
    height: c
  } = VT(l);
  return {
    width: s,
    height: c
  };
}
function mO(l, s, c) {
  const p = Go(s), y = Hl(s), E = c === "fixed", m = Nv(l, !0, E, s);
  let x = {
    scrollLeft: 0,
    scrollTop: 0
  };
  const T = ns(0);
  if (p || !p && !E)
    if ((bd(s) !== "body" || Uv(y)) && (x = Qy(s)), p) {
      const F = Nv(s, !0, E, s);
      T.x = F.x + s.clientLeft, T.y = F.y + s.clientTop;
    } else y && (T.x = V1(y));
  let D = 0, A = 0;
  if (y && !p && !E) {
    const F = y.getBoundingClientRect();
    A = F.top + x.scrollTop, D = F.left + x.scrollLeft - // RTL <body> scrollbar.
    V1(y, F);
  }
  const L = m.left + x.scrollLeft - T.x - D, j = m.top + x.scrollTop - T.y - A;
  return {
    x: L,
    y: j,
    width: m.width,
    height: m.height
  };
}
function C1(l) {
  return ao(l).position === "static";
}
function cT(l, s) {
  if (!Go(l) || ao(l).position === "fixed")
    return null;
  if (s)
    return s(l);
  let c = l.offsetParent;
  return Hl(l) === c && (c = c.ownerDocument.body), c;
}
function YT(l, s) {
  const c = ti(l);
  if (Wy(l))
    return c;
  if (!Go(l)) {
    let y = rs(l);
    for (; y && !yd(y); ) {
      if (ro(y) && !C1(y))
        return y;
      y = rs(y);
    }
    return c;
  }
  let p = cT(l, s);
  for (; p && aO(p) && C1(p); )
    p = cT(p, s);
  return p && yd(p) && C1(p) && !J1(p) ? c : p || iO(l) || c;
}
const yO = async function(l) {
  const s = this.getOffsetParent || YT, c = this.getDimensions, p = await c(l.floating);
  return {
    reference: mO(l.reference, await s(l.floating), l.strategy),
    floating: {
      x: 0,
      y: 0,
      width: p.width,
      height: p.height
    }
  };
};
function gO(l) {
  return ao(l).direction === "rtl";
}
const SO = {
  convertOffsetParentRelativeRectToViewportRelativeRect: uO,
  getDocumentElement: Hl,
  getClippingRect: vO,
  getOffsetParent: YT,
  getElementRects: yO,
  getClientRects: sO,
  getDimensions: hO,
  getScale: hd,
  isElement: ro,
  isRTL: gO
}, EO = nO, bO = rO, CO = eO, wO = (l, s, c) => {
  const p = /* @__PURE__ */ new Map(), y = {
    platform: SO,
    ...c
  }, E = {
    ...y.platform,
    _c: p
  };
  return J2(l, s, {
    ...y,
    platform: E
  });
};
var Lv = typeof globalThis < "u" ? globalThis : typeof window < "u" ? window : typeof global < "u" ? global : typeof self < "u" ? self : {};
function WT(l) {
  return l && l.__esModule && Object.prototype.hasOwnProperty.call(l, "default") ? l.default : l;
}
var QT = { exports: {} };
(function(l) {
  (function(s, c) {
    l.exports ? l.exports = c() : s.log = c();
  })(Lv, function() {
    var s = function() {
    }, c = "undefined", p = typeof window !== c && typeof window.navigator !== c && /Trident\/|MSIE /.test(window.navigator.userAgent), y = [
      "trace",
      "debug",
      "info",
      "warn",
      "error"
    ], E = {}, m = null;
    function x(H, G) {
      var W = H[G];
      if (typeof W.bind == "function")
        return W.bind(H);
      try {
        return Function.prototype.bind.call(W, H);
      } catch {
        return function() {
          return Function.prototype.apply.apply(W, [H, arguments]);
        };
      }
    }
    function T() {
      console.log && (console.log.apply ? console.log.apply(console, arguments) : Function.prototype.apply.apply(console.log, [console, arguments])), console.trace && console.trace();
    }
    function D(H) {
      return H === "debug" && (H = "log"), typeof console === c ? !1 : H === "trace" && p ? T : console[H] !== void 0 ? x(console, H) : console.log !== void 0 ? x(console, "log") : s;
    }
    function A() {
      for (var H = this.getLevel(), G = 0; G < y.length; G++) {
        var W = y[G];
        this[W] = G < H ? s : this.methodFactory(W, H, this.name);
      }
      if (this.log = this.debug, typeof console === c && H < this.levels.SILENT)
        return "No console available for logging";
    }
    function L(H) {
      return function() {
        typeof console !== c && (A.call(this), this[H].apply(this, arguments));
      };
    }
    function j(H, G, W) {
      return D(H) || L.apply(this, arguments);
    }
    function F(H, G) {
      var W = this, ve, fe, oe, ne = "loglevel";
      typeof H == "string" ? ne += ":" + H : typeof H == "symbol" && (ne = void 0);
      function Ce(ye) {
        var Ue = (y[ye] || "silent").toUpperCase();
        if (!(typeof window === c || !ne)) {
          try {
            window.localStorage[ne] = Ue;
            return;
          } catch {
          }
          try {
            window.document.cookie = encodeURIComponent(ne) + "=" + Ue + ";";
          } catch {
          }
        }
      }
      function me() {
        var ye;
        if (!(typeof window === c || !ne)) {
          try {
            ye = window.localStorage[ne];
          } catch {
          }
          if (typeof ye === c)
            try {
              var Ue = window.document.cookie, qe = encodeURIComponent(ne), rt = Ue.indexOf(qe + "=");
              rt !== -1 && (ye = /^([^;]+)/.exec(
                Ue.slice(rt + qe.length + 1)
              )[1]);
            } catch {
            }
          return W.levels[ye] === void 0 && (ye = void 0), ye;
        }
      }
      function We() {
        if (!(typeof window === c || !ne)) {
          try {
            window.localStorage.removeItem(ne);
          } catch {
          }
          try {
            window.document.cookie = encodeURIComponent(ne) + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
          } catch {
          }
        }
      }
      function Et(ye) {
        var Ue = ye;
        if (typeof Ue == "string" && W.levels[Ue.toUpperCase()] !== void 0 && (Ue = W.levels[Ue.toUpperCase()]), typeof Ue == "number" && Ue >= 0 && Ue <= W.levels.SILENT)
          return Ue;
        throw new TypeError("log.setLevel() called with invalid level: " + ye);
      }
      W.name = H, W.levels = {
        TRACE: 0,
        DEBUG: 1,
        INFO: 2,
        WARN: 3,
        ERROR: 4,
        SILENT: 5
      }, W.methodFactory = G || j, W.getLevel = function() {
        return oe ?? fe ?? ve;
      }, W.setLevel = function(ye, Ue) {
        return oe = Et(ye), Ue !== !1 && Ce(oe), A.call(W);
      }, W.setDefaultLevel = function(ye) {
        fe = Et(ye), me() || W.setLevel(ye, !1);
      }, W.resetLevel = function() {
        oe = null, We(), A.call(W);
      }, W.enableAll = function(ye) {
        W.setLevel(W.levels.TRACE, ye);
      }, W.disableAll = function(ye) {
        W.setLevel(W.levels.SILENT, ye);
      }, W.rebuild = function() {
        if (m !== W && (ve = Et(m.getLevel())), A.call(W), m === W)
          for (var ye in E)
            E[ye].rebuild();
      }, ve = Et(
        m ? m.getLevel() : "WARN"
      );
      var Rt = me();
      Rt != null && (oe = Et(Rt)), A.call(W);
    }
    m = new F(), m.getLogger = function(H) {
      if (typeof H != "symbol" && typeof H != "string" || H === "")
        throw new TypeError("You must supply a name when creating a logger.");
      var G = E[H];
      return G || (G = E[H] = new F(
        H,
        m.methodFactory
      )), G;
    };
    var $ = typeof window !== c ? window.log : void 0;
    return m.noConflict = function() {
      return typeof window !== c && window.log === m && (window.log = $), m;
    }, m.getLoggers = function() {
      return E;
    }, m.default = m, m;
  });
})(QT);
var TO = QT.exports;
const RO = /* @__PURE__ */ WT(TO);
var $1 = function(l, s) {
  return $1 = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(c, p) {
    c.__proto__ = p;
  } || function(c, p) {
    for (var y in p) Object.prototype.hasOwnProperty.call(p, y) && (c[y] = p[y]);
  }, $1(l, s);
};
function jl(l, s) {
  if (typeof s != "function" && s !== null)
    throw new TypeError("Class extends value " + String(s) + " is not a constructor or null");
  $1(l, s);
  function c() {
    this.constructor = l;
  }
  l.prototype = s === null ? Object.create(s) : (c.prototype = s.prototype, new c());
}
function xO(l, s, c, p) {
  function y(E) {
    return E instanceof c ? E : new c(function(m) {
      m(E);
    });
  }
  return new (c || (c = Promise))(function(E, m) {
    function x(A) {
      try {
        D(p.next(A));
      } catch (L) {
        m(L);
      }
    }
    function T(A) {
      try {
        D(p.throw(A));
      } catch (L) {
        m(L);
      }
    }
    function D(A) {
      A.done ? E(A.value) : y(A.value).then(x, T);
    }
    D((p = p.apply(l, [])).next());
  });
}
function GT(l, s) {
  var c = { label: 0, sent: function() {
    if (E[0] & 1) throw E[1];
    return E[1];
  }, trys: [], ops: [] }, p, y, E, m = Object.create((typeof Iterator == "function" ? Iterator : Object).prototype);
  return m.next = x(0), m.throw = x(1), m.return = x(2), typeof Symbol == "function" && (m[Symbol.iterator] = function() {
    return this;
  }), m;
  function x(D) {
    return function(A) {
      return T([D, A]);
    };
  }
  function T(D) {
    if (p) throw new TypeError("Generator is already executing.");
    for (; m && (m = 0, D[0] && (c = 0)), c; ) try {
      if (p = 1, y && (E = D[0] & 2 ? y.return : D[0] ? y.throw || ((E = y.return) && E.call(y), 0) : y.next) && !(E = E.call(y, D[1])).done) return E;
      switch (y = 0, E && (D = [D[0] & 2, E.value]), D[0]) {
        case 0:
        case 1:
          E = D;
          break;
        case 4:
          return c.label++, { value: D[1], done: !1 };
        case 5:
          c.label++, y = D[1], D = [0];
          continue;
        case 7:
          D = c.ops.pop(), c.trys.pop();
          continue;
        default:
          if (E = c.trys, !(E = E.length > 0 && E[E.length - 1]) && (D[0] === 6 || D[0] === 2)) {
            c = 0;
            continue;
          }
          if (D[0] === 3 && (!E || D[1] > E[0] && D[1] < E[3])) {
            c.label = D[1];
            break;
          }
          if (D[0] === 6 && c.label < E[1]) {
            c.label = E[1], E = D;
            break;
          }
          if (E && c.label < E[2]) {
            c.label = E[2], c.ops.push(D);
            break;
          }
          E[2] && c.ops.pop(), c.trys.pop();
          continue;
      }
      D = s.call(l, c);
    } catch (A) {
      D = [6, A], y = 0;
    } finally {
      p = E = 0;
    }
    if (D[0] & 5) throw D[1];
    return { value: D[0] ? D[1] : void 0, done: !0 };
  }
}
function gd(l) {
  var s = typeof Symbol == "function" && Symbol.iterator, c = s && l[s], p = 0;
  if (c) return c.call(l);
  if (l && typeof l.length == "number") return {
    next: function() {
      return l && p >= l.length && (l = void 0), { value: l && l[p++], done: !l };
    }
  };
  throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}
function Sd(l, s) {
  var c = typeof Symbol == "function" && l[Symbol.iterator];
  if (!c) return l;
  var p = c.call(l), y, E = [], m;
  try {
    for (; (s === void 0 || s-- > 0) && !(y = p.next()).done; ) E.push(y.value);
  } catch (x) {
    m = { error: x };
  } finally {
    try {
      y && !y.done && (c = p.return) && c.call(p);
    } finally {
      if (m) throw m.error;
    }
  }
  return E;
}
function Av(l, s, c) {
  if (arguments.length === 2) for (var p = 0, y = s.length, E; p < y; p++)
    (E || !(p in s)) && (E || (E = Array.prototype.slice.call(s, 0, p)), E[p] = s[p]);
  return l.concat(E || Array.prototype.slice.call(s));
}
function md(l) {
  return this instanceof md ? (this.v = l, this) : new md(l);
}
function kO(l, s, c) {
  if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
  var p = c.apply(l, s || []), y, E = [];
  return y = Object.create((typeof AsyncIterator == "function" ? AsyncIterator : Object).prototype), x("next"), x("throw"), x("return", m), y[Symbol.asyncIterator] = function() {
    return this;
  }, y;
  function m(F) {
    return function($) {
      return Promise.resolve($).then(F, L);
    };
  }
  function x(F, $) {
    p[F] && (y[F] = function(H) {
      return new Promise(function(G, W) {
        E.push([F, H, G, W]) > 1 || T(F, H);
      });
    }, $ && (y[F] = $(y[F])));
  }
  function T(F, $) {
    try {
      D(p[F]($));
    } catch (H) {
      j(E[0][3], H);
    }
  }
  function D(F) {
    F.value instanceof md ? Promise.resolve(F.value.v).then(A, L) : j(E[0][2], F);
  }
  function A(F) {
    T("next", F);
  }
  function L(F) {
    T("throw", F);
  }
  function j(F, $) {
    F($), E.shift(), E.length && T(E[0][0], E[0][1]);
  }
}
function _O(l) {
  if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
  var s = l[Symbol.asyncIterator], c;
  return s ? s.call(l) : (l = typeof gd == "function" ? gd(l) : l[Symbol.iterator](), c = {}, p("next"), p("throw"), p("return"), c[Symbol.asyncIterator] = function() {
    return this;
  }, c);
  function p(E) {
    c[E] = l[E] && function(m) {
      return new Promise(function(x, T) {
        m = l[E](m), y(x, T, m.done, m.value);
      });
    };
  }
  function y(E, m, x, T) {
    Promise.resolve(T).then(function(D) {
      E({ value: D, done: x });
    }, m);
  }
}
function un(l) {
  return typeof l == "function";
}
function tE(l) {
  var s = function(p) {
    Error.call(p), p.stack = new Error().stack;
  }, c = l(s);
  return c.prototype = Object.create(Error.prototype), c.prototype.constructor = c, c;
}
var w1 = tE(function(l) {
  return function(s) {
    l(this), this.message = s ? s.length + ` errors occurred during unsubscription:
` + s.map(function(c, p) {
      return p + 1 + ") " + c.toString();
    }).join(`
  `) : "", this.name = "UnsubscriptionError", this.errors = s;
  };
});
function Hy(l, s) {
  if (l) {
    var c = l.indexOf(s);
    0 <= c && l.splice(c, 1);
  }
}
var Pv = function() {
  function l(s) {
    this.initialTeardown = s, this.closed = !1, this._parentage = null, this._finalizers = null;
  }
  return l.prototype.unsubscribe = function() {
    var s, c, p, y, E;
    if (!this.closed) {
      this.closed = !0;
      var m = this._parentage;
      if (m)
        if (this._parentage = null, Array.isArray(m))
          try {
            for (var x = gd(m), T = x.next(); !T.done; T = x.next()) {
              var D = T.value;
              D.remove(this);
            }
          } catch (H) {
            s = { error: H };
          } finally {
            try {
              T && !T.done && (c = x.return) && c.call(x);
            } finally {
              if (s) throw s.error;
            }
          }
        else
          m.remove(this);
      var A = this.initialTeardown;
      if (un(A))
        try {
          A();
        } catch (H) {
          E = H instanceof w1 ? H.errors : [H];
        }
      var L = this._finalizers;
      if (L) {
        this._finalizers = null;
        try {
          for (var j = gd(L), F = j.next(); !F.done; F = j.next()) {
            var $ = F.value;
            try {
              fT($);
            } catch (H) {
              E = E ?? [], H instanceof w1 ? E = Av(Av([], Sd(E)), Sd(H.errors)) : E.push(H);
            }
          }
        } catch (H) {
          p = { error: H };
        } finally {
          try {
            F && !F.done && (y = j.return) && y.call(j);
          } finally {
            if (p) throw p.error;
          }
        }
      }
      if (E)
        throw new w1(E);
    }
  }, l.prototype.add = function(s) {
    var c;
    if (s && s !== this)
      if (this.closed)
        fT(s);
      else {
        if (s instanceof l) {
          if (s.closed || s._hasParent(this))
            return;
          s._addParent(this);
        }
        (this._finalizers = (c = this._finalizers) !== null && c !== void 0 ? c : []).push(s);
      }
  }, l.prototype._hasParent = function(s) {
    var c = this._parentage;
    return c === s || Array.isArray(c) && c.includes(s);
  }, l.prototype._addParent = function(s) {
    var c = this._parentage;
    this._parentage = Array.isArray(c) ? (c.push(s), c) : c ? [c, s] : s;
  }, l.prototype._removeParent = function(s) {
    var c = this._parentage;
    c === s ? this._parentage = null : Array.isArray(c) && Hy(c, s);
  }, l.prototype.remove = function(s) {
    var c = this._finalizers;
    c && Hy(c, s), s instanceof l && s._removeParent(this);
  }, l.EMPTY = function() {
    var s = new l();
    return s.closed = !0, s;
  }(), l;
}(), qT = Pv.EMPTY;
function XT(l) {
  return l instanceof Pv || l && "closed" in l && un(l.remove) && un(l.add) && un(l.unsubscribe);
}
function fT(l) {
  un(l) ? l() : l.unsubscribe();
}
var KT = {
  onUnhandledError: null,
  onStoppedNotification: null,
  Promise: void 0,
  useDeprecatedSynchronousErrorHandling: !1,
  useDeprecatedNextContext: !1
}, DO = {
  setTimeout: function(l, s) {
    for (var c = [], p = 2; p < arguments.length; p++)
      c[p - 2] = arguments[p];
    return setTimeout.apply(void 0, Av([l, s], Sd(c)));
  },
  clearTimeout: function(l) {
    return clearTimeout(l);
  },
  delegate: void 0
};
function ZT(l) {
  DO.setTimeout(function() {
    throw l;
  });
}
function B1() {
}
function Ay(l) {
  l();
}
var nE = function(l) {
  jl(s, l);
  function s(c) {
    var p = l.call(this) || this;
    return p.isStopped = !1, c ? (p.destination = c, XT(c) && c.add(p)) : p.destination = NO, p;
  }
  return s.create = function(c, p, y) {
    return new I1(c, p, y);
  }, s.prototype.next = function(c) {
    this.isStopped || this._next(c);
  }, s.prototype.error = function(c) {
    this.isStopped || (this.isStopped = !0, this._error(c));
  }, s.prototype.complete = function() {
    this.isStopped || (this.isStopped = !0, this._complete());
  }, s.prototype.unsubscribe = function() {
    this.closed || (this.isStopped = !0, l.prototype.unsubscribe.call(this), this.destination = null);
  }, s.prototype._next = function(c) {
    this.destination.next(c);
  }, s.prototype._error = function(c) {
    try {
      this.destination.error(c);
    } finally {
      this.unsubscribe();
    }
  }, s.prototype._complete = function() {
    try {
      this.destination.complete();
    } finally {
      this.unsubscribe();
    }
  }, s;
}(Pv), OO = Function.prototype.bind;
function T1(l, s) {
  return OO.call(l, s);
}
var MO = function() {
  function l(s) {
    this.partialObserver = s;
  }
  return l.prototype.next = function(s) {
    var c = this.partialObserver;
    if (c.next)
      try {
        c.next(s);
      } catch (p) {
        My(p);
      }
  }, l.prototype.error = function(s) {
    var c = this.partialObserver;
    if (c.error)
      try {
        c.error(s);
      } catch (p) {
        My(p);
      }
    else
      My(s);
  }, l.prototype.complete = function() {
    var s = this.partialObserver;
    if (s.complete)
      try {
        s.complete();
      } catch (c) {
        My(c);
      }
  }, l;
}(), I1 = function(l) {
  jl(s, l);
  function s(c, p, y) {
    var E = l.call(this) || this, m;
    if (un(c) || !c)
      m = {
        next: c ?? void 0,
        error: p ?? void 0,
        complete: y ?? void 0
      };
    else {
      var x;
      E && KT.useDeprecatedNextContext ? (x = Object.create(c), x.unsubscribe = function() {
        return E.unsubscribe();
      }, m = {
        next: c.next && T1(c.next, x),
        error: c.error && T1(c.error, x),
        complete: c.complete && T1(c.complete, x)
      }) : m = c;
    }
    return E.destination = new MO(m), E;
  }
  return s;
}(nE);
function My(l) {
  ZT(l);
}
function LO(l) {
  throw l;
}
var NO = {
  closed: !0,
  next: B1,
  error: LO,
  complete: B1
}, rE = function() {
  return typeof Symbol == "function" && Symbol.observable || "@@observable";
}();
function aE(l) {
  return l;
}
function AO(l) {
  return l.length === 0 ? aE : l.length === 1 ? l[0] : function(s) {
    return l.reduce(function(c, p) {
      return p(c);
    }, s);
  };
}
var Jr = function() {
  function l(s) {
    s && (this._subscribe = s);
  }
  return l.prototype.lift = function(s) {
    var c = new l();
    return c.source = this, c.operator = s, c;
  }, l.prototype.subscribe = function(s, c, p) {
    var y = this, E = UO(s) ? s : new I1(s, c, p);
    return Ay(function() {
      var m = y, x = m.operator, T = m.source;
      E.add(x ? x.call(E, T) : T ? y._subscribe(E) : y._trySubscribe(E));
    }), E;
  }, l.prototype._trySubscribe = function(s) {
    try {
      return this._subscribe(s);
    } catch (c) {
      s.error(c);
    }
  }, l.prototype.forEach = function(s, c) {
    var p = this;
    return c = dT(c), new c(function(y, E) {
      var m = new I1({
        next: function(x) {
          try {
            s(x);
          } catch (T) {
            E(T), m.unsubscribe();
          }
        },
        error: E,
        complete: y
      });
      p.subscribe(m);
    });
  }, l.prototype._subscribe = function(s) {
    var c;
    return (c = this.source) === null || c === void 0 ? void 0 : c.subscribe(s);
  }, l.prototype[rE] = function() {
    return this;
  }, l.prototype.pipe = function() {
    for (var s = [], c = 0; c < arguments.length; c++)
      s[c] = arguments[c];
    return AO(s)(this);
  }, l.prototype.toPromise = function(s) {
    var c = this;
    return s = dT(s), new s(function(p, y) {
      var E;
      c.subscribe(function(m) {
        return E = m;
      }, function(m) {
        return y(m);
      }, function() {
        return p(E);
      });
    });
  }, l.create = function(s) {
    return new l(s);
  }, l;
}();
function dT(l) {
  var s;
  return (s = l ?? KT.Promise) !== null && s !== void 0 ? s : Promise;
}
function zO(l) {
  return l && un(l.next) && un(l.error) && un(l.complete);
}
function UO(l) {
  return l && l instanceof nE || zO(l) && XT(l);
}
function PO(l) {
  return un(l == null ? void 0 : l.lift);
}
function qo(l) {
  return function(s) {
    if (PO(s))
      return s.lift(function(c) {
        try {
          return l(c, this);
        } catch (p) {
          this.error(p);
        }
      });
    throw new TypeError("Unable to lift unknown Observable type");
  };
}
function Fl(l, s, c, p, y) {
  return new FO(l, s, c, p, y);
}
var FO = function(l) {
  jl(s, l);
  function s(c, p, y, E, m, x) {
    var T = l.call(this, c) || this;
    return T.onFinalize = m, T.shouldUnsubscribe = x, T._next = p ? function(D) {
      try {
        p(D);
      } catch (A) {
        c.error(A);
      }
    } : l.prototype._next, T._error = E ? function(D) {
      try {
        E(D);
      } catch (A) {
        c.error(A);
      } finally {
        this.unsubscribe();
      }
    } : l.prototype._error, T._complete = y ? function() {
      try {
        y();
      } catch (D) {
        c.error(D);
      } finally {
        this.unsubscribe();
      }
    } : l.prototype._complete, T;
  }
  return s.prototype.unsubscribe = function() {
    var c;
    if (!this.shouldUnsubscribe || this.shouldUnsubscribe()) {
      var p = this.closed;
      l.prototype.unsubscribe.call(this), !p && ((c = this.onFinalize) === null || c === void 0 || c.call(this));
    }
  }, s;
}(nE), HO = tE(function(l) {
  return function() {
    l(this), this.name = "ObjectUnsubscribedError", this.message = "object unsubscribed";
  };
}), Ed = function(l) {
  jl(s, l);
  function s() {
    var c = l.call(this) || this;
    return c.closed = !1, c.currentObservers = null, c.observers = [], c.isStopped = !1, c.hasError = !1, c.thrownError = null, c;
  }
  return s.prototype.lift = function(c) {
    var p = new pT(this, this);
    return p.operator = c, p;
  }, s.prototype._throwIfClosed = function() {
    if (this.closed)
      throw new HO();
  }, s.prototype.next = function(c) {
    var p = this;
    Ay(function() {
      var y, E;
      if (p._throwIfClosed(), !p.isStopped) {
        p.currentObservers || (p.currentObservers = Array.from(p.observers));
        try {
          for (var m = gd(p.currentObservers), x = m.next(); !x.done; x = m.next()) {
            var T = x.value;
            T.next(c);
          }
        } catch (D) {
          y = { error: D };
        } finally {
          try {
            x && !x.done && (E = m.return) && E.call(m);
          } finally {
            if (y) throw y.error;
          }
        }
      }
    });
  }, s.prototype.error = function(c) {
    var p = this;
    Ay(function() {
      if (p._throwIfClosed(), !p.isStopped) {
        p.hasError = p.isStopped = !0, p.thrownError = c;
        for (var y = p.observers; y.length; )
          y.shift().error(c);
      }
    });
  }, s.prototype.complete = function() {
    var c = this;
    Ay(function() {
      if (c._throwIfClosed(), !c.isStopped) {
        c.isStopped = !0;
        for (var p = c.observers; p.length; )
          p.shift().complete();
      }
    });
  }, s.prototype.unsubscribe = function() {
    this.isStopped = this.closed = !0, this.observers = this.currentObservers = null;
  }, Object.defineProperty(s.prototype, "observed", {
    get: function() {
      var c;
      return ((c = this.observers) === null || c === void 0 ? void 0 : c.length) > 0;
    },
    enumerable: !1,
    configurable: !0
  }), s.prototype._trySubscribe = function(c) {
    return this._throwIfClosed(), l.prototype._trySubscribe.call(this, c);
  }, s.prototype._subscribe = function(c) {
    return this._throwIfClosed(), this._checkFinalizedStatuses(c), this._innerSubscribe(c);
  }, s.prototype._innerSubscribe = function(c) {
    var p = this, y = this, E = y.hasError, m = y.isStopped, x = y.observers;
    return E || m ? qT : (this.currentObservers = null, x.push(c), new Pv(function() {
      p.currentObservers = null, Hy(x, c);
    }));
  }, s.prototype._checkFinalizedStatuses = function(c) {
    var p = this, y = p.hasError, E = p.thrownError, m = p.isStopped;
    y ? c.error(E) : m && c.complete();
  }, s.prototype.asObservable = function() {
    var c = new Jr();
    return c.source = this, c;
  }, s.create = function(c, p) {
    return new pT(c, p);
  }, s;
}(Jr), pT = function(l) {
  jl(s, l);
  function s(c, p) {
    var y = l.call(this) || this;
    return y.destination = c, y.source = p, y;
  }
  return s.prototype.next = function(c) {
    var p, y;
    (y = (p = this.destination) === null || p === void 0 ? void 0 : p.next) === null || y === void 0 || y.call(p, c);
  }, s.prototype.error = function(c) {
    var p, y;
    (y = (p = this.destination) === null || p === void 0 ? void 0 : p.error) === null || y === void 0 || y.call(p, c);
  }, s.prototype.complete = function() {
    var c, p;
    (p = (c = this.destination) === null || c === void 0 ? void 0 : c.complete) === null || p === void 0 || p.call(c);
  }, s.prototype._subscribe = function(c) {
    var p, y;
    return (y = (p = this.source) === null || p === void 0 ? void 0 : p.subscribe(c)) !== null && y !== void 0 ? y : qT;
  }, s;
}(Ed), jO = function(l) {
  jl(s, l);
  function s(c) {
    var p = l.call(this) || this;
    return p._value = c, p;
  }
  return Object.defineProperty(s.prototype, "value", {
    get: function() {
      return this.getValue();
    },
    enumerable: !1,
    configurable: !0
  }), s.prototype._subscribe = function(c) {
    var p = l.prototype._subscribe.call(this, c);
    return !p.closed && c.next(this._value), p;
  }, s.prototype.getValue = function() {
    var c = this, p = c.hasError, y = c.thrownError, E = c._value;
    if (p)
      throw y;
    return this._throwIfClosed(), E;
  }, s.prototype.next = function(c) {
    l.prototype.next.call(this, this._value = c);
  }, s;
}(Ed), VO = {
  now: function() {
    return Date.now();
  },
  delegate: void 0
}, $O = function(l) {
  jl(s, l);
  function s(c, p) {
    return l.call(this) || this;
  }
  return s.prototype.schedule = function(c, p) {
    return this;
  }, s;
}(Pv), vT = {
  setInterval: function(l, s) {
    for (var c = [], p = 2; p < arguments.length; p++)
      c[p - 2] = arguments[p];
    return setInterval.apply(void 0, Av([l, s], Sd(c)));
  },
  clearInterval: function(l) {
    return clearInterval(l);
  },
  delegate: void 0
}, BO = function(l) {
  jl(s, l);
  function s(c, p) {
    var y = l.call(this, c, p) || this;
    return y.scheduler = c, y.work = p, y.pending = !1, y;
  }
  return s.prototype.schedule = function(c, p) {
    var y;
    if (p === void 0 && (p = 0), this.closed)
      return this;
    this.state = c;
    var E = this.id, m = this.scheduler;
    return E != null && (this.id = this.recycleAsyncId(m, E, p)), this.pending = !0, this.delay = p, this.id = (y = this.id) !== null && y !== void 0 ? y : this.requestAsyncId(m, this.id, p), this;
  }, s.prototype.requestAsyncId = function(c, p, y) {
    return y === void 0 && (y = 0), vT.setInterval(c.flush.bind(c, this), y);
  }, s.prototype.recycleAsyncId = function(c, p, y) {
    if (y === void 0 && (y = 0), y != null && this.delay === y && this.pending === !1)
      return p;
    p != null && vT.clearInterval(p);
  }, s.prototype.execute = function(c, p) {
    if (this.closed)
      return new Error("executing a cancelled action");
    this.pending = !1;
    var y = this._execute(c, p);
    if (y)
      return y;
    this.pending === !1 && this.id != null && (this.id = this.recycleAsyncId(this.scheduler, this.id, null));
  }, s.prototype._execute = function(c, p) {
    var y = !1, E;
    try {
      this.work(c);
    } catch (m) {
      y = !0, E = m || new Error("Scheduled action threw falsy error");
    }
    if (y)
      return this.unsubscribe(), E;
  }, s.prototype.unsubscribe = function() {
    if (!this.closed) {
      var c = this, p = c.id, y = c.scheduler, E = y.actions;
      this.work = this.state = this.scheduler = null, this.pending = !1, Hy(E, this), p != null && (this.id = this.recycleAsyncId(y, p, null)), this.delay = null, l.prototype.unsubscribe.call(this);
    }
  }, s;
}($O), hT = function() {
  function l(s, c) {
    c === void 0 && (c = l.now), this.schedulerActionCtor = s, this.now = c;
  }
  return l.prototype.schedule = function(s, c, p) {
    return c === void 0 && (c = 0), new this.schedulerActionCtor(this, s).schedule(p, c);
  }, l.now = VO.now, l;
}(), IO = function(l) {
  jl(s, l);
  function s(c, p) {
    p === void 0 && (p = hT.now);
    var y = l.call(this, c, p) || this;
    return y.actions = [], y._active = !1, y;
  }
  return s.prototype.flush = function(c) {
    var p = this.actions;
    if (this._active) {
      p.push(c);
      return;
    }
    var y;
    this._active = !0;
    do
      if (y = c.execute(c.state, c.delay))
        break;
    while (c = p.shift());
    if (this._active = !1, y) {
      for (; c = p.shift(); )
        c.unsubscribe();
      throw y;
    }
  }, s;
}(hT), YO = new IO(BO);
function WO(l) {
  return l && un(l.schedule);
}
function QO(l) {
  return l[l.length - 1];
}
function iE(l) {
  return WO(QO(l)) ? l.pop() : void 0;
}
var oE = function(l) {
  return l && typeof l.length == "number" && typeof l != "function";
};
function JT(l) {
  return un(l == null ? void 0 : l.then);
}
function eR(l) {
  return un(l[rE]);
}
function tR(l) {
  return Symbol.asyncIterator && un(l == null ? void 0 : l[Symbol.asyncIterator]);
}
function nR(l) {
  return new TypeError("You provided " + (l !== null && typeof l == "object" ? "an invalid object" : "'" + l + "'") + " where a stream was expected. You can provide an Observable, Promise, ReadableStream, Array, AsyncIterable, or Iterable.");
}
function GO() {
  return typeof Symbol != "function" || !Symbol.iterator ? "@@iterator" : Symbol.iterator;
}
var rR = GO();
function aR(l) {
  return un(l == null ? void 0 : l[rR]);
}
function iR(l) {
  return kO(this, arguments, function() {
    var s, c, p, y;
    return GT(this, function(E) {
      switch (E.label) {
        case 0:
          s = l.getReader(), E.label = 1;
        case 1:
          E.trys.push([1, , 9, 10]), E.label = 2;
        case 2:
          return [4, md(s.read())];
        case 3:
          return c = E.sent(), p = c.value, y = c.done, y ? [4, md(void 0)] : [3, 5];
        case 4:
          return [2, E.sent()];
        case 5:
          return [4, md(p)];
        case 6:
          return [4, E.sent()];
        case 7:
          return E.sent(), [3, 2];
        case 8:
          return [3, 10];
        case 9:
          return s.releaseLock(), [7];
        case 10:
          return [2];
      }
    });
  });
}
function oR(l) {
  return un(l == null ? void 0 : l.getReader);
}
function as(l) {
  if (l instanceof Jr)
    return l;
  if (l != null) {
    if (eR(l))
      return qO(l);
    if (oE(l))
      return XO(l);
    if (JT(l))
      return KO(l);
    if (tR(l))
      return lR(l);
    if (aR(l))
      return ZO(l);
    if (oR(l))
      return JO(l);
  }
  throw nR(l);
}
function qO(l) {
  return new Jr(function(s) {
    var c = l[rE]();
    if (un(c.subscribe))
      return c.subscribe(s);
    throw new TypeError("Provided object does not correctly implement Symbol.observable");
  });
}
function XO(l) {
  return new Jr(function(s) {
    for (var c = 0; c < l.length && !s.closed; c++)
      s.next(l[c]);
    s.complete();
  });
}
function KO(l) {
  return new Jr(function(s) {
    l.then(function(c) {
      s.closed || (s.next(c), s.complete());
    }, function(c) {
      return s.error(c);
    }).then(null, ZT);
  });
}
function ZO(l) {
  return new Jr(function(s) {
    var c, p;
    try {
      for (var y = gd(l), E = y.next(); !E.done; E = y.next()) {
        var m = E.value;
        if (s.next(m), s.closed)
          return;
      }
    } catch (x) {
      c = { error: x };
    } finally {
      try {
        E && !E.done && (p = y.return) && p.call(y);
      } finally {
        if (c) throw c.error;
      }
    }
    s.complete();
  });
}
function lR(l) {
  return new Jr(function(s) {
    eM(l, s).catch(function(c) {
      return s.error(c);
    });
  });
}
function JO(l) {
  return lR(iR(l));
}
function eM(l, s) {
  var c, p, y, E;
  return xO(this, void 0, void 0, function() {
    var m, x;
    return GT(this, function(T) {
      switch (T.label) {
        case 0:
          T.trys.push([0, 5, 6, 11]), c = _O(l), T.label = 1;
        case 1:
          return [4, c.next()];
        case 2:
          if (p = T.sent(), !!p.done) return [3, 4];
          if (m = p.value, s.next(m), s.closed)
            return [2];
          T.label = 3;
        case 3:
          return [3, 1];
        case 4:
          return [3, 11];
        case 5:
          return x = T.sent(), y = { error: x }, [3, 11];
        case 6:
          return T.trys.push([6, , 9, 10]), p && !p.done && (E = c.return) ? [4, E.call(c)] : [3, 8];
        case 7:
          T.sent(), T.label = 8;
        case 8:
          return [3, 10];
        case 9:
          if (y) throw y.error;
          return [7];
        case 10:
          return [7];
        case 11:
          return s.complete(), [2];
      }
    });
  });
}
function ts(l, s, c, p, y) {
  p === void 0 && (p = 0), y === void 0 && (y = !1);
  var E = s.schedule(function() {
    c(), y ? l.add(this.schedule(null, p)) : this.unsubscribe();
  }, p);
  if (l.add(E), !y)
    return E;
}
function uR(l, s) {
  return s === void 0 && (s = 0), qo(function(c, p) {
    c.subscribe(Fl(p, function(y) {
      return ts(p, l, function() {
        return p.next(y);
      }, s);
    }, function() {
      return ts(p, l, function() {
        return p.complete();
      }, s);
    }, function(y) {
      return ts(p, l, function() {
        return p.error(y);
      }, s);
    }));
  });
}
function sR(l, s) {
  return s === void 0 && (s = 0), qo(function(c, p) {
    p.add(l.schedule(function() {
      return c.subscribe(p);
    }, s));
  });
}
function tM(l, s) {
  return as(l).pipe(sR(s), uR(s));
}
function nM(l, s) {
  return as(l).pipe(sR(s), uR(s));
}
function rM(l, s) {
  return new Jr(function(c) {
    var p = 0;
    return s.schedule(function() {
      p === l.length ? c.complete() : (c.next(l[p++]), c.closed || this.schedule());
    });
  });
}
function aM(l, s) {
  return new Jr(function(c) {
    var p;
    return ts(c, s, function() {
      p = l[rR](), ts(c, s, function() {
        var y, E, m;
        try {
          y = p.next(), E = y.value, m = y.done;
        } catch (x) {
          c.error(x);
          return;
        }
        m ? c.complete() : c.next(E);
      }, 0, !0);
    }), function() {
      return un(p == null ? void 0 : p.return) && p.return();
    };
  });
}
function cR(l, s) {
  if (!l)
    throw new Error("Iterable cannot be null");
  return new Jr(function(c) {
    ts(c, s, function() {
      var p = l[Symbol.asyncIterator]();
      ts(c, s, function() {
        p.next().then(function(y) {
          y.done ? c.complete() : c.next(y.value);
        });
      }, 0, !0);
    });
  });
}
function iM(l, s) {
  return cR(iR(l), s);
}
function oM(l, s) {
  if (l != null) {
    if (eR(l))
      return tM(l, s);
    if (oE(l))
      return rM(l, s);
    if (JT(l))
      return nM(l, s);
    if (tR(l))
      return cR(l, s);
    if (aR(l))
      return aM(l, s);
    if (oR(l))
      return iM(l, s);
  }
  throw nR(l);
}
function fR(l, s) {
  return s ? oM(l, s) : as(l);
}
function mT() {
  for (var l = [], s = 0; s < arguments.length; s++)
    l[s] = arguments[s];
  var c = iE(l);
  return fR(l, c);
}
function lM(l) {
  return l instanceof Date && !isNaN(l);
}
var uM = tE(function(l) {
  return function(s) {
    s === void 0 && (s = null), l(this), this.message = "Timeout has occurred", this.name = "TimeoutError", this.info = s;
  };
});
function sM(l, s) {
  var c = lM(l) ? { first: l } : typeof l == "number" ? { each: l } : l, p = c.first, y = c.each, E = c.with, m = E === void 0 ? cM : E, x = c.scheduler, T = x === void 0 ? YO : x, D = c.meta, A = D === void 0 ? null : D;
  if (p == null && y == null)
    throw new TypeError("No timeout provided.");
  return qo(function(L, j) {
    var F, $, H = null, G = 0, W = function(ve) {
      $ = ts(j, T, function() {
        try {
          F.unsubscribe(), as(m({
            meta: A,
            lastValue: H,
            seen: G
          })).subscribe(j);
        } catch (fe) {
          j.error(fe);
        }
      }, ve);
    };
    F = L.subscribe(Fl(j, function(ve) {
      $ == null || $.unsubscribe(), G++, j.next(H = ve), y > 0 && W(y);
    }, void 0, void 0, function() {
      $ != null && $.closed || $ == null || $.unsubscribe(), H = null;
    })), !G && W(p != null ? typeof p == "number" ? p : +p - T.now() : y);
  });
}
function cM(l) {
  throw new uM(l);
}
function Rr(l, s) {
  return qo(function(c, p) {
    var y = 0;
    c.subscribe(Fl(p, function(E) {
      p.next(l.call(s, E, y++));
    }));
  });
}
var fM = Array.isArray;
function dM(l, s) {
  return fM(s) ? l.apply(void 0, Av([], Sd(s))) : l(s);
}
function pM(l) {
  return Rr(function(s) {
    return dM(l, s);
  });
}
function vM(l, s, c, p, y, E, m, x) {
  var T = [], D = 0, A = 0, L = !1, j = function() {
    L && !T.length && !D && s.complete();
  }, F = function(H) {
    return D < p ? $(H) : T.push(H);
  }, $ = function(H) {
    D++;
    var G = !1;
    as(c(H, A++)).subscribe(Fl(s, function(W) {
      s.next(W);
    }, function() {
      G = !0;
    }, void 0, function() {
      if (G)
        try {
          D--;
          for (var W = function() {
            var ve = T.shift();
            m || $(ve);
          }; T.length && D < p; )
            W();
          j();
        } catch (ve) {
          s.error(ve);
        }
    }));
  };
  return l.subscribe(Fl(s, F, function() {
    L = !0, j();
  })), function() {
  };
}
function lE(l, s, c) {
  return c === void 0 && (c = 1 / 0), un(s) ? lE(function(p, y) {
    return Rr(function(E, m) {
      return s(p, E, y, m);
    })(as(l(p, y)));
  }, c) : (typeof s == "number" && (c = s), qo(function(p, y) {
    return vM(p, y, l, c);
  }));
}
function hM(l) {
  return lE(aE, l);
}
function mM() {
  return hM(1);
}
function jy() {
  for (var l = [], s = 0; s < arguments.length; s++)
    l[s] = arguments[s];
  return mM()(fR(l, iE(l)));
}
var yM = ["addListener", "removeListener"], gM = ["addEventListener", "removeEventListener"], SM = ["on", "off"];
function Y1(l, s, c, p) {
  if (un(c) && (p = c, c = void 0), p)
    return Y1(l, s, c).pipe(pM(p));
  var y = Sd(CM(l) ? gM.map(function(x) {
    return function(T) {
      return l[x](s, T, c);
    };
  }) : EM(l) ? yM.map(yT(l, s)) : bM(l) ? SM.map(yT(l, s)) : [], 2), E = y[0], m = y[1];
  if (!E && oE(l))
    return lE(function(x) {
      return Y1(x, s, c);
    })(as(l));
  if (!E)
    throw new TypeError("Invalid event target");
  return new Jr(function(x) {
    var T = function() {
      for (var D = [], A = 0; A < arguments.length; A++)
        D[A] = arguments[A];
      return x.next(1 < D.length ? D : D[0]);
    };
    return E(T), function() {
      return m(T);
    };
  });
}
function yT(l, s) {
  return function(c) {
    return function(p) {
      return l[c](s, p);
    };
  };
}
function EM(l) {
  return un(l.addListener) && un(l.removeListener);
}
function bM(l) {
  return un(l.on) && un(l.off);
}
function CM(l) {
  return un(l.addEventListener) && un(l.removeEventListener);
}
function dR(l, s) {
  return qo(function(c, p) {
    var y = 0;
    c.subscribe(Fl(p, function(E) {
      return l.call(s, E, y++) && p.next(E);
    }));
  });
}
function wM(l, s) {
  return s === void 0 && (s = aE), l = l ?? TM, qo(function(c, p) {
    var y, E = !0;
    c.subscribe(Fl(p, function(m) {
      var x = s(m);
      (E || !l(y, x)) && (E = !1, y = x, p.next(m));
    }));
  });
}
function TM(l, s) {
  return l === s;
}
function RM(l) {
  return qo(function(s, c) {
    try {
      s.subscribe(c);
    } finally {
      c.add(l);
    }
  });
}
function xM(l) {
  return qo(function(s, c) {
    var p = !1, y = Fl(c, function() {
      y == null || y.unsubscribe(), p = !0;
    }, B1);
    as(l).subscribe(y), s.subscribe(Fl(c, function(E) {
      return p && c.next(E);
    }));
  });
}
function Ri() {
  for (var l = [], s = 0; s < arguments.length; s++)
    l[s] = arguments[s];
  var c = iE(l);
  return qo(function(p, y) {
    (c ? jy(l, p, c) : jy(l, p)).subscribe(y);
  });
}
var kM = Object.defineProperty, gT = Object.getOwnPropertySymbols, _M = Object.prototype.hasOwnProperty, DM = Object.prototype.propertyIsEnumerable, ST = (l, s, c) => s in l ? kM(l, s, { enumerable: !0, configurable: !0, writable: !0, value: c }) : l[s] = c, ET = (l, s) => {
  for (var c in s || (s = {}))
    _M.call(s, c) && ST(l, c, s[c]);
  if (gT)
    for (var c of gT(s))
      DM.call(s, c) && ST(l, c, s[c]);
  return l;
}, mc = (l, s, c) => new Promise((p, y) => {
  var E = (T) => {
    try {
      x(c.next(T));
    } catch (D) {
      y(D);
    }
  }, m = (T) => {
    try {
      x(c.throw(T));
    } catch (D) {
      y(D);
    }
  }, x = (T) => T.done ? p(T.value) : Promise.resolve(T.value).then(E, m);
  x((c = c.apply(l, s)).next());
}), pR = "lk";
function Ci(l) {
  return typeof l > "u" ? !1 : OM(l) || MM(l);
}
function OM(l) {
  var s;
  return l ? l.hasOwnProperty("participant") && l.hasOwnProperty("source") && l.hasOwnProperty("track") && typeof ((s = l.publication) == null ? void 0 : s.track) < "u" : !1;
}
function MM(l) {
  return l ? l.hasOwnProperty("participant") && l.hasOwnProperty("source") && l.hasOwnProperty("publication") && typeof l.publication < "u" : !1;
}
function zv(l) {
  return l ? l.hasOwnProperty("participant") && l.hasOwnProperty("source") && typeof l.publication > "u" : !1;
}
function Zr(l) {
  if (typeof l == "string" || typeof l == "number")
    return `${l}`;
  if (zv(l))
    return `${l.participant.identity}_${l.source}_placeholder`;
  if (Ci(l))
    return `${l.participant.identity}_${l.publication.source}_${l.publication.trackSid}`;
  throw new Error(`Can't generate a id for the given track reference: ${l}`);
}
function LM(l, s) {
  return typeof s > "u" ? !1 : Ci(l) ? s.some(
    (c) => c.participant.identity === l.participant.identity && Ci(c) && c.publication.trackSid === l.publication.trackSid
  ) : zv(l) ? s.some(
    (c) => c.participant.identity === l.participant.identity && zv(c) && c.source === l.source
  ) : !1;
}
function NM(l, s) {
  return zv(l) && Ci(s) && s.participant.identity === l.participant.identity && s.source === l.source;
}
function vR(l) {
  return l instanceof Z1;
}
function AM() {
  const l = document.createElement("p");
  l.style.width = "100%", l.style.height = "200px";
  const s = document.createElement("div");
  s.style.position = "absolute", s.style.top = "0px", s.style.left = "0px", s.style.visibility = "hidden", s.style.width = "200px", s.style.height = "150px", s.style.overflow = "hidden", s.appendChild(l), document.body.appendChild(s);
  const c = l.offsetWidth;
  s.style.overflow = "scroll";
  let p = l.offsetWidth;
  return c === p && (p = s.clientWidth), document.body.removeChild(s), c - p;
}
function zM(l, s) {
  return mc(this, null, function* () {
    const { x: c, y: p } = yield wO(l, s, {
      placement: "top",
      middleware: [EO(6), CO(), bO({ padding: 5 })]
    });
    return { x: c, y: p };
  });
}
function UM(l, s) {
  return !l.contains(s.target);
}
var PM = [
  Pe.ConnectionStateChanged,
  Pe.RoomMetadataChanged,
  Pe.ActiveSpeakersChanged,
  Pe.ConnectionQualityChanged,
  Pe.ParticipantConnected,
  Pe.ParticipantDisconnected,
  Pe.ParticipantPermissionsChanged,
  Pe.ParticipantMetadataChanged,
  Pe.ParticipantNameChanged,
  Pe.ParticipantAttributesChanged,
  Pe.TrackMuted,
  Pe.TrackUnmuted,
  Pe.TrackPublished,
  Pe.TrackUnpublished,
  Pe.TrackStreamStateChanged,
  Pe.TrackSubscriptionFailed,
  Pe.TrackSubscriptionPermissionChanged,
  Pe.TrackSubscriptionStatusChanged
], FM = [
  ...PM,
  Pe.LocalTrackPublished,
  Pe.LocalTrackUnpublished
];
De.TrackPublished, De.TrackUnpublished, De.TrackMuted, De.TrackUnmuted, De.TrackStreamStateChanged, De.TrackSubscribed, De.TrackUnsubscribed, De.TrackSubscriptionPermissionChanged, De.TrackSubscriptionFailed, De.LocalTrackPublished, De.LocalTrackUnpublished;
var HM = [
  De.ConnectionQualityChanged,
  De.IsSpeakingChanged,
  De.ParticipantMetadataChanged,
  De.ParticipantPermissionsChanged,
  De.TrackMuted,
  De.TrackUnmuted,
  De.TrackPublished,
  De.TrackUnpublished,
  De.TrackStreamStateChanged,
  De.TrackSubscriptionFailed,
  De.TrackSubscriptionPermissionChanged,
  De.TrackSubscriptionStatusChanged
];
[
  ...HM,
  De.LocalTrackPublished,
  De.LocalTrackUnpublished
];
var Mt = RO.getLogger("lk-components-js");
Mt.setDefaultLevel("WARN");
var jM = [
  {
    columns: 1,
    rows: 1
  },
  {
    columns: 1,
    rows: 2,
    orientation: "portrait"
  },
  {
    columns: 2,
    rows: 1,
    orientation: "landscape"
  },
  {
    columns: 2,
    rows: 2,
    minWidth: 560
  },
  {
    columns: 3,
    rows: 3,
    minWidth: 700
  },
  {
    columns: 4,
    rows: 4,
    minWidth: 960
  },
  {
    columns: 5,
    rows: 5,
    minWidth: 1100
  }
];
function hR(l, s, c, p) {
  if (l.length < 1)
    throw new Error("At least one grid layout definition must be provided.");
  const y = VM(l);
  if (c <= 0 || p <= 0)
    return y[0];
  let E = 0;
  const m = c / p > 1 ? "landscape" : "portrait";
  let x = y.find((T, D, A) => {
    E = D;
    const L = A.findIndex((j, F) => {
      const $ = !j.orientation || j.orientation === m, H = F > D, G = j.maxTiles === T.maxTiles;
      return H && G && $;
    }) !== -1;
    return T.maxTiles >= s && !L;
  });
  if (x === void 0)
    if (x = y[y.length - 1], x)
      Mt.warn(
        `No layout found for: participantCount: ${s}, width/height: ${c}/${p} fallback to biggest available layout (${x}).`
      );
    else
      throw new Error("No layout or fallback layout found.");
  if ((c < x.minWidth || p < x.minHeight) && E > 0) {
    const T = y[E - 1];
    x = hR(
      y.slice(0, E),
      T.maxTiles,
      c,
      p
    );
  }
  return x;
}
function VM(l) {
  return [...l].map((s) => {
    var c, p;
    return {
      name: `${s.columns}x${s.rows}`,
      columns: s.columns,
      rows: s.rows,
      maxTiles: s.columns * s.rows,
      minWidth: (c = s.minWidth) != null ? c : 0,
      minHeight: (p = s.minHeight) != null ? p : 0,
      orientation: s.orientation
    };
  }).sort((s, c) => s.maxTiles !== c.maxTiles ? s.maxTiles - c.maxTiles : s.minWidth !== 0 || c.minWidth !== 0 ? s.minWidth - c.minWidth : s.minHeight !== 0 || c.minHeight !== 0 ? s.minHeight - c.minHeight : 0);
}
var mR = [], yR = {
  showChat: !1,
  unreadMessages: 0,
  showSettings: !1
};
function gR(l) {
  return typeof l == "object";
}
function SR(l) {
  return Array.isArray(l) && l.filter(gR).length > 0;
}
function $M(l, s) {
  return s.audioLevel - l.audioLevel;
}
function BM(l, s) {
  return l.isSpeaking === s.isSpeaking ? 0 : l.isSpeaking ? -1 : 1;
}
function IM(l, s) {
  var c, p, y, E;
  return l.lastSpokeAt !== void 0 || s.lastSpokeAt !== void 0 ? ((p = (c = s.lastSpokeAt) == null ? void 0 : c.getTime()) != null ? p : 0) - ((E = (y = l.lastSpokeAt) == null ? void 0 : y.getTime()) != null ? E : 0) : 0;
}
function W1(l, s) {
  var c, p, y, E;
  return ((p = (c = l.joinedAt) == null ? void 0 : c.getTime()) != null ? p : 0) - ((E = (y = s.joinedAt) == null ? void 0 : y.getTime()) != null ? E : 0);
}
function YM(l, s) {
  return Ci(l) ? Ci(s) ? 0 : -1 : Ci(s) ? 1 : 0;
}
function WM(l, s) {
  const c = l.participant.isCameraEnabled, p = s.participant.isCameraEnabled;
  return c !== p ? c ? -1 : 1 : 0;
}
function QM(l) {
  const s = [], c = [], p = [], y = [];
  l.forEach((x) => {
    x.participant.isLocal && x.source === ht.Source.Camera ? s.push(x) : x.source === ht.Source.ScreenShare ? c.push(x) : x.source === ht.Source.Camera ? p.push(x) : y.push(x);
  });
  const E = GM(c), m = qM(p);
  return [...s, ...E, ...m, ...y];
}
function GM(l) {
  const s = [], c = [];
  return l.forEach((p) => {
    p.participant.isLocal ? s.push(p) : c.push(p);
  }), s.sort((p, y) => W1(p.participant, y.participant)), c.sort((p, y) => W1(p.participant, y.participant)), [...c, ...s];
}
function qM(l) {
  const s = [], c = [];
  return l.forEach((p) => {
    p.participant.isLocal ? s.push(p) : c.push(p);
  }), c.sort((p, y) => p.participant.isSpeaking && y.participant.isSpeaking ? $M(p.participant, y.participant) : p.participant.isSpeaking !== y.participant.isSpeaking ? BM(p.participant, y.participant) : p.participant.lastSpokeAt !== y.participant.lastSpokeAt ? IM(p.participant, y.participant) : Ci(p) !== Ci(y) ? YM(p, y) : p.participant.isCameraEnabled !== y.participant.isCameraEnabled ? WM(p, y) : W1(p.participant, y.participant)), [...s, ...c];
}
function XM(l, s) {
  return l.reduce(
    (c, p, y) => y % s === 0 ? [...c, [p]] : [...c.slice(0, -1), [...c.slice(-1)[0], p]],
    []
  );
}
function bT(l, s) {
  const c = Math.max(l.length, s.length);
  return new Array(c).fill([]).map((p, y) => [l[y], s[y]]);
}
function Vy(l, s, c) {
  return l.filter((p) => !s.map((y) => c(y)).includes(c(p)));
}
function Q1(l) {
  return l.map((s) => typeof s == "string" || typeof s == "number" ? `${s}` : Zr(s));
}
function KM(l, s) {
  return {
    dropped: Vy(l, s, Zr),
    added: Vy(s, l, Zr)
  };
}
function ZM(l) {
  return l.added.length !== 0 || l.dropped.length !== 0;
}
function G1(l, s) {
  const c = s.findIndex(
    (p) => Zr(p) === Zr(l)
  );
  if (c === -1)
    throw new Error(
      `Element not part of the array: ${Zr(
        l
      )} not in ${Q1(s)}`
    );
  return c;
}
function JM(l, s, c) {
  const p = G1(l, c), y = G1(s, c);
  return c.splice(p, 1, s), c.splice(y, 1, l), c;
}
function eL(l, s) {
  const c = G1(l, s);
  return s.splice(c, 1), s;
}
function tL(l, s) {
  return [...s, l];
}
function R1(l, s) {
  return XM(l, s);
}
function nL(l, s, c) {
  let p = rL(l, s);
  if (p.length < s.length) {
    const m = Vy(s, p, Zr);
    p = [...p, ...m];
  }
  const y = R1(p, c), E = R1(s, c);
  if (bT(y, E).forEach(([m, x], T) => {
    if (m && x) {
      const D = R1(p, c)[T], A = KM(D, x);
      ZM(A) && (Mt.debug(
        `Detected visual changes on page: ${T}, current: ${Q1(
          m
        )}, next: ${Q1(x)}`,
        { changes: A }
      ), A.added.length === A.dropped.length && bT(A.added, A.dropped).forEach(([L, j]) => {
        if (L && j)
          p = JM(L, j, p);
        else
          throw new Error(
            `For a swap action we need a addition and a removal one is missing: ${L}, ${j}`
          );
      }), A.added.length === 0 && A.dropped.length > 0 && A.dropped.forEach((L) => {
        p = eL(L, p);
      }), A.added.length > 0 && A.dropped.length === 0 && A.added.forEach((L) => {
        p = tL(L, p);
      }));
    }
  }), p.length > s.length) {
    const m = Vy(p, s, Zr);
    p = p.filter(
      (x) => !m.map(Zr).includes(Zr(x))
    );
  }
  return p;
}
function rL(l, s) {
  return l.map((c) => s.find(
    (y) => (
      // If the IDs match or ..
      Zr(c) === Zr(y) || // ... if the current item is a placeholder and the new item is the track reference can replace it.
      typeof c != "number" && zv(c) && Ci(y) && NM(c, y)
    )
  ) ?? c);
}
function wi(l) {
  return `${pR}-${l}`;
}
function aL(l) {
  const s = q1(l), c = ER(l.participant).pipe(
    Rr(() => q1(l)),
    Ri(s)
  );
  return { className: wi(
    l.source === ht.Source.Camera || l.source === ht.Source.ScreenShare ? "participant-media-video" : "participant-media-audio"
  ), trackObserver: c };
}
function q1(l) {
  if (Ci(l))
    return l.publication;
  {
    const { source: s, name: c, participant: p } = l;
    if (s && c)
      return p.getTrackPublications().find((y) => y.source === s && y.trackName === c);
    if (c)
      return p.getTrackPublicationByName(c);
    if (s)
      return p.getTrackPublication(s);
    throw new Error("At least one of source and name needs to be defined");
  }
}
function uE(l, ...s) {
  return new Jr((c) => {
    const p = () => {
      c.next(l);
    };
    return s.forEach((y) => {
      l.on(y, p);
    }), () => {
      s.forEach((y) => {
        l.off(y, p);
      });
    };
  }).pipe(Ri(l));
}
function sE(l, s) {
  return new Jr((c) => {
    const p = (...y) => {
      c.next(y);
    };
    return l.on(s, p), () => {
      l.off(s, p);
    };
  });
}
function iL(l) {
  return sE(l, Pe.ConnectionStateChanged).pipe(
    Rr(([s]) => s),
    Ri(l.state)
  );
}
function oL(l, s, c = !0) {
  var p;
  const y = () => mc(this, null, function* () {
    try {
      const x = yield M1.getLocalDevices(l, c);
      E.next(x);
    } catch (x) {
      s == null || s(x);
    }
  }), E = new Ed(), m = E.pipe(
    RM(() => {
      var x;
      (x = navigator == null ? void 0 : navigator.mediaDevices) == null || x.removeEventListener("devicechange", y);
    })
  );
  if (typeof window < "u") {
    if (!window.isSecureContext)
      throw new Error(
        "Accessing media devices is available only in secure contexts (HTTPS and localhost), in some or all supporting browsers. See: https://developer.mozilla.org/en-US/docs/Web/API/Navigator/mediaDevices"
      );
    (p = navigator == null ? void 0 : navigator.mediaDevices) == null || p.addEventListener("devicechange", y);
  }
  return jy(
    M1.getLocalDevices(l, c).catch((x) => (s == null || s(x), [])),
    m
  );
}
function lL(l) {
  return uE(l, Pe.AudioPlaybackStatusChanged).pipe(
    Rr((s) => ({ canPlayAudio: s.canPlaybackAudio }))
  );
}
function uL(l) {
  return uE(l, Pe.VideoPlaybackStatusChanged).pipe(
    Rr((s) => ({ canPlayVideo: s.canPlaybackVideo }))
  );
}
function sL(l, s) {
  return sE(l, Pe.ActiveDeviceChanged).pipe(
    dR(([c]) => c === s),
    Rr(([c, p]) => (Mt.debug("activeDeviceObservable | RoomEvent.ActiveDeviceChanged", { kind: c, deviceId: p }), p)),
    Ri(l.getActiveDevice(s))
  );
}
function cL(l, s) {
  return sE(l, Pe.ParticipantEncryptionStatusChanged).pipe(
    dR(
      ([, c]) => (s == null ? void 0 : s.identity) === (c == null ? void 0 : c.identity) || !c && (s == null ? void 0 : s.identity) === l.localParticipant.identity
    ),
    Rr(([c]) => c),
    Ri(
      s instanceof Z1 ? s.isE2EEEnabled : !!(s != null && s.isEncrypted)
    )
  );
}
function cE(l, ...s) {
  return new Jr((c) => {
    const p = () => {
      c.next(l);
    };
    return s.forEach((y) => {
      l.on(y, p);
    }), () => {
      s.forEach((y) => {
        l.off(y, p);
      });
    };
  }).pipe(Ri(l));
}
function ER(l) {
  return cE(
    l,
    De.TrackMuted,
    De.TrackUnmuted,
    De.ParticipantPermissionsChanged,
    // ParticipantEvent.IsSpeakingChanged,
    De.TrackPublished,
    De.TrackUnpublished,
    De.LocalTrackPublished,
    De.LocalTrackUnpublished,
    De.MediaDevicesError,
    De.TrackSubscriptionStatusChanged
    // ParticipantEvent.ConnectionQualityChanged,
  ).pipe(
    Rr((s) => {
      const { isMicrophoneEnabled: c, isCameraEnabled: p, isScreenShareEnabled: y } = s, E = s.getTrackPublication(ht.Source.Microphone), m = s.getTrackPublication(ht.Source.Camera);
      return {
        isCameraEnabled: p,
        isMicrophoneEnabled: c,
        isScreenShareEnabled: y,
        cameraTrack: m,
        microphoneTrack: E,
        participant: s
      };
    })
  );
}
function fL(l) {
  return l ? cE(
    l,
    De.ParticipantMetadataChanged,
    De.ParticipantNameChanged
  ).pipe(
    Rr(({ name: s, identity: c, metadata: p }) => ({
      name: s,
      identity: c,
      metadata: p
    })),
    Ri({
      name: l.name,
      identity: l.identity,
      metadata: l.metadata
    })
  ) : void 0;
}
function dL(l) {
  return fE(
    l,
    De.ConnectionQualityChanged
  ).pipe(
    Rr(([s]) => s),
    Ri(l.connectionQuality)
  );
}
function fE(l, s) {
  return new Jr((c) => {
    const p = (...y) => {
      c.next(y);
    };
    return l.on(s, p), () => {
      l.off(s, p);
    };
  });
}
function bR(l) {
  var s, c, p, y;
  return cE(
    l.participant,
    De.TrackMuted,
    De.TrackUnmuted,
    De.TrackSubscribed,
    De.TrackUnsubscribed,
    De.LocalTrackPublished,
    De.LocalTrackUnpublished
  ).pipe(
    Rr((E) => {
      var m, x;
      const T = (m = l.publication) != null ? m : E.getTrackPublication(l.source);
      return (x = T == null ? void 0 : T.isMuted) != null ? x : !0;
    }),
    Ri(
      (y = (p = (s = l.publication) == null ? void 0 : s.isMuted) != null ? p : (c = l.participant.getTrackPublication(l.source)) == null ? void 0 : c.isMuted) != null ? y : !0
    )
  );
}
function pL(l) {
  return fE(l, De.IsSpeakingChanged).pipe(
    Rr(([s]) => s)
  );
}
function vL(l) {
  return fE(
    l,
    De.ParticipantPermissionsChanged
  ).pipe(
    Rr(() => l.permissions),
    Ri(l.permissions)
  );
}
function hL(l, s, c, p, y) {
  const { localParticipant: E } = s, m = (A, L) => {
    let j = !1;
    switch (A) {
      case ht.Source.Camera:
        j = L.isCameraEnabled;
        break;
      case ht.Source.Microphone:
        j = L.isMicrophoneEnabled;
        break;
      case ht.Source.ScreenShare:
        j = L.isScreenShareEnabled;
        break;
    }
    return j;
  }, x = ER(E).pipe(
    Rr((A) => m(l, A.participant)),
    Ri(m(l, E))
  ), T = new Ed(), D = (A, L) => mc(this, null, function* () {
    try {
      switch (L ?? (L = c), T.next(!0), l) {
        case ht.Source.Camera:
          return yield E.setCameraEnabled(
            A ?? !E.isCameraEnabled,
            L,
            p
          ), E.isCameraEnabled;
        case ht.Source.Microphone:
          return yield E.setMicrophoneEnabled(
            A ?? !E.isMicrophoneEnabled,
            L,
            p
          ), E.isMicrophoneEnabled;
        case ht.Source.ScreenShare:
          return yield E.setScreenShareEnabled(
            A ?? !E.isScreenShareEnabled,
            L,
            p
          ), E.isScreenShareEnabled;
        default:
          throw new TypeError("Tried to toggle unsupported source");
      }
    } catch (j) {
      if (y && j instanceof Error) {
        y == null || y(j);
        return;
      } else
        throw j;
    } finally {
      T.next(!1);
    }
  });
  return {
    className: wi("button"),
    toggle: D,
    enabledObserver: x,
    pendingObserver: T.asObservable()
  };
}
function mL() {
  let l = !1;
  const s = new Ed(), c = new Ed(), p = (y) => mc(this, null, function* () {
    c.next(!0), l = y ?? !l, s.next(l), c.next(!1);
  });
  return {
    className: wi("button"),
    toggle: p,
    enabledObserver: s.asObservable(),
    pendingObserver: c.asObservable()
  };
}
function yL(l, s, c) {
  const p = new jO(void 0), y = s ? sL(s, l) : p.asObservable(), E = (m, ...x) => mc(this, [m, ...x], function* (T, D = {}) {
    var A, L, j;
    if (s) {
      Mt.debug(`Switching active device of kind "${l}" with id ${T}.`), yield s.switchActiveDevice(l, T, D.exact);
      const F = (A = s.getActiveDevice(l)) != null ? A : T;
      F !== T && T !== "default" && Mt.info(
        `We tried to select the device with id (${T}), but the browser decided to select the device with id (${F}) instead.`
      );
      let $;
      l === "audioinput" ? $ = (L = s.localParticipant.getTrackPublication(ht.Source.Microphone)) == null ? void 0 : L.track : l === "videoinput" && ($ = (j = s.localParticipant.getTrackPublication(ht.Source.Camera)) == null ? void 0 : j.track);
      const H = T === "default" && !$ || T === "default" && ($ == null ? void 0 : $.mediaStreamTrack.label.startsWith("Default"));
      p.next(H ? T : F);
    } else if (c) {
      yield c.setDeviceId(D.exact ? { exact: T } : T);
      const F = yield c.getDeviceId();
      p.next(
        T === "default" && c.mediaStreamTrack.label.startsWith("Default") ? T : F
      );
    } else p.value !== T && (Mt.warn(
      "device switch skipped, please provide either a room or a local track to switch on. "
    ), p.next(T));
  });
  return {
    className: wi("media-device-select"),
    activeDeviceObservable: y,
    setActiveMediaDevice: E
  };
}
function gL(l) {
  const s = (c) => {
    l.disconnect(c);
  };
  return { className: wi("disconnect-button"), disconnect: s };
}
function SL(l) {
  const s = wi("connection-quality"), c = dL(l);
  return { className: s, connectionQualityObserver: c };
}
function EL(l) {
  let s = "track-muted-indicator-camera";
  switch (l.source) {
    case ht.Source.Camera:
      s = "track-muted-indicator-camera";
      break;
    case ht.Source.Microphone:
      s = "track-muted-indicator-microphone";
      break;
  }
  const c = wi(s), p = bR(l);
  return { className: c, mediaMutedObserver: p };
}
function bL(l) {
  return { className: "lk-participant-name", infoObserver: fL(l) };
}
function CL() {
  return {
    className: wi("participant-tile")
  };
}
new TextEncoder();
new TextDecoder();
function wL() {
  const l = (s) => mc(this, null, function* () {
    Mt.info("Start Audio for room: ", s), yield s.startAudio();
  });
  return { className: wi("start-audio-button"), roomAudioPlaybackAllowedObservable: lL, handleStartAudioPlayback: l };
}
function TL() {
  const l = (s) => mc(this, null, function* () {
    Mt.info("Start Video for room: ", s), yield s.startVideo();
  });
  return { className: wi("start-audio-button"), roomVideoPlaybackAllowedObservable: uL, handleStartVideoPlayback: l };
}
function RL() {
  return { className: [wi("button"), wi("focus-toggle-button")].join(" ") };
}
function xL() {
  return { className: "lk-room-container" };
}
function CT(l, s, c = !0) {
  const p = [l.localParticipant, ...Array.from(l.remoteParticipants.values())], y = [];
  return p.forEach((E) => {
    s.forEach((m) => {
      const x = Array.from(
        E.trackPublications.values()
      ).filter(
        (T) => T.source === m && // either return all or only the ones that are subscribed
        (!c || T.track)
      ).map((T) => ({
        participant: E,
        publication: T,
        source: T.source
      }));
      y.push(...x);
    });
  }), { trackReferences: y, participants: p };
}
function kL(l, s, c) {
  var p, y;
  const E = (p = c.additionalRoomEvents) != null ? p : FM, m = (y = c.onlySubscribed) != null ? y : !0, x = Array.from(
    (/* @__PURE__ */ new Set([
      Pe.ParticipantConnected,
      Pe.ParticipantDisconnected,
      Pe.ConnectionStateChanged,
      Pe.LocalTrackPublished,
      Pe.LocalTrackUnpublished,
      Pe.TrackPublished,
      Pe.TrackUnpublished,
      Pe.TrackSubscriptionStatusChanged,
      ...E
    ])).values()
  );
  return uE(l, ...x).pipe(
    Rr((T) => {
      const D = CT(T, s, m);
      return Mt.debug(`TrackReference[] was updated. (length ${D.trackReferences.length})`, D), D;
    }),
    Ri(CT(l, s, m))
  );
}
function _L(l, s = 1e3) {
  if (l === null) return mT(!1);
  const c = Y1(l, "mousemove", { passive: !0 }).pipe(Rr(() => !0)), p = c.pipe(
    sM({
      each: s,
      with: () => jy(mT(!1), p.pipe(xM(c)))
    }),
    wM()
  );
  return p;
}
function DL(l, s) {
  if (typeof localStorage > "u") {
    Mt.error("Local storage is not available.");
    return;
  }
  try {
    if (s) {
      const c = Object.fromEntries(
        Object.entries(s).filter(([, p]) => p !== "")
      );
      localStorage.setItem(l, JSON.stringify(c));
    }
  } catch (c) {
    Mt.error(`Error setting item to local storage: ${c}`);
  }
}
function OL(l) {
  if (typeof localStorage > "u") {
    Mt.error("Local storage is not available.");
    return;
  }
  try {
    const s = localStorage.getItem(l);
    if (!s) {
      Mt.warn(`Item with key ${l} does not exist in local storage.`);
      return;
    }
    return JSON.parse(s);
  } catch (s) {
    Mt.error(`Error getting item from local storage: ${s}`);
    return;
  }
}
function ML(l) {
  return {
    load: () => OL(l),
    save: (s) => DL(l, s)
  };
}
var LL = `${pR}-user-choices`, Ov = {
  videoEnabled: !0,
  audioEnabled: !0,
  videoDeviceId: "",
  audioDeviceId: "",
  username: ""
}, { load: NL, save: AL } = ML(LL);
function zL(l, s = !1) {
  s !== !0 && AL(l);
}
function UL(l, s = !1) {
  var c, p, y, E, m;
  const x = {
    videoEnabled: (c = l == null ? void 0 : l.videoEnabled) != null ? c : Ov.videoEnabled,
    audioEnabled: (p = l == null ? void 0 : l.audioEnabled) != null ? p : Ov.audioEnabled,
    videoDeviceId: (y = l == null ? void 0 : l.videoDeviceId) != null ? y : Ov.videoDeviceId,
    audioDeviceId: (E = l == null ? void 0 : l.audioDeviceId) != null ? E : Ov.audioDeviceId,
    username: (m = l == null ? void 0 : l.username) != null ? m : Ov.username
  };
  if (s)
    return x;
  {
    const T = NL();
    return ET(ET({}, x), T ?? {});
  }
}
function CR(l, s) {
  if (s.msg === "show_chat")
    return { ...l, showChat: !0, unreadMessages: 0 };
  if (s.msg === "hide_chat")
    return { ...l, showChat: !1 };
  if (s.msg === "toggle_chat") {
    const c = { ...l, showChat: !l.showChat };
    return c.showChat === !0 && (c.unreadMessages = 0), c;
  } else return s.msg === "unread_msg" ? { ...l, unreadMessages: s.count } : s.msg === "toggle_settings" ? { ...l, showSettings: !l.showSettings } : { ...l };
}
function wR(l, s) {
  return s.msg === "set_pin" ? [s.trackReference] : s.msg === "clear_pin" ? [] : { ...l };
}
const Gy = R.createContext(void 0);
function PL() {
  const l = R.useContext(Gy);
  if (!l)
    throw Error("Tried to access LayoutContext context outside a LayoutContextProvider provider.");
  return l;
}
function FL(l) {
  const s = dE();
  if (l ?? (l = s), !l)
    throw Error("Tried to access LayoutContext context outside a LayoutContextProvider provider.");
  return l;
}
function HL() {
  const [l, s] = R.useReducer(wR, mR), [c, p] = R.useReducer(CR, yR);
  return {
    pin: { dispatch: s, state: l },
    widget: { dispatch: p, state: c }
  };
}
function jL(l) {
  const [s, c] = R.useReducer(wR, mR), [p, y] = R.useReducer(CR, yR);
  return l ?? {
    pin: { dispatch: c, state: s },
    widget: { dispatch: y, state: p }
  };
}
function dE() {
  return R.useContext(Gy);
}
const pE = R.createContext(
  void 0
);
function qy() {
  return R.useContext(pE);
}
function Cd(l) {
  const s = qy(), c = l ?? s;
  if (!c)
    throw new Error(
      "No TrackRef, make sure you are inside a TrackRefContext or pass the TrackRef explicitly"
    );
  return c;
}
const TR = R.createContext(void 0);
function RR() {
  return R.useContext(TR);
}
function Fv(l) {
  const s = RR(), c = qy(), p = l ?? s ?? (c == null ? void 0 : c.participant);
  if (!p)
    throw new Error(
      "No participant provided, make sure you are inside a participant context or pass the participant explicitly"
    );
  return p;
}
const vE = R.createContext(void 0);
function hE() {
  const l = R.useContext(vE);
  if (!l)
    throw Error("tried to access room context outside of livekit room component");
  return l;
}
function Xy() {
  return R.useContext(vE);
}
function Hv(l) {
  const s = Xy(), c = l ?? s;
  if (!c)
    throw new Error(
      "No room provided, make sure you are inside a Room context or pass the room explicitly"
    );
  return c;
}
const xR = R.createContext(void 0);
function VL(l) {
  return R.useContext(xR);
}
function kR(l) {
  var s, c, p = "";
  if (typeof l == "string" || typeof l == "number") p += l;
  else if (typeof l == "object") if (Array.isArray(l)) {
    var y = l.length;
    for (s = 0; s < y; s++) l[s] && (c = kR(l[s])) && (p && (p += " "), p += c);
  } else for (c in l) l[c] && (p && (p += " "), p += c);
  return p;
}
function _R() {
  for (var l, s, c = 0, p = "", y = arguments.length; c < y; c++) (l = arguments[c]) && (s = kR(l)) && (p && (p += " "), p += s);
  return p;
}
function $L(...l) {
  return (...s) => {
    for (const c of l)
      if (typeof c == "function")
        try {
          c(...s);
        } catch (p) {
          console.error(p);
        }
  };
}
function is(...l) {
  const s = { ...l[0] };
  for (let c = 1; c < l.length; c++) {
    const p = l[c];
    for (const y in p) {
      const E = s[y], m = p[y];
      typeof E == "function" && typeof m == "function" && // This is a lot faster than a regex.
      y[0] === "o" && y[1] === "n" && y.charCodeAt(2) >= /* 'A' */
      65 && y.charCodeAt(2) <= /* 'Z' */
      90 ? s[y] = $L(E, m) : (y === "className" || y === "UNSAFE_className") && typeof E == "string" && typeof m == "string" ? s[y] = _R(E, m) : s[y] = m !== void 0 ? m : E;
    }
  }
  return s;
}
const BL = {
  connect: !0,
  audio: !1,
  video: !1
};
function IL(l) {
  const {
    token: s,
    serverUrl: c,
    options: p,
    room: y,
    connectOptions: E,
    connect: m,
    audio: x,
    video: T,
    screen: D,
    onConnected: A,
    onDisconnected: L,
    onError: j,
    onMediaDeviceFailure: F,
    onEncryptionError: $,
    simulateParticipants: H,
    ...G
  } = { ...BL, ...l };
  p && y && Mt.warn(
    "when using a manually created room, the options object will be ignored. set the desired options directly when creating the room instead."
  );
  const [W, ve] = R.useState();
  R.useEffect(() => {
    ve(y ?? new M1(p));
  }, [y]);
  const fe = R.useMemo(() => {
    const { className: oe } = xL();
    return is(G, { className: oe });
  }, [G]);
  return R.useEffect(() => {
    if (!W) return;
    const oe = () => {
      const me = W.localParticipant;
      Mt.debug("trying to publish local tracks"), Promise.all([
        me.setMicrophoneEnabled(!!x, typeof x != "boolean" ? x : void 0),
        me.setCameraEnabled(!!T, typeof T != "boolean" ? T : void 0),
        me.setScreenShareEnabled(!!D, typeof D != "boolean" ? D : void 0)
      ]).catch((We) => {
        Mt.warn(We), j == null || j(We);
      });
    }, ne = (me) => {
      const We = L2.getFailure(me);
      F == null || F(We);
    }, Ce = (me) => {
      $ == null || $(me);
    };
    return W.on(Pe.SignalConnected, oe).on(Pe.MediaDevicesError, ne).on(Pe.EncryptionError, Ce), () => {
      W.off(Pe.SignalConnected, oe).off(Pe.MediaDevicesError, ne).off(Pe.EncryptionError, Ce);
    };
  }, [W, x, T, D, j, $, F]), R.useEffect(() => {
    if (W) {
      if (H) {
        W.simulateParticipants({
          participants: {
            count: H
          },
          publish: {
            audio: !0,
            useRealTracks: !0
          }
        });
        return;
      }
      if (!s) {
        Mt.debug("no token yet");
        return;
      }
      if (!c) {
        Mt.warn("no livekit url provided"), j == null || j(Error("no livekit url provided"));
        return;
      }
      m ? (Mt.debug("connecting"), W.connect(c, s, E).catch((oe) => {
        Mt.warn(oe), j == null || j(oe);
      })) : (Mt.debug("disconnecting because connect is false"), W.disconnect());
    }
  }, [
    m,
    s,
    JSON.stringify(E),
    W,
    j,
    c,
    H
  ]), R.useEffect(() => {
    if (!W) return;
    const oe = (ne) => {
      switch (ne) {
        case L1.Disconnected:
          L && L();
          break;
        case L1.Connected:
          A && A();
          break;
      }
    };
    return W.on(Pe.ConnectionStateChanged, oe), () => {
      W.off(Pe.ConnectionStateChanged, oe);
    };
  }, [s, A, L, W]), R.useEffect(() => {
    if (W)
      return () => {
        Mt.info("disconnecting on onmount"), W.disconnect();
      };
  }, [W]), { room: W, htmlProps: fe };
}
const x1 = /* @__PURE__ */ R.forwardRef(function(l, s) {
  const { room: c, htmlProps: p } = IL(l);
  return /* @__PURE__ */ R.createElement("div", { ref: s, ...p }, c && /* @__PURE__ */ R.createElement(vE.Provider, { value: c }, /* @__PURE__ */ R.createElement(xR.Provider, { value: l.featureFlags }, l.children)));
}), YL = (l) => {
  const s = R.useRef(l);
  return R.useEffect(() => {
    s.current = l;
  }), s;
};
function WL(l, s) {
  const c = GL(), p = YL(s);
  return R.useLayoutEffect(() => {
    let y = !1;
    const E = l.current;
    if (!E) return;
    function m(x, T) {
      y || p.current(x, T);
    }
    return c == null || c.subscribe(E, m), () => {
      y = !0, c == null || c.unsubscribe(E, m);
    };
  }, [l.current, c, p]), c == null ? void 0 : c.observer;
}
function QL() {
  let l = !1, s = [];
  const c = /* @__PURE__ */ new Map();
  if (typeof window > "u")
    return;
  const p = new ResizeObserver((y, E) => {
    s = s.concat(y), l || window.requestAnimationFrame(() => {
      const m = /* @__PURE__ */ new Set();
      for (let x = 0; x < s.length; x++) {
        if (m.has(s[x].target)) continue;
        m.add(s[x].target);
        const T = c.get(s[x].target);
        T == null || T.forEach((D) => D(s[x], E));
      }
      s = [], l = !1;
    }), l = !0;
  });
  return {
    observer: p,
    subscribe(y, E) {
      p.observe(y);
      const m = c.get(y) ?? [];
      m.push(E), c.set(y, m);
    },
    unsubscribe(y, E) {
      const m = c.get(y) ?? [];
      if (m.length === 1) {
        p.unobserve(y), c.delete(y);
        return;
      }
      const x = m.indexOf(E);
      x !== -1 && m.splice(x, 1), c.set(y, m);
    }
  };
}
let wT;
const GL = () => wT || (wT = QL()), DR = (l) => {
  const [s, c] = R.useState({ width: 0, height: 0 });
  R.useLayoutEffect(() => {
    if (l.current) {
      const { width: y, height: E } = l.current.getBoundingClientRect();
      c({ width: y, height: E });
    }
  }, [l.current]);
  const p = R.useCallback(
    (y) => c(y.contentRect),
    []
  );
  return WL(l, p), s;
};
function Ti(l, s, c = !0) {
  const [p, y] = R.useState(s);
  return R.useEffect(() => {
    if (c && y(s), typeof window > "u" || !l) return;
    const E = l.subscribe(y);
    return () => E.unsubscribe();
  }, [l, c]), p;
}
function qL(l = {}) {
  const s = Fv(l.participant), { className: c, connectionQualityObserver: p } = R.useMemo(
    () => SL(s),
    [s]
  ), y = Ti(p, Ly.Unknown);
  return { className: c, quality: y };
}
function XL(l) {
  const s = Hv(l), c = R.useMemo(() => iL(s), [s]);
  return Ti(c, s.state);
}
function KL(l) {
  const s = hE(), c = XL(s);
  return { buttonProps: R.useMemo(() => {
    const { className: p, disconnect: y } = gL(s);
    return is(l, {
      className: p,
      onClick: () => y(l.stopTracks ?? !0),
      disabled: c === L1.Disconnected
    });
  }, [s, l, c]) };
}
function ZL(l) {
  if (l.publication instanceof N2) {
    const s = l.publication.track;
    if (s) {
      const { facingMode: c } = A2(s);
      return c;
    }
  }
  return "undefined";
}
function JL({ trackRef: l, props: s }) {
  const c = Cd(l), p = dE(), { className: y } = R.useMemo(() => RL(), []), E = R.useMemo(() => LM(c, p == null ? void 0 : p.pin.state), [c, p == null ? void 0 : p.pin.state]);
  return { mergedProps: R.useMemo(
    () => is(s, {
      className: y,
      onClick: (m) => {
        var x, T, D, A, L;
        (x = s.onClick) == null || x.call(s, m), E ? (D = p == null ? void 0 : (T = p.pin).dispatch) == null || D.call(T, {
          msg: "clear_pin"
        }) : (L = p == null ? void 0 : (A = p.pin).dispatch) == null || L.call(A, {
          msg: "set_pin",
          trackReference: c
        });
      }
    }),
    [s, y, c, E, p == null ? void 0 : p.pin]
  ), inFocus: E };
}
function eN(l, s, c = {}) {
  const p = c.gridLayouts ?? jM, { width: y, height: E } = DR(l), m = hR(p, s, y, E);
  return R.useEffect(() => {
    l.current && m && (l.current.style.setProperty("--lk-col-count", m == null ? void 0 : m.columns.toString()), l.current.style.setProperty("--lk-row-count", m == null ? void 0 : m.rows.toString()));
  }, [l, m]), {
    layout: m,
    containerWidth: y,
    containerHeight: E
  };
}
function TT(l, s = {}) {
  var c, p;
  const y = typeof l == "string" ? s.participant : l.participant, E = Fv(y), m = typeof l == "string" ? { participant: E, source: l } : l, [x, T] = R.useState(
    !!((c = m.publication) != null && c.isMuted || (p = E.getTrackPublication(m.source)) != null && p.isMuted)
  );
  return R.useEffect(() => {
    const D = bR(m).subscribe(T);
    return () => D.unsubscribe();
  }, [Zr(m)]), x;
}
function tN(l) {
  const s = Fv(l), c = R.useMemo(() => pL(s), [s]);
  return Ti(c, s.isSpeaking);
}
function nN() {
  const l = hE(), s = R.useMemo(
    () => vL(l.localParticipant),
    [l]
  );
  return Ti(s, l.localParticipant.permissions);
}
function rN({
  kind: l,
  room: s,
  track: c,
  requestPermissions: p,
  onError: y
}) {
  const E = Xy(), m = R.useMemo(
    () => oL(l, y, p),
    [l, p, y]
  ), x = Ti(m, []), [T, D] = R.useState(""), { className: A, activeDeviceObservable: L, setActiveMediaDevice: j } = R.useMemo(
    () => yL(l, s ?? E, c),
    [l, s, E, c]
  );
  return R.useEffect(() => {
    const F = L.subscribe(($) => {
      Mt.info("setCurrentDeviceId", $), $ && D($);
    });
    return () => {
      F == null || F.unsubscribe();
    };
  }, [L]), { devices: x, className: A, activeDeviceId: T, setActiveMediaDevice: j };
}
function OR(l, s, c = {}) {
  const p = R.useRef([]), y = R.useRef(-1), E = s !== y.current, m = typeof c.customSortFunction == "function" ? c.customSortFunction(l) : QM(l);
  let x = [...m];
  if (E === !1)
    try {
      x = nL(p.current, m, s);
    } catch (T) {
      Mt.error("Error while running updatePages(): ", T);
    }
  return E ? p.current = m : p.current = x, y.current = s, x;
}
function aN(l, s) {
  const [c, p] = R.useState(1), y = Math.max(Math.ceil(s.length / l), 1);
  c > y && p(y);
  const E = c * l, m = E - l, x = (A) => {
    p((L) => A === "next" ? L === y ? L : L + 1 : L === 1 ? L : L - 1);
  }, T = (A) => {
    A > y ? p(y) : A < 1 ? p(1) : p(A);
  }, D = OR(s, l).slice(m, E);
  return {
    totalPageCount: y,
    nextPage: () => x("next"),
    prevPage: () => x("previous"),
    setPage: T,
    firstItemIndex: m,
    lastItemIndex: E,
    tracks: D,
    currentPage: c
  };
}
function iN({
  trackRef: l,
  onParticipantClick: s,
  disableSpeakingIndicator: c,
  htmlProps: p
}) {
  const y = Cd(l), E = R.useMemo(() => {
    const { className: j } = CL();
    return is(p, {
      className: j,
      onClick: (F) => {
        var $;
        if (($ = p.onClick) == null || $.call(p, F), typeof s == "function") {
          const H = y.publication ?? y.participant.getTrackPublication(y.source);
          s({ participant: y.participant, track: H });
        }
      }
    });
  }, [
    p,
    s,
    y.publication,
    y.source,
    y.participant
  ]), m = y.participant.getTrackPublication(ht.Source.Microphone), x = R.useMemo(() => ({
    participant: y.participant,
    source: ht.Source.Microphone,
    publication: m
  }), [m, y.participant]), T = TT(y), D = TT(x), A = tN(y.participant), L = ZL(y);
  return {
    elementProps: {
      "data-lk-audio-muted": D,
      "data-lk-video-muted": T,
      "data-lk-speaking": c === !0 ? !1 : A,
      "data-lk-local-participant": y.participant.isLocal,
      "data-lk-source": y.source,
      "data-lk-facing-mode": L,
      ...E
    }
  };
}
function oN(l) {
  return l = FL(l), R.useMemo(() => (l == null ? void 0 : l.pin.state) !== void 0 && l.pin.state.length >= 1 ? l.pin.state : [], [l.pin.state]);
}
function lN({ room: l, props: s }) {
  const c = Hv(l), { className: p, roomAudioPlaybackAllowedObservable: y, handleStartAudioPlayback: E } = R.useMemo(
    () => wL(),
    []
  ), m = R.useMemo(
    () => y(c),
    [c, y]
  ), { canPlayAudio: x } = Ti(m, {
    canPlayAudio: c.canPlaybackAudio
  });
  return { mergedProps: R.useMemo(
    () => is(s, {
      className: p,
      onClick: () => {
        E(c);
      },
      style: { display: x ? "none" : "block" }
    }),
    [s, p, x, E, c]
  ), canPlayAudio: x };
}
function uN({ room: l, props: s }) {
  const c = Hv(l), { className: p, roomVideoPlaybackAllowedObservable: y, handleStartVideoPlayback: E } = R.useMemo(
    () => TL(),
    []
  ), m = R.useMemo(
    () => y(c),
    [c, y]
  ), { canPlayVideo: x } = Ti(m, {
    canPlayVideo: c.canPlaybackVideo
  });
  return { mergedProps: R.useMemo(
    () => is(s, {
      className: p,
      onClick: () => {
        E(c);
      },
      style: { display: x ? "none" : "block" }
    }),
    [s, p, x, E, c]
  ), canPlayVideo: x };
}
function sN(l, s = {}) {
  const c = R.useRef(null), p = R.useRef(null), y = s.minSwipeDistance ?? 50, E = (T) => {
    p.current = null, c.current = T.targetTouches[0].clientX;
  }, m = (T) => {
    p.current = T.targetTouches[0].clientX;
  }, x = R.useCallback(() => {
    if (!c.current || !p.current)
      return;
    const T = c.current - p.current, D = T > y, A = T < -y;
    D && s.onLeftSwipe && s.onLeftSwipe(), A && s.onRightSwipe && s.onRightSwipe();
  }, [y, s]);
  R.useEffect(() => {
    const T = l.current;
    return T && (T.addEventListener("touchstart", E, { passive: !0 }), T.addEventListener("touchmove", m, { passive: !0 }), T.addEventListener("touchend", x, { passive: !0 })), () => {
      T && (T.removeEventListener("touchstart", E), T.removeEventListener("touchmove", m), T.removeEventListener("touchend", x));
    };
  }, [l, x]);
}
function cN(l) {
  var s, c;
  const p = Cd(l), { className: y, mediaMutedObserver: E } = R.useMemo(
    () => EL(p),
    [Zr(p)]
  );
  return { isMuted: Ti(
    E,
    !!((s = p.publication) != null && s.isMuted || (c = p.participant.getTrackPublication(p.source)) != null && c.isMuted)
  ), className: y };
}
function fN({
  source: l,
  onChange: s,
  initialState: c,
  captureOptions: p,
  publishOptions: y,
  onDeviceError: E,
  ...m
}) {
  var x;
  const T = Xy(), D = (x = T == null ? void 0 : T.localParticipant) == null ? void 0 : x.getTrackPublication(l), A = R.useRef(!1), { toggle: L, className: j, pendingObserver: F, enabledObserver: $ } = R.useMemo(
    () => T ? hL(l, T, p, y, E) : mL(),
    [T, l, JSON.stringify(p), y]
  ), H = Ti(F, !1), G = Ti($, c ?? !!(D != null && D.isEnabled));
  R.useEffect(() => {
    s == null || s(G, A.current), A.current = !1;
  }, [G, s]), R.useEffect(() => {
    c !== void 0 && (Mt.debug("forcing initial toggle state", l, c), L(c));
  }, []);
  const W = R.useMemo(() => is(m, { className: j }), [m, j]), ve = R.useCallback(
    (fe) => {
      var oe;
      A.current = !0, L().catch(() => A.current = !1), (oe = m.onClick) == null || oe.call(m, fe);
    },
    [m, L]
  );
  return {
    toggle: L,
    enabled: G,
    pending: H,
    track: D,
    buttonProps: {
      ...W,
      "aria-pressed": G,
      "data-lk-source": l,
      "data-lk-enabled": G,
      disabled: H,
      onClick: ve
    }
  };
}
function MR(l = [
  ht.Source.Camera,
  ht.Source.Microphone,
  ht.Source.ScreenShare,
  ht.Source.ScreenShareAudio,
  ht.Source.Unknown
], s = {}) {
  const c = Hv(s.room), [p, y] = R.useState([]), [E, m] = R.useState([]), x = R.useMemo(() => l.map((T) => gR(T) ? T.source : T), [JSON.stringify(l)]);
  return R.useEffect(() => {
    const T = kL(c, x, {
      additionalRoomEvents: s.updateOnlyOn,
      onlySubscribed: s.onlySubscribed
    }).subscribe(({ trackReferences: D, participants: A }) => {
      Mt.debug("setting track bundles", D, A), y(D), m(A);
    });
    return () => T.unsubscribe();
  }, [
    c,
    JSON.stringify(s.onlySubscribed),
    JSON.stringify(s.updateOnlyOn),
    JSON.stringify(l)
  ]), R.useMemo(() => {
    if (SR(l)) {
      const T = pN(l, E), D = Array.from(p);
      return E.forEach((A) => {
        T.has(A.identity) && (T.get(A.identity) ?? []).forEach((L) => {
          if (p.find(
            ({ participant: F, publication: $ }) => A.identity === F.identity && $.source === L
          ))
            return;
          Mt.debug(
            `Add ${L} placeholder for participant ${A.identity}.`
          );
          const j = {
            participant: A,
            source: L
          };
          D.push(j);
        });
      }), D;
    } else
      return p;
  }, [p, E, l]);
}
function dN(l, s) {
  const c = new Set(l);
  for (const p of s)
    c.delete(p);
  return c;
}
function pN(l, s) {
  const c = /* @__PURE__ */ new Map();
  if (SR(l)) {
    const p = l.filter((y) => y.withPlaceholder).map((y) => y.source);
    s.forEach((y) => {
      const E = y.getTrackPublications().map((x) => {
        var T;
        return (T = x.track) == null ? void 0 : T.source;
      }).filter((x) => x !== void 0), m = Array.from(
        dN(new Set(p), new Set(E))
      );
      m.length > 0 && c.set(y.identity, m);
    });
  }
  return c;
}
function vN(l = {}) {
  const [s, c] = R.useState(
    UL(l.defaults, l.preventLoad ?? !1)
  ), p = R.useCallback((T) => {
    c((D) => ({ ...D, audioEnabled: T }));
  }, []), y = R.useCallback((T) => {
    c((D) => ({ ...D, videoEnabled: T }));
  }, []), E = R.useCallback((T) => {
    c((D) => ({ ...D, audioDeviceId: T }));
  }, []), m = R.useCallback((T) => {
    c((D) => ({ ...D, videoDeviceId: T }));
  }, []), x = R.useCallback((T) => {
    c((D) => ({ ...D, username: T }));
  }, []);
  return R.useEffect(() => {
    zL(s, l.preventSave ?? !1);
  }, [s, l.preventSave]), {
    userChoices: s,
    saveAudioInputEnabled: p,
    saveVideoInputEnabled: y,
    saveAudioInputDeviceId: E,
    saveVideoInputDeviceId: m,
    saveUsername: x
  };
}
function hN(l, s = {}) {
  const c = Fv(l), p = Hv(s.room), y = R.useMemo(() => cL(p, c), [p, c]);
  return Ti(
    y,
    c instanceof Z1 ? c.isE2EEEnabled : !!(c != null && c.isEncrypted)
  );
}
const mN = /* @__PURE__ */ R.forwardRef(
  function(l, s) {
    const { buttonProps: c } = KL(l);
    return /* @__PURE__ */ R.createElement("button", { ref: s, ...c }, l.children);
  }
), yN = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 16, height: 16, fill: "currentColor", ...l }, /* @__PURE__ */ R.createElement("path", { d: "M1.354.646a.5.5 0 1 0-.708.708l14 14a.5.5 0 0 0 .708-.708L11 10.293V4.5A1.5 1.5 0 0 0 9.5 3H3.707zM0 4.5a1.5 1.5 0 0 1 .943-1.393l9.532 9.533c-.262.224-.603.36-.975.36h-8A1.5 1.5 0 0 1 0 11.5z" }), /* @__PURE__ */ R.createElement("path", { d: "m15.2 3.6-2.8 2.1a1 1 0 0 0-.4.8v3a1 1 0 0 0 .4.8l2.8 2.1a.5.5 0 0 0 .8-.4V4a.5.5 0 0 0-.8-.4z" })), gN = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 16, height: 16, fill: "currentColor", ...l }, /* @__PURE__ */ R.createElement("path", { d: "M0 4.5A1.5 1.5 0 0 1 1.5 3h8A1.5 1.5 0 0 1 11 4.5v7A1.5 1.5 0 0 1 9.5 13h-8A1.5 1.5 0 0 1 0 11.5zM15.2 3.6l-2.8 2.1a1 1 0 0 0-.4.8v3a1 1 0 0 0 .4.8l2.8 2.1a.5.5 0 0 0 .8-.4V4a.5.5 0 0 0-.8-.4z" })), RT = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 16, height: 16, fill: "none", ...l }, /* @__PURE__ */ R.createElement(
  "path",
  {
    fill: "currentcolor",
    fillRule: "evenodd",
    d: "M5.293 2.293a1 1 0 0 1 1.414 0l4.823 4.823a1.25 1.25 0 0 1 0 1.768l-4.823 4.823a1 1 0 0 1-1.414-1.414L9.586 8 5.293 3.707a1 1 0 0 1 0-1.414z",
    clipRule: "evenodd"
  }
)), SN = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 16, height: 16, fill: "none", ...l }, /* @__PURE__ */ R.createElement("g", { stroke: "currentColor", strokeLinecap: "round", strokeLinejoin: "round", strokeWidth: 1.5 }, /* @__PURE__ */ R.createElement("path", { d: "M10 1.75h4.25m0 0V6m0-4.25L9 7M6 14.25H1.75m0 0V10m0 4.25L7 9" }))), EN = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 16, height: 16, fill: "none", ...l }, /* @__PURE__ */ R.createElement(
  "path",
  {
    fill: "currentcolor",
    fillRule: "evenodd",
    d: "M8.961.894C8.875-.298 7.125-.298 7.04.894c-.066.912-1.246 1.228-1.76.472-.67-.99-2.186-.115-1.664.96.399.824-.465 1.688-1.288 1.289-1.076-.522-1.95.994-.961 1.665.756.513.44 1.693-.472 1.759-1.192.086-1.192 1.836 0 1.922.912.066 1.228 1.246.472 1.76-.99.67-.115 2.186.96 1.664.824-.399 1.688.465 1.289 1.288-.522 1.076.994 1.95 1.665.961.513-.756 1.693-.44 1.759.472.086 1.192 1.836 1.192 1.922 0 .066-.912 1.246-1.228 1.76-.472.67.99 2.186.115 1.664-.96-.399-.824.465-1.688 1.288-1.289 1.076.522 1.95-.994.961-1.665-.756-.513-.44-1.693.472-1.759 1.192-.086 1.192-1.836 0-1.922-.912-.066-1.228-1.246-.472-1.76.99-.67.115-2.186-.96-1.664-.824.399-1.688-.465-1.289-1.288.522-1.076-.994-1.95-1.665-.961-.513.756-1.693.44-1.759-.472ZM8 13A5 5 0 1 0 8 3a5 5 0 0 0 0 10Z",
    clipRule: "evenodd"
  }
)), bN = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 16, height: 16, fill: "none", ...l }, /* @__PURE__ */ R.createElement(
  "path",
  {
    fill: "currentColor",
    fillRule: "evenodd",
    d: "M2 2.75A2.75 2.75 0 0 1 4.75 0h6.5A2.75 2.75 0 0 1 14 2.75v10.5A2.75 2.75 0 0 1 11.25 16h-6.5A2.75 2.75 0 0 1 2 13.25v-.5a.75.75 0 0 1 1.5 0v.5c0 .69.56 1.25 1.25 1.25h6.5c.69 0 1.25-.56 1.25-1.25V2.75c0-.69-.56-1.25-1.25-1.25h-6.5c-.69 0-1.25.56-1.25 1.25v.5a.75.75 0 0 1-1.5 0v-.5Z",
    clipRule: "evenodd"
  }
), /* @__PURE__ */ R.createElement(
  "path",
  {
    fill: "currentColor",
    fillRule: "evenodd",
    d: "M8.78 7.47a.75.75 0 0 1 0 1.06l-2.25 2.25a.75.75 0 1 1-1.06-1.06l.97-.97H1.75a.75.75 0 0 1 0-1.5h4.69l-.97-.97a.75.75 0 0 1 1.06-1.06l2.25 2.25Z",
    clipRule: "evenodd"
  }
)), CN = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 16, height: 16, fill: "none", ...l }, /* @__PURE__ */ R.createElement(
  "path",
  {
    fill: "currentcolor",
    fillRule: "evenodd",
    d: "M4 6.104V4a4 4 0 1 1 8 0v2.104c1.154.326 2 1.387 2 2.646v4.5A2.75 2.75 0 0 1 11.25 16h-6.5A2.75 2.75 0 0 1 2 13.25v-4.5c0-1.259.846-2.32 2-2.646ZM5.5 4a2.5 2.5 0 0 1 5 0v2h-5V4Z",
    clipRule: "evenodd"
  }
)), wN = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 16, height: 16, fill: "currentColor", ...l }, /* @__PURE__ */ R.createElement("path", { d: "M12.227 11.52a5.477 5.477 0 0 0 1.246-2.97.5.5 0 0 0-.995-.1 4.478 4.478 0 0 1-.962 2.359l-1.07-1.07C10.794 9.247 11 8.647 11 8V3a3 3 0 0 0-6 0v1.293L1.354.646a.5.5 0 1 0-.708.708l14 14a.5.5 0 0 0 .708-.708zM8 12.5c.683 0 1.33-.152 1.911-.425l.743.743c-.649.359-1.378.59-2.154.66V15h2a.5.5 0 0 1 0 1h-5a.5.5 0 0 1 0-1h2v-1.522a5.502 5.502 0 0 1-4.973-4.929.5.5 0 0 1 .995-.098A4.5 4.5 0 0 0 8 12.5z" }), /* @__PURE__ */ R.createElement("path", { d: "M8.743 10.907 5 7.164V8a3 3 0 0 0 3.743 2.907z" })), TN = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 16, height: 16, fill: "currentColor", ...l }, /* @__PURE__ */ R.createElement(
  "path",
  {
    fillRule: "evenodd",
    d: "M2.975 8.002a.5.5 0 0 1 .547.449 4.5 4.5 0 0 0 8.956 0 .5.5 0 1 1 .995.098A5.502 5.502 0 0 1 8.5 13.478V15h2a.5.5 0 0 1 0 1h-5a.5.5 0 0 1 0-1h2v-1.522a5.502 5.502 0 0 1-4.973-4.929.5.5 0 0 1 .448-.547z",
    clipRule: "evenodd"
  }
), /* @__PURE__ */ R.createElement("path", { d: "M5 3a3 3 0 1 1 6 0v5a3 3 0 0 1-6 0z" })), RN = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 16, height: 16, fill: "currentcolor", ...l }, /* @__PURE__ */ R.createElement("path", { d: "M0 11.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5zm6-5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5zm6-6a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5z" }), /* @__PURE__ */ R.createElement("path", { d: "M0 11.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5zm6-5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5zm6-6a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5z" })), xN = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 16, height: 16, fill: "currentcolor", ...l }, /* @__PURE__ */ R.createElement("path", { d: "M0 11.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5zm6-5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5z" }), /* @__PURE__ */ R.createElement("path", { d: "M0 11.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5zm6-5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5z" }), /* @__PURE__ */ R.createElement("g", { opacity: 0.25 }, /* @__PURE__ */ R.createElement("path", { d: "M12 .5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5z" }), /* @__PURE__ */ R.createElement("path", { d: "M12 .5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5z" }))), kN = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 16, height: 16, fill: "currentcolor", ...l }, /* @__PURE__ */ R.createElement("path", { d: "M0 11.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5z" }), /* @__PURE__ */ R.createElement("path", { d: "M0 11.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5z" }), /* @__PURE__ */ R.createElement("g", { opacity: 0.25 }, /* @__PURE__ */ R.createElement("path", { d: "M6 6.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5z" }), /* @__PURE__ */ R.createElement("path", { d: "M6 6.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5zm6-6a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5z" }), /* @__PURE__ */ R.createElement("path", { d: "M12 .5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5z" }))), _N = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 16, height: 16, fill: "currentColor", ...l }, /* @__PURE__ */ R.createElement("g", { opacity: 0.25 }, /* @__PURE__ */ R.createElement("path", { d: "M0 11.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-4Zm6-5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-9Zm6-6a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5V.5Z" }), /* @__PURE__ */ R.createElement("path", { d: "M0 11.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-4Zm6-5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-9Zm6-6a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5V.5Z" }))), LR = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 20, height: 16, fill: "none", ...l }, /* @__PURE__ */ R.createElement(
  "path",
  {
    fill: "currentColor",
    fillRule: "evenodd",
    d: "M0 2.75A2.75 2.75 0 0 1 2.75 0h14.5A2.75 2.75 0 0 1 20 2.75v10.5A2.75 2.75 0 0 1 17.25 16H2.75A2.75 2.75 0 0 1 0 13.25V2.75ZM2.75 1.5c-.69 0-1.25.56-1.25 1.25v10.5c0 .69.56 1.25 1.25 1.25h14.5c.69 0 1.25-.56 1.25-1.25V2.75c0-.69-.56-1.25-1.25-1.25H2.75Z",
    clipRule: "evenodd"
  }
), /* @__PURE__ */ R.createElement(
  "path",
  {
    fill: "currentColor",
    fillRule: "evenodd",
    d: "M9.47 4.22a.75.75 0 0 1 1.06 0l2.25 2.25a.75.75 0 0 1-1.06 1.06l-.97-.97v4.69a.75.75 0 0 1-1.5 0V6.56l-.97.97a.75.75 0 0 1-1.06-1.06l2.25-2.25Z",
    clipRule: "evenodd"
  }
)), DN = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 20, height: 16, fill: "none", ...l }, /* @__PURE__ */ R.createElement("g", { fill: "currentColor" }, /* @__PURE__ */ R.createElement("path", { d: "M7.28 4.22a.75.75 0 0 0-1.06 1.06L8.94 8l-2.72 2.72a.75.75 0 1 0 1.06 1.06L10 9.06l2.72 2.72a.75.75 0 1 0 1.06-1.06L11.06 8l2.72-2.72a.75.75 0 0 0-1.06-1.06L10 6.94z" }), /* @__PURE__ */ R.createElement(
  "path",
  {
    fillRule: "evenodd",
    d: "M2.75 0A2.75 2.75 0 0 0 0 2.75v10.5A2.75 2.75 0 0 0 2.75 16h14.5A2.75 2.75 0 0 0 20 13.25V2.75A2.75 2.75 0 0 0 17.25 0zM1.5 2.75c0-.69.56-1.25 1.25-1.25h14.5c.69 0 1.25.56 1.25 1.25v10.5c0 .69-.56 1.25-1.25 1.25H2.75c-.69 0-1.25-.56-1.25-1.25z",
    clipRule: "evenodd"
  }
))), ON = (l) => /* @__PURE__ */ R.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: 16, height: 16, fill: "none", ...l }, /* @__PURE__ */ R.createElement("g", { stroke: "currentColor", strokeLinecap: "round", strokeLinejoin: "round", strokeWidth: 1.5 }, /* @__PURE__ */ R.createElement("path", { d: "M13.25 7H9m0 0V2.75M9 7l5.25-5.25M2.75 9H7m0 0v4.25M7 9l-5.25 5.25" }))), MN = /* @__PURE__ */ R.forwardRef(
  function({ trackRef: l, ...s }, c) {
    const p = qy(), { mergedProps: y, inFocus: E } = JL({
      trackRef: l ?? p,
      props: s
    });
    return /* @__PURE__ */ R.createElement(Gy.Consumer, null, (m) => m !== void 0 && /* @__PURE__ */ R.createElement("button", { ref: c, ...y }, s.children ? s.children : E ? /* @__PURE__ */ R.createElement(ON, null) : /* @__PURE__ */ R.createElement(SN, null)));
  }
);
function LN(l) {
  return l !== void 0;
}
function yc(...l) {
  return is(...l.filter(LN));
}
function NN(l, s, c) {
  return R.Children.map(l, (p) => R.isValidElement(p) && R.Children.only(l) ? (p.props.class && (s ?? (s = {}), s.class = _R(p.props.class, s.class), s.style = { ...p.props.style, ...s.style }), R.cloneElement(p, { ...s, key: c })) : p);
}
const k1 = /* @__PURE__ */ R.forwardRef(
  function({
    kind: l,
    initialSelection: s,
    onActiveDeviceChange: c,
    onDeviceListChange: p,
    onDeviceSelectError: y,
    exactMatch: E,
    track: m,
    requestPermissions: x,
    onError: T,
    ...D
  }, A) {
    const L = Xy(), j = R.useCallback(
      (oe) => {
        L && L.emit(Pe.MediaDevicesError, oe), T == null || T(oe);
      },
      [L, T]
    ), { devices: F, activeDeviceId: $, setActiveMediaDevice: H, className: G } = rN({
      kind: l,
      room: L,
      track: m,
      requestPermissions: x,
      onError: j
    });
    R.useEffect(() => {
      s !== void 0 && H(s);
    }, [H]), R.useEffect(() => {
      typeof p == "function" && p(F);
    }, [p, F]), R.useEffect(() => {
      $ && $ !== "" && (c == null || c($));
    }, [$]);
    const W = async (oe) => {
      try {
        await H(oe, { exact: E });
      } catch (ne) {
        if (ne instanceof Error)
          y == null || y(ne);
        else
          throw ne;
      }
    }, ve = R.useMemo(
      () => yc(D, { className: G }, { className: "lk-list" }),
      [G, D]
    );
    function fe(oe, ne, Ce) {
      return oe === ne || Ce === 0 && ne === "default";
    }
    return /* @__PURE__ */ R.createElement("ul", { ref: A, ...ve }, F.map((oe, ne) => /* @__PURE__ */ R.createElement(
      "li",
      {
        key: oe.deviceId,
        id: oe.deviceId,
        "data-lk-active": fe(oe.deviceId, $, ne),
        "aria-selected": fe(oe.deviceId, $, ne),
        role: "option"
      },
      /* @__PURE__ */ R.createElement("button", { className: "lk-button", onClick: () => W(oe.deviceId) }, oe.label)
    )));
  }
);
function NR(l, s) {
  switch (l) {
    case ht.Source.Microphone:
      return s ? /* @__PURE__ */ R.createElement(TN, null) : /* @__PURE__ */ R.createElement(wN, null);
    case ht.Source.Camera:
      return s ? /* @__PURE__ */ R.createElement(gN, null) : /* @__PURE__ */ R.createElement(yN, null);
    case ht.Source.ScreenShare:
      return s ? /* @__PURE__ */ R.createElement(DN, null) : /* @__PURE__ */ R.createElement(LR, null);
    default:
      return;
  }
}
function AN(l) {
  switch (l) {
    case Ly.Excellent:
      return /* @__PURE__ */ R.createElement(RN, null);
    case Ly.Good:
      return /* @__PURE__ */ R.createElement(xN, null);
    case Ly.Poor:
      return /* @__PURE__ */ R.createElement(kN, null);
    default:
      return /* @__PURE__ */ R.createElement(_N, null);
  }
}
const _1 = /* @__PURE__ */ R.forwardRef(function({ showIcon: l, ...s }, c) {
  const { buttonProps: p, enabled: y } = fN(s);
  return /* @__PURE__ */ R.createElement("button", { ref: c, ...p }, (l ?? !0) && NR(s.source, y), s.children);
}), zN = /* @__PURE__ */ R.forwardRef(function(l, s) {
  const { className: c, quality: p } = qL(l), y = R.useMemo(() => ({ ...yc(l, { className: c }), "data-lk-quality": p }), [p, l, c]);
  return /* @__PURE__ */ R.createElement("div", { ref: s, ...y }, l.children ?? AN(p));
}), xT = /* @__PURE__ */ R.forwardRef(
  function({ participant: l, ...s }, c) {
    const p = Fv(l), { className: y, infoObserver: E } = R.useMemo(() => bL(p), [p]), { identity: m, name: x } = Ti(E, {
      name: p.name,
      identity: p.identity,
      metadata: p.metadata
    }), T = R.useMemo(() => yc(s, { className: y, "data-lk-participant-name": x }), [s, y, x]);
    return /* @__PURE__ */ R.createElement("span", { ref: c, ...T }, x !== "" ? x : m, s.children);
  }
), UN = /* @__PURE__ */ R.forwardRef(
  function({ trackRef: l, show: s = "always", ...c }, p) {
    const { className: y, isMuted: E } = cN(l), m = s === "always" || s === "muted" && E || s === "unmuted" && !E, x = R.useMemo(
      () => yc(c, {
        className: y
      }),
      [y, c]
    );
    return m ? /* @__PURE__ */ R.createElement("div", { ref: p, ...x, "data-lk-muted": E }, c.children ?? NR(l.source, !E)) : null;
  }
), PN = (l) => /* @__PURE__ */ R.createElement(
  "svg",
  {
    width: 320,
    height: 320,
    viewBox: "0 0 320 320",
    preserveAspectRatio: "xMidYMid meet",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg",
    ...l
  },
  /* @__PURE__ */ R.createElement(
    "path",
    {
      d: "M160 180C204.182 180 240 144.183 240 100C240 55.8172 204.182 20 160 20C115.817 20 79.9997 55.8172 79.9997 100C79.9997 144.183 115.817 180 160 180Z",
      fill: "white",
      fillOpacity: 0.25
    }
  ),
  /* @__PURE__ */ R.createElement(
    "path",
    {
      d: "M97.6542 194.614C103.267 191.818 109.841 192.481 115.519 195.141C129.025 201.466 144.1 205 159.999 205C175.899 205 190.973 201.466 204.48 195.141C210.158 192.481 216.732 191.818 222.345 194.614C262.703 214.719 291.985 253.736 298.591 300.062C300.15 310.997 291.045 320 280 320H39.9997C28.954 320 19.8495 310.997 21.4087 300.062C28.014 253.736 57.2966 214.72 97.6542 194.614Z",
      fill: "white",
      fillOpacity: 0.25
    }
  )
);
function AR(l, s = {}) {
  const [c, p] = R.useState(q1(l)), [y, E] = R.useState(c == null ? void 0 : c.isMuted), [m, x] = R.useState(c == null ? void 0 : c.isSubscribed), [T, D] = R.useState(c == null ? void 0 : c.track), [A, L] = R.useState("landscape"), j = R.useRef(), { className: F, trackObserver: $ } = R.useMemo(() => aL(l), [
    l.participant.sid ?? l.participant.identity,
    l.source,
    Ci(l) && l.publication.trackSid
  ]);
  return R.useEffect(() => {
    const H = $.subscribe((G) => {
      Mt.debug("update track", G), p(G), E(G == null ? void 0 : G.isMuted), x(G == null ? void 0 : G.isSubscribed), D(G == null ? void 0 : G.track);
    });
    return () => H == null ? void 0 : H.unsubscribe();
  }, [$]), R.useEffect(() => {
    var H, G;
    return T && (j.current && T.detach(j.current), (H = s.element) != null && H.current && !(vR(l.participant) && (T == null ? void 0 : T.kind) === "audio") && T.attach(s.element.current)), j.current = (G = s.element) == null ? void 0 : G.current, () => {
      j.current && (T == null || T.detach(j.current));
    };
  }, [T, s.element]), R.useEffect(() => {
    var H, G;
    if (typeof ((H = c == null ? void 0 : c.dimensions) == null ? void 0 : H.width) == "number" && typeof ((G = c == null ? void 0 : c.dimensions) == null ? void 0 : G.height) == "number") {
      const W = c.dimensions.width > c.dimensions.height ? "landscape" : "portrait";
      L(W);
    }
  }, [c]), {
    publication: c,
    isMuted: y,
    isSubscribed: m,
    track: T,
    elementProps: yc(s.props, {
      className: F,
      "data-lk-local-participant": l.participant.isLocal,
      "data-lk-source": c == null ? void 0 : c.source,
      ...(c == null ? void 0 : c.kind) === "video" && { "data-lk-orientation": A }
    })
  };
}
var FN = "Expected a function", kT = NaN, HN = "[object Symbol]", jN = /^\s+|\s+$/g, VN = /^[-+]0x[0-9a-f]+$/i, $N = /^0b[01]+$/i, BN = /^0o[0-7]+$/i, IN = parseInt, YN = typeof Lv == "object" && Lv && Lv.Object === Object && Lv, WN = typeof self == "object" && self && self.Object === Object && self, QN = YN || WN || Function("return this")(), GN = Object.prototype, qN = GN.toString, XN = Math.max, KN = Math.min, D1 = function() {
  return QN.Date.now();
};
function ZN(l, s, c) {
  var p, y, E, m, x, T, D = 0, A = !1, L = !1, j = !0;
  if (typeof l != "function")
    throw new TypeError(FN);
  s = _T(s) || 0, X1(c) && (A = !!c.leading, L = "maxWait" in c, E = L ? XN(_T(c.maxWait) || 0, s) : E, j = "trailing" in c ? !!c.trailing : j);
  function F(Ce) {
    var me = p, We = y;
    return p = y = void 0, D = Ce, m = l.apply(We, me), m;
  }
  function $(Ce) {
    return D = Ce, x = setTimeout(W, s), A ? F(Ce) : m;
  }
  function H(Ce) {
    var me = Ce - T, We = Ce - D, Et = s - me;
    return L ? KN(Et, E - We) : Et;
  }
  function G(Ce) {
    var me = Ce - T, We = Ce - D;
    return T === void 0 || me >= s || me < 0 || L && We >= E;
  }
  function W() {
    var Ce = D1();
    if (G(Ce))
      return ve(Ce);
    x = setTimeout(W, H(Ce));
  }
  function ve(Ce) {
    return x = void 0, j && p ? F(Ce) : (p = y = void 0, m);
  }
  function fe() {
    x !== void 0 && clearTimeout(x), D = 0, p = T = y = x = void 0;
  }
  function oe() {
    return x === void 0 ? m : ve(D1());
  }
  function ne() {
    var Ce = D1(), me = G(Ce);
    if (p = arguments, y = this, T = Ce, me) {
      if (x === void 0)
        return $(T);
      if (L)
        return x = setTimeout(W, s), F(T);
    }
    return x === void 0 && (x = setTimeout(W, s)), m;
  }
  return ne.cancel = fe, ne.flush = oe, ne;
}
function X1(l) {
  var s = typeof l;
  return !!l && (s == "object" || s == "function");
}
function JN(l) {
  return !!l && typeof l == "object";
}
function eA(l) {
  return typeof l == "symbol" || JN(l) && qN.call(l) == HN;
}
function _T(l) {
  if (typeof l == "number")
    return l;
  if (eA(l))
    return kT;
  if (X1(l)) {
    var s = typeof l.valueOf == "function" ? l.valueOf() : l;
    l = X1(s) ? s + "" : s;
  }
  if (typeof l != "string")
    return l === 0 ? l : +l;
  l = l.replace(jN, "");
  var c = $N.test(l);
  return c || BN.test(l) ? IN(l.slice(2), c ? 2 : 8) : VN.test(l) ? kT : +l;
}
var tA = ZN;
const DT = /* @__PURE__ */ WT(tA);
function nA(l) {
  const s = R.useRef(l);
  s.current = l, R.useEffect(
    () => () => {
      s.current();
    },
    []
  );
}
function rA(l, s = 500, c) {
  const p = R.useRef();
  nA(() => {
    p.current && p.current.cancel();
  });
  const y = R.useMemo(() => {
    const E = DT(l, s, c), m = (...x) => E(...x);
    return m.cancel = () => {
      E.cancel();
    }, m.isPending = () => !!p.current, m.flush = () => E.flush(), m;
  }, [l, s, c]);
  return R.useEffect(() => {
    p.current = DT(l, s, c);
  }, [l, s, c]), y;
}
function aA(l, s, c) {
  const p = (D, A) => D === A, y = l instanceof Function ? l() : l, [E, m] = R.useState(y), x = R.useRef(y), T = rA(
    m,
    s,
    c
  );
  return p(x.current, y) || (T(y), x.current = y), [E, T];
}
function iA({
  threshold: l = 0,
  root: s = null,
  rootMargin: c = "0%",
  freezeOnceVisible: p = !1,
  initialIsIntersecting: y = !1,
  onChange: E
} = {}) {
  var m;
  const [x, T] = R.useState(null), [D, A] = R.useState(() => ({
    isIntersecting: y,
    entry: void 0
  })), L = R.useRef();
  L.current = E;
  const j = ((m = D.entry) == null ? void 0 : m.isIntersecting) && p;
  R.useEffect(() => {
    if (!x || !("IntersectionObserver" in window) || j)
      return;
    let H;
    const G = new IntersectionObserver(
      (W) => {
        const ve = Array.isArray(G.thresholds) ? G.thresholds : [G.thresholds];
        W.forEach((fe) => {
          const oe = fe.isIntersecting && ve.some((ne) => fe.intersectionRatio >= ne);
          A({ isIntersecting: oe, entry: fe }), L.current && L.current(oe, fe), oe && p && H && (H(), H = void 0);
        });
      },
      { threshold: l, root: s, rootMargin: c }
    );
    return G.observe(x), () => {
      G.disconnect();
    };
  }, [
    x,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    JSON.stringify(l),
    s,
    c,
    j,
    p
  ]);
  const F = R.useRef(null);
  R.useEffect(() => {
    var H;
    !x && (H = D.entry) != null && H.target && !p && !j && F.current !== D.entry.target && (F.current = D.entry.target, A({ isIntersecting: y, entry: void 0 }));
  }, [x, D.entry, p, j, y]);
  const $ = [
    T,
    !!D.isIntersecting,
    D.entry
  ];
  return $.ref = $[0], $.isIntersecting = $[1], $.entry = $[2], $;
}
const oA = /* @__PURE__ */ R.forwardRef(
  function({
    onTrackClick: l,
    onClick: s,
    onSubscriptionStatusChanged: c,
    trackRef: p,
    manageSubscription: y,
    ...E
  }, m) {
    const x = Cd(p), T = R.useRef(null);
    R.useImperativeHandle(m, () => T.current);
    const D = iA({ root: T.current }), [A] = aA(D, 3e3);
    R.useEffect(() => {
      y && x.publication instanceof N1 && (A == null ? void 0 : A.isIntersecting) === !1 && (D == null ? void 0 : D.isIntersecting) === !1 && x.publication.setSubscribed(!1);
    }, [A, x, y]), R.useEffect(() => {
      y && x.publication instanceof N1 && (D == null ? void 0 : D.isIntersecting) === !0 && x.publication.setSubscribed(!0);
    }, [D, x, y]);
    const {
      elementProps: L,
      publication: j,
      isSubscribed: F
    } = AR(x, {
      element: T,
      props: E
    });
    R.useEffect(() => {
      c == null || c(!!F);
    }, [F, c]);
    const $ = (H) => {
      s == null || s(H), l == null || l({ participant: x == null ? void 0 : x.participant, track: j });
    };
    return /* @__PURE__ */ R.createElement("video", { ref: T, ...L, muted: !0, onClick: $ });
  }
), zR = /* @__PURE__ */ R.forwardRef(
  function({ trackRef: l, onSubscriptionStatusChanged: s, volume: c, muted: p, ...y }, E) {
    const m = Cd(l), x = R.useRef(null);
    R.useImperativeHandle(E, () => x.current);
    const {
      elementProps: T,
      isSubscribed: D,
      track: A,
      publication: L
    } = AR(m, {
      element: x,
      props: y
    });
    return R.useEffect(() => {
      s == null || s(!!D);
    }, [D, s]), R.useEffect(() => {
      A === void 0 || c === void 0 || (A instanceof z2 ? A.setVolume(c) : Mt.warn("Volume can only be set on remote audio tracks."));
    }, [c, A]), R.useEffect(() => {
      L === void 0 || p === void 0 || (L instanceof N1 ? L.setEnabled(!p) : Mt.warn("Can only call setEnabled on remote track publications."));
    }, [p, L, A]), /* @__PURE__ */ R.createElement("audio", { ref: x, ...T });
  }
);
function lA(l) {
  const s = yc(l, { className: "lk-focus-layout" });
  return /* @__PURE__ */ R.createElement("div", { ...s }, l.children);
}
function UR({ tracks: l, ...s }) {
  return /* @__PURE__ */ R.createElement(R.Fragment, null, l.map((c) => /* @__PURE__ */ R.createElement(
    pE.Provider,
    {
      value: c,
      key: Zr(c)
    },
    NN(s.children)
  )));
}
function uA({
  totalPageCount: l,
  nextPage: s,
  prevPage: c,
  currentPage: p,
  pagesContainer: y
}) {
  const [E, m] = R.useState(!1);
  return R.useEffect(() => {
    let x;
    return y && (x = _L(y.current, 2e3).subscribe(
      m
    )), () => {
      x && x.unsubscribe();
    };
  }, [y]), /* @__PURE__ */ R.createElement("div", { className: "lk-pagination-control", "data-lk-user-interaction": E }, /* @__PURE__ */ R.createElement("button", { className: "lk-button", onClick: c }, /* @__PURE__ */ R.createElement(RT, null)), /* @__PURE__ */ R.createElement("span", { className: "lk-pagination-count" }, `${p} of ${l}`), /* @__PURE__ */ R.createElement("button", { className: "lk-button", onClick: s }, /* @__PURE__ */ R.createElement(RT, null)));
}
const sA = /* @__PURE__ */ R.forwardRef(
  function({ totalPageCount: l, currentPage: s }, c) {
    const p = new Array(l).fill("").map((y, E) => E + 1 === s ? /* @__PURE__ */ R.createElement("span", { "data-lk-active": !0, key: E }) : /* @__PURE__ */ R.createElement("span", { key: E }));
    return /* @__PURE__ */ R.createElement("div", { ref: c, className: "lk-pagination-indicator" }, p);
  }
);
function cA({ tracks: l, ...s }) {
  const c = R.createRef(), p = R.useMemo(
    () => yc(s, { className: "lk-grid-layout" }),
    [s]
  ), { layout: y } = eN(c, l.length), E = aN(y.maxTiles, l);
  return sN(c, {
    onLeftSwipe: E.nextPage,
    onRightSwipe: E.prevPage
  }), /* @__PURE__ */ R.createElement("div", { ref: c, "data-lk-pagination": E.totalPageCount > 1, ...p }, /* @__PURE__ */ R.createElement(UR, { tracks: E.tracks }, s.children), l.length > y.maxTiles && /* @__PURE__ */ R.createElement(R.Fragment, null, /* @__PURE__ */ R.createElement(
    sA,
    {
      totalPageCount: E.totalPageCount,
      currentPage: E.currentPage
    }
  ), /* @__PURE__ */ R.createElement(uA, { pagesContainer: c, ...E })));
}
const fA = 130, dA = 140, pA = 1, PR = 16 / 10, vA = (1 - PR) * -1;
function hA({ tracks: l, orientation: s, ...c }) {
  const p = R.useRef(null), [y, E] = R.useState(0), { width: m, height: x } = DR(p), T = s || (x >= m ? "vertical" : "horizontal"), D = T === "vertical" ? Math.max(m * vA, fA) : Math.max(x * PR, dA), A = AM(), L = Math.max(T === "vertical" ? (x - A) / D : (m - A) / D, pA);
  let j = Math.round(L);
  Math.abs(L - y) < 0.5 ? j = Math.round(y) : y !== L && E(L);
  const F = OR(l, j);
  return R.useLayoutEffect(() => {
    p.current && (p.current.dataset.lkOrientation = T, p.current.style.setProperty("--lk-max-visible-tiles", j.toString()));
  }, [j, T]), /* @__PURE__ */ R.createElement("aside", { key: T, className: "lk-carousel", ref: p, ...c }, /* @__PURE__ */ R.createElement(UR, { tracks: F }, c.children));
}
function mA({
  value: l,
  onPinChange: s,
  onWidgetChange: c,
  children: p
}) {
  const y = jL(l);
  return R.useEffect(() => {
    Mt.debug("PinState Updated", { state: y.pin.state }), s && y.pin.state && s(y.pin.state);
  }, [y.pin.state, s]), R.useEffect(() => {
    Mt.debug("Widget Updated", { widgetState: y.widget.state }), c && y.widget.state && c(y.widget.state);
  }, [c, y.widget.state]), /* @__PURE__ */ R.createElement(Gy.Provider, { value: y }, p);
}
function yA({ volume: l, muted: s }) {
  const c = MR(
    [ht.Source.Microphone, ht.Source.ScreenShareAudio, ht.Source.Unknown],
    {
      updateOnlyOn: [],
      onlySubscribed: !0
    }
  ).filter((p) => !vR(p.participant) && p.publication.kind === ht.Kind.Audio);
  return /* @__PURE__ */ R.createElement("div", { style: { display: "none" } }, c.map((p) => /* @__PURE__ */ R.createElement(
    zR,
    {
      key: Zr(p),
      trackRef: p,
      volume: l,
      muted: s
    }
  )));
}
function OT({
  kind: l,
  initialSelection: s,
  onActiveDeviceChange: c,
  tracks: p,
  requestPermissions: y = !1,
  ...E
}) {
  const [m, x] = R.useState(!1), [T, D] = R.useState([]), [A, L] = R.useState(!0), [j, F] = R.useState(y), $ = (ve, fe) => {
    Mt.debug("handle device change"), x(!1), c == null || c(ve, fe);
  }, H = R.useRef(null), G = R.useRef(null);
  R.useLayoutEffect(() => {
    m && F(!0);
  }, [m]), R.useLayoutEffect(() => {
    H.current && G.current && (T || A) && zM(H.current, G.current).then(({ x: ve, y: fe }) => {
      G.current && Object.assign(G.current.style, { left: `${ve}px`, top: `${fe}px` });
    }), L(!1);
  }, [H, G, T, A]);
  const W = R.useCallback(
    (ve) => {
      G.current && ve.target !== H.current && m && UM(G.current, ve) && x(!1);
    },
    [m, G, H]
  );
  return R.useEffect(() => (document.addEventListener("click", W), window.addEventListener("resize", () => L(!0)), () => {
    document.removeEventListener("click", W), window.removeEventListener("resize", () => L(!0));
  }), [W, L]), /* @__PURE__ */ R.createElement(R.Fragment, null, /* @__PURE__ */ R.createElement(
    "button",
    {
      className: "lk-button lk-button-menu",
      "aria-pressed": m,
      ...E,
      onClick: () => x(!m),
      ref: H
    },
    E.children
  ), !E.disabled && /* @__PURE__ */ R.createElement(
    "div",
    {
      className: "lk-device-menu",
      ref: G,
      style: { visibility: m ? "visible" : "hidden" }
    },
    l ? /* @__PURE__ */ R.createElement(
      k1,
      {
        initialSelection: s,
        onActiveDeviceChange: (ve) => $(l, ve),
        onDeviceListChange: D,
        kind: l,
        track: p == null ? void 0 : p[l],
        requestPermissions: j
      }
    ) : /* @__PURE__ */ R.createElement(R.Fragment, null, /* @__PURE__ */ R.createElement("div", { className: "lk-device-menu-heading" }, "Audio inputs"), /* @__PURE__ */ R.createElement(
      k1,
      {
        kind: "audioinput",
        onActiveDeviceChange: (ve) => $("audioinput", ve),
        onDeviceListChange: D,
        track: p == null ? void 0 : p.audioinput,
        requestPermissions: j
      }
    ), /* @__PURE__ */ R.createElement("div", { className: "lk-device-menu-heading" }, "Video inputs"), /* @__PURE__ */ R.createElement(
      k1,
      {
        kind: "videoinput",
        onActiveDeviceChange: (ve) => $("videoinput", ve),
        onDeviceListChange: D,
        track: p == null ? void 0 : p.videoinput,
        requestPermissions: j
      }
    ))
  ));
}
var FR = { exports: {} };
(function(l) {
  (function(s, c) {
    l.exports ? l.exports = c() : s.log = c();
  })(P2, function() {
    var s = function() {
    }, c = "undefined", p = typeof window !== c && typeof window.navigator !== c && /Trident\/|MSIE /.test(window.navigator.userAgent), y = [
      "trace",
      "debug",
      "info",
      "warn",
      "error"
    ], E = {}, m = null;
    function x(H, G) {
      var W = H[G];
      if (typeof W.bind == "function")
        return W.bind(H);
      try {
        return Function.prototype.bind.call(W, H);
      } catch {
        return function() {
          return Function.prototype.apply.apply(W, [H, arguments]);
        };
      }
    }
    function T() {
      console.log && (console.log.apply ? console.log.apply(console, arguments) : Function.prototype.apply.apply(console.log, [console, arguments])), console.trace && console.trace();
    }
    function D(H) {
      return H === "debug" && (H = "log"), typeof console === c ? !1 : H === "trace" && p ? T : console[H] !== void 0 ? x(console, H) : console.log !== void 0 ? x(console, "log") : s;
    }
    function A() {
      for (var H = this.getLevel(), G = 0; G < y.length; G++) {
        var W = y[G];
        this[W] = G < H ? s : this.methodFactory(W, H, this.name);
      }
      if (this.log = this.debug, typeof console === c && H < this.levels.SILENT)
        return "No console available for logging";
    }
    function L(H) {
      return function() {
        typeof console !== c && (A.call(this), this[H].apply(this, arguments));
      };
    }
    function j(H, G, W) {
      return D(H) || L.apply(this, arguments);
    }
    function F(H, G) {
      var W = this, ve, fe, oe, ne = "loglevel";
      typeof H == "string" ? ne += ":" + H : typeof H == "symbol" && (ne = void 0);
      function Ce(ye) {
        var Ue = (y[ye] || "silent").toUpperCase();
        if (!(typeof window === c || !ne)) {
          try {
            window.localStorage[ne] = Ue;
            return;
          } catch {
          }
          try {
            window.document.cookie = encodeURIComponent(ne) + "=" + Ue + ";";
          } catch {
          }
        }
      }
      function me() {
        var ye;
        if (!(typeof window === c || !ne)) {
          try {
            ye = window.localStorage[ne];
          } catch {
          }
          if (typeof ye === c)
            try {
              var Ue = window.document.cookie, qe = encodeURIComponent(ne), rt = Ue.indexOf(qe + "=");
              rt !== -1 && (ye = /^([^;]+)/.exec(
                Ue.slice(rt + qe.length + 1)
              )[1]);
            } catch {
            }
          return W.levels[ye] === void 0 && (ye = void 0), ye;
        }
      }
      function We() {
        if (!(typeof window === c || !ne)) {
          try {
            window.localStorage.removeItem(ne);
          } catch {
          }
          try {
            window.document.cookie = encodeURIComponent(ne) + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
          } catch {
          }
        }
      }
      function Et(ye) {
        var Ue = ye;
        if (typeof Ue == "string" && W.levels[Ue.toUpperCase()] !== void 0 && (Ue = W.levels[Ue.toUpperCase()]), typeof Ue == "number" && Ue >= 0 && Ue <= W.levels.SILENT)
          return Ue;
        throw new TypeError("log.setLevel() called with invalid level: " + ye);
      }
      W.name = H, W.levels = {
        TRACE: 0,
        DEBUG: 1,
        INFO: 2,
        WARN: 3,
        ERROR: 4,
        SILENT: 5
      }, W.methodFactory = G || j, W.getLevel = function() {
        return oe ?? fe ?? ve;
      }, W.setLevel = function(ye, Ue) {
        return oe = Et(ye), Ue !== !1 && Ce(oe), A.call(W);
      }, W.setDefaultLevel = function(ye) {
        fe = Et(ye), me() || W.setLevel(ye, !1);
      }, W.resetLevel = function() {
        oe = null, We(), A.call(W);
      }, W.enableAll = function(ye) {
        W.setLevel(W.levels.TRACE, ye);
      }, W.disableAll = function(ye) {
        W.setLevel(W.levels.SILENT, ye);
      }, W.rebuild = function() {
        if (m !== W && (ve = Et(m.getLevel())), A.call(W), m === W)
          for (var ye in E)
            E[ye].rebuild();
      }, ve = Et(
        m ? m.getLevel() : "WARN"
      );
      var Rt = me();
      Rt != null && (oe = Et(Rt)), A.call(W);
    }
    m = new F(), m.getLogger = function(G) {
      if (typeof G != "symbol" && typeof G != "string" || G === "")
        throw new TypeError("You must supply a name when creating a logger.");
      var W = E[G];
      return W || (W = E[G] = new F(
        G,
        m.methodFactory
      )), W;
    };
    var $ = typeof window !== c ? window.log : void 0;
    return m.noConflict = function() {
      return typeof window !== c && window.log === m && (window.log = $), m;
    }, m.getLoggers = function() {
      return E;
    }, m.default = m, m;
  });
})(FR);
var gA = FR.exports;
const SA = /* @__PURE__ */ LT(gA);
function Qo(l) {
  return typeof l > "u" ? !1 : EA(l) || bA(l);
}
function EA(l) {
  var s;
  return l ? l.hasOwnProperty("participant") && l.hasOwnProperty("source") && l.hasOwnProperty("track") && typeof ((s = l.publication) == null ? void 0 : s.track) < "u" : !1;
}
function bA(l) {
  return l ? l.hasOwnProperty("participant") && l.hasOwnProperty("source") && l.hasOwnProperty("publication") && typeof l.publication < "u" : !1;
}
function K1(l) {
  return l ? l.hasOwnProperty("participant") && l.hasOwnProperty("source") && typeof l.publication > "u" : !1;
}
function MT(l) {
  if (typeof l == "string" || typeof l == "number")
    return `${l}`;
  if (K1(l))
    return `${l.participant.identity}_${l.source}_placeholder`;
  if (Qo(l))
    return `${l.participant.identity}_${l.publication.source}_${l.publication.trackSid}`;
  throw new Error(`Can't generate a id for the given track reference: ${l}`);
}
function CA(l, s) {
  return l === void 0 || s === void 0 ? !1 : Qo(l) && Qo(s) ? l.publication.trackSid === s.publication.trackSid : MT(l) === MT(s);
}
function wA(l, s) {
  return typeof s > "u" ? !1 : Qo(l) ? s.some(
    (c) => c.participant.identity === l.participant.identity && Qo(c) && c.publication.trackSid === l.publication.trackSid
  ) : K1(l) ? s.some(
    (c) => c.participant.identity === l.participant.identity && K1(c) && c.source === l.source
  ) : !1;
}
function TA() {
  return typeof document < "u";
}
var RA = [
  Pe.ConnectionStateChanged,
  Pe.RoomMetadataChanged,
  Pe.ActiveSpeakersChanged,
  Pe.ConnectionQualityChanged,
  Pe.ParticipantConnected,
  Pe.ParticipantDisconnected,
  Pe.ParticipantPermissionsChanged,
  Pe.ParticipantMetadataChanged,
  Pe.ParticipantNameChanged,
  Pe.ParticipantAttributesChanged,
  Pe.TrackMuted,
  Pe.TrackUnmuted,
  Pe.TrackPublished,
  Pe.TrackUnpublished,
  Pe.TrackStreamStateChanged,
  Pe.TrackSubscriptionFailed,
  Pe.TrackSubscriptionPermissionChanged,
  Pe.TrackSubscriptionStatusChanged
];
[
  ...RA,
  Pe.LocalTrackPublished,
  Pe.LocalTrackUnpublished
];
De.TrackPublished, De.TrackUnpublished, De.TrackMuted, De.TrackUnmuted, De.TrackStreamStateChanged, De.TrackSubscribed, De.TrackUnsubscribed, De.TrackSubscriptionPermissionChanged, De.TrackSubscriptionFailed, De.LocalTrackPublished, De.LocalTrackUnpublished;
var xA = [
  De.ConnectionQualityChanged,
  De.IsSpeakingChanged,
  De.ParticipantMetadataChanged,
  De.ParticipantPermissionsChanged,
  De.TrackMuted,
  De.TrackUnmuted,
  De.TrackPublished,
  De.TrackUnpublished,
  De.TrackStreamStateChanged,
  De.TrackSubscriptionFailed,
  De.TrackSubscriptionPermissionChanged,
  De.TrackSubscriptionStatusChanged
];
[
  ...xA,
  De.LocalTrackPublished,
  De.LocalTrackUnpublished
];
var zy = SA.getLogger("lk-components-js");
zy.setDefaultLevel("WARN");
function kA() {
  return typeof navigator < "u" && navigator.mediaDevices && !!navigator.mediaDevices.getDisplayMedia;
}
new TextEncoder();
new TextDecoder();
function HR(l) {
  const s = (E) => typeof window < "u" ? window.matchMedia(E).matches : !1, [c, p] = R.useState(s(l));
  function y() {
    p(s(l));
  }
  return R.useEffect(() => {
    const E = window.matchMedia(l);
    return y(), E.addListener ? E.addListener(y) : E.addEventListener("change", y), () => {
      E.removeListener ? E.removeListener(y) : E.removeEventListener("change", y);
    };
  }, [l]), c;
}
function jR(l) {
  var s, c, p = "";
  if (typeof l == "string" || typeof l == "number") p += l;
  else if (typeof l == "object") if (Array.isArray(l)) {
    var y = l.length;
    for (s = 0; s < y; s++) l[s] && (c = jR(l[s])) && (p && (p += " "), p += c);
  } else for (c in l) l[c] && (p && (p += " "), p += c);
  return p;
}
function _A() {
  for (var l, s, c = 0, p = "", y = arguments.length; c < y; c++) (l = arguments[c]) && (s = jR(l)) && (p && (p += " "), p += s);
  return p;
}
function DA(...l) {
  return (...s) => {
    for (const c of l)
      if (typeof c == "function")
        try {
          c(...s);
        } catch (p) {
          console.error(p);
        }
  };
}
function OA(...l) {
  const s = { ...l[0] };
  for (let c = 1; c < l.length; c++) {
    const p = l[c];
    for (const y in p) {
      const E = s[y], m = p[y];
      typeof E == "function" && typeof m == "function" && // This is a lot faster than a regex.
      y[0] === "o" && y[1] === "n" && y.charCodeAt(2) >= /* 'A' */
      65 && y.charCodeAt(2) <= /* 'Z' */
      90 ? s[y] = DA(E, m) : (y === "className" || y === "UNSAFE_className") && typeof E == "string" && typeof m == "string" ? s[y] = _A(E, m) : s[y] = m !== void 0 ? m : E;
    }
  }
  return s;
}
function MA(l) {
  return l !== void 0;
}
function VR(...l) {
  return OA(...l.filter(MA));
}
const LA = /* @__PURE__ */ R.forwardRef(function({ label: s, ...c }, p) {
  const y = hE(), { mergedProps: E, canPlayAudio: m } = lN({ room: y, props: c }), { mergedProps: x, canPlayVideo: T } = uN({ room: y, props: E }), { style: D, ...A } = x;
  return D.display = m && T ? "none" : "block", /* @__PURE__ */ R.createElement("button", { ref: p, style: D, ...A }, s || `Start ${m ? "Video" : "Audio"}`);
});
function NA({ props: l }) {
  const { dispatch: s, state: c } = PL().widget, p = "lk-button lk-settings-toggle";
  return { mergedProps: R.useMemo(() => VR(l, {
    className: p,
    onClick: () => {
      s && s({ msg: "toggle_settings" });
    },
    "aria-pressed": c && c.showSettings ? "true" : "false"
  }), [l, p, s, c]) };
}
const AA = R.forwardRef(function(s, c) {
  const { mergedProps: p } = NA({ props: s });
  return /* @__PURE__ */ R.createElement("button", { ref: c, ...p }, s.children);
});
function zA({
  variation: l,
  controls: s,
  saveUserChoices: c = !0,
  trans: p = (E) => E,
  ...y
}) {
  const m = HR("(max-width: 760px)") ? "minimal" : "verbose", x = l || m, T = { leave: !0, ...s }, D = nN();
  D ? (T.camera = T.camera ? T.camera : D.canPublish, T.microphone = T.microphone ? T.microphone : D.canPublish, T.screenShare = T.screenShare ? T.screenShare : D.canPublish) : (T.camera = !1, T.microphone = !1, T.screenShare = !1);
  const A = R.useMemo(
    () => x === "minimal" || x === "verbose",
    [x]
  ), L = R.useMemo(
    () => x === "textOnly" || x === "verbose",
    [x]
  ), j = kA(), [F, $] = R.useState(!1), H = R.useCallback(
    (me) => {
      $(me);
    },
    [$]
  ), G = VR({ className: "lk-control-bar" }, y), {
    saveAudioInputEnabled: W,
    saveVideoInputEnabled: ve,
    saveAudioInputDeviceId: fe,
    saveVideoInputDeviceId: oe
  } = vN({ preventSave: !c }), ne = R.useCallback(
    (me, We) => We ? W(me) : null,
    [W]
  ), Ce = R.useCallback(
    (me, We) => We ? ve(me) : null,
    [ve]
  );
  return /* @__PURE__ */ R.createElement("div", { ...G }, T.microphone && /* @__PURE__ */ R.createElement("div", { className: "lk-button-group" }, /* @__PURE__ */ R.createElement(
    _1,
    {
      source: ht.Source.Microphone,
      showIcon: A,
      onChange: ne
    },
    L && p("livekit.microphone")
  ), /* @__PURE__ */ R.createElement("div", { className: "lk-button-group-menu" }, /* @__PURE__ */ R.createElement(
    OT,
    {
      kind: "audioinput",
      onActiveDeviceChange: (me, We) => fe(We || "")
    }
  ))), T.camera && /* @__PURE__ */ R.createElement("div", { className: "lk-button-group" }, /* @__PURE__ */ R.createElement(_1, { source: ht.Source.Camera, showIcon: A, onChange: Ce }, L && p("livekit.camera")), /* @__PURE__ */ R.createElement("div", { className: "lk-button-group-menu" }, /* @__PURE__ */ R.createElement(
    OT,
    {
      kind: "videoinput",
      onActiveDeviceChange: (me, We) => oe(We || "")
    }
  ))), T.screenShare && j && /* @__PURE__ */ R.createElement(
    _1,
    {
      source: ht.Source.ScreenShare,
      captureOptions: { audio: !0, selfBrowserSurface: "include" },
      showIcon: A,
      onChange: H
    },
    L && p(F ? "livekit.share_stop" : "livekit.share_start")
  ), T.settings && /* @__PURE__ */ R.createElement(AA, null, A && /* @__PURE__ */ R.createElement(EN, null), L && "Settings"), T.leave && /* @__PURE__ */ R.createElement(mN, null, A && /* @__PURE__ */ R.createElement(bN, null), L && p("livekit.leave")), /* @__PURE__ */ R.createElement(LA, { label: p("livekit.start_media") }));
}
function UA(l) {
  const s = !!RR();
  return l.participant && !s ? /* @__PURE__ */ R.createElement(TR.Provider, { value: l.participant }, l.children) : /* @__PURE__ */ R.createElement(R.Fragment, null, l.children);
}
function PA(l) {
  const s = !!qy();
  return l.trackRef && !s ? /* @__PURE__ */ R.createElement(pE.Provider, { value: l.trackRef }, l.children) : /* @__PURE__ */ R.createElement(R.Fragment, null, l.children);
}
const O1 = /* @__PURE__ */ R.forwardRef(function({
  trackRef: s,
  children: c,
  onParticipantClick: p,
  disableSpeakingIndicator: y,
  trans: E = (T) => T,
  ...m
}, x) {
  var $, H;
  const T = Cd(s), { elementProps: D } = iN({
    htmlProps: m,
    disableSpeakingIndicator: y,
    onParticipantClick: p,
    trackRef: T
  }), A = hN(T.participant), L = dE(), j = ($ = VL()) == null ? void 0 : $.autoSubscription, F = R.useCallback(
    (G) => {
      T.source && !G && L && L.pin.dispatch && wA(T, L.pin.state) && L.pin.dispatch({ msg: "clear_pin" });
    },
    [T, L]
  );
  return /* @__PURE__ */ R.createElement("div", { ref: x, style: { position: "relative" }, ...D }, /* @__PURE__ */ R.createElement(PA, { trackRef: T }, /* @__PURE__ */ R.createElement(UA, { participant: T.participant }, c ?? /* @__PURE__ */ R.createElement(R.Fragment, null, Qo(T) && (((H = T.publication) == null ? void 0 : H.kind) === "video" || T.source === ht.Source.Camera || T.source === ht.Source.ScreenShare) ? /* @__PURE__ */ R.createElement(
    oA,
    {
      trackRef: T,
      onSubscriptionStatusChanged: F,
      manageSubscription: j
    }
  ) : Qo(T) && /* @__PURE__ */ R.createElement(
    zR,
    {
      trackRef: T,
      onSubscriptionStatusChanged: F
    }
  ), /* @__PURE__ */ R.createElement("div", { className: "lk-participant-placeholder" }, /* @__PURE__ */ R.createElement(PN, null)), /* @__PURE__ */ R.createElement("div", { className: "lk-participant-metadata" }, /* @__PURE__ */ R.createElement("div", { className: "lk-participant-metadata-item" }, T.source === ht.Source.Camera ? /* @__PURE__ */ R.createElement(R.Fragment, null, A && /* @__PURE__ */ R.createElement(CN, { style: { marginRight: "0.25rem" } }), /* @__PURE__ */ R.createElement(
    UN,
    {
      trackRef: {
        participant: T.participant,
        source: ht.Source.Microphone
      },
      show: "muted"
    }
  ), /* @__PURE__ */ R.createElement(xT, null)) : /* @__PURE__ */ R.createElement(R.Fragment, null, /* @__PURE__ */ R.createElement("div", { className: "lk-track-muted-indicator-microphone" }, /* @__PURE__ */ R.createElement(LR, { style: { marginRight: "0.25rem" } })), /* @__PURE__ */ R.createElement(xT, null, " ", E("livekit.user_share_postfix"), " "))), /* @__PURE__ */ R.createElement(zN, { className: "lk-participant-metadata-item" }))), /* @__PURE__ */ R.createElement(MN, { trackRef: T }))));
});
function FA({ trans: l = (c) => c, ...s }) {
  var j, F;
  const [c, p] = R.useState({
    showChat: !1,
    unreadMessages: 0,
    showSettings: !1
  }), y = R.useRef(null), E = MR(
    [
      { source: ht.Source.Camera, withPlaceholder: !0 },
      { source: ht.Source.ScreenShare, withPlaceholder: !1 }
    ],
    { updateOnlyOn: [Pe.ActiveSpeakersChanged], onlySubscribed: !1 }
  ), m = ($) => {
    zy.debug("updating widget state", $), p($);
  }, x = HL(), T = E.filter(Qo).filter(($) => $.publication.source === ht.Source.ScreenShare), D = (j = oN(x)) == null ? void 0 : j[0], A = E.filter(($) => !CA($, D));
  R.useEffect(() => {
    var $, H, G, W, ve, fe;
    if (T.some((oe) => oe.publication.isSubscribed) && y.current === null ? (zy.debug("Auto set screen share focus:", { newScreenShareTrack: T[0] }), (H = ($ = x.pin).dispatch) == null || H.call($, { msg: "set_pin", trackReference: T[0] }), y.current = T[0]) : y.current && !T.some(
      (oe) => {
        var ne, Ce;
        return oe.publication.trackSid === ((Ce = (ne = y.current) == null ? void 0 : ne.publication) == null ? void 0 : Ce.trackSid);
      }
    ) && (zy.debug("Auto clearing screen share focus."), (W = (G = x.pin).dispatch) == null || W.call(G, { msg: "clear_pin" }), y.current = null), D && !Qo(D)) {
      const oe = E.find(
        (ne) => ne.participant.identity === D.participant.identity && ne.source === D.source
      );
      oe !== D && Qo(oe) && ((fe = (ve = x.pin).dispatch) == null || fe.call(ve, { msg: "set_pin", trackReference: oe }));
    }
  }, [
    T.map(($) => `${$.publication.trackSid}_${$.publication.isSubscribed}`).join(),
    (F = D == null ? void 0 : D.publication) == null ? void 0 : F.trackSid,
    E
  ]);
  const L = HR("(max-width: 768px)");
  return /* @__PURE__ */ R.createElement("div", { className: "lk-video-conference", ...s }, TA() && /* @__PURE__ */ R.createElement(
    mA,
    {
      value: x,
      onWidgetChange: m
    },
    /* @__PURE__ */ R.createElement("div", { className: "lk-video-conference-inner" }, D ? /* @__PURE__ */ R.createElement("div", { className: "lk-focus-layout-wrapper" }, /* @__PURE__ */ R.createElement(lA, null, /* @__PURE__ */ R.createElement(hA, { orientation: L ? "horizontal" : "vertical", tracks: A }, /* @__PURE__ */ R.createElement(O1, { trans: l })), D && /* @__PURE__ */ R.createElement(O1, { trackRef: D, trans: l }))) : /* @__PURE__ */ R.createElement("div", { className: "lk-grid-layout-wrapper" }, /* @__PURE__ */ R.createElement(cA, { tracks: E }, /* @__PURE__ */ R.createElement(O1, { trans: l }))), /* @__PURE__ */ R.createElement(zA, { trans: l }))
  ), /* @__PURE__ */ R.createElement(yA, null));
}
const HA = () => {
  var l;
  return l = class extends Ny.Component {
    constructor(c) {
      super(c), this.state = { ...c };
    }
    render() {
      const {
        // Vue attaches an event handler, but it is missing an event name, so
        // it ends up using an empty string. Prevent passing an empty string
        // named prop to React.
        "": c,
        ...p
      } = this.state;
      return /* @__PURE__ */ Ny.createElement(
        x1,
        {
          "data-lk-theme": "default",
          ...p,
          trans: void 0
        },
        /* @__PURE__ */ Ny.createElement(FA, { trans: p.trans })
      );
    }
  }, Kw(l, "displayName", `ReactInVue${x1.displayName || x1.name || "Component"}`), l;
}, QA = {
  render() {
    return U2("div", { ref: "react" });
  },
  data() {
    return {
      root: null,
      reactRef: null
    };
  },
  methods: {
    mountReactComponent() {
      if (this.$refs.react) {
        const l = HA(), s = Dv(this.$refs.react);
        this.root = Dv(U1(s)), this.root.render(
          /* @__PURE__ */ Ny.createElement(
            l,
            {
              ...Dv(this.$attrs),
              ref: (c) => {
                this.reactRef = Dv(c);
              }
            }
          )
        );
      }
    }
  },
  mounted() {
    this.mountReactComponent();
  },
  // beforeUnmount() {
  // this.root.unmount()
  // },
  inheritAttrs: !1,
  watch: {
    $attrs: {
      handler(l) {
        if (this.reactRef && this.reactRef.setState) {
          const s = l ? { ...l } : {};
          this.reactRef.setState(Dv(s));
        }
      },
      deep: !0
    }
  }
};
export {
  QA as LiveKitWrapper
};
