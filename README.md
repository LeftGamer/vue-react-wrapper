# VueReactWrapper

> ⚠️ Forked and modified [Vuera](https://github.com/akxcv/vuera) library for [SmartNation](https://snation.kz) products. Especially for including [LiveKit React Components](https://github.com/livekit/components-js) in our Vue repository.
